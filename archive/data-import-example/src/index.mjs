import { fetchResource } from "./alpinebits.mjs";
import sequelizeModule from "sequelize";
const { Sequelize } = sequelizeModule;

import { Event, create } from "./models.mjs";

const database = "postgres"; // database name
const user = "postgres"; // database user name
const password = 'admin'; // database user password

const sequelize = new Sequelize(database, user, password, {
  host: "localhost", // database address
  dialect: "postgres",
  logging: false,
});

async function dataImport() {
  // Test connection to database
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (err) {
    console.log("Unable to connect to the database:", err);
    throw err;
  }

  // Initializes Event class describing the columns of the
  // Events table on the database
  await Event.initDB(sequelize);

  //
  await sequelize
    .sync(
      // Passing no arguments creates Events table if it does not exist
      // { alter: true } // Passing this creates or overrides the table if they don't match
      { force: true } // Passing this forcefully creates the table always dropping previous data
    )
    .then(() => console.log("Successfully synced with DB"))
    .catch((err) => {
      console.log("Failed to synchronize Event class and Events table", err);
      throw err;
    });

  // Make an minimal request to the server in order to retrieve
  // metadata: number of pages and number of resources
  let requestOpts = {
    resourceType: "events",
    pageSize: 1,
    fields: {
      events: ["name"],
    },
  };

  let events = [];
  let publishers = {},
    venues = {};

  await fetchResource(requestOpts)
    .then((response) => {
      if (!response || !response.data) {
        return;
      }

      // Make an actual request to the server dynamically sized for the
      // actual number resources available and ignore unnecessary information
      // NOTE: iterative requests should replace the data cap in an actual
      // implementation
      requestOpts = {
        resourceType: "events",
        pageSize: response.meta.count > 100 ? 100 : response.meta.count,
        fields: {
          agents: ["name", "url"],
          venues: ["address", "geometries"],
        },
        include: ["publisher", "venues"],
        filter: "filter[lastUpdate][gt]=2021-01-01"
      };
      // requestOpts.pageSize =
      //   response.meta.count > 100 ? 100 : response.meta.count;
      // delete requestOpts.fields.events;
      // requestOpts.fields.agents = ["name", "url"];
      // requestOpts.fields.venues = ["address", "geometries"];
      // requestOpts.include = ["publisher", "venues"];

      return fetchResource(requestOpts);
    })
    .then(async (response) => {
      if (!response || !response.data) {
        return;
      }

      // Extracts event resources array
      events = response.data;

      let included = response.included;
      (publishers = {}), (venues = {});

      // Organizes included resources into publishers and venues
      // adding easy <id,resource> map
      while (included.length > 0) {
        let resource = included.pop();
        if (resource.type === "agents") {
          publishers[resource.id] = resource;
        } else if (resource.type === "venues") {
          venues[resource.id] = resource;
        }
      }
    })
    .then(() => console.log("Successfully fetched events data"))
    .catch((err) => {
      console.log("Failed to fetch data");
      throw err;
    });

  // Creates event objects mapped to Events table
  for (const event of events) {
    await create(event, publishers, venues);
  }

  // Synchronizes event objects with Events table
  await Event.sync()
    .then(() => console.log("Successfully persisted events data"))
    .catch((err) => {
      console.log("Failed to synchronize data with database");
      throw err;
    });

  await sequelize.close().catch((err) => {
    console.log("Error on close database connection");
    throw err;
  });
}

dataImport();
