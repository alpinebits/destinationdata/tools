import sequelizeModule from "sequelize";
const { DataTypes, Model, Op } = sequelizeModule;

export class Event extends Model {
  static initDB(sequelize) {
    return Event.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        dataProviderId: DataTypes.STRING,
        dataProvider: DataTypes.STRING,
        lastUpdate: DataTypes.STRING,
        description: DataTypes.JSON,
        endDate: DataTypes.DATE,
        name: DataTypes.JSON,
        startDate: DataTypes.DATE,
        url: DataTypes.JSON,
        publisherUrl: DataTypes.STRING,
        publisherName: DataTypes.JSON,
        locations: DataTypes.ARRAY(DataTypes.JSON),
      },
      { sequelize }
    );
  }
}

export async function create(originalEvent, publishers, venues) {
  // Retrieve related publisher info
  let publisherId = originalEvent.relationships.publisher.data.id;
  let publisher = publishers[publisherId];

  // Retrieve related venues location info
  let locations = [];
  for (let venueReference of originalEvent.relationships.venues.data) {
    let locationReference = venueReference;

    if (venueReference) {
      let venue = venues[locationReference.id];
      venue = {
        address: venue.attributes.address,
        geometries: venue.attributes.geometries,
      };
      locations.push(venue);
    }
  }

  // Data mapping
  const event = {
    dataProviderId: originalEvent.id,
    dataProvider: originalEvent.meta.dataProvider,
    lastUpdate: originalEvent.meta.lastUpdate,
    description: originalEvent.attributes.description,
    endDate: originalEvent.attributes.endDate
      ? new Date(originalEvent.attributes.endDate)
      : null,
    name: originalEvent.attributes.name,
    startDate: originalEvent.attributes.startDate
      ? new Date(originalEvent.attributes.startDate)
      : null,
    url: originalEvent.attributes.url,
    publisherUrl: publisher.attributes.url,
    publisherName: publisher.attributes.name,
    locations: locations,
  };

  // Creates or updates the data accordingly
  return Event.findOne({
    where: { dataProviderId: { [Op.eq]: event.dataProviderId } },
  }).then((oldValue) => {
    if (oldValue) {
      return oldValue
        .update(event)
        .catch((_err) => {
          console.log("Bad data handling event update.")
          console.error(_err);
        });
    }
    return Event.create(event).catch((_err) => {
      console.log("Bad data handling prevented event creation.");
      console.error(_err);
    });
  });
}
