# Data Import Example

This project is a JavaScript project that retrieves data from a DestinationData server and maps into a relational database showing an example of data integration.

This project was developed using the following tools, nonetheless it may run on older versions as well:

* **npm** - version 6.14.5
* **node** - version 12.18.2
* **PostgreSQL** - version 12.4
* **pgAdmin4** (used to manipulate PostgreSQL databases, but it not required)

In order to run this project, start by running PostgreSQL creating a database for this application to access.

Next, download the project dependencies to your project by running:

```shell
npm install
```

Next, go to `./src/index.mjs` and update the following variables to allow the application to connect to the PostgreSQL database:

```javascript
const database = "events_db"; // database name
const user = "admin"; // database user name
const password = null; // database user password

const sequelize = new Sequelize(database, user, password, {
  host: "localhost", // database address
  dialect: "postgres",
  logging: false,
});
```

Finally, run the application with the command below:

```shell
npm start
```

You can now interact with your database and query the data retrieved from OpenDataHub's DestinationData [endpoint](https://destinationdata.alpinebits.opendatahub.bz.it/2021-04).
For instance, try querying for future events ordered by date:

```sql
SELECT
   *
FROM
   "Events"
WHERE
   "Events"."startDate" > now()
   AND "Events"."endDate" > now()
ORDER BY
   "Events"."startDate",
   "Events"."endDate"
```
