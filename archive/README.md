# AlpineBits DestinationData: Archive

This repository contains open-source resources to support adopters of the AlpineBits DestinationData standard. These resources, however, are outdated and no longer meet the current release of DestinationData

- **[schema-builder](/schema-builder)**: a JavaScript console app that generates JSON Schema documents describing the structure of the messages defined in the standard. Any given schema can be fed to a validator and used to programmatically check if a given message complies to it.

- **[website-example](/website-example)**: an example project in JavaScript/React  that presents a single page application enriched with data provided by a DestinationData endpoint.

- **[data-import-example](/data-import-example)**: an example JavaScript project that shows a case of data integration from a DestinationData endpoint into a client's relational database.

- **[ski-data-portal](/ski-data-portal)**: an example DestinationData portal/browser focused on SkiData.
