const Ajv = require('ajv');
const fs = require('fs');
const resources = require('./resources');
const jsonApiSchema = require('./resources/jsonapi.schema.json');

const schemas = {
  jsonapi: jsonApiSchema,
  '/1.0': resources.baseEndpointSchema,
  '/1.0/agents': resources.createDataArrayMessageSchema(['agents'],['mediaObjects'],true),
  '/1.0/agents/:id': resources.createDataMessageSchema(['agents'],['mediaObjects']),
  '/1.0/agents/:id/multimediaDescriptions': resources.createDataArrayMessageSchema(['mediaObjects'],['agents']),
  '/1.0/events': resources.createDataArrayMessageSchema(['events'],['agents','eventSeries','mediaObjects','venues'],true),
  '/1.0/events/:id': resources.createDataMessageSchema(['events'],['agents','eventSeries','mediaObjects','venues']),
  '/1.0/events/:id/contributors': resources.createDataArrayMessageSchema(['agents'],['mediaObjects']),
  '/1.0/events/:id/multimediaDescriptions': resources.createDataArrayMessageSchema(['mediaObjects'],['agents']),
  '/1.0/events/:id/organizers': resources.createDataArrayMessageSchema(['agents'],['mediaObjects']),
  '/1.0/events/:id/publisher': resources.createDataMessageSchema(['agents'],['mediaObjects']),
  '/1.0/events/:id/series': resources.createDataMessageSchema(['eventSeries'],['events','mediaObjects']),
  '/1.0/events/:id/sponsors': resources.createDataArrayMessageSchema(['agents'],['mediaObjects']),
  '/1.0/events/:id/subEvents': resources.createDataArrayMessageSchema(['events'],['agents','eventSeries','mediaObjects','venues']),
  '/1.0/events/:id/venues': resources.createDataArrayMessageSchema(['venues'],['mediaObjects']),
  '/1.0/eventSeries': resources.createDataArrayMessageSchema(['eventSeries'],['events','mediaObjects'],true),
  '/1.0/eventSeries/:id': resources.createDataMessageSchema(['eventSeries'],['events','mediaObjects']),
  '/1.0/eventSeries/:id/editions': resources.createDataArrayMessageSchema(['events'],['agents','eventSeries','mediaObjects','venues']),
  '/1.0/eventSeries/:id/multimediaDescriptions': resources.createDataArrayMessageSchema(['mediaObjects'],['agents']),
  '/1.0/lifts': resources.createDataArrayMessageSchema(['lifts'],['lifts','mediaObjects','mountainAreas','snowparks','trails'],true),
  '/1.0/lifts/:id': resources.createDataMessageSchema(['lifts'],['lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/lifts/:id/connections': resources.createDataArrayMessageSchema(['lifts', 'mountainAreas', 'snowparks', 'trails'],['agents','lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/lifts/:id/multimediaDescriptions': resources.createDataArrayMessageSchema(['mediaObjects'],['agents']),
  '/1.0/mediaObjects': resources.createDataArrayMessageSchema(['mediaObjects'],['agents'],true),
  '/1.0/mediaObjects/:id': resources.createDataMessageSchema(['mediaObjects'],['agents']),
  '/1.0/mediaObjects/:id/copyrightOwner': resources.createDataMessageSchema(['agents'],['mediaObjects']),
  '/1.0/mountainAreas': resources.createDataArrayMessageSchema(['mountainAreas'],['agents','lifts','mediaObjects','mountainAreas','snowparks','trails'],true),
  '/1.0/mountainAreas/:id': resources.createDataMessageSchema(['mountainAreas'],['agents','lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/mountainAreas/:id/areaOwner': resources.createDataMessageSchema(['agents'],['mediaObjects']),
  '/1.0/mountainAreas/:id/connections': resources.createDataArrayMessageSchema(['lifts', 'mountainAreas', 'snowparks', 'trails'],['agents','lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/mountainAreas/:id/lifts': resources.createDataArrayMessageSchema(['lifts'],['lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/mountainAreas/:id/multimediaDescriptions ': resources.createDataArrayMessageSchema(['mediaObjects'],['agents']),
  '/1.0/mountainAreas/:id/snowparks': resources.createDataArrayMessageSchema(['snowparks'],['lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/mountainAreas/:id/subAreas': resources.createDataArrayMessageSchema(['mountainAreas'],['agents','lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/mountainAreas/:id/trails': resources.createDataArrayMessageSchema(['trails'],['lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/snowparks': resources.createDataArrayMessageSchema(['snowparks'],['lifts','mediaObjects','mountainAreas','snowparks','trails'],true),
  '/1.0/snowparks/:id': resources.createDataMessageSchema(['snowparks'],['lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/snowparks/:id/connections': resources.createDataArrayMessageSchema(['lifts', 'mountainAreas', 'snowparks', 'trails'],['agents','lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/snowparks/:id/multimediaDescriptions': resources.createDataArrayMessageSchema(['mediaObjects'],['agents']),
  '/1.0/trails': resources.createDataArrayMessageSchema(['trails'],['lifts','mediaObjects','mountainAreas','snowparks','trails'],true),
  '/1.0/trails/:id': resources.createDataMessageSchema(['trails'],['lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/trails/:id/connections': resources.createDataArrayMessageSchema(['lifts', 'mountainAreas', 'snowparks', 'trails'],['agents','lifts','mediaObjects','mountainAreas','snowparks','trails']),
  '/1.0/trails/:id/multimediaDescriptions': resources.createDataArrayMessageSchema(['mediaObjects'],['agents']),
  '/1.0/venues': resources.createDataArrayMessageSchema(['venues'],['mediaObjects'],true),
  '/1.0/venues/:id': resources.createDataMessageSchema(['venues'],['mediaObjects']),
  '/1.0/venues/:id/multimediaDescriptions': resources.createDataArrayMessageSchema(['mediaObjects'],['agents']),
};

function buildSchema(filename,schema){
  let ajv = new Ajv({
    $data: true,
    verbose: false,
    allErrors: true
  });
  
  // Test compiling schema
  ajv.compile(schema);

  fs.writeFileSync(filename, JSON.stringify(schema,null,2), function (err) {
    if (err) throw err;
  });
};

function buildSchemas(){
  // Exports schema for base endpoint
  buildSchema('./../schemas/base.endpoint.schema.json',schemas['/1.0']);
  
  // Exports schemas for resource type endpoints
  buildSchema('./../schemas/agents.schema.json',schemas['/1.0/agents']);
  buildSchema('./../schemas/events.schema.json',schemas['/1.0/events']);
  buildSchema('./../schemas/eventseries.schema.json',schemas['/1.0/eventSeries']);
  buildSchema('./../schemas/lifts.schema.json',schemas['/1.0/lifts']);
  buildSchema('./../schemas/mediaobjects.schema.json',schemas['/1.0/mediaObjects']);
  buildSchema('./../schemas/mountainareas.schema.json',schemas['/1.0/mountainAreas']);
  buildSchema('./../schemas/snowparks.schema.json',schemas['/1.0/snowparks']);
  buildSchema('./../schemas/trails.schema.json',schemas['/1.0/trails']);
  buildSchema('./../schemas/venues.schema.json',schemas['/1.0/venues']);
  
  // Exports schemas for resource endpoints
  buildSchema('./../schemas/agents.id.schema.json',schemas['/1.0/agents/:id']);
  buildSchema('./../schemas/events.id.schema.json',schemas['/1.0/events/:id']);
  buildSchema('./../schemas/eventseries.id.schema.json',schemas['/1.0/eventSeries/:id']);
  buildSchema('./../schemas/lifts.id.schema.json',schemas['/1.0/lifts/:id']);
  buildSchema('./../schemas/mediaobjects.id.schema.json',schemas['/1.0/mediaObjects/:id']);
  buildSchema('./../schemas/mountainareas.id.schema.json',schemas['/1.0/mountainAreas/:id']);
  buildSchema('./../schemas/snowparks.id.schema.json',schemas['/1.0/snowparks/:id']);
  buildSchema('./../schemas/trails.id.schema.json',schemas['/1.0/trails/:id']);
  buildSchema('./../schemas/venues.id.schema.json',schemas['/1.0/venues/:id']);

  // Exports schemas for agents relationships
  buildSchema('./../schemas/agents.id.multimediadescriptions.schema.json',schemas['/1.0/agents/:id/multimediaDescriptions']);
  
  // Exports schemas for events relationships
  buildSchema('./../schemas/events.id.contributors.schema.json',schemas['/1.0/events/:id/contributors']);
  buildSchema('./../schemas/events.id.multimediadescriptions.schema.json',schemas['/1.0/events/:id/multimediaDescriptions']);
  buildSchema('./../schemas/events.id.organizers.schema.json',schemas['/1.0/events/:id/organizers']);
  buildSchema('./../schemas/events.id.publisher.schema.json',schemas['/1.0/events/:id/publisher']);
  buildSchema('./../schemas/events.id.series.schema.json',schemas['/1.0/events/:id/series']);
  buildSchema('./../schemas/events.id.sponsors.schema.json',schemas['/1.0/events/:id/sponsors']);
  buildSchema('./../schemas/events.id.subevents.schema.json',schemas['/1.0/events/:id/subEvents']);
  buildSchema('./../schemas/events.id.venues.schema.json',schemas['/1.0/events/:id/venues']);

  // Exports schemas for event series relationships
  buildSchema('./../schemas/eventseries.id.editions.schema.json',schemas['/1.0/eventSeries/:id/editions']);
  buildSchema('./../schemas/eventseries.id.multimediadescriptions.schema.json',schemas['/1.0/eventSeries/:id/multimediaDescriptions']);
  
  // Exports schemas for lifts relationships
  buildSchema('./../schemas/lifts.id.connections.schema.json',schemas['/1.0/lifts/:id/connections']);
  buildSchema('./../schemas/lifts.id.multimediadescriptions.schema.json',schemas['/1.0/lifts/:id/multimediaDescriptions']);
  
  // Exports schemas for media objects relationships
  buildSchema('./../schemas/mediaobjects.id.copyrightowner.schema.json',schemas['/1.0/mediaObjects/:id/copyrightOwner']);
  
  // Exports schemas for mountain areas relationships
  buildSchema('./../schemas/mountainareas.id.areaowner.schema.json',schemas['/1.0/mountainAreas/:id/areaOwner']);
  buildSchema('./../schemas/mountainareas.id.connections.schema.json',schemas['/1.0/mountainAreas/:id/connections']);
  buildSchema('./../schemas/mountainareas.id.lifts.schema.json',schemas['/1.0/mountainAreas/:id/lifts']);
  buildSchema('./../schemas/mountainareas.id.multimediadescriptions.schema.json',schemas['/1.0/mountainAreas/:id/multimediaDescriptions ']);
  buildSchema('./../schemas/mountainareas.id.snowparks.schema.json',schemas['/1.0/mountainAreas/:id/snowparks']);
  buildSchema('./../schemas/mountainareas.id.subareas.schema.json',schemas['/1.0/mountainAreas/:id/subAreas']);
  buildSchema('./../schemas/mountainareas.id.trails.schema.json',schemas['/1.0/mountainAreas/:id/trails']);
  
  // Exports schemas for snowparks relationships
  buildSchema('./../schemas/snowparks.id.connections.schema.json',schemas['/1.0/snowparks/:id/connections']);
  buildSchema('./../schemas/snowparks.id.multimediadescriptions.schema.json',schemas['/1.0/snowparks/:id/multimediaDescriptions']);
  
  // Exports schemas for trails relationships
  buildSchema('./../schemas/trails.id.connections.schema.json',schemas['/1.0/trails/:id/connections']);
  buildSchema('./../schemas/trails.id.multimediadescriptions.schema.json',schemas['/1.0/trails/:id/multimediaDescriptions']);
  
  // Exports schemas for venues relationships
  buildSchema('./../schemas/venues.id.multimediadescriptions.schema.json',schemas['/1.0/venues/:id/multimediaDescriptions']);
  
}

function validateMessage(message,schema) {
  let ajv = new Ajv({
    $data: true,
    verbose: false,
    allErrors: true
  });
  let validator = ajv.compile(schemas.jsonapi);
  let validate = validator(message);

  // Message invalid against JSON:API
  if(!validate) {
    return validator;
  }

  validator = ajv.compile(schema);
  validate = validator(message);

  if(!validate) {
    return validator;
  }
  else{
    // Message invalid against specific DestinationData schema
    return validate;
  }
}

buildSchemas();

module.exports ={
  schemas,
  validateMessage,
  buildSchemas,
};