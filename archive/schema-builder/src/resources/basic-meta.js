
const dataProvider = {
    ref: '#/definitions/dataProvider',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'string',
                format: 'uri'
            }
        ]
    }
}

const lastUpdate = {
    ref: '#/definitions/lastUpdate',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'string',
                format: 'date-time'
            }
        ]
    }
}

const basicMeta = {
    ref: '#/definitions/basicMeta',
    def: {
        type: 'object',
        description: 'An object containing metadata of the resource (e.g., the URL representing the resource\'s data provider or a date-time string of the instant of the resource\'s last update).',
        properties: {
            dataProvider: {
                description: 'A url string that identifies the organization responsible for the data about a resource and who can be contacted in case of questions or issues regarding it.',
                $ref: dataProvider.ref
            },
            lastUpdate: {
                description: 'A date-time string that identifies when a resource was last modified in the server.',
                $ref: lastUpdate.ref
            },
        },
        required: [
            'dataProvider',
            'lastUpdate',
        ]
    }
}

module.exports = {
    basicMeta,
    dataProvider,
    lastUpdate,
}