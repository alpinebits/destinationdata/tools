const functions = require('./functions');
const datatypes = require('./datatypes');

const  description = 'A resource representing a trail, a physical path that supports the practice of some type of sport activity.';

const trails = functions.createResourceType('trails', description);

functions.addToAttributes(trails,{
    patternProperties: {
        name: {
            type: 'object'
        }
    }
});

functions.addToAttributes(trails,{
    type: 'object',
    properties: {
        address: {
            description: 'An address object representing the address of the trail.',
            $ref: datatypes.address.ref
        },
        difficulty: {
            description: 'An object containing the difficulty level of a ski slope.',
            $ref: datatypes.trailDifficulty.ref
        },
        geometries: {
            description: 'An array of geometry objects each of which represents the location of the trail in terms of GPS coordinates.',
            $ref: datatypes.optionalGeometries.ref
        },
        howToArrive: {
            description: 'A text object containing instructions on how to arrive at the trail.',
            $ref: datatypes.text.ref
        },
        length: {
            description: 'A number representing the length of the trail in meters.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        maxAltitude: {
            description: 'A number representing the highest elevation point of the trail in meters above sea level.',
            $ref: datatypes.optionalPositiveInteger.ref
        },
        minAltitude: {
            description: 'A number representing the lowest elevation point of the trail in meters above sea level.',
            $ref: datatypes.optionalPositiveInteger.ref
        },
        openingHours: {
            description: 'An array of hours specification objects representing the hours in which the trail is open to the public.',
            $ref: datatypes.hoursSpecifications.ref
        },
        snowCondition: {
            description: 'A snow condition object containing the latest reported condition of the snow in the trail.',
            $ref: datatypes.snowCondition.ref
        }
    },
});

const connectionsDescription = 'An object representing references to place resources that identify the places that are physically accessible from the trail, which may include mountain areas, other lifts, snowparks, and trails.';
functions.addRelationshipToMany(trails,'connections',['lifts', 'mountainAreas', 'snowparks', 'trails'],true,connectionsDescription);

const mediaDescription = 'An object representing a reference towards media objects that describe the trail.';
functions.addRelationshipToMany(trails,'multimediaDescriptions',['mediaObjects'],true,mediaDescription);

module.exports = { trails };

