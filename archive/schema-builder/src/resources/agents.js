const datatypes = require('./datatypes');
const functions = require('./functions');

const description = 'A resource representing an individual who bears mental attitudes and is capable of performing actions and perceiving events.';

const agents = functions.createResourceType('agents',description);

functions.addToAttributes(agents,{
   patternProperties: {
    name: {
      type: 'object'
    }
  }
});

functions.addToAttributes(agents,{
  type: 'object',
  properties: {
      contactPoints: {
        description: 'An array of contact point objects.',
        $ref: datatypes.contactPoints.ref
      }
  },
});

const mediaDescription = 'An object representing a reference towards media objects that describe the agent.';
functions.addRelationshipToMany(agents,'multimediaDescriptions',['mediaObjects'],true,mediaDescription);

module.exports = { agents };