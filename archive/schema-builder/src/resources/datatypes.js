
const categoryString = {
    ref: '#/definitions/categoryString',
    def: {
        type: 'string',
        regex: '^([a-z]|[A-Z]|[0-9])+\/([a-z]|[A-Z]|[0-9])+$'
           
    }
}

const countryCodes = {
    ref: '#/definitions/countryCodes',
    def: {
        type: 'string',
        pattern: '[A-Z]{2}'
    }
}

const languageCodes = {
    ref: '#/definitions/languageCodes',
    def: {
        type: 'string',
        pattern: '[a-z]{3}'
    }
}

const text = {
    ref: '#/definitions/text',
    def: {
        description: 'An object that allows exchange of textual data (e.g. name, description) using multiple languages.',
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                minProperties: 1,
                propertyNames: {
                    $ref: languageCodes.ref
                },
                additionalProperties: {
                    type: 'string'
                }
            }
        ]
    }
};

const intervalObject = {
    ref: '#/definitions/intervalObject',
    def: {
        type: 'object',
        properties: {
            opens: {
                type: 'string',
                format: 'time'
            },
            closes: {
                type: 'string',
                format: 'time'
            }
        },
        required: [
            'opens',
            'closes'
        ],
        additionalProperties: false
    }
}

const intervalObjectArray = {
    ref: '#/definitions/intervalObjectArray',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: {
                    $ref: intervalObject.ref
                }
            }
        ]
    }
}

const dailySchedulesSpecification = {
    ref: '#/definitions/dailySchedulesSpecification',
    def: {
        type: 'object',
        propertyNames: {
            type: 'string',
            format: 'date'
        },
        additionalProperties: {
            $ref: intervalObjectArray.ref
        }
    }
}

const weeklySchedulesSpecification = {
    ref: '#/definitions/weeklySchedulesSpecification',
    def: {
        type: 'object',
        properties: {
            validFrom: {
                oneOf: [
                    {
                        type: 'null'
                    },
                    {
                        type: 'string',
                        format: 'date'
                    }
                ]
            },
            validTo: {
                oneOf: [
                    {
                        type: 'null'
                    },
                    {
                        type: 'string',
                        format: 'date'
                    }
                ]
            },
            sunday: {
                $ref: intervalObjectArray.ref
            },
            monday: {
                $ref: intervalObjectArray.ref
            },
            tuesday: {
                $ref: intervalObjectArray.ref
            },
            wednesday: {
                $ref: intervalObjectArray.ref
            },
            thursday: {
                $ref: intervalObjectArray.ref
            },
            friday: {
                $ref: intervalObjectArray.ref
            },
            saturday: {
                $ref: intervalObjectArray.ref
            },
        },
        required: [
            'validFrom',
            'validTo',
            'sunday',
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
        ],
        additionalProperties: false
    }
}

const hoursSpecifications = {
    ref: '#/definitions/hoursSpecifications',
    def: {
        description: 'An hours specification object allows the representations of schedules over periods of time.',
        oneOf: [
            { 
                type: 'null'
            },
            {
                type: 'object',
                properties: {
                    dailySchedules: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                $ref: dailySchedulesSpecification.ref
                            }
                        ]
                    },
                    weeklySchedules: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'array',
                                minItems: 1,
                                uniqueItems: true,
                                items: {
                                    $ref: weeklySchedulesSpecification.ref
                                }
                            }
                        ]
                    }
                },
                required: [
                    'dailySchedules',
                    'weeklySchedules'
                ],
                additionalProperties: false
            }
        ]
    }
}

const address = {
    ref: '#/definitions/address',
    def: {
        description: 'An object that represents an address.',
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                properties: {
                    categories: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'array',
                                minItems: 1,
                                items: {
                                    $ref: categoryString.ref
                                }
                            }
                        ]
                    },
                    city: {
                        $ref: text.ref
                    },
                    complement: {
                        $ref: text.ref
                    },
                    country: {
                        $ref: countryCodes.ref
                    },
                    region: {
                        $ref: text.ref
                    },
                    street: {
                        $ref: text.ref
                    },
                    zipcode: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'string',
                                minLength: 1
                            }
                        ]
                    }
                },
                required: [
                    'categories',
                    'city',
                    'complement',
                    'country',
                    'region',
                    'street',
                    'zipcode',
                ],
                not: {
                    properties: {
                        city: {
                            type: 'null'
                        },
                        country: {
                            type: 'null'
                        }
                    }
                }
            }
        ]
    }
}

const contactPoint = {
    ref: '#/definitions/contactPoint',
    def: {
        type: 'object',
        properties: {
            address: {
                $ref: address.ref
            },
            availableHours: {
                $ref: hoursSpecifications.ref
            },
            email: {
                type: 'string',
                format: 'email'
            },
            telephone: {
                oneOf: [
                    {
                        type: 'null'
                    },
                    {
                        type: 'string',
                        minLength: 1
                    }
                ]
            },
        },
        required: [
            'address',
            'availableHours',
            'email',
            'telephone'
        ],
        anyOf: [
            {
                not: {
                    properties: {
                        address: {
                            type: 'null'
                        }
                    }
                }
            },
            {
                not: {
                    properties: {
                        email: {
                            type: 'null'
                        }
                    }
                }
            },
            {
                not: {
                    properties: {
                        telephone: {
                            type: 'null'
                        }
                    }
                }
            }
        ]
    }
}

const contactPoints = {
    ref: '#/definitions/contactPoints',
    def: {
        description: 'A contact point array contains data that one can use to contact an agent resource.',
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: {
                    $ref: contactPoint.ref
                }
            }
        ]
    }
}

const geometryPoint = {
    ref: '#/definitions/geometryPoint',
    def: {
        type: 'object',
        description: 'The coordinates member is a single position.',
        properties: {
            type: {
                const: 'Point'
            },
            coordinates: {
                type: 'array',
                minItems: 2,
                items: {
                    type: 'number'
                }
            }
        },
        required: [
            'type',
            'coordinates'
        ]
    }
}

const geometryMultiPoint = {
    ref: '#/definitions/geometryMultiPoint',
    def: {
        type: 'object',
        description: 'The coordinates member is an array of positions.',
        properties: {
            type: {
                const: 'MultiPoint'
            },
            coordinates: {
                type: 'array',
                items: {
                    type: 'array',
                    minItems: 2,
                    items: {
                        type: 'number'
                    }
                }
            }
        },
        required: [
            'type',
            'coordinates'
        ]
    }
}

const geometryLineString = {
    ref: '#/definitions/geometryLineString',
    def: {
        type: 'object',
        description: 'The coordinates member is an array of two or more positions.',
        properties: {
            type: {
                const: 'LineString'
            },
            coordinates: {
                type: 'array',
                minItems: 2,
                items: {
                    type: 'array',
                    minItems: 2,
                    items: {
                        type: 'number'
                    }
                }
            }
        },
        required: [
            'type',
            'coordinates'
        ]
    }
}

const geometryMultiLineString = {
    ref: '#/definitions/geometryMultiLineString',
    def: {
        type: 'object',
        description: 'The coordinates member is an array of LineString coordinate arrays.',
        properties: {
            type: {
                const: 'MultiLineString'
            },
            coordinates: {
                type: 'array',
                items: {
                    type: 'array',
                    minItems: 2,
                    items: {
                        type: 'array',
                        minItems: 2,
                        items: {
                            type: 'number'
                        }
                    }
                }
            }
        },
        required: [
            'type',
            'coordinates'
        ]
    }
}

const geometryPolygon = {
    ref: '#/definitions/geometryPolygon',
    def: {
        type: 'object',
        description: 'The coordinates member is an array different coordinate arrays forming a polygon.',
        properties: {
            type: {
                const: 'Polygon'
            },
            coordinates: {
                type: 'array',
                items: {
                    type: 'array',
                    minItems: 4,
                    items: {
                        type: 'array',
                        minItems: 2,
                        items: {
                            type: 'number'
                        }
                    }
                }
            }
        },
        required: [
            'type',
            'coordinates'
        ]
    }
}

const geometryMultiPolygon = {
    ref: '#/definitions/geometryMultiPolygon',
    def: {
        type: 'object',
        description: 'The coordinates member is an array of Polygon coordinate arrays',
        properties: {
            type: {
                const: 'MultiPolygon'
            },
            coordinates: {
                type: 'array',
                items: {
                    type: 'array',
                    items: {
                        type: 'array',
                        minItems: 4,
                        items: {
                            type: 'array',
                            minItems: 2,
                            items: {
                                type: 'number'
                            }
                        }
                    }
                }
            }
        },
        required: [
            'type',
            'coordinates'
        ]
    }
}

const geometry = {
    ref: '#/definitions/geometry',
    def: {
        description: 'The representation of geographic data structures in this standard is borrowed from the GeoJSON standard specification.',
        oneOf: [
            {
                $ref: geometryPoint.ref
            },
            {
                $ref: geometryMultiPoint.ref
            },
            {
                $ref: geometryLineString.ref
            },
            {
                $ref: geometryMultiLineString.ref
            },
            {
                $ref: geometryPolygon.ref
            },
            {
                $ref: geometryMultiPolygon.ref
            }
        ]
    }
}

const optionalGeometries = {
    ref: '#/definitions/optionalGeometries',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'array',
                minItems: 1,
                items: {
                    $ref: geometry.ref
                }
            }
        ]
    }
}

const snowCondition = {
    ref: '#/definitions/snowCondition',
    def: {
        description: 'A snow condition object allows the representation of the conditions of a trail, snowpark or a mountain area at a given moment in time.',
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                properties: {
                    baseSnow: {
                        type: 'integer'
                    },
                    baseSnowRange: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'object',
                                properties: {
                                    lower: {
                                        type: 'integer'
                                    },
                                    upper: {
                                        type: 'integer'
                                    }
                                }
                            }
                        ]
                    },
                    groomed: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'boolean'
                            }
                        ]
                    },
                    latestStorm: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'integer'
                            }
                        ]
                    },
                    obtainedIn: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'string',
                                format: 'date'
                            },
                            {
                                type: 'string',
                                format: 'date-time'
                            }
                        ]
                    },
                    primarySurface: {
                        $ref: categoryString.ref
                    },
                    secondarySurface: {
                        $ref: categoryString.ref
                    },
                    snowMaking: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'boolean'
                            }
                        ]
                    },
                    snowOverNight: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'integer'
                            }
                        ]
                    },
                },
                required: [
                    'baseSnow',
                    'baseSnowRange',
                    'groomed',
                    'latestStorm',
                    'obtainedIn',
                    'primarySurface',
                    'secondarySurface',
                    'snowMaking',
                    'snowOverNight',
                ]
            }
        ]
    }
}

const urlDatatype = {
    ref: '#/definitions/urlDatatype',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'string',
                format: 'uri'
            },
            {
                type: 'object',
                propertyNames: {
                    $ref: languageCodes.ref
                },
                additionalProperties: {
                    type: 'string'
                }
            }
        ]
    }
}

const snowparkDifficulty = {
    ref: '#/definitions/snowparkDifficulty',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'string',
                enum: [
                    'beginner',
                    'intermediate',
                    'advanced',
                    'expert'
                ]
            }
        ]
    }
}

const snowparkFeatures = {
    ref: '#/definitions/snowparkFeatures',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: {
                    type: 'string',
                    enum: [
                        'jump',
                        'jib',
                        'pipe',
                        'rail'
                    ]
                }
            }
        ]
    }
}

const trailDifficulty = {
    ref: '#/definitions/trailDifficulty',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                properties: {
                    eu: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'string',
                                enum: [
                                    'novice',
                                    'beginner',
                                    'intermediate',
                                    'expert'
                                ]
                            }
                        ]
                    },
                    us: {
                        oneOf: [
                            {
                                type: 'null'
                            },
                            {
                                type: 'string',
                                enum: [
                                    'beginner',
                                    'beginner-intermediate',
                                    'intermediate',
                                    'intermediate-advanced',
                                    'expert'
                                ]
                            }
                        ]
                    }
                },
                additionalProperties: false,
                if: {
                    anyOf: [
                        {
                            properties: {
                                eu: {
                                    type: 'null'
                                }
                            }
                        },
                        {
                            properties: {
                                us: {
                                    type: 'null'
                                }
                            }
                        }
                    ]
                },
                then: {
                    anyOf: [
                        {
                            properties: {
                                eu: {
                                    not: {
                                        type: 'null'
                                    }
                                }
                            }
                        },
                        {
                            properties: {
                                us: {
                                    not: {
                                        type: 'null'
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        ]
    }
}

const optionalPositiveInteger = {
    ref: '#/definitions/optionalPositiveInteger',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'integer',
                minimum: 0
            }
        ]
    }
}

const optionalNonZeroPositiveInteger = {
    ref: '#/definitions/optionalNonZeroPositiveInteger',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'integer',
                minimum: 1
            }
        ]
    }
}

const optionalNonEmptyString = {
    ref: '#/definitions/optionalNonEmptyString',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'string',
                minLength: 1
            }
        ]
    }
}

const optionalDateOrDateTime = {
    ref: '#/definitions/optionalDateOrDateTime',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'string',
                format: 'date'
            },
            {
                type: 'string',
                format: 'date-time'
            }
        ]
    }
}


module.exports = {
    address,
    contactPoint,
    contactPoints,
    countryCodes,
    geometry,
    geometryPoint,
    geometryMultiPoint,
    geometryLineString,
    geometryMultiLineString,
    geometryPolygon,
    geometryMultiPolygon,
    optionalGeometries,
    languageCodes,
    snowCondition,
    text,
    categoryString,
    urlDatatype,
    snowparkDifficulty,
    snowparkFeatures,
    trailDifficulty,
    optionalPositiveInteger,
    optionalNonZeroPositiveInteger,
    optionalNonEmptyString,
    optionalDateOrDateTime,
    intervalObject,
    intervalObjectArray,
    dailySchedulesSpecification,
    weeklySchedulesSpecification,
    hoursSpecifications,
}
