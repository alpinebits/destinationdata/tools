const functions = require('./functions');
const datatypes = require('./datatypes');

const description = 'A resource representing a venue, a place that may host an event.';

const venues = functions.createResourceType('venues', description);

functions.addToAttributes(venues,{
    patternProperties: {
        name: {
            type: 'object'
        }
    }
});

functions.addToAttributes(venues,{
    type: 'object',
    properties: {
        address: {
            description: 'An address object representing the address of the venue.',
            $ref: datatypes.address.ref
        },
        geometries: {
            description: 'An array of geometry objects each of which represents the location of the venue in terms of GPS coordinates.',
            $ref: datatypes.optionalGeometries.ref
        },
        howToArrive: {
            description: 'A text object containing instructions on how to arrive at the venue.',
            $ref: datatypes.text.ref
        },
    },
});

const mediaDescription = 'An object representing a reference towards media objects that describe the venue.';
functions.addRelationshipToMany(venues,'multimediaDescriptions',['mediaObjects'],true,mediaDescription);

module.exports = { venues };
