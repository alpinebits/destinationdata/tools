const functions = require('./functions');
const datatypes = require('./datatypes');

const description = 'A resource representing a mountain area, a geographical region in which alpine sports and activities can be performed, such as skiing, snowboarding, climbing, and hiking.';

const mountainAreas = functions.createResourceType('mountainAreas', description);

functions.addToAttributes(mountainAreas,{
    patternProperties: {
        name: {
            type: 'object'
        }
    }
});

functions.addToAttributes(mountainAreas,{
    type: 'object',
    properties: {
        area: {
            description: 'A number representing the total area, in square meters, of the mountain area.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        geometries: {
            description: 'An array of geometry objects each of which represents the location of the mountain area in terms of GPS coordinates.',
            $ref: datatypes.optionalGeometries.ref
        },
        howToArrive: {
            description: 'A text object containing instructions on how to arrive at the mountain area.',
            $ref: datatypes.text.ref
        },
        maxAltitude: {
            description: 'A number representing the highest elevation point of the mountain area in meters above sea level.',
            $ref: datatypes.optionalPositiveInteger.ref
        },
        minAltitude: {
            description: 'A number representing the lowest elevation point of the mountain area in meters above sea level.',
            $ref: datatypes.optionalPositiveInteger.ref
        },
        openingHours: {
            description: 'An array of hours specification objects representing the hours in which the mountain area is open to the public.',
            $ref: datatypes.hoursSpecifications.ref
        },
        snowCondition: {
            description: 'A snow condition object containing the latest reported condition of the snow in the mountain area.',
            $ref: datatypes.snowCondition.ref
        },
        totalParkLength: {
            description: 'An integer representing the total length, in meters, of all snowparks located within the mountain area.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        totalTrailLength: {
            description: 'An integer representing the total length, in meters, of all trails located within the mountain area.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
    },
});

const areaOwnerDescription = 'An object representing references to an agent resource who owns the mountain area.';
functions.addRelationshipToOne(mountainAreas,'areaOwner',['agents'],true,areaOwnerDescription);

const connectionsDescription = 'An object representing references to place resources that identify the places that are physically accessible from the mountain area, which may include mountain areas, other lifts, snowparks, and trails.';
functions.addRelationshipToMany(mountainAreas,'connections',['lifts', 'mountainAreas', 'snowparks', 'trails'],true,connectionsDescription);

const liftsDescription = 'An object representing references to lift resources that identify the lifts located within the mountain area.';
functions.addRelationshipToMany(mountainAreas,'lifts',['lifts'],true,liftsDescription);

const mediaDescription = 'An object representing a reference towards media objects that describe the mountain area.';
functions.addRelationshipToMany(mountainAreas,'multimediaDescriptions',['mediaObjects'],true,mediaDescription);

const snowparksDescription = 'An object representing references to snowpark resources that identify the snowparks located within the mountain area.';
functions.addRelationshipToMany(mountainAreas,'snowparks',['snowparks'],true,snowparksDescription);

const subAreasDescription = 'An object representing references to mountain area resources that identify the mountain areas located within the mountain area.';
functions.addRelationshipToMany(mountainAreas,'subAreas',['mountainAreas'],true,subAreasDescription);

const trailsDescription = 'An object representing references to trail resources that identify the trails located within the mountain area.';
functions.addRelationshipToMany(mountainAreas,'trails',['trails'],true,trailsDescription);

module.exports = { mountainAreas };
