const basicAttributes = require('./basic-attributes');
const basicMeta = require('./basic-meta');
const basicRelationships = require('./basic-relationships');
const datatypes = require('./datatypes');
const functions = require('./functions');

const { agents } = require('./agents');
const { events } = require('./events');
const { eventSeries } = require('./eventSeries');
const { lifts } = require('./lifts');
const { mediaObjects } = require('./mediaObjects');
const { mountainAreas } = require('./mountainAreas');
const { snowparks } = require('./snowparks');
const { trails } = require('./trails');
const { venues } = require('./venues');

const resourceTypesMap = {
    agents, 
    events, 
    eventSeries, 
    lifts, 
    mediaObjects, 
    mountainAreas, 
    snowparks, 
    trails, 
    venues
}

function createBasicSchema() {
    const imports = {};

    Object.assign(
        imports,
        basicAttributes, 
        basicMeta, 
        basicRelationships, 
        datatypes
    );

    const schema = {
        type: 'object',
        properties: {
            data: {
                oneOf: [
                    {
                        type: 'null'
                    },
                    {
                        type: 'object'
                    },
                    {
                        type: 'array',
                        minItems: 1
                    }
                ]
            },
            included: {}
        }
    };

    schema.definitions = {};
    Object.keys(imports).forEach(key => {
        schema.definitions[key] = imports[key].def;
    });

    return schema;
}

function createDataMessageSchema(resourceTypes,includedResourceTypes) {
    const messageSchema = createBasicSchema();

    messageSchema.properties.data.oneOf = [
        {
            type: 'null'
        }
    ];

    resourceTypes.forEach(resourceType => {
        messageSchema.properties.data.oneOf.push({
            $ref: functions.definitionPointer(resourceType)
        });
        messageSchema.definitions[resourceType] = resourceTypesMap[resourceType].def
    });

    const conditionalInclusions = [];
    includedResourceTypes.forEach(includedResourceType => {
        conditionalInclusions.push({
            if: {
                properties: {
                    type: {
                        const: includedResourceType
                    }
                }
            },
            then: {
                allOf: [
                    {
                        $ref: functions.definitionPointer(includedResourceType)
                    }
                ]
            }
        })

        messageSchema.definitions[includedResourceType] = resourceTypesMap[includedResourceType].def;
    });
    
    messageSchema.properties.included = {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'array',
                items: {
                    type: 'object',
                    anyOf: conditionalInclusions
                }
            }
        ]
    }

    messageSchema.properties.links = {
        type: 'object',
        required: [
            'self'
        ]
    }

    messageSchema.properties.jsonapi = {
        type: 'object',
        properties: {
            version: {
                const: '1.0'
            }
        },
        required: [
            'version'
        ],
        additionalProperties: false
    }

    messageSchema.required = [
        'data',
        'links',
    ]

    return messageSchema;
}

function createDataArrayMessageSchema(resourceTypes,includedResourceTypes,enablePagination) {
    const messageSchema = createBasicSchema();
    const dataArray = {
        type: 'array',
        minItems: 1,
        items: {
            oneOf: []
        }
    };

    resourceTypes.forEach(resourceType => {
        dataArray.items.oneOf.push({
            $ref: functions.definitionPointer(resourceType)
        });
        messageSchema.definitions[resourceType] = resourceTypesMap[resourceType].def;
    });

    messageSchema.properties.data.oneOf = [
        {
            type: 'null'
        },
        dataArray
    ];

    const conditionalInclusions = [];
    includedResourceTypes.forEach(includedResourceType => {
        conditionalInclusions.push({
            if: {
                properties: {
                    type: {
                        const: includedResourceType
                    }
                }
            },
            then: {
                allOf: [
                    {
                        $ref: functions.definitionPointer(includedResourceType)
                    }
                ]
            }
        })

        messageSchema.definitions[includedResourceType] = resourceTypesMap[includedResourceType].def;
    });
    
    messageSchema.properties.included = {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'array',
                items: {
                    type: 'object',
                    anyOf: conditionalInclusions
                }
            }
        ]
    }

    if(enablePagination) {
        messageSchema.properties.meta = {
            type: 'object',
            properties: {
                count: {
                    type: 'integer',
                    minimum: 0
                },
                pages: {
                    type: 'integer',
                    minimum: 0
                },
            },
            required: [
                'count',
                'pages'
            ]
        }

        messageSchema.properties.links = {
            type: 'object',
            required: [
                'first',
                'last',
                'next',
                'prev',
                'self',
            ]
        }
    }
    else{
        messageSchema.properties.links = {
            type: 'object',
            required: [
                'self'
            ]
        }
    }

    messageSchema.properties.jsonapi = {
        type: 'object',
        properties: {
            version: {
                const: '1.0'
            }
        },
        required: [
            'version'
        ],
        additionalProperties: false
    }

    messageSchema.required = [
        'data',
        'links',
    ]

    return messageSchema;
}

const baseEndpointSchema = {
    type: 'object',
    properties: {
        jsonapi: {
            type: 'object',
            properties: {
                version: {
                    const: '1.0'
                }
            },
            required: [
                'version'
            ],
            additionalProperties: false
        },
        links: {
            type: 'object',
            required: [
                'self',
            ]
        },
        data: {
            type: 'null'
        },
    },
    required: [
        'jsonapi',
        'data',
        'links',
    ]
}


module.exports = {
    createBasicSchema,
    createDataMessageSchema,
    createDataArrayMessageSchema,
    baseEndpointSchema,
}