const datatypes = require('./datatypes');
const functions = require('./functions');

const description = 'A resource representing an event, something that is planned to happen at a particular place and time, organized by an agent for a particular purpose, and is of interest to a general audience.'

const events = functions.createResourceType('events',description);

functions.addToAttributes(events, {
  patternProperties: {
    name: {
      type: 'object'
    }
  }
});

functions.addToAttributes(events, {
  type: 'object',
  properties: {
    capacity: {
      description: 'An integer representing the total number of individuals that can attend the event.',
      $ref: datatypes.optionalNonZeroPositiveInteger.ref
    },
    endDate: {
      description: 'A string, formatted as a date or date-time string, representing when the event is supposed to end.',
      $ref: datatypes.optionalDateOrDateTime.ref
    },
    startDate: {
      description: 'A string, formatted as a date or date-time string, representing when the event is supposed to start.',
      $ref: datatypes.optionalDateOrDateTime.ref
    },
    status: {
      description: 'A string representing the current status of the event.',
      oneOf: [
        {
          type: 'null'
        },
        {
          type: 'string',
          enum: [
            'canceled',
            'published'
          ]
        }
      ]
    }
  },
  if: {
    allOf: [
      {
        properties: {
          startDate: {
            anyOf: [
              {
                type: 'null'
              },
              {
                type: 'string'
              }
            ]
          },
          endDate: {
            anyOf: [
              {
                type: 'null'
              },
              {
                type: 'string'
              }
            ]
          }
        }
      }
    ]
  },
  then: {
    anyOf: [
      {
        properties: {
          startDate: {
            not: {
              type: 'null'
            }
          }
        }
      },
      {
        properties: {
          endDate: {
            not: {
              type: 'null'
            }
          }
        }
      }
    ]
  }
});

const contributorsDescription = 'An object representing references to agent resources, which identifies the agents expected to participate in the event, such as a speaker who will give a talk or a musician who will perform at a concert.';
functions.addRelationshipToMany(events, 'contributors', ['agents'], true,contributorsDescription);

const mediaDescription = 'An object representing a reference towards media objects that describe the event.';
functions.addRelationshipToMany(events, 'multimediaDescriptions', ['mediaObjects'], true, mediaDescription);

const organizersDescription = 'An object representing references to agent resources identifying the persons or organizations responsible for organizing the event.';
functions.addRelationshipToMany(events, 'organizers', ['agents'], false,organizersDescription);

const publisherDescription = 'An object representing a reference to an agent resource who originally provided the data about the event. The publisher is not the organization who manages the system where the data was originally inserted, but the organization actually provided the data.';
functions.addRelationshipToOne(events, 'publisher', ['agents'], false,publisherDescription);

const seriesDescription = 'An object representing a reference to an event series resource of which the event is an edition of.';
functions.addRelationshipToOne(events, 'series', ['eventSeries'], true,seriesDescription);

const sponsorsDescription = 'An object representing references to agent resources identifying the persons or organizations who are sponsoring the event.';
functions.addRelationshipToMany(events, 'sponsors', ['agents'], true,sponsorsDescription);

const subEventsDescription = 'An object representing references to event resources identifying the parts of the event.';
functions.addRelationshipToMany(events, 'subEvents', ['events'], true,subEventsDescription);

const venuesDescription = 'An object representing references to venue resources identifying where the event will happen.';
functions.addRelationshipToMany(events, 'venues', ['venues'], false,venuesDescription);

module.exports = { events };