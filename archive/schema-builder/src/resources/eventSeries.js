const functions = require('./functions');

const description = 'A resource representing a series of event recurrent in time.';

const eventSeries = functions.createResourceType('eventSeries', description);

functions.addToAttributes(eventSeries,{
    patternProperties: {
        name: {
            type: 'object'
        }
    }
});

functions.addToAttributes(eventSeries,{
    type: 'object',
    properties: {
        frequency: {
            description: 'A string representing how often editions of the event series are organized according to an enumeration of possible values.',
            oneOf: [
                {
                    type: 'null'
                },
                {
                    type: 'string',
                    enum: [
                        'daily',
                        'weekly',
                        'monthly',
                        'bimonthly',
                        'quarterly',
                        'annual',
                        'biennial',
                        'triennial',
                    ]
                }
            ]
        }
    }
});

const editionsDescription = 'An object representing references to the event resources that are editions of the event series.';
functions.addRelationshipToMany(eventSeries,'editions',['events'],true,editionsDescription);

const mediaDescription = 'An object representing a reference towards media objects that describe the event series.';
functions.addRelationshipToMany(eventSeries,'multimediaDescriptions',['mediaObjects'],true,mediaDescription);

module.exports = { eventSeries };
