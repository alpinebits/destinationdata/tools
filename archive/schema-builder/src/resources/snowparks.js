const functions = require('./functions');
const datatypes = require('./datatypes');
const basicAttributes = require('./basic-attributes');

const description = 'A resource representing a snowpark, a particular type of trail that is designed to allow skiers and snowboarders to perform free-style tricks by providing them with special features, such as jumps and rails. ';

const snowparks = functions.createResourceType('snowparks',description);

functions.addToAttributes(snowparks,{
    patternProperties: {
        name: {
            type: 'object'
        }
    }
});

functions.addToAttributes(snowparks,{
    type: 'object',
    properties: {
        address: {
            description: 'An address object representing the address of the snowpark.',
            $ref: datatypes.address.ref
        },
        difficulty: {
            description: 'A string describing the difficulty level of a snowpark.',
            $ref: datatypes.snowparkDifficulty.ref
        },
        features: {
            description: 'An array of category strings that informs the types of features available within the snowpark, such as a jump or a rail.',
            $ref: basicAttributes.categories.ref
        },
        geometries: {
            description: 'An array of geometry objects each of which represents the location of the snowpark in terms of GPS coordinates.',
            $ref: datatypes.optionalGeometries.ref
        },
        howToArrive: {
            description: 'A text object containing instructions on how to arrive at the snowpark.',
            $ref: datatypes.text.ref
        },
        length: {
            description: 'A number representing the length of the snowpark in meters.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        maxAltitude: {
            description: 'A number representing the highest elevation point of the snowpark in meters above sea level.',
            $ref: datatypes.optionalPositiveInteger.ref
        },
        minAltitude: {
            description: 'A number representing the lowest elevation point of the snowpark in meters above sea level.',
            $ref: datatypes.optionalPositiveInteger.ref
        },
        openingHours: {
            description: 'An array of hours specification objects representing the hours in which the snowpark is open to the public.',
            $ref: datatypes.hoursSpecifications.ref
        },
        snowCondition: {
            description: 'A snow condition object containing the latest reported condition of the snow in the snowpark.',
            $ref: datatypes.snowCondition.ref
        }
    },
});

const connectionsDescription = 'An object representing references to place resources that identify the places that are physically accessible from the snowpark, which may include mountain areas, other lifts, snowparks, and trails.';
functions.addRelationshipToMany(snowparks,'connections',['lifts', 'mountainAreas', 'snowparks', 'trails'],true,connectionsDescription);

const mediaDescription = 'An object representing a reference towards media objects that describe the snowpark.';
functions.addRelationshipToMany(snowparks,'multimediaDescriptions',['mediaObjects'],true,mediaDescription);

module.exports = { snowparks };
