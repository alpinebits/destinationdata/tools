const datatypes = require('./datatypes');
const functions = require('./functions');

const description = 'A resource representing an object that materializes creative works into a digital format to enable processing and sharing.';

const mediaObjects = functions.createResourceType('mediaObjects', description);

functions.addToAttributes(mediaObjects,{
    patternProperties: {
        url: {
            oneOf: [
                {
                    type: 'string'
                },
                {
                    type: 'object'
                }
            ]
        }
    }
});

functions.addToAttributes(mediaObjects,{
    type: 'object',
    properties: {
        contentType: {
            description: 'A string that represents the media type (formerly known as MIME type) of the media object.',
            type: 'string',
            pattern: '^(application|audio|font|example|image|message|model|multipart|text|video)/[a-zA-Z0-9-.+]+$'
        },
        duration: {
            description: 'A number representing the duration of an audio or a video in seconds.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        height: {
            description: 'A number representing the height of an image or a video in pixels.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        license: {
            description: 'A string that represents the license applied to the media object.',
            $ref: datatypes.optionalNonEmptyString.ref
        },
        width: {
            description: 'A number representing the width of an image or a video in pixels.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
    },
    allOf: [
        {
            if: {
                properties: {
                    contentType: {
                        type: 'string',
                        pattern: '^(application|font|example|image|message|model|multipart|text)/[a-zA-Z0-9-.+]+$'
                    }
                }
            },
            then: {
                properties: {
                    duration: {
                        type: 'null',
                    }
                }
            }
        },
        {
            if: {
                properties: {
                    contentType: {
                        type: 'string',
                        pattern: '^(application|audio|font|example|message|model|multipart|text)/[a-zA-Z0-9-.+]+$'
                    }
                }
            },
            then: {
                properties: {
                    height: {
                        type: 'null',
                    },
                    width: {
                        type: 'null',
                    }
                }
            }
        }
    ]
});

const copyrightOwnerDescription = 'An object representing a reference towards an agent resource who owns the rights over the media object.'
functions.addRelationshipToOne(mediaObjects,'copyrightOwner',['agents'],true,copyrightOwnerDescription);

module.exports = { mediaObjects };
