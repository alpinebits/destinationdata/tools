const functions = require('./functions');
const datatypes = require('./datatypes');

const description = 'A resource representing a lift, a machine designed to transport people uphill, being often used to transport skiers in mountain areas.';

const lifts = functions.createResourceType('lifts',description);

functions.addToAttributes(lifts,{
    patternProperties: {
        name: {
            type: 'object'
        }
    }
});

functions.addToAttributes(lifts,{
    type: 'object',
    properties: {
        address: {
            description: 'An address object representing the address of the lift.',
            $ref: datatypes.address.ref
        },
        capacity: {
            description: 'A number representing how many persons the lift can transport hourly on average.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        geometries: {
            description: 'An array of geometry objects each of which represents the location of the lift in terms of GPS coordinates.',
            $ref: datatypes.optionalGeometries.ref
        },
        howToArrive: {
            description: 'A text object containing instructions on how to arrive at the lift.',
            $ref: datatypes.text.ref
        },
        length: {
            description: 'A number representing the length of the lift in meters.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        maxAltitude: {
            description: 'A number representing the highest elevation point of the lift in meters above sea level.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        minAltitude: {
            description: 'A number representing the lowest elevation point of the lift in meters above sea level.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
        openingHours: {
            description: 'An array of hours specification objects representing the hours in which the lift is open to the public.',
            $ref: datatypes.hoursSpecifications.ref
        },
        personsPerChair: {
            description: 'An integer representing the number of persons that fit in a single chair/cabin of a lift.',
            $ref: datatypes.optionalNonZeroPositiveInteger.ref
        },
    }
});

const connectionsDescription = 'An object representing references to place resources that identify the places that are physically accessible from the lift, which may include mountain areas, other lifts, snowparks, and trails.';
functions.addRelationshipToMany(lifts,'connections',['lifts', 'mountainAreas', 'snowparks', 'trails'],true,connectionsDescription);

const mediaDescription = 'An object representing a reference towards media objects that describe the lifts.';
functions.addRelationshipToMany(lifts,'multimediaDescriptions',['mediaObjects'],true,mediaDescription);

module.exports = { lifts };

