const datatypes = require('./datatypes');
const { basicAttributes } = require('./basic-attributes');
const { basicMeta } = require('./basic-meta');
const { links, relationshipToOne, relationshipToMany } = require('./basic-relationships');

function definitionPointer(name) {
    return `#/definitions/${name}`;
}

function createDefinition(name,definition) {
    return {
        ref: definitionPointer(name),
        def: definition
    };
}

function createResourceType(name,description) {
    const definition = {
        type: 'object',
        description: description,
        properties: {
            type: {
                description: 'A string that identifies the resource\'s type.',
                const: name
            },
            id: {
                description: 'A string that uniquely and persistently identifies the resource within a server.',
                type: 'string',
                minLength: 1
            },
            meta: {
                allOf: [
                    {
                        $ref: basicMeta.ref
                    }
                ]
            },
            links: {
                allOf: [
                    {
                        $ref: links.ref
                    }
                ]
            },
            attributes: {
                allOf: [
                    {
                        $ref: basicAttributes.ref
                    }
                ]
            },
            relationships: {
                type: 'object',
                description: 'An object containing the resource\'s data referring to other resources (e.g. the organizers of an event, the trails within a mountain area).',
                properties: {},
                additionalProperties: {
                    oneOf: [
                        {
                            type: 'null'
                        },
                        {
                            $ref: relationshipToOne.ref
                        },
                        {
                            $ref: relationshipToMany.ref
                        }
                    ]
                },
                required: []
            }
        },
    };

    const resourceDefinition = createDefinition(name,definition);
    addRequiredLinks(resourceDefinition,['self']);
    
    return resourceDefinition;
}

function addRequiredLinks(resourceDefinition,requiredLinks) {
    resourceDefinition.def.properties.links.allOf.push({
        type: 'object',
        required: requiredLinks
    });
}

function addAttribute(resourceDefinition, attributeDefinition) {
    resourceDefinition.def.properties.attributes.allOf.push(attributeDefinition);
}

function addRelationshipToOne(resourceDefinition, relationshipName, allowedTypes, nullable, description) {
    const dataDef = {
        oneOf: [
            {
                type: 'object',
                properties: {
                    id: {
                        type: 'string',
                        minLength: 1
                    },
                    type: {
                        type: 'string',
                        enum: allowedTypes
                    }
                },
                required: [
                    'id',
                    'type'
                ],
                additionalProperties: false
            }
        ]
    };

    const linksDef = {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                additionalProperties: {
                    oneOf: [
                        {
                            type: 'null'
                        },
                        {
                            type: 'string',
                            format: 'uri'
                        }
                    ]
                },
                required: [
                    'related'
                ]
            }
        ]
    }

    resourceDefinition.def.properties.relationships.properties[relationshipName] = {
        description: description,
        oneOf: [
            {
                type: 'object',
                properties: {
                    data: dataDef,
                    links: linksDef
                },
                required: [
                    'data',
                    'links'
                ]
            }
        ]
    };

    if(nullable) {
        dataDef.oneOf.push({ type: 'null' });
        resourceDefinition.def.properties.relationships.properties[relationshipName].oneOf.push({ type: 'null' });
    }
}

function addRelationshipToMany(resourceDefinition, relationshipName, allowedTypes, nullable, description) {
    const dataDef = {
        oneOf: [
            {
                type: 'array',
                minItems: 1,
                items: {
                    type: 'object',
                    properties: {
                        id: {
                            type: 'string',
                            minLength: 1
                        },
                        type: {
                            type: 'string',
                            enum: allowedTypes
                        }
                    },
                    required: [
                        'id',
                        'type'
                    ],
                    additionalProperties: false
                }
            }
        ]
    };

    const linksDef = {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                additionalProperties: {
                    oneOf: [
                        {
                            type: 'null'
                        },
                        {
                            type: 'string',
                            format: 'uri'
                        }
                    ]
                },
                required: [
                    'related'
                ]
            }
        ]
    }

    resourceDefinition.def.properties.relationships.properties[relationshipName] = {
        description: description,
        oneOf: [
            {
                type: 'object',
                properties: {
                    data: dataDef,
                    links: linksDef
                },
                required: [
                    'data',
                    'links'
                ]
            }
        ]
    };

    if(nullable) {
        dataDef.oneOf.push({ type: 'null' });
        resourceDefinition.def.properties.relationships.properties[relationshipName].oneOf.push({ type: 'null' });
    }
}

module.exports = {
    definitionPointer,
    createDefinition,
    createResourceType,
    addRequiredLinks,
    addToAttributes: addAttribute,
    addRelationshipToOne,
    addRelationshipToMany,
}