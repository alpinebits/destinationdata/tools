
const allowedTypes = {
    ref: '#/definitions/allowedTypes',
    def: {
        type: 'string',
        minLength: 1
    }
}

const links = {
    ref: '#/definitions/links',
    def: {
        description: 'An object containing the links related to access related resources (e.g., a link to the agent resources representing organizers and sponsors of an event, a link to the media object resources representing multimedia descriptions of a mountain area resource, or a link to the resource itself, also referred to as `self`).',
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                additionalProperties: {
                    oneOf: [
                        {
                            type: 'null'
                        },
                        {
                            type: 'string',
                            format: "uri"
                        }
                    ]
                }
            }
        ]
    }
}

const link = {
    ref: '#/definitions/link',
    def: {
        type: 'object',
        additionalProperties: {
            oneOf: [
                {
                    type: 'null'
                },
                {
                    type: 'string',
                    format: "uri"
                }
            ]
        }
    }
}

const referenceObject = {
    ref: '#/definitions/referenceObject',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                properties: {
                    type: {
                        $ref: allowedTypes.ref
                    },
                    id: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: [
                    'type',
                    'id'
                ]
            }
        ]
    }
}

const referenceArray = {
    ref: '#/definitions/referenceArray',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: {
                    $ref: referenceObject.ref
                }
            }
        ]
    }
}

const relationshipToMany = {
    ref: '#/definitions/relationshipToMany',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                properties: {
                    data: {
                        $ref: referenceArray.ref    
                    },
                    links: {
                        $ref: links.ref
                    }
                },
                required: [
                    'data',
                    'links'
                ]
            }
        ]
    }
}

const relationshipToOne = {
    ref: '#/definitions/relationshipToOne',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                properties: {
                    data: {
                        $ref: referenceObject.ref
                    },
                    links: {
                        $ref: links.ref
                    }
                },
                required: [
                    'data',
                    'links'
                ]
            }
        ]
    }
}

const basicRelationships = {
    ref: '#/definitions/basicRelationships',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'object',
                additionalProperties: {
                    oneOf: [
                        {
                            type: 'null'
                        },
                        {
                            $ref: relationshipToOne.ref
                        },
                        {
                            $ref: relationshipToMany.ref
                        }
                    ]
                }
            }
        ]
    }
}

module.exports = {
    allowedTypes,
    basicRelationships,
    link,
    links,
    referenceObject,
    referenceArray,
    relationshipToMany,
    relationshipToOne,
}