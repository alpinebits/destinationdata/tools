const { categoryString, text, urlDatatype, dailyHoursSpecification, weeklyHoursSpecification } = require('./datatypes');

const abstract = {
    ref: '#/definitions/abstract',
    def: {
        $ref: text.ref
    }
}

const categories = {
    ref: '#/definitions/categories',
    def: {
        oneOf: [
            {
                type: 'null'
            },
            {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: {
                    $ref: categoryString.ref
                }
            }
        ]
    }
}

const description = {
    ref: '#/definitions/description',
    def: {
        $ref: text.ref
    }
}

const name = {
    ref: '#/definitions/name',
    def: {
        $ref: text.ref
    }
}

const shortName = {
    ref: '#/definitions/shortName',
    def: {
        $ref: text.ref
    }
}

const url = {
    ref: '#/definitions/url',
    def: {
        $ref: urlDatatype.ref
    }
}

const basicAttributes = {
    ref: '#/definitions/basicAttributes',
    def: {
        type: 'object',
        properties: {
            abstract: {
                description: 'A text object containing a short textual description of the resource.',
                $ref: abstract.ref
            },
            categories: {
                description: 'An array of category strings identifying the types instantiated by the resource.',
                $ref: categories.ref
            },
            description: {
                description: 'A text object containing the complete textual description of the resource.',
                $ref: description.ref
            },
            name: {
                description: 'A text object containing the complete name of the resource.',
                $ref: name.ref
            },
            shortName: {
                description: 'A text object containing a short name of the resource.',
                $ref: shortName.ref
            },
            url: {
                description: 'A url string or a multilingual url object containing language-specific url strings.',
                $ref: url.ref
            },
        }
    }
}


module.exports = {
    abstract,
    basicAttributes,
    categories,
    description,
    name,
    shortName,
    url,
}
