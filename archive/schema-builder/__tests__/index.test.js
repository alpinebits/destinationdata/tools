const schemaBuilder = require('./../src/');
const resources = require('./../src/resources/');
const Ajv = require('ajv');
const ajv = new Ajv();

function testMessageSchema(messageSchema, message) {
    let messageSchemaValidator;

    test('Attempt to compile message schema', () => {
        expect(() => {
            messageSchemaValidator = ajv.compile(messageSchema);
        }).not.toThrow();
    });
    
    test('Attempt to compile message schema', () => {
        expect(() => {
            messageSchemaValidator = ajv.compile(messageSchema);
        }).not.toThrow();
    });

    test('Try to validate message', () => {
        let result = schemaBuilder.validateMessage(message, messageSchema);
        
        if(result.errors) {
            console.log(result);
        }

        expect(result).toBe(true);
    });
}

const requiredFields = {
    data: {},
    jsonapi: {
        version: '1.0'
    },
    meta: {
        count: 1,
        pages: 1
    },
    links: {
        first: 'http://example.com/1.0/resource?page[number]=1',
        last: 'http://example.com/1.0/resource?page[number]=1',
        next: 'http://example.com/1.0/resource?page[number]=1',
        prev: 'http://example.com/1.0/resource?page[number]=1',
        self: 'http://example.com/1.0/resource?page[number]=1',
    }
}

describe('Testing building and (internally) compiling schemas', () => {
    schemaBuilder.buildSchemas();
});

const testObjects = {
    'base.endpoint.msg': require('./../../examples/base.endpoint.msg.json'),
    'agents.id.min': require('./../../examples/agents.id.min.json'),
    'agents.id.full': require('./../../examples/agents.id.full.json'),
    'agents.min': require('./../../examples/agents.min.json'),
    'agents.full': require('./../../examples/agents.full.json'),
    'events.id.min': require('./../../examples/events.id.min.json'),
    'events.id.full': require('./../../examples/events.id.full.json'),
    'events.min': require('./../../examples/events.min.json'),
    'events.full': require('./../../examples/events.full.json'),
    'events.additionalTest1': require('./test_objects/events-response1.json'),
    'events.additionalTest2': require('./test_objects/event.ajv.json'),
    'eventseries.id.min': require('./../../examples/eventseries.id.min.json'),
    'eventseries.id.full': require('./../../examples/eventseries.id.full.json'),
    'eventseries.min': require('./../../examples/eventseries.min.json'),
    'eventseries.full': require('./../../examples/eventseries.full.json'),
    'lifts.id.min': require('./../../examples/lifts.id.min.json'),
    'lifts.id.full': require('./../../examples/lifts.id.full.json'),
    'lifts.min': require('./../../examples/lifts.min.json'),
    'lifts.full': require('./../../examples/lifts.full.json'),
    'mediaobjects.id.min': require('./../../examples/mediaobjects.id.min.json'),
    'mediaobjects.id.full': require('./../../examples/mediaobjects.id.full.json'),
    'mediaobjects.min': require('./../../examples/mediaobjects.min.json'),
    'mediaobjects.full': require('./../../examples/mediaobjects.full.json'),
    'mediaobjects.example': require('./../../examples/mediaobjects.example.json'),
    'mountainareas.id.min': require('./../../examples/mountainareas.id.min.json'),
    'mountainareas.id.full': require('./../../examples/mountainareas.id.full.json'),
    'mountainareas.id.connections.min': require('./../../examples/mountainareas.id.connections.min.json'),
    'mountainareas.id.connections.example': require('./../../examples/mountainareas.id.connections.example.json'),
    'mountainareas.min': require('./../../examples/mountainareas.min.json'),
    'mountainareas.full': require('./../../examples/mountainareas.full.json'),
    'mediaobjects.example': require('./../../examples/mediaobjects.example.json'),
    'snowparks.id.min': require('./../../examples/snowparks.id.min.json'),
    'snowparks.id.full': require('./../../examples/snowparks.id.full.json'),
    'snowparks.min': require('./../../examples/snowparks.min.json'),
    'snowparks.full': require('./../../examples/snowparks.full.json'),
    'trails.id.min': require('./../../examples/trails.id.min.json'),
    'trails.id.full': require('./../../examples/trails.id.full.json'),
    'trails.min': require('./../../examples/trails.min.json'),
    'trails.full': require('./../../examples/trails.full.json'),
    'venues.id.min': require('./../../examples/venues.id.min.json'),
    'venues.id.full': require('./../../examples/venues.id.full.json'),
    'venues.min': require('./../../examples/venues.min.json'),
    'venues.full': require('./../../examples/venues.full.json'),
}

const schemas = {
    'base.endpoint': require('./../../schemas/base.endpoint.schema.json'),
    'agents.id': require('./../../schemas/agents.id.schema.json'),
    'agents': require('./../../schemas/agents.schema.json'),
    'events.id': require('./../../schemas/events.id.schema.json'),
    'events': require('./../../schemas/events.schema.json'),
    'eventseries.id': require('./../../schemas/eventseries.id.schema.json'),
    'eventseries': require('./../../schemas/eventseries.schema.json'),
    'lifts.id': require('./../../schemas/lifts.id.schema.json'),
    'lifts': require('./../../schemas/lifts.schema.json'),
    'mediaobjects.id': require('./../../schemas/mediaobjects.id.schema.json'),
    'mediaobjects': require('./../../schemas/mediaobjects.schema.json'),
    'mountainareas.id': require('./../../schemas/mountainareas.id.schema.json'),
    'mountainareas.id.connections': require('./../../schemas/mountainareas.id.connections.schema.json'),
    'mountainareas': require('./../../schemas/mountainareas.schema.json'),
    'snowparks.id': require('./../../schemas/snowparks.id.schema.json'),
    'snowparks': require('./../../schemas/snowparks.schema.json'),
    'trails.id': require('./../../schemas/trails.id.schema.json'),
    'trails': require('./../../schemas/trails.schema.json'),
    'venues.id': require('./../../schemas/venues.id.schema.json'),
    'venues': require('./../../schemas/venues.schema.json'),
}

// The schema described in this test covers the following endpoints:
    // /1.0
describe('Testing base endpoint message', () => {
    const baseEndpointSchema = schemas['base.endpoint'];
    testMessageSchema(baseEndpointSchema, testObjects['base.endpoint.msg']);
});    
