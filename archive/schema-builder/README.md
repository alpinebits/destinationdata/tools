# Validator

This is a javascript project used to generate [JSON Schema](https://json-schema.org/) specifications to enable the validation of data provided by AlpineBits DestinationData servers.
A JSON Schema specification is available for each endpoint in the defined in the ALpineBits DestinationData standard.
The generated schemas are stored in the [../schemas](../schemas) folder.

## Instructions

### 1. Installing dependencies
  
The first thing you need to do is to install the project's dependencies. 

To do that, go to the project directory and run:

```bash
$ npm install
```

### 2. Generating schemas

To generate the schemas, run:

```bash
$ npm start
```

The generated schemas are stored in the [/schemas](../schemas) folder.

### 3. Testing schemas

To test the generated schemas, run:

```bash
$ npm test
```

The automated tests of this project rely both in test files included in the project and in example files available in the [../examples](../examples) folder.
