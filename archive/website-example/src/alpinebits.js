import Axios from "axios";

// The requests are made to an open endpoint, but authenticated requests
// are just as simple thanks to basic authentication. Simple add an
// "Authorization" header set to ${"Basic " + base64("username:password")}
// where base64() refers to the need of encoding the "username/password"
// in base 64. The HTTPS connection is responsible to keep the header secret
//    Y2hyaXM6c2VjcmV0 = "username:password"
const axios = Axios.create({
  // baseURL: "http://localhost:8080/2021-04",
  baseURL: "https://destinationdata.alpinebits.opendatahub.bz.it/2021-04",
  // headers: {
  //   'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ'
  // },
  });

// This method supports generic requests to the known endpoint
// adding parameters to the request as we wish
export async function fetchResource(opts) {
  if (!opts || !opts.resourceType) {
    return;
  }

  // The actual request is built based on passed on the information
  // in "opts"
  let request = "";
  request += opts.resourceType ? `/${opts.resourceType}` : "";
  request += opts.resourceId ? `/${opts.resourceId}` : "";

  // At the moment, pagination is the only required server feature
  // to custom requests. Nonetheless, the Open DataHub endpoint
  // also supports field selection and resource inclusion
  // (recommended features)
  let params = [];
  if (opts.pageSize) {
    params.push(`page[size]=${opts.pageSize}`);
  }
  if (opts.pageNumber) {
    params.push(`page[number]=${opts.pageNumber}`);
  }
  if (opts.sortBy) {
    params.push(`sort=${opts.sortBy}`);
  }
  if (Array.isArray(opts.filter)) {
    opts.filter.forEach((filter) => {
      if (filter.operand)
        params.push(`filter[${filter.name}][${filter.operand}]=${filter.value}`);
      else
        params.push(`filter[${filter.name}]=${filter.value}`);
    });
  }

  request = `${request}${params ? "?" + params.join("&") : ""}`;
  console.log("Performing request at", request)

  // This is the request, a one-liner
  return axios
    .get(request)
    .then((res) => res.data)
    .catch((err) => {
      throw err;
    });
}

// For navigation in hypermedia, it is more simple to just
// use the available links instead of building a new request
export async function fetchResourceFromURL(url) {
  if (!url) {
    return;
  }

  console.log("Performing request at", url)
  
  // This is the request, a one-liner
  return Axios.get(url)
    .then((res) => {
      console.log("Response", res.data);
      return res.data;
    })
    .catch((err) => {
      throw err;
    });
}

// Simple function to support multilingual fields get the
// preferred language
export function getValueInLanguage(object, languages) {
  for (const lang of languages) {
    if (object && object[lang]) {
      return object[lang];
    }
  }
}

// Pretty print for event dates
export function getDatesString(event) {
  if (event.attributes.startDate && event.attributes.endDate) {
    return `${new Date(
      event.attributes.startDate
    ).toLocaleDateString()} - ${new Date(
      event.attributes.endDate
    ).toLocaleDateString()}`;
  } else if (event.startDate) {
    return `${new Date(event.attributes.endDate).toLocaleDateString()}`;
  } else if (event.endDate) {
    return `${new Date(event.attributes.endDate).toLocaleDateString()}`;
  }
}
