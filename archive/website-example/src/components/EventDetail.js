import React from "react";
import { Component } from "react";
import { Descriptions, Layout } from "antd";
import { SimpleMap } from "./SimpleMap";
import { getValueInLanguage, getDatesString } from "../alpinebits";

export class EventDetail extends Component {
  render() {
    let { event } = this.props;

    // Renders component when no event is passed
    if (!event || !event.type) {
      return (
        <p align="center" style={{ padding: "5px" }}>
          No event selected
        </p>
      );
    }
    
    // Extracts event url for conditional rendering
    let eventUrlField;
    let url = getValueInLanguage(event.attributes.url, ["eng", "ita", "deu"]);
    
    if (url) {
      eventUrlField = (
        <Descriptions.Item label="More Info">
          <a href={url} target="_blank" rel="noopener noreferrer">
            {url}
          </a>
        </Descriptions.Item>
      );
    }
    
    // Extracts location information from venue for
    // conditional rendering
    let { venue } = this.props;
    let location = {};
    if (venue) {
      location.street = getValueInLanguage(venue.attributes.address.street, [
        "eng",
        "ita",
        "deu",
      ]);
      location.city = getValueInLanguage(venue.attributes.address.city, [
        "eng",
        "ita",
        "deu",
      ]);
      location.country = venue.attributes.address.country;

      venue.attributes.geometries.forEach((geometry) => {
        if (geometry.type === "Point") {
          location.lng = geometry.coordinates[0];
          location.lat = geometry.coordinates[1];
        }
      });
    }

    return (
      <>
        <Layout>
          <Layout.Content
            style={{ backgroundColor: "#FFFFFF", padding: "15px" }}
          >
            <h1>
              <b>
                {getValueInLanguage(event.attributes.name, [
                  "eng",
                  "ita",
                  "deu",
                ])}
              </b>
            </h1>

            <p>
              {getValueInLanguage(event.attributes.description, [
                "eng",
                "ita",
                "deu",
              ])}
            </p>

            <Descriptions bordered column={1}>
              <Descriptions.Item label="Date">
                {getDatesString(event)}
              </Descriptions.Item>

              <Descriptions.Item label="Location">
                {`${location.street ? location.street + ", " : ""}${
                  location.city
                } ${location.country ? '- ' + location.country : ''}`}
              </Descriptions.Item>

              {eventUrlField}
            </Descriptions>
          </Layout.Content>
        </Layout>

        <br />

        <SimpleMap
          lat={location.lat}
          lng={location.lng}
          width={350}
          height={350}
        />
      </>
    );
  }
}
