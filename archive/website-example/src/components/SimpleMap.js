import React, { Component } from "react";

export class SimpleMap extends Component {
  render() {
    const lng = this.props.lng || "11.33171";
    const lat = this.props.lat || "46.47861";
    const width = this.props.width || 425;
    const height = this.props.height || 350;

    // We build OpenStreetMaps urls in order to avoid
    // requesting an API key, so there is more potential
    // using DestinationData places' information that we
    // can show here
    const sourceURL = `https://www.openstreetmap.org/#map=17/${lat}/${lng}`;
    const mapURL = `https://www.openstreetmap.org/export/embed.html?bbox=${[
      lng,
      lat,
      lng,
      lat,
    ].join("%2C")}&amp;layer=mapnik`;

    console.log("Rendering map", this.props);
    return (
      <p style={{ textAlign: "center" }}>
        <iframe
          title={this.props.title}
          width={width}
          height={height}
          frameBorder="0"
          marginHeight="0"
          marginWidth="0"
          src={mapURL}
          style={{ border: "1px solid black" }}
        ></iframe>
        <br />
        <small>
          <a href={sourceURL} target="_blank" rel="noopener noreferrer">
            View Larger Map
          </a>
        </small>
      </p>
    );
  }
}
