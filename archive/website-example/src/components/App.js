import React from "react";
import "antd/dist/antd.css";
import { Layout } from "antd";
import { ConferenceDetail } from "./ConferenceDetail";
import { EventTable } from "./EventTable";
import { EventDetail } from "./EventDetail";
import { fetchResourceFromURL } from "../alpinebits";

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedEvent: {},
    };
  }

  async retrieveSelectedVenue(event) {
    // Skips so the callback only triggers this for valid events
    if (!event || !event.relationships || !event.relationships.venues) {
      return;
    }

    // Navigation with hypermedia allows us to follow the links
    // to retrieve more data, like in here where we get the
    // venues of an event
    let venuesRelationship = event.relationships.venues;
    let response = await fetchResourceFromURL(venuesRelationship.links.related);
    let venues = response.data;

    // Due to the simple map used, we are only interested on
    // the first available venue
    return venues && venues[0];
  }

  // This callback is called when an event is selected for
  // more information
  async setSelectedEvent(event) {
    let venue = await this.retrieveSelectedVenue(event);
    this.setState({
      selectedEvent: event,
      selectedEventVenue: venue,
    });
  }

  render() {
    let { selectedEvent, selectedEventVenue } = this.state;
    return (
      <Layout>
        <Layout.Content style={{ padding: "0 30px" }}>
          <ConferenceDetail />
        </Layout.Content>
        <Layout.Sider width="400" theme="light">
          <EventTable
            fetchEventHandler={(event) => this.setSelectedEvent(event)}
          />
          <br />
          <EventDetail event={selectedEvent} venue={selectedEventVenue} />
        </Layout.Sider>
      </Layout>
    );
  }
}
