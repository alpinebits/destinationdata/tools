import React from "react";
import { Component } from "react";
import {
  fetchResource,
  getValueInLanguage,
  getDatesString,
} from "../alpinebits";
import { Table, Button, Layout } from "antd";

const tableColumns = [
  {
    title: "Name",
    dataIndex: ["attributes", "name", "eng"],
    width: 240,
    fixed: "left",
    render: (_text, record) =>
      getValueInLanguage(record.attributes.name, ["eng", "ita", "deu"]),
  },
  {
    title: "Dates",
    dataIndex: ["attributes", "startDate"],
    width: 130,
    render: (_text, record) => getDatesString(record).replace(" - ", "\n"),
  },
];

export class EventTable extends Component {
  constructor(props) {
    super(props);
    this.currentPage = 1;

    this.state = {
      data: [],
    };

    if (props.fetchEventHandler) {
      tableColumns[0].render = (_text, record) => (
        <a
          role="button"
          tabIndex="0"
          onClick={() => props.fetchEventHandler(record)}
        >
          {getValueInLanguage(record.attributes.name, ["eng", "ita", "deu"])}
        </a>
      );
    }
  }

  async componentDidMount() {
    this.fetchDataAndUpdate();
  }

  async fetchDataAndUpdate() {
    let newEventsFound = false;

    // Fetch make requests for new pages until desired events are found
    while (!newEventsFound) {
      let request = {
        resourceType: "events",
        pageSize: 4,
        pageNumber: this.currentPage++,
        sortBy: 'startDate',
        filter: [
          {
            name: 'endDate',
            operand: 'gt',
            value: '2021-07-15'
          },
          {
            name: 'endDate',
            operand: 'lt',
            value: '2022-12-31'
          },
          {
            name: 'categories',
            operand: 'any',
            value: 'schema:DanceEvent,schema:ExhibitionEvent,schema:Festival,schema:FoodEvent,schema:MusicEvent,schema:SportsEvent,schema:TheaterEvent,schema:VisualArtsEvent'
          },
        ]
      };
      let response = await fetchResource(request);

      // Stops if now valid response is received
      if (!response || !response.data) {
        return;
      }

      // In the response, we filter out past events
      let events = response.data.filter((event) => {
        const now = new Date();
        const startDate = new Date(event.attributes.startDate || new Date(0));
        const endDate = new Date(event.attributes.endDate || new Date(0));

        return startDate > now || endDate > now;
      });

      // We update newEventsFound if the filtered response is
      // not empty so we can exit the loop
      if ((newEventsFound = events.length > 0)) {
        // The table component requires the presence of a "key" field
        events = events.map((event) => {
          event.key = event.id;
          return event;
        });

        events = [...this.state.data, ...events];

        // Since the response is not sorted by date, we must
        // sort as more events are loaded
        // events.sort((eventA, eventB) => {
        //   let eventADate =
        //     eventA.attributes.startDate || eventA.attributes.endDate;
        //   let eventBDate =
        //     eventB.attributes.startDate || eventB.attributes.endDate;

        //   eventADate = new Date(eventADate);
        //   eventBDate = new Date(eventBDate);

        //   return eventADate >= eventBDate ? 1 : -1;
        // });

        this.setState({ data: events });
      }
    }
  }

  render() {
    return (
      <>
        <Layout.Content style={{ padding: "5px", textAlign: "center" }}>
          <Table
            title={() => (
              <b>
                <h1>Upcoming events in South Tyrol</h1>
              </b>
            )}
            columns={tableColumns}
            dataSource={this.state.data}
            pagination={{
              hideOnSinglePage: true,
              pageSize: this.state.data.length,
            }}
            scroll={{ y: 300 }}
          />
          <br />
          <Button onClick={async () => this.fetchDataAndUpdate()}>
            Load More
          </Button>
        </Layout.Content>
      </>
    );
  }
}
