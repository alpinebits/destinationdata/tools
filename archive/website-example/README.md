# Web Client Example

This project is a JavaScript React single page application that enriches a website with a list of future events and details on each event and its location.

This project was developed using the following tools, nonetheless it may run on older versions as well:

* **npm** - version 6.14.5
* **node** - version 12.18.2
* Any modern browser

In order to run it, start by download the project dependencies to your project by running:

```shell
npm install
```

Then, run the command below to start a web server.
The webpage will be available at <http://localhost:3000/>.

```shell
npm start
```

To read the code, start on `./src/components/App.js` and navigate through the components.
Fetch function responsible for retrieving data from OpenDataHub's DestinationData [endpoint](https://destinationdata.alpinebits.opendatahub.bz.it/2021-04) are defined on `./src/alpinebits.js`, as well as a couple of auxiliary method for handling data.
