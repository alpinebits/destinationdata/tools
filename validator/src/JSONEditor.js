import React from 'react';

import Box from '@material-ui/core/Box';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/styles';

import AceEditor from 'react-ace';
import 'brace/mode/json';
import 'brace/theme/github';

class JSONEditor extends React.Component {

  render = () => {
    const { classes, value, onChange, onValidate, onButtonClick } = this.props;
    return (
      <Box border={1} className={classes.box}>
        <AceEditor
            mode="json"
            theme="github"
            value={value}
            onChange={onChange}
            onValidate={onValidate}
            width="100%"
            height="100%"
            tabSize={3}
            wrapEnabled={true}
            editorProps={{
                $blockScrolling: true
            }}
            setOptions={{
              fontSize: "10pt",
              fontFamily: '"Fira code", "Fira Mono", monospace'
            }}
        />
        <Tooltip title="Format JSON code">
          <IconButton
            className={classes.iconFloat}
            onClick={onButtonClick}
          >
            <Icon>format_indent_increase</Icon>
          </IconButton>
        </Tooltip>
      </Box>
    );
  }
}

const styles = theme => ({
  box: {
    width: "100%",
    minWidth: 350,
    minHeight: 350,
    height: 350,
    borderColor: "rgba(0, 0, 0, 0.42)",
    marginTop: "8px",
    position: "relative"
  },
  iconFloat: {
    position: "absolute",
    bottom: 12,
    right: 20,
    zIndex: 2,
    '&:hover': {
      color: '#F39101',
    }
  }
});

export default withStyles(styles)(JSONEditor);
