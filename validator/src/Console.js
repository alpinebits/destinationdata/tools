import React from 'react';

import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/styles';

class Console extends React.Component {

  renderMessageBlock = (message, idx) => {
    const { classes } = this.props;

    let messageRightClass = classes.messageRight + ' ' + (!this.props.hasError ? classes.colorPrimary : classes.colorError);

    const messageLine = (
      <Box key={1} className={classes.messageLine}>
        <Typography className={classes.messageLeft}>Message:</Typography>
        <Typography className={classes.bold+' '+messageRightClass}>{message.text}</Typography>
      </Box>
    );

    let pathLine = null;
    if(message.path)
      pathLine = (
        <Box key={2} className={classes.messageLine}>
          <Typography className={classes.messageLeft} >Schema path:</Typography>
          <Typography className={messageRightClass}>{message.path}</Typography>
        </Box>
      );

    return (
      <Box key={idx} className={classes.messageBlock}>
        {[messageLine,pathLine]}
      </Box>
    );
  }

  renderMessages = () => {
    const { isLoading, classes, messages } = this.props;

    if(isLoading || !messages || !Array.isArray(messages) || messages.length===0)
      return;

    const messageBlocks = messages.map( (message,idx) => {
      return this.renderMessageBlock(message,idx);
    });

    return(
      <Box className={classes.consoleMessages}>
        {messageBlocks}
      </Box>
    )
  }

  render = () => {
    const { classes, messages } = this.props;

    const consoleType = this.props.errorInSchema ? 'JSON Schema' : 'JSON'

    let iconComponent = null;
    let titleText = '';
    let titleClass = classes.consoleTitleBox;


    if(this.props.isLoading){
      iconComponent = <CircularProgress size={20} className={classes.progress} />
      titleText = 'Parsing JSON...';
    }
    else if(this.props.hasError){
      iconComponent = <Icon>clear</Icon>
      titleText = 'Found '+messages.length+' error(s) in the '+consoleType;
      titleClass += ' '+classes.colorError;
    }
    else {
      iconComponent = <Icon>done</Icon>
      titleText = consoleType + ' complies with the schema.';
      titleClass += ' '+classes.colorPrimary;
    }

    return (
      <Box className={classes.consoleBox}>
        <Box className={titleClass}>
          {iconComponent}
          <Typography component='span' className={classes.consoleTitle}>
            {titleText}
          </Typography>
        </Box>
        {this.renderMessages()}
      </Box>
    );
  }
}

const styles = theme => ({
  consoleBox: {
    marginTop: 24,
    border: '1px solid rgba(0, 0, 0, 0.42)',
    width: 'calc(100% - 32px)',
    padding: 16
  },
  consoleTitleBox: {
    display: 'flex',
    alignItems: 'center'
  },
  consoleTitle: {
    fontSize: '20px',
    paddingLeft: 8
  },
  colorPrimary: {
    color: 'green'
  },
  colorError: {
    color: 'red'
  },
  progress: {
    color: 'black',
    marginRight: 4
  },
  consoleMessages: {
    paddingTop: 8,
    display: 'flex',
    flexDirection: 'column'
  },
  messageBlock: {
    paddingTop: 16
  },
  messageLine: {
    display: 'flex'
  },
  messageLeft: {
    width: 108
  },
  messageRight: {
    width: 'calc(100% - 108px)'
  },
  bold: {
    fontWeight: 700
  }
});

export default withStyles(styles)(Console);
