module.exports = [
  {
    "schema": "Datatypes",
    "file": "schemas/datatypes.schema.json",
    "examples": [
      "examples/datatype.address.json",
      "examples/datatype.contactpoint.json",
      "examples/datatype.geometry.json",
      "examples/datatype.hoursspecification.json",
      "examples/datatype.snowcondition.json",
      "examples/datatype.text.json",
    ]
  },
  {
    "schema": "Agent Resource",
    "file": "schemas/agent.schema.json",
    "examples": [
      "examples/agent.min.json",
      "examples/agent.full.json"
    ]
  },
  {
    "schema": "Category Resource",
    "file": "schemas/category.schema.json",
    "examples": [
      "examples/category.min.json",
      "examples/category.full.json"
    ]
  },
  {
    "schema": "Event Resource",
    "file": "schemas/event.schema.json",
    "examples": [
      "examples/event.min.json",
      "examples/event.full.json"
    ]
  },
  {
    "schema": "Event Series Resource",
    "file": "schemas/eventseries.schema.json",
    "examples": [
      "examples/eventseries.min.json",
      "examples/eventseries.full.json"
    ]
  },
  {
    "schema": "Feature Resource",
    "file": "schemas/feature.schema.json",
    "examples": [
      "examples/feature.min.json",
      "examples/feature.full.json"
    ]
  },
  {
    "schema": "Lift Resource",
    "file": "schemas/lift.schema.json",
    "examples": [
      "examples/lift.min.json",
      "examples/lift.full.json"
    ]
  },
  {
    "schema": "Media Object Resource",
    "file": "schemas/mediaobject.schema.json",
    "examples": [
      "examples/mediaobject.min.json",
      "examples/mediaobject.full.json"
    ]
  },
  {
    "schema": "Mountain Area Resource",
    "file": "schemas/mountainarea.schema.json",
    "examples": [
      "examples/mountainarea.min.json",
      "examples/mountainarea.full.json"
    ]
  },
  {
    "schema": "Ski Slope Resource",
    "file": "schemas/skislope.schema.json",
    "examples": [
      "examples/skislope.min.json",
      "examples/skislope.full.json"
    ]
  },
  {
    "schema": "Snowpark Resource",
    "file": "schemas/snowpark.schema.json",
    "examples": [
      "examples/snowpark.min.json",
      "examples/snowpark.full.json"
    ]
  },
  {
    "schema": "Venue Resource",
    "file": "schemas/venue.schema.json",
    "examples": [
      "examples/venue.min.json",
      "examples/venue.full.json"
    ]
  },
  {
    "schema": "Base Endpoint - GET",
    "file": "schemas/base.get.schema.json",
    "examples": [
      "examples/base.endpoint.msg.json"
    ]
  },
  {
    "schema": "/2022-04/agents - GET",
    "file": "schemas/agents.get.schema.json",
    "examples": [
      "examples/agents.get.json"
    ]
  },
  {
    "schema": "/2022-04/agents - POST",
    "file": "schemas/agents.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/agents/:id - GET",
    "file": "schemas/agents.id.get.schema.json",
    "examples": [
      "examples/agents.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/agents/:id - PATCH",
    "file": "schemas/agents.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/categories - GET",
    "file": "schemas/categories.get.schema.json",
    "examples": [
      "examples/categories.get.json"
    ]
  },
  {
    "schema": "/2022-04/categories - POST",
    "file": "schemas/categories.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/categories/:id - GET",
    "file": "schemas/categories.id.get.schema.json",
    "examples": [
      "examples/categories.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/categories/:id - PATCH",
    "file": "schemas/categories.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/events - GET",
    "file": "schemas/events.get.schema.json",
    "examples": [
      "examples/events.get.json"
    ]
  },
  {
    "schema": "/2022-04/events - POST",
    "file": "schemas/events.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/events/:id - GET",
    "file": "schemas/events.id.get.schema.json",
    "examples": [
      "examples/events.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/events/:id - PATCH",
    "file": "schemas/events.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/eventseries - GET",
    "file": "schemas/eventseries.get.schema.json",
    "examples": [
      "examples/eventseries.get.json"
    ]
  },
  {
    "schema": "/2022-04/eventseries - POST",
    "file": "schemas/eventseries.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/eventseries/:id - GET",
    "file": "schemas/eventseries.id.get.schema.json",
    "examples": [
      "examples/eventseries.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/eventseries/:id - PATCH",
    "file": "schemas/eventseries.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/features - GET",
    "file": "schemas/features.get.schema.json",
    "examples": [
      "examples/features.get.json"
    ]
  },
  {
    "schema": "/2022-04/features - POST",
    "file": "schemas/features.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/features/:id - GET",
    "file": "schemas/features.id.get.schema.json",
    "examples": [
      "examples/features.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/features/:id - PATCH",
    "file": "schemas/features.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/lifts - GET",
    "file": "schemas/lifts.get.schema.json",
    "examples": [
      "examples/lifts.get.json"
    ]
  },
  {
    "schema": "/2022-04/lifts - POST",
    "file": "schemas/lifts.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/lifts/:id - GET",
    "file": "schemas/lifts.id.get.schema.json",
    "examples": [
      "examples/lifts.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/lifts/:id - PATCH",
    "file": "schemas/lifts.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/mediaObjects - GET",
    "file": "schemas/mediaobjects.get.schema.json",
    "examples": [
      "examples/mediaobjects.get.json"
    ]
  },
  {
    "schema": "/2022-04/mediaObjects - POST",
    "file": "schemas/mediaobjects.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/mediaObjects/:id - GET",
    "file": "schemas/mediaobjects.id.get.schema.json",
    "examples": [
      "examples/mediaobjects.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/mediaObjects/:id - PATCH",
    "file": "schemas/mediaobjects.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/mountainAreas - GET",
    "file": "schemas/mountainareas.get.schema.json",
    "examples": [
      "examples/mountainareas.get.json"
    ]
  },
  {
    "schema": "/2022-04/mountainAreas - POST",
    "file": "schemas/mountainareas.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/mountainAreas/:id - GET",
    "file": "schemas/mountainareas.id.get.schema.json",
    "examples": [
      "examples/mountainareas.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/mountainAreas/:id - PATCH",
    "file": "schemas/mountainareas.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/skiSlopes - GET",
    "file": "schemas/skislopes.get.schema.json",
    "examples": [
      "examples/skislopes.get.json"
    ]
  },
  {
    "schema": "/2022-04/skiSlopes - POST",
    "file": "schemas/skislopes.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/skiSlopes/:id - GET",
    "file": "schemas/skislopes.id.get.schema.json",
    "examples": [
      "examples/skislopes.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/skiSlopes/:id - PATCH",
    "file": "schemas/skislopes.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/snowparks - GET",
    "file": "schemas/snowparks.get.schema.json",
    "examples": [
      "examples/snowparks.get.json"
    ]
  },
  {
    "schema": "/2022-04/snowparks - POST",
    "file": "schemas/snowparks.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/snowparks/:id - GET",
    "file": "schemas/snowparks.id.get.schema.json",
    "examples": [
      "examples/snowparks.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/snowparks/:id - PATCH",
    "file": "schemas/snowparks.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/venues - GET",
    "file": "schemas/venues.get.schema.json",
    "examples": [
      "examples/venues.get.json"
    ]
  },
  {
    "schema": "/2022-04/venues - POST",
    "file": "schemas/venues.post.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },
  {
    "schema": "/2022-04/venues/:id - GET",
    "file": "schemas/venues.id.get.schema.json",
    "examples": [
      "examples/venues.id.get.json"
    ]
  },
  {
    "schema": "/2022-04/venues/:id - PATCH",
    "file": "schemas/venues.id.patch.schema.json",
    "examples": [
      "examples/_empty.json"
    ]
  },

  // {
  //   "schema": "/agents",
  //   "file": "schemas/agents.schema.json",
  //   "examples": [
  //     "examples/agents.min.json",
  //     "examples/agents.full.json"
  //   ]
  // },
  // {
  //   "schema": "/agents/:id",
  //   "file": "schemas/agents.id.schema.json",
  //   "examples": [
  //     "examples/agents.id.min.json",
  //     "examples/agents.id.full.json"
  //   ]
  // },
  // {
  //   "schema": "/events",
  //   "file": "schemas/events.schema.json",
  //   "examples": [
  //     "examples/events.min.json",
  //     "examples/events.full.json"
  //   ]
  // },
  // {
  //   "schema": "/events/:id",
  //   "file": "schemas/events.id.schema.json",
  //   "examples": [
  //     "examples/events.id.min.json",
  //     "examples/events.id.full.json"
  //   ]
  // },
  // {
  //   "schema": "/eventSeries",
  //   "file": "schemas/eventsseries.schema.json",
  //   "examples": [
  //     "examples/eventseries.min.json",
  //     "examples/eventseries.full.json"
  //   ]
  // },
  // {
  //   "schema": "/eventSeries/:id",
  //   "file": "schemas/eventseries.id.schema.json",
  //   "examples": [
  //     "examples/eventseries.id.min.json",
  //     "examples/eventseries.id.full.json"
  //   ]
  // },
  // {
  //   "schema": "/lifts",
  //   "file": "schemas/lifts.schema.json",
  //   "examples": [
  //     "examples/lifts.min.json",
  //     "examples/lifts.full.json"
  //   ]
  // },
  // {
  //   "schema": "/lifts/:id",
  //   "file": "schemas/lifts.id.schema.json",
  //   "examples": [
  //     "examples/lifts.id.min.json",
  //     "examples/lifts.id.full.json"
  //   ]
  // },
  // {
  //   "schema": "/mediaObjects",
  //   "file": "schemas/mediaobjects.schema.json",
  //   "examples": [
  //     "examples/mediaobjects.min.json",
  //     "examples/mediaobjects.full.json"
  //   ]
  // },
  // {
  //   "schema": "/mediaObjects/:id",
  //   "file": "schemas/mediaobjects.id.schema.json",
  //   "examples": [
  //     "examples/mediaobjects.id.min.json",
  //     "examples/mediaobjects.id.full.json"
  //   ]
  // },
  // {
  //   "schema": "/mountainAreas",
  //   "file": "schemas/mountainareas.schema.json",
  //   "examples": [
  //     "examples/mountainareas.min.json",
  //     "examples/mountainareas.full.json"
  //   ]
  // },
  // {
  //   "schema": "/mountainAreas/:id",
  //   "file": "schemas/mountainareas.id.schema.json",
  //   "examples": [
  //     "examples/mountainareas.id.min.json",
  //     "examples/mountainareas.id.full.json"
  //   ]
  // },
  // {
  //   "schema": "/snowparks",
  //   "file": "schemas/snowparks.schema.json",
  //   "examples": [
  //     "examples/snowparks.min.json",
  //     "examples/snowparks.full.json"
  //   ]
  // },
  // {
  //   "schema": "/snowparks/:id",
  //   "file": "schemas/snowparks.id.schema.json",
  //   "examples": [
  //     "examples/snowparks.id.min.json",
  //     "examples/snowparks.id.full.json"
  //   ]
  // },
  // {
  //   "schema": "/trails",
  //   "file": "schemas/trails.schema.json",
  //   "examples": [
  //     "examples/trails.min.json",
  //     "examples/trails.full.json"
  //   ]
  // },
  // {
  //   "schema": "/trails/:id",
  //   "file": "schemas/trails.id.schema.json",
  //   "examples": [
  //     "examples/trails.id.min.json",
  //     "examples/trails.id.full.json"
  //   ]
  // },
  // {
  //   "schema": "/venues",
  //   "file": "schemas/venues.schema.json",
  //   "examples": [
  //     "examples/venues.min.json",
  //     "examples/venues.full.json"
  //   ]
  // },
  // {
  //   "schema": "/venues/:id",
  //   "file": "schemas/venues.id.schema.json",
  //   "examples": [
  //     "examples/venues.id.min.json",
  //     "examples/venues.id.full.json"
  //   ]
  // },
  
];
