# AlpineBits DestinationData: Examples

This repository contains a list of JSON files containing [JSON Schemas](https://json-schema.org/) for each endpoint on an AlpineBits Destination data server. These schemas are used in the [validator](./../validator) for testing of DestinationData messages and resources.

| File name | Use | Schema ID |
| --------- | --------- | --------- |
| base.get.schema.json | Validation of GET messages in the endpoint `/2022-04/agents` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/base/get` |
| agents.get.schema.json | Validation of GET messages in the endpoint `/2022-04/agents` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/agents/get` |
| agents.post.schema.json | Validation of POST messages in the endpoint `/2022-04/agents` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/agents/post` |
| agents.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/agents/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/agents/:id/get` |
| agents.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/agents/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/agents/:id/patch` |
| categories.get.schema.json | Validation of GET messages in the endpoint `/2022-04/categories` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/categories/get` |
| categories.post.schema.json | Validation of POST messages in the endpoint `/2022-04/categories` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/categories/post` |
| categories.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/categories/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/categories/:id/get` |
| categories.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/categories/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/categories/:id/patch` |
| events.get.schema.json | Validation of GET messages in the endpoint `/2022-04/events` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/events/get` |
| events.post.schema.json | Validation of POST messages in the endpoint `/2022-04/events` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/events/post` |
| events.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/events/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/events/:id/get` |
| events.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/events/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/events/:id/patch` |
| eventseries.get.schema.json | Validation of GET messages in the endpoint `/2022-04/eventSeries` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/eventSeries/get` |
| eventseries.post.schema.json | Validation of POST messages in the endpoint `/2022-04/eventSeries` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/eventSeries/post` |
| eventseries.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/eventSeries/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/eventSeries/:id/get` |
| eventseries.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/eventSeries/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/eventSeries/:id/patch` |
| features.get.schema.json | Validation of GET messages in the endpoint `/2022-04/features` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/features/get` |
| features.post.schema.json | Validation of POST messages in the endpoint `/2022-04/features` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/features/post` |
| features.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/features/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/features/:id/get` |
| features.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/features/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/features/:id/patch` |
| lifts.get.schema.json | Validation of GET messages in the endpoint `/2022-04/lifts` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/lifts/get` |
| lifts.post.schema.json | Validation of POST messages in the endpoint `/2022-04/lifts` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/lifts/post` |
| lifts.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/lifts/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/lifts/:id/get` |
| lifts.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/lifts/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/lifts/:id/patch` |
| mediaobjects.get.schema.json | Validation of GET messages in the endpoint `/2022-04/mediaObjects` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mediaObjects/get` |
| mediaobjects.post.schema.json | Validation of POST messages in the endpoint `/2022-04/mediaObjects` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mediaObjects/post` |
| mediaobjects.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/mediaObjects/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mediaObjects/:id/get` |
| mediaobjects.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/mediaObjects/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mediaObjects/:id/patch` |
| mountainareas.get.schema.json | Validation of GET messages in the endpoint `/2022-04/mountainareas` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mountainareas/get` |
| mountainareas.post.schema.json | Validation of POST messages in the endpoint `/2022-04/mountainareas` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mountainareas/post` |
| mountainareas.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/mountainareas/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mountainareas/:id/get` |
| mountainareas.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/mountainareas/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mountainareas/:id/patch` |
| skislopes.get.schema.json | Validation of GET messages in the endpoint `/2022-04/skiSlopes` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/skiSlopes/get` |
| skislopes.post.schema.json | Validation of POST messages in the endpoint `/2022-04/skiSlopes` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/skiSlopes/post` |
| skislopes.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/skiSlopes/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/skiSlopes/:id/get` |
| skislopes.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/skiSlopes/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/skiSlopes/:id/patch` |
| snowparks.get.schema.json | Validation of GET messages in the endpoint `/2022-04/snowparks` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/snowparks/get` |
| snowparks.post.schema.json | Validation of POST messages in the endpoint `/2022-04/snowparks` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/snowparks/post` |
| snowparks.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/snowparks/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/snowparks/:id/get` |
| snowparks.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/snowparks/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/snowparks/:id/patch` |
| venues.get.schema.json | Validation of GET messages in the endpoint `/2022-04/venues` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/venues/get` |
| venues.post.schema.json | Validation of POST messages in the endpoint `/2022-04/venues` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/venues/post` |
| venues.id.get.schema.json | Validation of GET messages in the endpoint `/2022-04/venues/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/venues/:id/get` |
| venues.id.patch.schema.json | Validation of PATCH messages in the endpoint `/2022-04/venues/:id` | `https://www.alpinebits.org/destinationdata/schemas/2022-04/venues/:id/patch` |
| agent.schema.json | Validation of `agent` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/agent` |
| category.schema.json | Validation of `category` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/category` |
| event.schema.json | Validation of `event` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/event` |
| eventseries.schema.json | Validation of `eventseries` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/eventSeries` |
| feature.schema.json | Validation of `feature` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/feature` |
| lift.schema.json | Validation of `lift` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/lift` |
| mediaobject.schema.json | Validation of `mediaobject` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mediaObject` |
| mountainarea.schema.json | Validation of `mountainarea` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/mountainArea` |
| skislope.schema.json | Validation of `skislope` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/skiSlope` |
| snowpark.schema.json | Validation of `snowpark` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/snowpark` |
| venue.schema.json | Validation of `venue` resources  | `https://www.alpinebits.org/destinationdata/schemas/2022-04/venue` |
| datatypes.schema.json | Validation of datatypes and reused structured | `https://www.alpinebits.org/destinationdata/schemas/2022-04/datatypes` |