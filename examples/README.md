# AlpineBits DestinationData: Examples

This repository contains a list of JSON files that exemplify different messages that can be returned by a AlpineBits Destination data server. These examples are employed in the [validator](./../validator) project for testing with JSON Schemas.

- `base.endpoint.msg.json`: example message retrieved from the base endpoint of a DestinationData server

- `agents.get.json`: example message retrieved from the endpoint `\2022-04\agents\`
- `agents.id.get.json`: example message retrieved from the endpoint `\2022-04\agents\:id`
- `categories.get.json`: example message retrieved from the endpoint `\2022-04\categories\`
- `categories.id.get.json`: example message retrieved from the endpoint `\2022-04\categories\:id`
- `events.get.json`: example message retrieved from the endpoint `\2022-04\events\`
- `events.id.get.json`: example message retrieved from the endpoint `\2022-04\events\:id`
- `eventseries.get.json`: example message retrieved from the endpoint `\2022-04\eventSeries\`
- `eventseries.id.get.json`: example message retrieved from the endpoint `\2022-04\eventSeries\:id`
- `features.get.json`: example message retrieved from the endpoint `\2022-04\features\`
- `features.id.get.json`: example message retrieved from the endpoint `\2022-04\features\:id`
- `lifts.get.json`: example message retrieved from the endpoint `\2022-04\lifts\`
- `lifts.id.get.json`: example message retrieved from the endpoint `\2022-04\lifts\:id`
- `mediaobjects.get.json`: example message retrieved from the endpoint `\2022-04\mediaObjects\`
- `mediaobjects.id.get.json`: example message retrieved from the endpoint `\2022-04\mediaObjects\:id`
- `mountainareas.get.json`: example message retrieved from the endpoint `\2022-04\mountainAreas\`
- `mountainareas.id.get.json`: example message retrieved from the endpoint `\2022-04\mountainAreas\:id`
- `skislopes.get.json`: example message retrieved from the endpoint `\2022-04\skiSlopes\`
- `skislopes.id.get.json`: example message retrieved from the endpoint `\2022-04\skiSlopes\:id`
- `snowparks.get.json`: example message retrieved from the endpoint `\2022-04\snowparks\`
- `snowparks.id.get.json`: example message retrieved from the endpoint `\2022-04\snowparks\:id`
- `venues.get.json`: example message retrieved from the endpoint `\2022-04\venues\`
- `venues.id.get.json`: example message retrieved from the endpoint `\2022-04\venues\:id`

- `agent.full.json`: an example of a `agent` resource containing using the entire schema
- `agent.min.json`: an example of a `agent` resource containing the minimum information required in the standard
- `category.full.json`: an example of a `category` resource containing using the entire schema
- `category.min.json`: an example of a `category` resource containing the minimum information required in the standard
- `event.full.json`: an example of a `event` resource containing using the entire schema
- `event.min.json`: an example of a `event` resource containing the minimum information required in the standard
- `eventseries.full.json`: an example of a `eventseries` resource containing using the entire schema
- `eventseries.min.json`: an example of a `eventseries` resource containing the minimum information required in the standard
- `feature.full.json`: an example of a `feature` resource containing using the entire schema
- `feature.min.json`: an example of a `feature` resource containing the minimum information required in the standard
- `lift.full.json`: an example of a `lift` resource containing using the entire schema
- `lift.min.json`: an example of a `lift` resource containing the minimum information required in the standard
- `mediaobject.full.json`: an example of a `mediaobject` resource containing using the entire schema
- `mediaobject.min.json`: an example of a `mediaobject` resource containing the minimum information required in the standard
- `mountainarea.full.json`: an example of a `mountainarea` resource containing using the entire schema
- `mountainarea.min.json`: an example of a `mountainarea` resource containing the minimum information required in the standard
- `skislope.full.json`: an example of a `skislope` resource containing using the entire schema
- `skislope.min.json`: an example of a `skislope` resource containing the minimum information required in the standard
- `snowpark.full.json`: an example of a `snowpark` resource containing using the entire schema
- `snowpark.min.json`: an example of a `snowpark` resource containing the minimum information required in the standard
- `venue.full.json`: an example of a `venue` resource containing using the entire schema
- `venue.min.json`: an example of a `venue` resource containing the minimum information required in the standard

- `datatype.address.json`: example object representing a `address` information in the standard
- `datatype.contactpoint.json`: example object representing a `contactpoint` information in the standard
- `datatype.geometry.json`: example object representing a `geometry` information in the standard
- `datatype.hoursspecification.json`: example object representing a `hoursspecification` information in the standard
- `datatype.snowcondition.json`: example object representing a `snowcondition` information in the standard
- `datatype.text.json`: example object representing a `text` information in the standard
