--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1 (Debian 15.1-1.pgdg110+1)
-- Dumped by pg_dump version 15.1 (Homebrew)

-- Started on 2022-12-22 22:47:14 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 12 (class 2615 OID 19225)
-- Name: tiger; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tiger;


ALTER SCHEMA tiger OWNER TO postgres;

--
-- TOC entry 13 (class 2615 OID 19481)
-- Name: tiger_data; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tiger_data;


ALTER SCHEMA tiger_data OWNER TO postgres;

--
-- TOC entry 11 (class 2615 OID 19051)
-- Name: topology; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA topology;


ALTER SCHEMA topology OWNER TO postgres;

--
-- TOC entry 5197 (class 0 OID 0)
-- Dependencies: 11
-- Name: SCHEMA topology; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA topology IS 'PostGIS Topology schema';


--
-- TOC entry 4 (class 3079 OID 19214)
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- TOC entry 5198 (class 0 OID 0)
-- Dependencies: 4
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


--
-- TOC entry 2 (class 3079 OID 18005)
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- TOC entry 5199 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';


--
-- TOC entry 5 (class 3079 OID 19226)
-- Name: postgis_tiger_geocoder; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder WITH SCHEMA tiger;


--
-- TOC entry 5200 (class 0 OID 0)
-- Dependencies: 5
-- Name: EXTENSION postgis_tiger_geocoder; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_tiger_geocoder IS 'PostGIS tiger geocoder and reverse geocoder';


--
-- TOC entry 3 (class 3079 OID 19052)
-- Name: postgis_topology; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis_topology WITH SCHEMA topology;


--
-- TOC entry 5201 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION postgis_topology; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_topology IS 'PostGIS topology spatial types and functions';


--
-- TOC entry 6 (class 3079 OID 19660)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 5202 (class 0 OID 0)
-- Dependencies: 6
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- TOC entry 1275 (class 1255 OID 20457)
-- Name: delete_contact_points_address(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_contact_points_address() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
      DELETE FROM addresses WHERE OLD.address_id = addresses.id;
      RETURN OLD;
    END;
    $$;


ALTER FUNCTION public.delete_contact_points_address() OWNER TO postgres;

--
-- TOC entry 1276 (class 1255 OID 20459)
-- Name: delete_place_address(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_place_address() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
      DELETE FROM addresses WHERE OLD.address_id = addresses.id;
      RETURN OLD;
    END;
    $$;


ALTER FUNCTION public.delete_place_address() OWNER TO postgres;

--
-- TOC entry 1264 (class 1255 OID 19659)
-- Name: rowjsonb_to_geojson(jsonb, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rowjsonb_to_geojson(rowjsonb jsonb, geom_column text DEFAULT 'geom'::text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
DECLARE 
 json_props jsonb;
 json_geom jsonb;
 json_type jsonb;
BEGIN
 IF NOT rowjsonb ? geom_column THEN
   RAISE EXCEPTION 'geometry column ''%'' is missing', geom_column;
 END IF;
 json_geom := ST_AsGeoJSON((rowjsonb ->> geom_column)::geometry)::jsonb;
 json_geom := jsonb_build_object('geometry', json_geom);
 json_props := jsonb_build_object('properties', rowjsonb - geom_column);
 json_type := jsonb_build_object('type', 'Feature');
 return (json_type || json_geom || json_props)::text;
END; 
$$;


ALTER FUNCTION public.rowjsonb_to_geojson(rowjsonb jsonb, geom_column text) OWNER TO postgres;

--
-- TOC entry 1277 (class 1255 OID 20461)
-- Name: sync_last_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sync_last_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
      NEW.last_update := NOW();
      RETURN NEW;
    END;
    $$;


ALTER FUNCTION public.sync_last_update() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 297 (class 1259 OID 21574)
-- Name: abstracts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.abstracts (
    lang character varying(3) NOT NULL,
    resource_id character varying(100) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.abstracts OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 22116)
-- Name: abstract_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.abstract_objects AS
 SELECT abstracts.resource_id AS id,
    COALESCE(json_object_agg(DISTINCT abstracts.lang, abstracts.content) FILTER (WHERE (abstracts.lang IS NOT NULL))) AS abstract
   FROM public.abstracts
  GROUP BY abstracts.resource_id;


ALTER TABLE public.abstract_objects OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 21694)
-- Name: addresses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.addresses (
    id integer NOT NULL,
    country character varying(2) NOT NULL,
    zipcode character varying(20),
    type character varying(100)
);


ALTER TABLE public.addresses OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 21700)
-- Name: cities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cities (
    lang character varying(3) NOT NULL,
    address_id integer NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.cities OWNER TO postgres;

--
-- TOC entry 346 (class 1259 OID 22156)
-- Name: city_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.city_objects AS
 SELECT cities.address_id AS id,
    COALESCE(json_object_agg(DISTINCT cities.lang, cities.content) FILTER (WHERE (cities.lang IS NOT NULL))) AS city
   FROM public.cities
  GROUP BY cities.address_id;


ALTER TABLE public.city_objects OWNER TO postgres;

--
-- TOC entry 307 (class 1259 OID 21717)
-- Name: complements; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.complements (
    lang character varying(3) NOT NULL,
    address_id integer NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.complements OWNER TO postgres;

--
-- TOC entry 347 (class 1259 OID 22160)
-- Name: complement_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.complement_objects AS
 SELECT complements.address_id AS id,
    COALESCE(json_object_agg(DISTINCT complements.lang, complements.content) FILTER (WHERE (complements.lang IS NOT NULL))) AS complement
   FROM public.complements
  GROUP BY complements.address_id;


ALTER TABLE public.complement_objects OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 21734)
-- Name: regions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regions (
    lang character varying(3) NOT NULL,
    address_id integer NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.regions OWNER TO postgres;

--
-- TOC entry 348 (class 1259 OID 22164)
-- Name: region_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.region_objects AS
 SELECT regions.address_id AS id,
    COALESCE(json_object_agg(DISTINCT regions.lang, regions.content) FILTER (WHERE (regions.lang IS NOT NULL))) AS region
   FROM public.regions
  GROUP BY regions.address_id;


ALTER TABLE public.region_objects OWNER TO postgres;

--
-- TOC entry 309 (class 1259 OID 21751)
-- Name: streets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.streets (
    lang character varying(3) NOT NULL,
    address_id integer NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.streets OWNER TO postgres;

--
-- TOC entry 349 (class 1259 OID 22168)
-- Name: street_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.street_objects AS
 SELECT streets.address_id AS id,
    COALESCE(json_object_agg(DISTINCT streets.lang, streets.content) FILTER (WHERE (streets.lang IS NOT NULL))) AS street
   FROM public.streets
  GROUP BY streets.address_id;


ALTER TABLE public.street_objects OWNER TO postgres;

--
-- TOC entry 350 (class 1259 OID 22172)
-- Name: address_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.address_objects AS
 SELECT addresses.id,
    json_build_object('country', addresses.country, 'zipcode', addresses.zipcode, 'type', addresses.type, 'city', COALESCE(city_objects.city), 'complement', COALESCE(complement_objects.complement), 'region', COALESCE(region_objects.region), 'street', COALESCE(street_objects.street)) AS address
   FROM ((((public.addresses
     LEFT JOIN public.city_objects ON ((city_objects.id = addresses.id)))
     LEFT JOIN public.complement_objects ON ((complement_objects.id = addresses.id)))
     LEFT JOIN public.region_objects ON ((region_objects.id = addresses.id)))
     LEFT JOIN public.street_objects ON ((street_objects.id = addresses.id)));


ALTER TABLE public.address_objects OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 21693)
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.addresses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.addresses_id_seq OWNER TO postgres;

--
-- TOC entry 5203 (class 0 OID 0)
-- Dependencies: 304
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.addresses_id_seq OWNED BY public.addresses.id;


--
-- TOC entry 288 (class 1259 OID 21465)
-- Name: agents; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.agents (
    id character varying(100) NOT NULL
);


ALTER TABLE public.agents OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 21901)
-- Name: area_lifts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.area_lifts (
    area_id character varying(100) NOT NULL,
    lift_id character varying(100) NOT NULL
);


ALTER TABLE public.area_lifts OWNER TO postgres;

--
-- TOC entry 353 (class 1259 OID 22186)
-- Name: area_lifts_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.area_lifts_arrays AS
 SELECT area_lifts.area_id AS id,
    json_agg(json_build_object('id', area_lifts.lift_id, 'type', 'lifts')) AS lifts
   FROM public.area_lifts
  GROUP BY area_lifts.area_id;


ALTER TABLE public.area_lifts_arrays OWNER TO postgres;

--
-- TOC entry 322 (class 1259 OID 21916)
-- Name: area_ski_slopes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.area_ski_slopes (
    area_id character varying(100) NOT NULL,
    ski_slope_id character varying(100) NOT NULL
);


ALTER TABLE public.area_ski_slopes OWNER TO postgres;

--
-- TOC entry 354 (class 1259 OID 22190)
-- Name: area_ski_slopes_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.area_ski_slopes_arrays AS
 SELECT area_ski_slopes.area_id AS id,
    json_agg(json_build_object('id', area_ski_slopes.ski_slope_id, 'type', 'skiSlopes')) AS ski_slopes
   FROM public.area_ski_slopes
  GROUP BY area_ski_slopes.area_id;


ALTER TABLE public.area_ski_slopes_arrays OWNER TO postgres;

--
-- TOC entry 323 (class 1259 OID 21931)
-- Name: area_snowparks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.area_snowparks (
    area_id character varying(100) NOT NULL,
    snowpark_id character varying(100) NOT NULL
);


ALTER TABLE public.area_snowparks OWNER TO postgres;

--
-- TOC entry 355 (class 1259 OID 22194)
-- Name: area_snowparks_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.area_snowparks_arrays AS
 SELECT area_snowparks.area_id AS id,
    json_agg(json_build_object('id', area_snowparks.snowpark_id, 'type', 'snowparks')) AS snowparks
   FROM public.area_snowparks
  GROUP BY area_snowparks.area_id;


ALTER TABLE public.area_snowparks_arrays OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 21475)
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories (
    id character varying(100) NOT NULL,
    namespace character varying(100) NOT NULL
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- TOC entry 326 (class 1259 OID 21976)
-- Name: resource_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_categories (
    category_id character varying(100) NOT NULL,
    resource_id character varying(100) NOT NULL
);


ALTER TABLE public.resource_categories OWNER TO postgres;

--
-- TOC entry 343 (class 1259 OID 22144)
-- Name: categories_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.categories_arrays AS
 SELECT resource_categories.resource_id AS id,
    json_agg(json_build_object('id', resource_categories.category_id, 'type', 'categories')) AS categories
   FROM public.resource_categories
  GROUP BY resource_categories.resource_id;


ALTER TABLE public.categories_arrays OWNER TO postgres;

--
-- TOC entry 329 (class 1259 OID 22021)
-- Name: category_specializations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category_specializations (
    parent_id character varying(100) NOT NULL,
    child_id character varying(100) NOT NULL
);


ALTER TABLE public.category_specializations OWNER TO postgres;

--
-- TOC entry 359 (class 1259 OID 22210)
-- Name: category_children_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.category_children_arrays AS
 SELECT category_specializations.parent_id AS id,
    json_agg(json_build_object('id', category_specializations.child_id, 'type', 'categories')) AS children
   FROM public.category_specializations
  GROUP BY category_specializations.parent_id;


ALTER TABLE public.category_children_arrays OWNER TO postgres;

--
-- TOC entry 328 (class 1259 OID 22006)
-- Name: category_covered_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category_covered_types (
    category_id character varying(100) NOT NULL,
    type character varying(100) NOT NULL
);


ALTER TABLE public.category_covered_types OWNER TO postgres;

--
-- TOC entry 360 (class 1259 OID 22214)
-- Name: category_parents_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.category_parents_arrays AS
 SELECT category_specializations.child_id AS id,
    json_agg(json_build_object('id', category_specializations.parent_id, 'type', 'categories')) AS parents
   FROM public.category_specializations
  GROUP BY category_specializations.child_id;


ALTER TABLE public.category_parents_arrays OWNER TO postgres;

--
-- TOC entry 325 (class 1259 OID 21961)
-- Name: connections; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.connections (
    a_id character varying(100) NOT NULL,
    b_id character varying(100) NOT NULL
);


ALTER TABLE public.connections OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 21450)
-- Name: resources; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resources (
    id character varying(100) DEFAULT public.uuid_generate_v4() NOT NULL,
    type character varying(50) NOT NULL,
    odh_id character varying(100),
    data_provider text NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    last_update timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    simple_url text
);


ALTER TABLE public.resources OWNER TO postgres;

--
-- TOC entry 357 (class 1259 OID 22202)
-- Name: connections_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.connections_arrays AS
 SELECT connections.a_id AS id,
    json_agg(json_build_object('id', connections.b_id, 'type', resources.type)) AS connections
   FROM (public.connections
     LEFT JOIN public.resources ON (((resources.id)::text = (connections.b_id)::text)))
  GROUP BY connections.a_id;


ALTER TABLE public.connections_arrays OWNER TO postgres;

--
-- TOC entry 311 (class 1259 OID 21769)
-- Name: contact_points; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contact_points (
    id integer NOT NULL,
    agent_id character varying(100) NOT NULL,
    address_id integer,
    available_hours jsonb,
    email character varying(100),
    telephone character varying(100)
);


ALTER TABLE public.contact_points OWNER TO postgres;

--
-- TOC entry 310 (class 1259 OID 21768)
-- Name: contact_points_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.contact_points_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_points_id_seq OWNER TO postgres;

--
-- TOC entry 5204 (class 0 OID 0)
-- Dependencies: 310
-- Name: contact_points_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.contact_points_id_seq OWNED BY public.contact_points.id;


--
-- TOC entry 332 (class 1259 OID 22056)
-- Name: contributors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contributors (
    contributor_id character varying(100) NOT NULL,
    event_id character varying(100) NOT NULL
);


ALTER TABLE public.contributors OWNER TO postgres;

--
-- TOC entry 298 (class 1259 OID 21591)
-- Name: descriptions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.descriptions (
    lang character varying(3) NOT NULL,
    resource_id character varying(100) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.descriptions OWNER TO postgres;

--
-- TOC entry 337 (class 1259 OID 22120)
-- Name: description_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.description_objects AS
 SELECT descriptions.resource_id AS id,
    COALESCE(json_object_agg(DISTINCT descriptions.lang, descriptions.content) FILTER (WHERE (descriptions.lang IS NOT NULL))) AS description
   FROM public.descriptions
  GROUP BY descriptions.resource_id;


ALTER TABLE public.description_objects OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 21517)
-- Name: event_series; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.event_series (
    id character varying(100) NOT NULL,
    frequency character varying(50)
);


ALTER TABLE public.event_series OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 21532)
-- Name: event_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.event_status (
    status character varying(50) NOT NULL,
    title character varying(50)
);


ALTER TABLE public.event_status OWNER TO postgres;

--
-- TOC entry 316 (class 1259 OID 21841)
-- Name: event_venues; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.event_venues (
    venue_id character varying(100) NOT NULL,
    event_id character varying(100) NOT NULL
);


ALTER TABLE public.event_venues OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 21537)
-- Name: events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.events (
    id character varying(100) NOT NULL,
    end_date timestamp with time zone,
    start_date timestamp with time zone,
    parent_id character varying(100),
    publisher_id character varying(100) NOT NULL,
    series_id character varying(100),
    status character varying(50),
    in_person_capacity integer,
    online_capacity integer,
    simple_participation_url text,
    simple_registration_url text,
    recorded boolean
);


ALTER TABLE public.events OWNER TO postgres;

--
-- TOC entry 331 (class 1259 OID 22051)
-- Name: feature_specializations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.feature_specializations (
    parent_id character varying(100) NOT NULL,
    child_id character varying(100) NOT NULL
);


ALTER TABLE public.feature_specializations OWNER TO postgres;

--
-- TOC entry 361 (class 1259 OID 22218)
-- Name: feature_children_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.feature_children_arrays AS
 SELECT feature_specializations.parent_id AS id,
    json_agg(json_build_object('id', feature_specializations.child_id, 'type', 'features')) AS children
   FROM public.feature_specializations
  GROUP BY feature_specializations.parent_id;


ALTER TABLE public.feature_children_arrays OWNER TO postgres;

--
-- TOC entry 330 (class 1259 OID 22036)
-- Name: feature_covered_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.feature_covered_types (
    feature_id character varying(100) NOT NULL,
    type character varying(100) NOT NULL
);


ALTER TABLE public.feature_covered_types OWNER TO postgres;

--
-- TOC entry 362 (class 1259 OID 22222)
-- Name: feature_parents_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.feature_parents_arrays AS
 SELECT feature_specializations.child_id AS id,
    json_agg(json_build_object('id', feature_specializations.parent_id, 'type', 'features')) AS parents
   FROM public.feature_specializations
  GROUP BY feature_specializations.child_id;


ALTER TABLE public.feature_parents_arrays OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 21485)
-- Name: features; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.features (
    id character varying(100) NOT NULL,
    namespace character varying(100) NOT NULL
);


ALTER TABLE public.features OWNER TO postgres;

--
-- TOC entry 327 (class 1259 OID 21991)
-- Name: resource_features; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_features (
    feature_id character varying(100) NOT NULL,
    resource_id character varying(100) NOT NULL
);


ALTER TABLE public.resource_features OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 22148)
-- Name: features_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.features_arrays AS
 SELECT resource_features.resource_id AS id,
    json_agg(json_build_object('id', resource_features.feature_id, 'type', 'features')) AS features
   FROM public.resource_features
  GROUP BY resource_features.resource_id;


ALTER TABLE public.features_arrays OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 21804)
-- Name: how_to_arrive; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.how_to_arrive (
    lang character varying(3) NOT NULL,
    place_id character varying(100) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.how_to_arrive OWNER TO postgres;

--
-- TOC entry 352 (class 1259 OID 22182)
-- Name: how_to_arrive_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.how_to_arrive_objects AS
 SELECT how_to_arrive.place_id AS id,
    COALESCE(json_object_agg(DISTINCT how_to_arrive.lang, how_to_arrive.content) FILTER (WHERE (how_to_arrive.lang IS NOT NULL))) AS how_to_arrive
   FROM public.how_to_arrive
  GROUP BY how_to_arrive.place_id;


ALTER TABLE public.how_to_arrive_objects OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 21569)
-- Name: language_codes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.language_codes (
    lang character varying(3) NOT NULL,
    title character varying(100)
);


ALTER TABLE public.language_codes OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 21856)
-- Name: lifts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lifts (
    id character varying(100) NOT NULL,
    capacity integer,
    persons_per_chair integer
);


ALTER TABLE public.lifts OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 21495)
-- Name: media_objects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.media_objects (
    id character varying(100) NOT NULL,
    license_holder_id character varying(100),
    author character varying(255),
    content_type character varying(255),
    duration integer,
    height integer,
    license character varying(100),
    width integer
);


ALTER TABLE public.media_objects OWNER TO postgres;

--
-- TOC entry 318 (class 1259 OID 21866)
-- Name: mountain_areas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mountain_areas (
    id character varying(100) NOT NULL,
    area_owner_id character varying(100),
    area integer,
    total_park_area integer,
    total_slope_length integer
);


ALTER TABLE public.mountain_areas OWNER TO postgres;

--
-- TOC entry 335 (class 1259 OID 22101)
-- Name: multimedia_descriptions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.multimedia_descriptions (
    resource_id character varying(100) NOT NULL,
    media_object_id character varying(100) NOT NULL
);


ALTER TABLE public.multimedia_descriptions OWNER TO postgres;

--
-- TOC entry 345 (class 1259 OID 22152)
-- Name: multimedia_descriptions_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.multimedia_descriptions_arrays AS
 SELECT multimedia_descriptions.resource_id AS id,
    json_agg(json_build_object('id', multimedia_descriptions.media_object_id, 'type', 'mediaObjects')) AS media
   FROM public.multimedia_descriptions
  GROUP BY multimedia_descriptions.resource_id;


ALTER TABLE public.multimedia_descriptions_arrays OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 19651)
-- Name: mytable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mytable (
    pk integer NOT NULL,
    name text,
    size double precision,
    geom public.geometry
);


ALTER TABLE public.mytable OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 19650)
-- Name: mytable_pk_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.mytable_pk_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mytable_pk_seq OWNER TO postgres;

--
-- TOC entry 5205 (class 0 OID 0)
-- Dependencies: 284
-- Name: mytable_pk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.mytable_pk_seq OWNED BY public.mytable.pk;


--
-- TOC entry 299 (class 1259 OID 21608)
-- Name: names; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.names (
    lang character varying(3) NOT NULL,
    resource_id character varying(100) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.names OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 22124)
-- Name: name_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.name_objects AS
 SELECT names.resource_id AS id,
    COALESCE(json_object_agg(DISTINCT names.lang, names.content) FILTER (WHERE (names.lang IS NOT NULL))) AS name
   FROM public.names
  GROUP BY names.resource_id;


ALTER TABLE public.name_objects OWNER TO postgres;

--
-- TOC entry 333 (class 1259 OID 22071)
-- Name: organizers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.organizers (
    organizer_id character varying(100) NOT NULL,
    event_id character varying(100) NOT NULL
);


ALTER TABLE public.organizers OWNER TO postgres;

--
-- TOC entry 302 (class 1259 OID 21659)
-- Name: participation_urls; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.participation_urls (
    lang character varying(3) NOT NULL,
    event_id character varying(100) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.participation_urls OWNER TO postgres;

--
-- TOC entry 341 (class 1259 OID 22136)
-- Name: participation_url_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.participation_url_objects AS
 SELECT participation_urls.event_id AS id,
    COALESCE(json_object_agg(DISTINCT participation_urls.lang, participation_urls.content) FILTER (WHERE (participation_urls.lang IS NOT NULL))) AS participation_url
   FROM public.participation_urls
  GROUP BY participation_urls.event_id;


ALTER TABLE public.participation_url_objects OWNER TO postgres;

--
-- TOC entry 312 (class 1259 OID 21787)
-- Name: places; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.places (
    id character varying(100) NOT NULL,
    address_id integer,
    geometries jsonb,
    length integer,
    max_altitude integer,
    min_altitude integer,
    opening_hours jsonb
);


ALTER TABLE public.places OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 21821)
-- Name: snow_conditions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.snow_conditions (
    id character varying(100) NOT NULL,
    base_snow integer NOT NULL,
    base_snow_range_lower integer,
    base_snow_range_upper integer,
    groomed boolean,
    latest_storm integer,
    obtained_in timestamp with time zone,
    primary_surface character varying(50) NOT NULL,
    secondary_surface character varying(50),
    snow_making boolean,
    snow_over_night integer
);


ALTER TABLE public.snow_conditions OWNER TO postgres;

--
-- TOC entry 358 (class 1259 OID 22206)
-- Name: snow_condition_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.snow_condition_objects AS
 SELECT snow_conditions.id,
    json_build_object('baseSnow', snow_conditions.base_snow, 'baseSnowRange', json_build_object('lower', snow_conditions.base_snow_range_lower, 'upper', snow_conditions.base_snow_range_upper), 'groomed', snow_conditions.groomed, 'latestStorm', snow_conditions.latest_storm, 'obtainedIn', snow_conditions.obtained_in, 'primarySurface', snow_conditions.primary_surface, 'secondarySurface', snow_conditions.secondary_surface, 'snowMaking', snow_conditions.snow_making, 'snowOverNight', snow_conditions.snow_over_night) AS snow_condition
   FROM public.snow_conditions;


ALTER TABLE public.snow_condition_objects OWNER TO postgres;

--
-- TOC entry 363 (class 1259 OID 22226)
-- Name: place_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.place_objects AS
 SELECT places.id,
    places.geometries,
    places.length,
    places.max_altitude,
    places.min_altitude,
    places.opening_hours,
    address_objects.address,
    how_to_arrive_objects.how_to_arrive,
    snow_condition_objects.snow_condition,
        CASE
            WHEN (places.geometries IS NOT NULL) THEN (public.st_geomfromgeojson((places.geometries -> 0)))::public.geography
            ELSE NULL::public.geography
        END AS postgis_geography
   FROM (((public.places
     LEFT JOIN public.snow_condition_objects ON (((snow_condition_objects.id)::text = (places.id)::text)))
     LEFT JOIN public.address_objects ON ((address_objects.id = places.address_id)))
     LEFT JOIN public.how_to_arrive_objects ON (((how_to_arrive_objects.id)::text = (places.id)::text)));


ALTER TABLE public.place_objects OWNER TO postgres;

--
-- TOC entry 303 (class 1259 OID 21676)
-- Name: registration_urls; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registration_urls (
    lang character varying(3) NOT NULL,
    event_id character varying(100) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.registration_urls OWNER TO postgres;

--
-- TOC entry 342 (class 1259 OID 22140)
-- Name: registration_url_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.registration_url_objects AS
 SELECT registration_urls.event_id AS id,
    COALESCE(json_object_agg(DISTINCT registration_urls.lang, registration_urls.content) FILTER (WHERE (registration_urls.lang IS NOT NULL))) AS registration_url
   FROM public.registration_urls
  GROUP BY registration_urls.event_id;


ALTER TABLE public.registration_url_objects OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 21625)
-- Name: short_names; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.short_names (
    lang character varying(3) NOT NULL,
    resource_id character varying(100) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.short_names OWNER TO postgres;

--
-- TOC entry 339 (class 1259 OID 22128)
-- Name: short_name_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.short_name_objects AS
 SELECT short_names.resource_id AS id,
    COALESCE(json_object_agg(DISTINCT short_names.lang, short_names.content) FILTER (WHERE (short_names.lang IS NOT NULL))) AS short_name
   FROM public.short_names
  GROUP BY short_names.resource_id;


ALTER TABLE public.short_name_objects OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 21642)
-- Name: urls; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.urls (
    lang character varying(3) NOT NULL,
    resource_id character varying(100) NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.urls OWNER TO postgres;

--
-- TOC entry 340 (class 1259 OID 22132)
-- Name: url_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.url_objects AS
 SELECT urls.resource_id AS id,
    COALESCE(json_object_agg(DISTINCT urls.lang, urls.content) FILTER (WHERE (urls.lang IS NOT NULL))) AS url
   FROM public.urls
  GROUP BY urls.resource_id;


ALTER TABLE public.url_objects OWNER TO postgres;

--
-- TOC entry 351 (class 1259 OID 22177)
-- Name: resource_objects; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.resource_objects AS
 SELECT resources.id,
    resources.type,
    resources.data_provider,
    resources.last_update,
    abstract_objects.abstract,
    description_objects.description,
    name_objects.name,
    short_name_objects.short_name,
    COALESCE(to_json(resources.simple_url), url_objects.url) AS url,
    categories_arrays.categories,
    features_arrays.features,
    multimedia_descriptions_arrays.media
   FROM ((((((((public.resources
     LEFT JOIN public.abstract_objects ON (((abstract_objects.id)::text = (resources.id)::text)))
     LEFT JOIN public.description_objects ON (((description_objects.id)::text = (resources.id)::text)))
     LEFT JOIN public.name_objects ON (((name_objects.id)::text = (resources.id)::text)))
     LEFT JOIN public.short_name_objects ON (((short_name_objects.id)::text = (resources.id)::text)))
     LEFT JOIN public.url_objects ON (((url_objects.id)::text = (resources.id)::text)))
     LEFT JOIN public.categories_arrays ON (((categories_arrays.id)::text = (resources.id)::text)))
     LEFT JOIN public.features_arrays ON (((features_arrays.id)::text = (resources.id)::text)))
     LEFT JOIN public.multimedia_descriptions_arrays ON (((multimedia_descriptions_arrays.id)::text = (resources.id)::text)));


ALTER TABLE public.resource_objects OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 21445)
-- Name: resource_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.resource_types (
    type character varying(50) NOT NULL,
    title character varying(50)
);


ALTER TABLE public.resource_types OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 21512)
-- Name: series_frequencies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.series_frequencies (
    frequency character varying(50) NOT NULL,
    title character varying(50)
);


ALTER TABLE public.series_frequencies OWNER TO postgres;

--
-- TOC entry 319 (class 1259 OID 21881)
-- Name: ski_slopes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ski_slopes (
    id character varying(100) NOT NULL,
    difficulty_eu character varying(20),
    difficulty_us character varying(20)
);


ALTER TABLE public.ski_slopes OWNER TO postgres;

--
-- TOC entry 320 (class 1259 OID 21891)
-- Name: snowparks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.snowparks (
    id character varying(100) NOT NULL,
    difficulty character varying(20)
);


ALTER TABLE public.snowparks OWNER TO postgres;

--
-- TOC entry 334 (class 1259 OID 22086)
-- Name: sponsors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sponsors (
    sponsor_id character varying(100) NOT NULL,
    event_id character varying(100) NOT NULL
);


ALTER TABLE public.sponsors OWNER TO postgres;

--
-- TOC entry 324 (class 1259 OID 21946)
-- Name: sub_areas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sub_areas (
    parent_id character varying(100) NOT NULL,
    child_id character varying(100) NOT NULL
);


ALTER TABLE public.sub_areas OWNER TO postgres;

--
-- TOC entry 356 (class 1259 OID 22198)
-- Name: sub_areas_arrays; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.sub_areas_arrays AS
 SELECT sub_areas.parent_id AS id,
    json_agg(json_build_object('id', sub_areas.child_id, 'type', 'mountainAreas')) AS sub_areas
   FROM public.sub_areas
  GROUP BY sub_areas.parent_id;


ALTER TABLE public.sub_areas_arrays OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 21831)
-- Name: venues; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.venues (
    id character varying(100) NOT NULL
);


ALTER TABLE public.venues OWNER TO postgres;

--
-- TOC entry 4765 (class 2604 OID 21697)
-- Name: addresses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.addresses ALTER COLUMN id SET DEFAULT nextval('public.addresses_id_seq'::regclass);


--
-- TOC entry 4766 (class 2604 OID 21772)
-- Name: contact_points id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_points ALTER COLUMN id SET DEFAULT nextval('public.contact_points_id_seq'::regclass);


--
-- TOC entry 4761 (class 2604 OID 19654)
-- Name: mytable pk; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mytable ALTER COLUMN pk SET DEFAULT nextval('public.mytable_pk_seq'::regclass);


--
-- TOC entry 5153 (class 0 OID 21574)
-- Dependencies: 297
-- Data for Name: abstracts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.abstracts (lang, resource_id, content) FROM stdin;
\.


--
-- TOC entry 5161 (class 0 OID 21694)
-- Dependencies: 305
-- Data for Name: addresses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.addresses (id, country, zipcode, type) FROM stdin;
\.


--
-- TOC entry 5144 (class 0 OID 21465)
-- Dependencies: 288
-- Data for Name: agents; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.agents (id) FROM stdin;
\.


--
-- TOC entry 5177 (class 0 OID 21901)
-- Dependencies: 321
-- Data for Name: area_lifts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.area_lifts (area_id, lift_id) FROM stdin;
\.


--
-- TOC entry 5178 (class 0 OID 21916)
-- Dependencies: 322
-- Data for Name: area_ski_slopes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.area_ski_slopes (area_id, ski_slope_id) FROM stdin;
\.


--
-- TOC entry 5179 (class 0 OID 21931)
-- Dependencies: 323
-- Data for Name: area_snowparks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.area_snowparks (area_id, snowpark_id) FROM stdin;
\.


--
-- TOC entry 5145 (class 0 OID 21475)
-- Dependencies: 289
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories (id, namespace) FROM stdin;
schema:BusinessEvent	schema
schema:ChildrensEvent	schema
schema:ComedyEvent	schema
schema:CourseInstance	schema
schema:DanceEvent	schema
schema:DeliveryEvent	schema
schema:EducationEvent	schema
schema:EventSeries	schema
schema:ExhibitionEvent	schema
schema:Festival	schema
schema:FoodEvent	schema
schema:Hackathon	schema
schema:LiteraryEvent	schema
schema:MusicEvent	schema
schema:PublicationEvent	schema
schema:SaleEvent	schema
schema:ScreeningEvent	schema
schema:SocialEvent	schema
schema:SportsEvent	schema
schema:TheaterEvent	schema
schema:VisualArtsEvent	schema
alpinebits:inPersonEvent	alpinebits
alpinebits:virtualEvent	alpinebits
alpinebits:hybridEvent	alpinebits
alpinebits:person	alpinebits
alpinebits:organization	alpinebits
alpinebits:standard-ski-slope	alpinebits
alpinebits:sledge-slope	alpinebits
alpinebits:cross-country	alpinebits
alpinebits:chairlift	alpinebits
alpinebits:gondola	alpinebits
alpinebits:skilift	alpinebits
alpinebits:cablecar	alpinebits
alpinebits:funicular	alpinebits
alpinebits:magic-carpet	alpinebits
alpinebits:skibus	alpinebits
alpinebits:train	alpinebits
\.


--
-- TOC entry 5184 (class 0 OID 22006)
-- Dependencies: 328
-- Data for Name: category_covered_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category_covered_types (category_id, type) FROM stdin;
schema:BusinessEvent	events
schema:ChildrensEvent	events
schema:ComedyEvent	events
schema:CourseInstance	events
schema:DanceEvent	events
schema:DeliveryEvent	events
schema:EducationEvent	events
schema:EventSeries	events
schema:ExhibitionEvent	events
schema:Festival	events
schema:FoodEvent	events
schema:Hackathon	events
schema:LiteraryEvent	events
schema:MusicEvent	events
schema:PublicationEvent	events
schema:SaleEvent	events
schema:ScreeningEvent	events
schema:SocialEvent	events
schema:SportsEvent	events
schema:TheaterEvent	events
schema:VisualArtsEvent	events
alpinebits:inPersonEvent	events
alpinebits:virtualEvent	events
alpinebits:hybridEvent	events
alpinebits:person	agents
alpinebits:organization	agents
alpinebits:standard-ski-slope	skiSlopes
alpinebits:sledge-slope	skiSlopes
alpinebits:cross-country	skiSlopes
alpinebits:chairlift	lifts
alpinebits:gondola	lifts
alpinebits:skilift	lifts
alpinebits:cablecar	lifts
alpinebits:funicular	lifts
alpinebits:magic-carpet	lifts
alpinebits:skibus	lifts
alpinebits:train	lifts
\.


--
-- TOC entry 5185 (class 0 OID 22021)
-- Dependencies: 329
-- Data for Name: category_specializations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category_specializations (parent_id, child_id) FROM stdin;
\.


--
-- TOC entry 5162 (class 0 OID 21700)
-- Dependencies: 306
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cities (lang, address_id, content) FROM stdin;
\.


--
-- TOC entry 5163 (class 0 OID 21717)
-- Dependencies: 307
-- Data for Name: complements; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.complements (lang, address_id, content) FROM stdin;
\.


--
-- TOC entry 5181 (class 0 OID 21961)
-- Dependencies: 325
-- Data for Name: connections; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.connections (a_id, b_id) FROM stdin;
\.


--
-- TOC entry 5167 (class 0 OID 21769)
-- Dependencies: 311
-- Data for Name: contact_points; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.contact_points (id, agent_id, address_id, available_hours, email, telephone) FROM stdin;
\.


--
-- TOC entry 5188 (class 0 OID 22056)
-- Dependencies: 332
-- Data for Name: contributors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.contributors (contributor_id, event_id) FROM stdin;
\.


--
-- TOC entry 5154 (class 0 OID 21591)
-- Dependencies: 298
-- Data for Name: descriptions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.descriptions (lang, resource_id, content) FROM stdin;
eng	schema:BusinessEvent	An event classified as BusinessEvent under definitions of Schema.org
eng	schema:ChildrensEvent	An event classified as ChildrensEvent under definitions of Schema.org
eng	schema:ComedyEvent	An event classified as ComedyEvent under definitions of Schema.org
eng	schema:CourseInstance	An event classified as CourseInstance under definitions of Schema.org
eng	schema:DanceEvent	An event classified as DanceEvent under definitions of Schema.org
eng	schema:DeliveryEvent	An event classified as DeliveryEvent under definitions of Schema.org
eng	schema:EducationEvent	An event classified as EducationEvent under definitions of Schema.org
eng	schema:EventSeries	An event classified as EventSeries under definitions of Schema.org
eng	schema:ExhibitionEvent	An event classified as ExhibitionEvent under definitions of Schema.org
eng	schema:Festival	An event classified as Festival under definitions of Schema.org
eng	schema:FoodEvent	An event classified as FoodEvent under definitions of Schema.org
eng	schema:Hackathon	An event classified as Hackathon under definitions of Schema.org
eng	schema:LiteraryEvent	An event classified as LiteraryEvent under definitions of Schema.org
eng	schema:MusicEvent	An event classified as MusicEvent under definitions of Schema.org
eng	schema:PublicationEvent	An event classified as PublicationEvent under definitions of Schema.org
eng	schema:SaleEvent	An event classified as SaleEvent under definitions of Schema.org
eng	schema:ScreeningEvent	An event classified as ScreeningEvent under definitions of Schema.org
eng	schema:SocialEvent	An event classified as SocialEvent under definitions of Schema.org
eng	schema:SportsEvent	An event classified as SportsEvent under definitions of Schema.org
eng	schema:TheaterEvent	An event classified as TheaterEvent under definitions of Schema.org
eng	schema:VisualArtsEvent	An event classified as VisualArtsEvent under definitions of Schema.org
eng	alpinebits:inPersonEvent	An event resource representing a event planned for exclusive in-person attendance
eng	alpinebits:virtualEvent	An event resource representing a event planned for exclusive virtual attendance
eng	alpinebits:hybridEvent	An event resource representing a event planned for both in- person and virtual attendance
eng	alpinebits:person	An agent representing a person
eng	alpinebits:organization	An agent representing an organization
\.


--
-- TOC entry 5149 (class 0 OID 21517)
-- Dependencies: 293
-- Data for Name: event_series; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.event_series (id, frequency) FROM stdin;
\.


--
-- TOC entry 5150 (class 0 OID 21532)
-- Dependencies: 294
-- Data for Name: event_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.event_status (status, title) FROM stdin;
canceled	Canceled
published	Published
\.


--
-- TOC entry 5172 (class 0 OID 21841)
-- Dependencies: 316
-- Data for Name: event_venues; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.event_venues (venue_id, event_id) FROM stdin;
\.


--
-- TOC entry 5151 (class 0 OID 21537)
-- Dependencies: 295
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.events (id, end_date, start_date, parent_id, publisher_id, series_id, status, in_person_capacity, online_capacity, simple_participation_url, simple_registration_url, recorded) FROM stdin;
\.


--
-- TOC entry 5186 (class 0 OID 22036)
-- Dependencies: 330
-- Data for Name: feature_covered_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.feature_covered_types (feature_id, type) FROM stdin;
\.


--
-- TOC entry 5187 (class 0 OID 22051)
-- Dependencies: 331
-- Data for Name: feature_specializations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.feature_specializations (parent_id, child_id) FROM stdin;
\.


--
-- TOC entry 5146 (class 0 OID 21485)
-- Dependencies: 290
-- Data for Name: features; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.features (id, namespace) FROM stdin;
\.


--
-- TOC entry 5169 (class 0 OID 21804)
-- Dependencies: 313
-- Data for Name: how_to_arrive; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.how_to_arrive (lang, place_id, content) FROM stdin;
\.


--
-- TOC entry 5152 (class 0 OID 21569)
-- Dependencies: 296
-- Data for Name: language_codes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.language_codes (lang, title) FROM stdin;
aaa	Ghotuo
aab	Alumu-Tesu
aac	Ari
aad	Amal
aae	Arbëreshë Albanian
aaf	Aranadan
aag	Ambrak
aah	Abu Arapesh
aai	Arifama-Miniafia
aak	Ankave
aal	Afade
aan	Anambé
aao	Algerian Saharan Arabic
aap	Pará Arára
aar	Afar
aas	Aasáx
aat	Arvanitika Albanian
aau	Abau
aaw	Solong
aax	Mandobo Atas
aaz	Amarasi
aba	Abé
abb	Bankon
abc	Ambala Ayta
abd	Manide
abe	Western Abnaki
abf	Abai Sungai
abg	Abaga
abh	Tajiki Arabic
abi	Abidji
abk	Abkhazian
abl	Lampung Nyo
abm	Abanyom
abn	Abua
abo	Abon
abp	Abellen Ayta
abq	Abaza
abr	Abron
abs	Ambonese Malay
abt	Ambulas
abu	Abure
abv	Baharna Arabic
abw	Pal
abx	Inabaknon
aby	Aneme Wake
abz	Abui
aca	Achagua
acb	Áncá
acd	Gikyode
ace	Achinese
acf	Saint Lucian Creole French
ach	Acoli
acm	Mesopotamian Arabic
acn	Achang
acp	Eastern Acipa
acq	Taizzi-Adeni Arabic
acr	Achi
act	Achterhoeks
acu	Achuar-Shiwiar
acv	Achumawi
acw	Hijazi Arabic
acx	Omani Arabic
acy	Cypriot Arabic
acz	Acheron
ada	Adangme
adb	Atauran
add	Lidzonka
ade	Adele
adf	Dhofari Arabic
adg	Andegerebinha
adh	Adhola
adi	Adi
adj	Adioukrou
adl	Galo
adn	Adang
ado	Abu
adq	Adangbe
adr	Adonara
ads	Adamorobe Sign Language
adt	Adnyamathanha
adu	Aduge
adw	Amundava
adx	Amdo Tibetan
ady	Adyghe
adz	Adzera
aeb	Tunisian Arabic
aec	Saidi Arabic
aed	Argentine Sign Language
aee	Northeast Pashai
aek	Haeke
ael	Ambele
aem	Arem
aen	Armenian Sign Language
aeq	Aer
aer	Eastern Arrernte
aeu	Akeu
aew	Ambakich
aey	Amele
aez	Aeka
afb	Gulf Arabic
afd	Andai
afe	Putukwam
afg	Afghan Sign Language
afi	Akrukay
afk	Nanubae
afn	Defaka
afo	Eloyi
afp	Tapei
afr	Afrikaans
afs	Afro-Seminole Creole
aft	Afitti
afu	Awutu
afz	Obokuitai
agb	Legbo
agc	Agatu
agd	Agarabi
age	Angal
agf	Arguni
agg	Angor
agh	Ngelima
agi	Agariya
agj	Argobba
agk	Isarog Agta
agl	Fembe
agm	Angaataha
agn	Agutaynen
ago	Tainae
agq	Aghem
agr	Aguaruna
ags	Esimbi
agt	Central Cagayan Agta
agu	Aguacateco
agv	Remontado Dumagat
agw	Kahua
agx	Aghul
agy	Southern Alta
agz	Mt. Iriga Agta
aha	Ahanta
ahb	Axamb
ahg	Qimant
ahh	Aghu
ahi	Tiagbamrin Aizi
ahk	Akha
ahl	Igo
ahm	Mobumrin Aizi
ahn	Àhàn
ahp	Aproumu Aizi
ahr	Ahirani
ahs	Ashe
aht	Ahtena
aia	Arosi
aib	Ainu (China)
aic	Ainbai
aie	Amara
aif	Agi
aig	Antigua and Barbuda Creole English
aih	Ai-Cham
aii	Assyrian Neo-Aramaic
aij	Lishanid Noshan
aik	Ake
ail	Aimele
aim	Aimol
ain	Ainu (Japan)
aio	Aiton
aip	Burumakok
aiq	Aimaq
air	Airoran
aiw	Aari
aix	Aighon
aiy	Ali
aja	Aja (South Sudan)
ajg	Aja (Benin)
aji	Ajië
ajn	Andajin
ajp	South Levantine Arabic
ajt	Judeo-Tunisian Arabic
aju	Judeo-Moroccan Arabic
ajz	Amri Karbi
akb	Batak Angkola
akc	Mpur
akd	Ukpet-Ehom
ake	Akawaio
akf	Akpa
akg	Anakalangu
akh	Angal Heneng
aki	Aiome
akl	Aklanon
ako	Akurio
akp	Siwu
akq	Ak
akr	Araki
aks	Akaselem
akt	Akolet
aku	Akum
akv	Akhvakh
akw	Akwa
akz	Alabama
ala	Alago
alc	Qawasqar
ald	Alladian
ale	Aleut
alf	Alege
alh	Alawa
ali	Amaimon
alj	Alangan
alk	Alak
all	Allar
alm	Amblong
aln	Gheg Albanian
alo	Larike-Wakasihu
alp	Alune
alq	Algonquin
alr	Alutor
als	Tosk Albanian
alt	Southern Altai
alu	Areare
alw	Alaba-K’abeena
alx	Amol
aly	Alyawarr
alz	Alur
amb	Ambo
amc	Amahuaca
ame	Yanesha
amf	Hamer-Banna
amg	Amurdak
amh	Amharic
ami	Amis
amj	Amdang
amk	Ambai
aml	War-Jaintia
amm	Ama (Papua New Guinea)
amn	Amanab
amo	Amo
amp	Alamblak
amq	Amahai
amr	Amarakaeri
ams	Southern Amami-Oshima
amt	Amto
amu	Guerrero Amuzgo
amv	Ambelau
amw	Western Neo-Aramaic
amx	Anmatyerre
amy	Ami
anc	Ngas
and	Ansus
ane	Xârâcùù
anf	Animere
anh	Nend
ani	Andi
anj	Anor
ank	Goemai
anl	Anu-Hkongso Chin
anm	Anal
ann	Obolo
ano	Andoque
anp	Angika
anq	Jarawa (India)
anr	Andh
ant	Antakarinya
anu	Anuak
anv	Denya
anw	Anaang
anx	Andra-Hus
any	Anyin
anz	Anem
aoa	Angolar
aob	Abom
aoc	Pemon
aod	Andarum
aoe	Angal Enen
aof	Bragat
aog	Angoram
aoi	Anindilyakwa
aoj	Mufian
aok	Arhö
aol	Alor
aom	Ömie
aon	Bumbita Arapesh
aos	Taikat
aot	Atong (India)
aou	Aou
aox	Atorada
aoz	Uab Meto
apb	Saa
apc	North Levantine Arabic
apd	Sudanese Arabic
ape	Bukiyip
apf	Pahanan Agta
apg	Ampanang
aph	Athpariya
api	Apiaká
apj	Jicarilla Apache
apk	Kiowa Apache
apl	Lipan Apache
apm	Mescalero-Chiricahua Apache
apn	Apinayé
apo	Ambul
app	Apma
apq	A-Pucikwar
apr	Arop-Lokep
aps	Arop-Sissano
apt	Apatani
apu	Apurinã
apw	Western Apache
apx	Aputai
apy	Apalaí
apz	Safeyoka
aqc	Archi
aqd	Ampari Dogon
aqg	Arigidi
aqm	Atohwaim
aqn	Northern Alta
aqr	Arhâ
aqt	Angaité
aqz	Akuntsu
arb	Standard Arabic
are	Western Arrarnta
arg	Aragonese
arh	Arhuaco
ari	Arikara
ark	Arikapú
arl	Arabela
arn	Mapudungun
aro	Araona
arp	Arapaho
arq	Algerian Arabic
arr	Karo (Brazil)
ars	Najdi Arabic
arv	Arbore
arw	Arawak
arx	Aruá (Rodonia State)
ary	Moroccan Arabic
arz	Egyptian Arabic
asa	Asu (Tanzania)
asb	Assiniboine
asc	Casuarina Coast Asmat
ase	American Sign Language
asf	Auslan
asg	Cishingini
asi	Buruwai
asj	Sari
ask	Ashkun
asl	Asilulu
asm	Assamese
asn	Xingú Asuriní
aso	Dano
asp	Algerian Sign Language
asq	Austrian Sign Language
asr	Asuri
ass	Ipulo
ast	Asturian
asu	Tocantins Asurini
omi	Omi
asv	Asoa
asw	Australian Aborigines Sign Language
asx	Muratayak
asy	Yaosakor Asmat
asz	As
ata	Pele-Ata
atb	Zaiwa
atd	Ata Manobo
ate	Atemble
atg	Ivbie North-Okpela-Arhe
ati	Attié
atj	Atikamekw
atk	Ati
atl	Mt. Iraya Agta
atm	Ata
atn	Ashtiani
ato	Atong (Cameroon)
atp	Pudtol Atta
atq	Aralle-Tabulahan
atr	Waimiri-Atroari
ats	Gros Ventre
att	Pamplona Atta
atu	Reel
atv	Northern Altai
atw	Atsugewi
atx	Arutani
aty	Aneityum
atz	Arta
aua	Asumboa
aub	Alugu
auc	Waorani
aud	Anuta
aug	Aguna
auh	Aushi
aui	Anuki
auj	Awjilah
auk	Heyo
aul	Aulua
aum	Asu (Nigeria)
aun	Molmo One
aup	Makayam
auq	Anus
aur	Aruek
aut	Austral
auu	Auye
auw	Awyi
auy	Awiyaana
auz	Uzbeki Arabic
ava	Avaric
avb	Avau
avd	Alviri-Vidari
avi	Avikam
avl	Eastern Egyptian Bedawi Arabic
avn	Avatime
avt	Au
avu	Avokaya
avv	Avá-Canoeiro
awa	Awadhi
awb	Awa (Papua New Guinea)
awc	Cicipu
awe	Awetí
awh	Awbono
awi	Aekyom
awm	Arawum
awn	Awngi
awo	Awak
awr	Awera
aws	South Awyu
awt	Araweté
awu	Central Awyu
awv	Jair Awyu
aww	Awun
awx	Awara
awy	Edera Awyu
axk	Yaka (Central African Republic)
axx	Xârâgurè
aya	Awar
ayb	Ayizo Gbe
ayc	Southern Aymara
aye	Ayere
ayg	Ginyanga
ayh	Hadrami Arabic
ayi	Leyigha
ayk	Akuku
ayl	Libyan Arabic
ayn	Sanaani Arabic
ayo	Ayoreo
ayp	North Mesopotamian Arabic
ayq	Ayi (Papua New Guinea)
ayr	Central Aymara
ays	Sorsogon Ayta
ayt	Magbukun Ayta
ayu	Ayu
ayz	Mai Brat
aza	Azha
azb	South Azerbaijani
azd	Eastern Durango Nahuatl
azg	San Pedro Amuzgos Amuzgo
azj	North Azerbaijani
azm	Ipalapa Amuzgo
azn	Western Durango Nahuatl
azo	Awing
azt	Faire Atta
azz	Highland Puebla Nahuatl
baa	Babatana
bab	Bainouk-Gunyuño
bac	Badui
baf	Nubaca
bag	Tuki
bah	Bahamas Creole English
baj	Barakai
bak	Bashkir
bam	Bambara
ban	Balinese
bao	Waimaha
bap	Bantawa
bar	Bavarian
bas	Basa (Cameroon)
bau	Bada (Nigeria)
bav	Vengo
baw	Bambili-Bambui
bax	Bamun
bay	Batuley
bba	Baatonum
bbb	Barai
bbc	Batak Toba
bbd	Bau
bbe	Bangba
bbf	Baibai
bbg	Barama
bbh	Bugan
bbi	Barombi
bbj	Ghomálá
bbk	Babanki
bbl	Bats
bbm	Babango
bbn	Uneapa
bbo	Northern Bobo Madaré
bbp	West Central Banda
bbq	Bamali
bbr	Girawa
bbs	Bakpinka
bbt	Mburku
bbu	Kulung (Nigeria)
bbv	Karnai
bbw	Baba
bbx	Bubia
bby	Befang
bca	Central Bai
bcb	Bainouk-Samik
bcc	Southern Balochi
bcd	North Babar
bce	Bamenyam
bcf	Bamu
bcg	Baga Pokur
bch	Bariai
bci	Baoulé
bcj	Bardi
bck	Bunuba
bcl	Central Bikol
bcm	Bannoni
bcn	Bali (Nigeria)
bco	Kaluli
bcp	Bali (Democratic Republic of Congo)
bcq	Bench
bcr	Babine
bcs	Kohumono
bct	Bendi
bcu	Awad Bing
bcv	Shoo-Minda-Nye
bcw	Bana
bcy	Bacama
bcz	Bainouk-Gunyaamolo
bda	Bayot
bdb	Basap
bdc	Emberá-Baudó
bdd	Bunama
bde	Bade
bdf	Biage
bdg	Bonggi
bdh	Baka (South Sudan)
bdi	Burun
bdj	Bai (South Sudan)
bdk	Budukh
bdl	Indonesian Bajau
bdm	Buduma
bdn	Baldemu
bdo	Morom
bdp	Bende
bdq	Bahnar
bdr	West Coast Bajau
bds	Burunge
bdt	Bokoto
bdu	Oroko
bdv	Bodo Parja
bdw	Baham
bdx	Budong-Budong
bdy	Bandjalang
bdz	Badeshi
bea	Beaver
beb	Bebele
bec	Iceve-Maci
bed	Bedoanas
bee	Byangsi
bef	Benabena
beg	Belait
beh	Biali
bei	Bekati
bej	Beja
bek	Bebeli
bel	Belarusian
bem	Bemba (Zambia)
ben	Bengali
beo	Beami
bep	Besoa
beq	Beembe
bes	Besme
bet	Guiberoua Béte
beu	Blagar
bev	Daloa Bété
bew	Betawi
bex	Jur Modo
bey	Beli (Papua New Guinea)
bez	Bena (Tanzania)
bfa	Bari
bfb	Pauri Bareli
bfc	Panyi Bai
bfd	Bafut
bfe	Betaf
bff	Bofi
bfg	Busang Kayan
bfh	Blafe
bfi	British Sign Language
bfj	Bafanji
bfk	Ban Khor Sign Language
bfl	Banda-Ndélé
bfm	Mmen
bfn	Bunak
bfo	Malba Birifor
bfp	Beba
bfq	Badaga
bfr	Bazigar
bfs	Southern Bai
bft	Balti
bfu	Gahri
bfw	Bondo
bfx	Bantayanon
bfy	Bagheli
bfz	Mahasu Pahari
bga	Gwamhi-Wuri
bgb	Bobongko
bgc	Haryanvi
bgd	Rathwi Bareli
bge	Bauria
bgf	Bangandu
bgg	Bugun
bgi	Giangan
bgj	Bangolan
bgk	Bit
bgl	Bo (Laos)
bgn	Western Balochi
bgo	Baga Koga
bgp	Eastern Balochi
bgq	Bagri
bgr	Bawm Chin
bgs	Tagabawa
bgt	Bughotu
bgu	Mbongno
bgv	Warkay-Bipim
bgw	Bhatri
bgx	Balkan Gagauz Turkish
bgy	Benggoi
bgz	Banggai
bha	Bharia
bhb	Bhili
bhc	Biga
bhd	Bhadrawahi
bhe	Bhaya
bhf	Odiai
bhg	Binandere
bhh	Bukharic
bhi	Bhilali
bhj	Bahing
bhl	Bimin
bhm	Bathari
bhn	Bohtan Neo-Aramaic
bho	Bhojpuri
bhp	Bima
bhq	Tukang Besi South
bhr	Bara Malagasy
bhs	Buwal
bht	Bhattiyali
bhu	Bhunjia
bhv	Bahau
bhw	Biak
bhx	Bhalay
bhy	Bhele
bhz	Bada (Indonesia)
bia	Badimaya
bib	Bissa
bic	Bikaru
bid	Bidiyo
bie	Bepour
bif	Biafada
big	Biangai
bij	Vaghat-Ya-Bijim-Legeri
bil	Bile
bim	Bimoba
bin	Bini
bio	Nai
bip	Bila
biq	Bipi
bir	Bisorio
bis	Bislama
bit	Berinomo
biu	Biete
biv	Southern Birifor
biw	Kol (Cameroon)
bix	Bijori
biy	Birhor
biz	Baloi
bja	Budza
bjc	Bariji
bje	Biao-Jiao Mien
bjf	Barzani Jewish Neo-Aramaic
bjg	Bidyogo
bjh	Bahinemo
bji	Burji
bjj	Kanauji
bjk	Barok
bjl	Bulu (Papua New Guinea)
bjm	Bajelani
bjn	Banjar
bjo	Mid-Southern Banda
bjp	Fanamaket
bjr	Binumarien
bjs	Bajan
bjt	Balanta-Ganja
bju	Busuu
bjv	Bedjond
bjw	Bakwé
bjx	Banao Itneg
bjz	Baruga
bka	Kyak
bkc	Baka (Cameroon)
bkd	Binukid
bkf	Beeke
bkg	Buraka
bkh	Bakoko
bki	Baki
bkj	Pande
bkk	Brokskat
bkl	Berik
bkm	Kom (Cameroon)
bkn	Bukitan
bko	Kwa
bkp	Boko (Democratic Republic of Congo)
bkq	Bakairí
bkr	Bakumpai
bks	Northern Sorsoganon
bkt	Boloki
bku	Buhid
bkv	Bekwarra
bkw	Bekwel
bkx	Baikeno
bky	Bokyi
bkz	Bungku
bla	Siksika
blb	Bilua
blc	Bella Coola
bld	Bolango
ble	Balanta-Kentohe
blf	Buol
blg	Balau
blh	Kuwaa
bli	Bolia
blj	Bolongan
blk	Pao Karen
blm	Beli (South Sudan)
bln	Southern Catanduanes Bikol
blo	Anii
blp	Blablanga
blq	Baluan-Pam
blr	Blang
bls	Balaesang
blt	Tai Dam
blv	Kibala
blw	Balangao
blx	Mag-Indi Ayta
bly	Notre
blz	Balantak
bma	Lame
bmb	Bembe
bmc	Biem
bmd	Baga Manduri
bme	Limassa
bmf	Bom-Kim
bmg	Bamwe
bmh	Kein
bmi	Bagirmi
bmj	Bote-Majhi
bmk	Ghayavi
bml	Bomboli
bmm	Northern Betsimisaraka Malagasy
bmo	Bambalang
bmp	Bulgebi
bmq	Bomu
bmr	Muinane
bms	Bilma Kanuri
bmt	Biao Mon
bmu	Somba-Siawari
bmv	Bum
bmw	Bomwali
bmx	Baimak
bmz	Baramu
bna	Bonerate
bnb	Bookan
bnd	Banda (Indonesia)
bne	Bintauna
bnf	Masiwang
bng	Benga
bni	Bangi
bnj	Eastern Tawbuid
bnk	Bierebo
bnl	Boon
bnm	Batanga
bnn	Bunun
bno	Bantoanon
bnp	Bola
bnq	Bantik
bnr	Butmas-Tur
bns	Bundeli
bnu	Bentong
bnv	Bonerif
bnw	Bisis
bnx	Bangubangu
bny	Bintulu
bnz	Beezen
boa	Bora
bob	Aweer
bod	Tibetan
boe	Mundabli
bof	Bolon
bog	Bamako Sign Language
boh	Boma
boj	Anjam
bok	Bonjo
bol	Bole
bom	Berom
bon	Bine
boo	Tiemacèwè Bozo
bop	Bonkiman
boq	Bogaya
bor	Borôro
bos	Bosnian
bot	Bongo
bou	Bondei
bov	Tuwuli
box	Buamu
boy	Bodo (Central African Republic)
boz	Tiéyaxo Bozo
bpa	Daakaka
bpd	Banda-Banda
bpg	Bonggo
bph	Botlikh
bpi	Bagupi
bpj	Binji
bpk	Orowe
bpl	Broome Pearling Lugger Pidgin
bpm	Biyom
bpn	Dzao Min
bpo	Anasi
bpp	Kaure
bpq	Banda Malay
bpr	Koronadal Blaan
bps	Sarangani Blaan
bpu	Bongu
bpv	Bian Marind
bpw	Bo (Papua New Guinea)
bpx	Palya Bareli
bpy	Bishnupriya
bpz	Bilba
bqa	Tchumbuli
bqb	Bagusa
bqc	Boko (Benin)
bqd	Bung
bqg	Bago-Kusuntu
bqh	Baima
bqi	Bakhtiari
bqj	Bandial
bqk	Banda-Mbrès
bql	Bilakura
bqm	Wumboko
bqn	Bulgarian Sign Language
bqo	Balo
bqp	Busa
bqq	Biritai
bqr	Burusu
bqs	Bosngun
bqt	Bamukumbit
bqu	Boguru
bqv	Koro Wachi
bqw	Buru (Nigeria)
bqx	Baangi
bqy	Bengkala Sign Language
bqz	Bakaka
bra	Braj
brb	Lave
brd	Baraamu
bre	Breton
brf	Bira
brg	Baure
brh	Brahui
bri	Mokpwe
brj	Bieria
brl	Birwa
brm	Barambu
brn	Boruca
bro	Brokkat
brp	Barapasi
brq	Breri
brr	Birao
brs	Baras
brt	Bitare
bru	Eastern Bru
brv	Western Bru
brw	Bellari
brx	Bodo (India)
bry	Burui
brz	Bilbil
bsa	Abinomn
bsb	Brunei Bisaya
bsc	Bassari
bse	Wushi
bsf	Bauchi
bsg	Bashkardi
bsh	Kati
bsi	Bassossi
bsj	Bangwinji
bsk	Burushaski
bsm	Busami
bsn	Barasana-Eduria
bso	Buso
bsp	Baga Sitemu
bsq	Bassa
bsr	Bassa-Kontagora
bss	Akoose
bst	Basketo
bsu	Bahonsuai
bsw	Baiso
bsx	Yangkam
bsy	Sabah Bisaya
bta	Bata
btc	Bati (Cameroon)
btd	Batak Dairi
btf	Birgit
btg	Gagnoa Bété
bth	Biatah Bidayuh
bti	Burate
btj	Bacanese Malay
btm	Batak Mandailing
btn	Ratagnon
bto	Rinconada Bikol
btp	Budibud
btq	Batek
btr	Baetora
bts	Batak Simalungun
btt	Bete-Bendi
btu	Batu
btv	Bateri
btw	Butuanon
btx	Batak Karo
bty	Bobot
btz	Batak Alas-Kluet
bub	Bua
buc	Bushi
bud	Ntcham
buf	Bushoong
bug	Buginese
buh	Younuo Bunu
bui	Bongili
buj	Basa-Gurmana
buk	Bugawac
bul	Bulgarian
bum	Bulu (Cameroon)
bun	Sherbro
buo	Terei
bup	Busoa
buq	Brem
bus	Bokobaru
but	Bungain
buu	Budu
buv	Bun
buw	Bubi
bux	Boghom
buy	Bullom So
buz	Bukwen
bva	Barein
bvb	Bube
bvc	Baelelea
bvd	Baeggu
bve	Berau Malay
bvf	Boor
bvg	Bonkeng
bvh	Bure
bvi	Belanda Viri
bvj	Baan
bvk	Bukat
bvl	Bolivian Sign Language
bvm	Bamunka
bvn	Buna
bvo	Bolgo
bvp	Bumang
bvq	Birri
bvr	Burarra
bvt	Bati (Indonesia)
bvu	Bukit Malay
bvw	Boga
bvx	Dibole
bvy	Baybayanon
bvz	Bauzi
bwa	Bwatoo
bwb	Namosi-Naitasiri-Serua
bwc	Bwile
bwd	Bwaidoka
bwe	Bwe Karen
bwf	Boselewa
bwg	Barwe
bwh	Bishuo
bwi	Baniwa
bwj	Láá Láá Bwamu
bwk	Bauwaki
bwl	Bwela
bwm	Biwat
bwn	Wunai Bunu
bwo	Boro (Ethiopia)
bwp	Mandobo Bawah
bwq	Southern Bobo Madaré
bwr	Bura-Pabir
bws	Bomboma
bwt	Bafaw-Balong
bwu	Buli (Ghana)
bww	Bwa
bwx	Bu-Nao Bunu
bwy	Cwi Bwamu
bwz	Bwisi
bxa	Tairaha
bxb	Belanda Bor
bxc	Molengue
bxd	Pela
bxe	Birale
bxf	Bilur
bxg	Bangala
bxh	Buhutu
bxj	Bayungu
bxk	Bukusu
bxl	Jalkunan
bxm	Mongolia Buriat
bxn	Burduna
bxo	Barikanchi
bxp	Bebil
bxq	Beele
bxr	Russia Buriat
bxs	Busam
bxu	China Buriat
bxv	Berakou
bxw	Bankagooma
bxz	Binahari
bya	Batak
byb	Bikya
byc	Ubaghara
byd	Benyadu
bye	Pouye
byf	Bete
byh	Bhujel
byi	Buyu
byj	Bina (Nigeria)
byk	Biao
byl	Bayono
bym	Bidjara
byn	Bilin
byo	Biyo
byp	Bumaji
byr	Baruya
bys	Burak
byv	Medumba
byw	Belhariya
byx	Qaqet
byz	Banaro
bza	Bandi
bzb	Andio
bzc	Southern Betsimisaraka Malagasy
bzd	Bribri
bze	Jenaama Bozo
bzf	Boikin
bzg	Babuza
bzh	Mapos Buang
bzi	Bisu
bzj	Belize Kriol English
bzk	Nicaragua Creole English
bzl	Boano (Sulawesi)
bzm	Bolondo
bzn	Boano (Maluku)
bzo	Bozaba
bzp	Kemberano
bzq	Buli (Indonesia)
bzs	Brazilian Sign Language
bzu	Burmeso
bzv	Naami
bzw	Basa (Nigeria)
bzx	Kɛlɛngaxo Bozo
bzy	Obanliku
bzz	Evant
caa	Chortí
cab	Garifuna
cac	Chuj
cad	Caddo
cae	Lehar
caf	Southern Carrier
cag	Nivaclé
cah	Cahuarano
cak	Kaqchikel
cal	Carolinian
cam	Cemuhî
can	Chambri
cao	Chácobo
cap	Chipaya
caq	Car Nicobarese
car	Galibi Carib
cas	Tsimané
cat	Catalan
cav	Cavineña
caw	Callawalla
cax	Chiquitano
cay	Cayuga
cbb	Cabiyarí
cbc	Carapana
cbd	Carijona
cbg	Chimila
cbi	Chachi
cbj	Ede Cabe
cbk	Chavacano
cbl	Bualkhaw Chin
cbn	Nyahkur
cbo	Izora
cbq	Tsucuba
cbr	Cashibo-Cacataibo
cbs	Cashinahua
cbt	Chayahuita
cbu	Candoshi-Shapra
cbv	Cacua
cbw	Kinabalian
cby	Carabayo
ccc	Chamicuro
ccd	Cafundo Creole
cce	Chopi
ccg	Samba Daka
cch	Atsam
ccj	Kasanga
ccl	Cutchi-Swahili
ccm	Malaccan Creole Malay
cco	Comaltepec Chinantec
ccp	Chakma
cda	Choni
cde	Chenchu
cdf	Chiru
cdh	Chambeali
cdi	Chodri
cdj	Churahi
cdm	Chepang
cdn	Chaudangsi
cdo	Min Dong Chinese
cdr	Cinda-Regi-Tiyal
cds	Chadian Sign Language
cdy	Chadong
cdz	Koda
ceb	Cebuano
ceg	Chamacoco
cek	Eastern Khumi Chin
cen	Cen
ces	Czech
cet	Centúúm
cey	Ekai Chin
cfa	Dijim-Bwilim
cfd	Cara
cfg	Como Karim
cfm	Falam Chin
cga	Changriwa
cgc	Kagayanen
cgg	Chiga
cgk	Chocangacakha
cha	Chamorro
chd	Highland Oaxaca Chontal
che	Chechen
chf	Tabasco Chontal
chj	Ojitlán Chinantec
chk	Chuukese
chl	Cahuilla
chn	Chinook jargon
cho	Choctaw
chp	Chipewyan
chq	Quiotepec Chinantec
chr	Cherokee
chv	Chuvash
chw	Chuwabu
chx	Chantyal
chy	Cheyenne
chz	Ozumacín Chinantec
cia	Cia-Cia
cib	Ci Gbe
cic	Chickasaw
cie	Cineni
cih	Chinali
cik	Chitkuli Kinnauri
cim	Cimbrian
cin	Cinta Larga
cip	Chiapanec
cir	Tiri
ciw	Chippewa
ciy	Chaima
cja	Western Cham
cje	Chru
cji	Chamalal
cjk	Chokwe
cjm	Eastern Cham
cjn	Chenapian
cjo	Ashéninka Pajonal
cjp	Cabécar
cjs	Shor
cjv	Chuave
cjy	Jinyu Chinese
ckb	Central Kurdish
ckh	Chak
ckl	Cibak
ckm	Chakavian
ckn	Kaang Chin
cko	Anufo
ckq	Kajakse
ckr	Kairak
cks	Tayo
ckt	Chukot
cku	Koasati
ckv	Kavalan
ckx	Caka
cky	Cakfem-Mushere
ckz	Cakchiquel-Quiché Mixed Language
cla	Ron
clc	Chilcotin
cld	Chaldean Neo-Aramaic
cle	Lealao Chinantec
clh	Chilisso
cli	Chakali
clj	Laitu Chin
clk	Idu-Mishmi
cll	Chala
clm	Clallam
clo	Lowland Oaxaca Chontal
clt	Lautu Chin
clu	Caluyanun
clw	Chulym
cly	Eastern Highland Chatino
cma	Maa
cme	Cerma
cmi	Emberá-Chamí
cml	Campalagian
cmn	Mandarin Chinese
cmo	Central Mnong
cmr	Mro-Khimi Chin
cmt	Camtho
cna	Changthang
cnb	Chinbon Chin
cnc	Côông
cng	Northern Qiang
cnh	Hakha Chin
cni	Asháninka
cnk	Khumi Chin
cnl	Lalana Chinantec
cno	Con
cnp	Northern Ping Chinese
cnr	Montenegrin
cns	Central Asmat
cnt	Tepetotutla Chinantec
cnu	Chenoua
cnw	Ngawn Chin
coa	Cocos Islands Malay
coc	Cocopa
cod	Cocama-Cocamilla
coe	Koreguaje
cof	Colorado
cog	Chong
coh	Chonyi-Dzihana-Kauma
cok	Santa Teresa Cora
col	Columbia-Wenatchi
com	Comanche
con	Cofán
coo	Comox
cor	Cornish
cos	Corsican
cot	Caquinte
cou	Wamey
cov	Cao Miao
cox	Nanti
coz	Chochotec
cpa	Palantla Chinantec
cpb	Ucayali-Yurúa Ashéninka
cpc	Ajyíninka Apurucayali
cpi	Chinese Pidgin English
cpn	Cherepon
cpo	Kpeego
cps	Capiznon
cpu	Pichis Ashéninka
cpx	Pu-Xian Chinese
cpy	South Ucayali Ashéninka
cqd	Chuanqiandian Cluster Miao
cra	Chara
crc	Lonwolwol
crd	Coeur dAlene
crg	Michif
crh	Crimean Tatar
cri	Sãotomense
crj	Southern East Cree
crk	Plains Cree
crl	Northern East Cree
crm	Moose Cree
crn	El Nayar Cora
cro	Crow
crq	Iyowujwa Chorote
crs	Seselwa Creole French
crt	Iyojwaja Chorote
crv	Chaura
crw	Chrau
crx	Carrier
cry	Cori
csa	Chiltepec Chinantec
csb	Kashubian
csc	Catalan Sign Language
csd	Chiangmai Sign Language
cse	Czech Sign Language
csf	Cuba Sign Language
csg	Chilean Sign Language
csh	Asho Chin
csj	Songlai Chin
csk	Jola-Kasa
csl	Chinese Sign Language
csm	Central Sierra Miwok
csn	Colombian Sign Language
cso	Sochiapam Chinantec
csp	Southern Ping Chinese
csq	Croatia Sign Language
csr	Costa Rican Sign Language
cst	Northern Ohlone
csv	Sumtu Chin
csw	Swampy Cree
csy	Siyin Chin
csz	Coos
cta	Tataltepec Chatino
ctd	Tedim Chin
cte	Tepinapa Chinantec
ctg	Chittagonian
cth	Thaiphum Chin
ctl	Tlacoatzintepec Chinantec
ctn	Chhintange
cto	Emberá-Catío
ctp	Western Highland Chatino
cts	Northern Catanduanes Bikol
ctt	Wayanad Chetti
ctu	Chol
ctz	Zacatepec Chatino
cua	Cua
cub	Cubeo
cuc	Usila Chinantec
cug	Chungmboko
cuh	Chuka
cui	Cuiba
cuj	Mashco Piro
cuk	San Blas Kuna
cul	Culina
cuq	Cun
cur	Chhulung
cut	Teutila Cuicatec
cuu	Tai Ya
cuv	Cuvok
cuw	Chukwa
cux	Tepeuxila Cuicatec
cuy	Cuitlatec
cvg	Chug
cvn	Valle Nacional Chinantec
cwa	Kabwa
cwb	Maindo
cwd	Woods Cree
cwe	Kwere
cwg	Chewong
cwt	Kuwaataay
cya	Nopala Chatino
cym	Welsh
cyo	Cuyonon
czh	Huizhou Chinese
czn	Zenzontepec Chatino
czo	Min Zhong Chinese
czt	Zotung Chin
daa	Dangaléat
dac	Dambi
dad	Marik
dae	Duupa
dag	Dagbani
dah	Gwahatike
dai	Day
daj	Dar Fur Daju
dak	Dakota
dal	Dahalo
dam	Damakawa
dan	Danish
dao	Daai Chin
daq	Dandami Maria
dar	Dargwa
das	Daho-Doo
dau	Dar Sila Daju
dav	Taita
daw	Davawenyo
dax	Dayi
daz	Dao
dba	Bangime
dbb	Deno
dbd	Dadiya
dbe	Dabe
dbf	Edopi
dbg	Dogul Dom Dogon
dbi	Doka
dbj	Idaan
dbl	Dyirbal
dbm	Duguri
dbn	Duriankere
dbo	Dulbu
dbp	Duwai
dbq	Daba
dbr	Dabarre
dbt	Ben Tey Dogon
dbu	Bondum Dom Dogon
dbv	Dungu
dbw	Bankan Tey Dogon
dby	Dibiyaso
dcc	Deccan
ddd	Dongotono
dde	Doondo
ddg	Fataluku
ddi	West Goodenough
ddj	Jaru
ddn	Dendi (Benin)
ddo	Dido
dds	Donno So Dogon
ddw	Dawera-Daweloor
dec	Dagik
ded	Dedua
dee	Dewoin
def	Dezfuli
deg	Degema
deh	Dehwari
dei	Demisa
dek	Dek
dem	Dem
deq	Dendi (Central African Republic)
der	Deori
des	Desano
deu	German
dev	Domung
dez	Dengese
dga	Southern Dagaare
dgb	Bunoge Dogon
dgc	Casiguran Dumagat Agta
dgd	Dagaari Dioula
dge	Degenan
dgg	Doga
dgh	Dghwede
dgi	Northern Dagara
dgk	Dagba
dgl	Andaandi
dgo	Dogri (individual language)
dgr	Dogrib
dgs	Dogoso
dgx	Doghoro
dgz	Daga
dhd	Dhundari
dhg	Dhangu-Djangu
dhi	Dhimal
dhl	Dhalandji
dhm	Zemba
dhn	Dhanki
dho	Dhodia
dhr	Dhargari
dhs	Dhaiso
dhv	Dehu
dhw	Dhanwar (Nepal)
dhx	Dhungaloo
dia	Dia
dib	South Central Dinka
dic	Lakota Dida
did	Didinga
dig	Digo
dih	Kumiai
dii	Dimbong
dij	Dai
dik	Southwestern Dinka
dil	Dilling
dim	Dime
dio	Dibo
dip	Northeastern Dinka
diq	Dimli (individual language)
dir	Dirim
dis	Dimasa
diu	Diriku
div	Dhivehi
diw	Northwestern Dinka
dix	Dixon Reef
diy	Diuwe
diz	Ding
djb	Djinba
djc	Dar Daju Daju
djd	Djamindjung
dje	Zarma
dji	Djinang
djj	Djeebbana
djk	Eastern Maroon Creole
djm	Jamsay Dogon
djn	Jawoyn
djo	Jangkang
djr	Djambarrpuyngu
dju	Kapriman
dka	Dakpakha
dkk	Dakka
dkr	Kuijau
dks	Southeastern Dinka
dkx	Mazagway
dlg	Dolgan
dlk	Dahalik
dln	Darlong
dma	Duma
dmb	Mombo Dogon
dmc	Gavak
dme	Dugwor
dmg	Upper Kinabatangan
dmk	Domaaki
dml	Dameli
dmm	Dama
dmo	Kemedzung
dmr	East Damar
dms	Dampelas
dmu	Dubu
dmv	Dumpas
dmw	Mudburra
dmx	Dema
dmy	Demta
dna	Upper Grand Valley Dani
dnd	Daonda
dne	Ndendeule
dng	Dungan
dni	Lower Grand Valley Dani
dnj	Dan
dnk	Dengka
dnn	Dzùùngoo
dno	Ndrulo
dnr	Danaru
dnt	Mid Grand Valley Dani
dnu	Danau
dnv	Danu
dnw	Western Dani
dny	Dení
doa	Dom
dob	Dobu
doc	Northern Dong
doe	Doe
dof	Domu
doh	Dong
dok	Dondo
dol	Doso
don	Toura (Papua New Guinea)
doo	Dongo
dop	Lukpa
doq	Dominican Sign Language
dor	Dorio
dos	Dogosé
dot	Dass
dov	Dombe
dow	Doyayo
dox	Bussa
doy	Dompo
doz	Dorze
dpp	Papar
drb	Dair
drc	Minderico
drd	Darmiya
dre	Dolpo
drg	Rungus
dri	CLela
drl	Paakantyi
drn	West Damar
dro	Daro-Matu Melanau
drs	Gedeo
drt	Drents
dru	Rukai
dry	Darai
dsb	Lower Sorbian
dse	Dutch Sign Language
dsh	Daasanach
dsi	Disa
dsl	Danish Sign Language
dso	Desiya
dsq	Tadaksahak
dta	Daur
dtb	Labuk-Kinabatangan Kadazan
dtd	Ditidaht
dti	Ana Tinga Dogon
dtk	Tene Kan Dogon
dtm	Tomo Kan Dogon
dtn	Daatsʼíin
dto	Tommo So Dogon
dtp	Kadazan Dusun
dtr	Lotud
dts	Toro So Dogon
dtt	Toro Tegu Dogon
dtu	Tebul Ure Dogon
dty	Dotyali
dua	Duala
dub	Dubli
duc	Duna
due	Umiray Dumaget Agta
duf	Dumbea
dug	Duruma
duh	Dungra Bhil
dui	Dumun
duk	Uyajitaya
dul	Alabat Island Agta
dun	Dusun Deyah
duo	Dupaninan Agta
dup	Duano
duq	Dusun Malang
dur	Dii
dus	Dumi
duu	Drung
duv	Duvle
duw	Dusun Witu
dux	Duungooma
dva	Duau
dwa	Diri
dwk	Dawik Kui
dwr	Dawro
dwu	Dhuwal
dww	Dawawa
dwy	Dhuwaya
dwz	Dewas Rai
dya	Dyan
dyi	Djimini Senoufo
dym	Yanda Dom Dogon
dyn	Dyangadi
dyo	Jola-Fonyi
dyu	Dyula
dyy	Djabugay
dza	Tunzu
dzg	Dazaga
dzl	Dzalakha
dzn	Dzando
dzo	Dzongkha
ebc	Beginci
ebg	Ebughu
ebk	Eastern Bontok
ebo	Teke-Ebo
ebr	Ebrié
ebu	Embu
ecs	Ecuadorian Sign Language
eee	E
efa	Efai
efe	Efe
efi	Efik
ega	Ega
egl	Emilian
ego	Eggon
ehu	Ehueun
eip	Eipomek
eit	Eitiep
eiv	Askopan
eja	Ejamat
eka	Ekajuk
eke	Ekit
ekg	Ekari
eki	Eki
ekk	Standard Estonian
ekl	Kol (Bangladesh)
ekm	Elip
eko	Koti
ekp	Ekpeye
ekr	Yace
eky	Eastern Kayah
ele	Elepi
elh	El Hugeirat
elk	Elkei
ell	Modern Greek (1453-)
elm	Eleme
elo	El Molo
elu	Elu
ema	Emai-Iuleha-Ora
emb	Embaloh
eme	Emerillon
emg	Eastern Meohang
emi	Mussau-Emira
emk	Eastern Maninkakan
emn	Eman
emp	Northern Emberá
ems	Pacific Gulf Yupik
emu	Eastern Muria
emw	Emplawas
emx	Erromintxela
ena	Apali
enb	Markweeta
enc	En
end	Ende
enf	Forest Enets
eng	English
enh	Tundra Enets
enl	Enlhet
enn	Engenni
eno	Enggano
enq	Enga
enr	Emumu
enu	Enu
env	Enwan (Edu State)
enw	Enwan (Akwa Ibom State)
enx	Enxet
eot	Beti (Côte dIvoire)
epi	Epie
era	Eravallan
erg	Sie
erh	Eruwa
eri	Ogea
erk	South Efate
ero	Horpa
ers	Ersu
ert	Eritai
erw	Erokwanas
ese	Ese Ejja
esg	Aheri Gondi
esh	Eshtehardi
esi	North Alaskan Inupiatun
esk	Northwest Alaska Inupiatun
esl	Egypt Sign Language
esn	Salvadoran Sign Language
eso	Estonian Sign Language
ess	Central Siberian Yupik
esu	Central Yupik
esy	Eskayan
etb	Etebi
eth	Ethiopian Sign Language
etn	Eton (Vanuatu)
eto	Eton (Cameroon)
etr	Edolo
ets	Yekhee
etu	Ejagham
etx	Eten
etz	Semimi
eus	Basque
eve	Even
evh	Uvbie
evn	Evenki
ewe	Ewe
ewo	Ewondo
ext	Extremaduran
eyo	Keiyo
eza	Ezaa
eze	Uzekwe
faa	Fasu
fab	Fa dAmbu
fad	Wagi
faf	Fagani
fag	Finongan
fah	Baissa Fali
fai	Faiwol
faj	Faita
fak	Fang (Cameroon)
fal	South Fali
fam	Fam
fan	Fang (Equatorial Guinea)
fao	Faroese
fap	Paloor
far	Fataleka
fat	Fanti
fau	Fayu
fax	Fala
fay	Southwestern Fars
faz	Northwestern Fars
fbl	West Albay Bikol
fcs	Quebec Sign Language
fer	Feroge
ffi	Foia Foia
ffm	Maasina Fulfulde
fgr	Fongoro
fia	Nobiin
fie	Fyer
fif	Faifi
fij	Fijian
fil	Filipino
fin	Finnish
fip	Fipa
fir	Firan
fit	Tornedalen Finnish
fiw	Fiwaga
fkk	Kirya-Konzəl
fkv	Kven Finnish
fla	Kalispel-Pend dOreille
flh	Foau
fli	Fali
fll	North Fali
flr	Fuliiru
fly	Flaaitaal
fmp	Fefe
fmu	Far Western Muria
fnb	Fanbak
fng	Fanagalo
fni	Fania
fod	Foodo
foi	Foi
fom	Foma
fon	Fon
for	Fore
fpe	Fernando Po Creole English
fqs	Fas
fra	French
frc	Cajun French
frd	Fordata
frp	Arpitan
frq	Forak
frr	Northern Frisian
frs	Eastern Frisian
frt	Fortsenal
fry	Western Frisian
fse	Finnish Sign Language
fsl	French Sign Language
fss	Finland-Swedish Sign Language
fub	Adamawa Fulfulde
fuc	Pulaar
fud	East Futuna
fue	Borgu Fulfulde
fuf	Pular
fuh	Western Niger Fulfulde
fui	Bagirmi Fulfulde
fuj	Ko
fum	Fum
fun	Fulniô
fuq	Central-Eastern Niger Fulfulde
fur	Friulian
fut	Futuna-Aniwa
fuu	Furu
fuv	Nigerian Fulfulde
fuy	Fuyug
fvr	Fur
fwa	Fwâi
fwe	Fwe
gaa	Ga
gab	Gabri
gac	Mixed Great Andamanese
gad	Gaddang
gae	Guarequena
gaf	Gende
gag	Gagauz
gah	Alekano
gai	Borei
gaj	Gadsup
gak	Gamkonora
gal	Galolen
gam	Kandawo
gan	Gan Chinese
gao	Gants
gap	Gal
gaq	Gata
gar	Galeya
gas	Adiwasi Garasia
gat	Kenati
gau	Mudhili Gadaba
gaw	Nobonob
gax	Borana-Arsi-Guji Oromo
gay	Gayo
gaz	West Central Oromo
gbb	Kaytetye
gbd	Karajarri
gbe	Niksek
gbf	Gaikundi
gbg	Gbanziri
gbh	Defi Gbe
gbi	Galela
gbj	Bodo Gadaba
gbk	Gaddi
gbl	Gamit
gbm	Garhwali
gbn	Moda
gbo	Northern Grebo
gbp	Gbaya-Bossangoa
gbq	Gbaya-Bozoum
gbr	Gbagyi
gbs	Gbesi Gbe
gbu	Gagadu
gbv	Gbanu
gbw	Gabi-Gabi
gbx	Eastern Xwla Gbe
gby	Gbari
gbz	Zoroastrian Dari
gcc	Mali
gcf	Guadeloupean Creole French
gcl	Grenadian Creole English
gcn	Gaina
gcr	Guianese Creole French
gct	Colonia Tovar German
gda	Gade Lohar
gdb	Pottangi Ollar Gadaba
gdd	Gedaged
gde	Gude
gdf	Guduf-Gava
gdg	Gadang
gdh	Gadjerawang
gdi	Gundi
gdj	Gurdjar
gdk	Gadang
gdl	Dirasha
gdm	Laal
gdn	Umanakaina
gdo	Ghodoberi
gdq	Mehri
gdr	Wipi
gds	Ghandruk Sign Language
gdu	Gudu
gdx	Godwari
gea	Geruma
geb	Kire
gec	Gboloo Grebo
ged	Gade
gef	Gerai
geg	Gengle
geh	Hutterite German
gei	Gebe
gej	Gen
gek	Ywom
gel	ut-Main
geq	Geme
ges	Geser-Gorom
gev	Eviya
gew	Gera
gex	Garre
gey	Enya
gfk	Patpatar
gga	Gao
ggb	Gbii
gge	Gurr-goni
ggg	Gurgula
ggl	Ganglau
ggt	Gitua
ggu	Gagu
ggw	Gogodala
gha	Ghadamès
ghe	Southern Ghale
ghh	Northern Ghale
ghk	Geko Karen
ghl	Ghulfan
ghn	Ghanongga
ghr	Ghera
ghs	Guhu-Samane
ght	Kuke
gia	Kija
gib	Gibanawa
gic	Gail
gid	Gidar
gie	Gaɓogbo
gig	Goaria
gih	Githabul
gil	Gilbertese
gim	Gimi (Eastern Highlands)
gin	Hinukh
gip	Gimi (West New Britain)
giq	Green Gelao
gir	Red Gelao
gis	North Giziga
git	Gitxsan
giu	Mulao
giw	White Gelao
gix	Gilima
giy	Giyug
giz	South Giziga
gji	Geji
gjk	Kachi Koli
gjn	Gonja
gjr	Gurindji Kriol
gju	Gujari
gka	Guya
gkd	Magɨ (Madang Province)
gke	Ndai
gkn	Gokana
gkp	Guinea Kpelle
gla	Scottish Gaelic
glc	Bon Gula
gld	Nanai
gle	Irish
glg	Galician
glh	Northwest Pashai
glj	Gula Iro
glk	Gilaki
glo	Galambu
glr	Glaro-Twabo
glu	Gula (Chad)
glv	Manx
glw	Glavda
gmb	Gulaalaa
gmd	Mághdì
gmg	Magɨyi
gmm	Gbaya-Mbodomo
gmn	Gimnime
gmr	Mirning
gmu	Gumalu
gmv	Gamo
gmx	Magoma
gmz	Mgbolizhia
gna	Kaansa
gnb	Gangte
gnd	Zulgo-Gemzek
gne	Ganang
gng	Ngangam
gnh	Lere
gni	Gooniyandi
gnj	Ngen
gnk	ǁGana
gnm	Ginuman
gnn	Gumatj
gno	Northern Gondi
gnq	Gana
gnt	Guntai
gnu	Gnau
gnw	Western Bolivian Guaraní
gnz	Ganzi
goa	Guro
gob	Playero
goc	Gorakor
god	Godié
goe	Gongduk
gof	Gofa
gog	Gogo
goi	Gobasi
goj	Gowlan
gok	Gowli
gol	Gola
gom	Goan Konkani
goo	Gone Dau
gop	Yeretuar
goq	Gorap
gor	Gorontalo
gos	Gronings
gou	Gavar
gow	Gorowa
gox	Gobu
goy	Goundo
goz	Gozarkhani
gpa	Gupa-Abawa
gpe	Ghanaian Pidgin English
gpn	Taiap
gqa	Gaanda
gqi	Guiqiong
gqr	Gor
gqu	Qau
gra	Rajput Garasia
grd	Guruntum-Mbaaru
grg	Madi
grh	Gbiri-Niragu
gri	Ghari
grj	Southern Grebo
grm	Kota Marudu Talantang
gro	Groma
grq	Gorovu
grr	Taznatit
grs	Gresi
grt	Garo
gru	Kistane
grv	Central Grebo
grw	Gweda
grx	Guriaso
gry	Barclayville Grebo
grz	Guramalum
gse	Ghanaian Sign Language
gsg	German Sign Language
gsl	Gusilay
gsm	Guatemalan Sign Language
gsn	Nema
gso	Southwest Gbaya
gsp	Wasembo
gss	Greek Sign Language
gsw	Swiss German
gta	Guató
gua	Shiki
gub	Guajajára
guc	Wayuu
gud	Yocoboué Dida
gue	Gurindji
guf	Gupapuyngu
gug	Paraguayan Guaraní
guh	Guahibo
gui	Eastern Bolivian Guaraní
guj	Gujarati
guk	Gumuz
gul	Sea Island Creole English
gum	Guambiano
gun	Mbyá Guaraní
guo	Guayabero
gup	Gunwinggu
guq	Aché
gur	Farefare
gus	Guinean Sign Language
gut	Maléku Jaíka
guu	Yanomamö
guw	Gun
gux	Gourmanchéma
guz	Gusii
gva	Guana (Paraguay)
gvc	Guanano
gve	Duwet
gvf	Golin
gvj	Guajá
gvl	Gulay
gvm	Gurmana
gvn	Kuku-Yalanji
gvo	Gavião Do Jiparaná
gvp	Pará Gavião
gvr	Gurung
gvs	Gumawana
gwa	Mbato
gwb	Gwa
gwc	Gawri
gwd	Gawwada
gwe	Gweno
gwf	Gowro
gwg	Moo
gwi	Gwichʼin
gwj	ǀGwi
gwn	Gwandara
gwr	Gwere
gwt	Gawar-Bati
gww	Kwini
gwx	Gua
gxx	Wè Southern
gya	Northwest Gbaya
gyb	Garus
gyd	Kayardild
gye	Gyem
gyg	Gbayi
gyi	Gyele
gyl	Gayil
gym	Ngäbere
gyn	Guyanese Creole English
gyo	Gyalsumdo
gyr	Guarayu
gza	Ganza
gzi	Gazi
gzn	Gane
haa	Han
hab	Hanoi Sign Language
hac	Gurani
had	Hatam
hae	Eastern Oromo
haf	Haiphong Sign Language
hag	Hanga
hah	Hahon
haj	Hajong
hak	Hakka Chinese
hal	Halang
ham	Hewa
han	Hangaza
hao	Hakö
hap	Hupla
haq	Ha
har	Harari
has	Haisla
hat	Haitian
hau	Hausa
hav	Havu
haw	Hawaiian
hax	Southern Haida
hay	Haya
haz	Hazaragi
hba	Hamba
hbb	Huba
hbn	Heiban
hbu	Habu
hca	Andaman Creole Hindi
hch	Huichol
hdn	Northern Haida
hds	Honduras Sign Language
hdy	Hadiyya
hea	Northern Qiandong Miao
heb	Hebrew
hed	Herdé
heg	Helong
heh	Hehe
hei	Heiltsuk
hem	Hemba
her	Herero
hgm	Haiǁom
hgw	Haigwai
hhi	Hoia Hoia
hhr	Kerak
hhy	Hoyahoya
hia	Lamang
hid	Hidatsa
hif	Fiji Hindi
hig	Kamwe
hih	Pamosu
hii	Hinduri
hij	Hijuk
hik	Seit-Kaitetu
hil	Hiligaynon
hin	Hindi
hio	Tsoa
hir	Himarimã
hiw	Hiw
hix	Hixkaryána
hji	Haji
hka	Kahe
hke	Hunde
hkk	Hunjara-Kaina Ke
hkn	Mel-Khaonh
hks	Hong Kong Sign Language
hla	Halia
hlb	Halbi
hld	Halang Doan
hle	Hlersu
hlt	Matu Chin
hma	Southern Mashan Hmong
hmb	Humburi Senni Songhay
hmc	Central Huishui Hmong
hmd	Large Flowery Miao
hme	Eastern Huishui Hmong
hmf	Hmong Don
hmg	Southwestern Guiyang Hmong
hmh	Southwestern Huishui Hmong
hmi	Northern Huishui Hmong
hmj	Ge
hml	Luopohe Hmong
hmm	Central Mashan Hmong
hmo	Hiri Motu
hmp	Northern Mashan Hmong
hmq	Eastern Qiandong Miao
hmr	Hmar
hms	Southern Qiandong Miao
hmt	Hamtai
hmu	Hamap
hmv	Hmong Dô
hmw	Western Mashan Hmong
hmy	Southern Guiyang Hmong
hmz	Hmong Shua
hna	Mina (Cameroon)
hnd	Southern Hindko
hne	Chhattisgarhi
hng	Hungu
hnh	ǁAni
hni	Hani
hnj	Hmong Njua
hnn	Hanunoo
hno	Northern Hindko
hns	Caribbean Hindustani
hnu	Hung
hoa	Hoava
hob	Mari (Madang Province)
hoc	Ho
hoe	Horom
hoh	Hobyót
hoi	Holikachuk
hoj	Hadothi
hol	Holu
hoo	Holoholo
hop	Hopi
hos	Ho Chi Minh City Sign Language
hot	Hote
hov	Hovongan
how	Honi
hoy	Holiya
hoz	Hozo
hps	Hawaii Sign Language (HSL)
hra	Hrangkhol
hrc	Niwer Mil
hre	Hre
hrk	Haruku
hrm	Horned Miao
hro	Haroi
hrt	Hértevin
hru	Hruso
hrv	Croatian
hrw	Warwar Feni
hrx	Hunsrik
hrz	Harzani
hsb	Upper Sorbian
hsh	Hungarian Sign Language
hsl	Hausa Sign Language
hsn	Xiang Chinese
hss	Harsusi
hto	Minica Huitoto
hts	Hadza
htu	Hitu
hub	Huambisa
huc	ǂHua
hud	Huaulu
hue	San Francisco Del Mar Huave
huf	Humene
hug	Huachipaeri
huh	Huilliche
hui	Huli
huj	Northern Guiyang Hmong
hul	Hula
hum	Hungana
hun	Hungarian
huo	Hu
hup	Hupa
huq	Tsat
hur	Halkomelem
hus	Huastec
hut	Humla
huu	Murui Huitoto
huv	San Mateo Del Mar Huave
hux	Nüpode Huitoto
huy	Hulaulá
huz	Hunzib
hvc	Haitian Vodoun Culture Language
hve	San Dionisio Del Mar Huave
hvk	Haveke
hvn	Sabu
hvv	Santa María Del Mar Huave
hwa	Wané
hwc	Hawaii Creole English
hwo	Hwana
hya	Hya
hye	Armenian
hyw	Western Armenian
iai	Iaai
ian	Iatmul
iar	Purari
iba	Iban
ibb	Ibibio
ibd	Iwaidja
ibe	Akpes
ibg	Ibanag
ibh	Bih
ibl	Ibaloi
ibm	Agoi
ibn	Ibino
ibo	Igbo
ibr	Ibuoro
ibu	Ibu
iby	Ibani
ica	Ede Ica
ich	Etkywan
icl	Icelandic Sign Language
icr	Islander Creole English
ida	Idakho-Isukha-Tiriki
idb	Indo-Portuguese
idc	Idon
idd	Ede Idaca
ide	Idere
idi	Idi
idr	Indri
ids	Idesa
idt	Idaté
idu	Idoma
ifa	Amganad Ifugao
ifb	Batad Ifugao
ife	Ifè
ifk	Tuwali Ifugao
ifm	Teke-Fuumu
ifu	Mayoyao Ifugao
ify	Keley-I Kallahan
igb	Ebira
ige	Igede
igg	Igana
igl	Igala
igm	Kanggape
ign	Ignaciano
igo	Isebe
igw	Igwe
ihb	Iha Based Pidgin
ihi	Ihievbe
ihp	Iha
iii	Sichuan Yi
ijc	Izon
ije	Biseni
ijj	Ede Ije
ijn	Kalabari
ijs	Southeast Ijo
ike	Eastern Canadian Inuktitut
iki	Iko
ikk	Ika
ikl	Ikulu
iko	Olulumo-Ikom
ikp	Ikpeshi
iks	Inuit Sign Language
ikt	Inuinnaqtun
ikv	Iku-Gora-Ankwa
ikw	Ikwere
ikx	Ik
ikz	Ikizu
ila	Ile Ape
ilb	Ila
ili	Ili Turki
ilk	Ilongot
ilm	Iranun (Malaysia)
ilo	Iloko
ilp	Iranun (Philippines)
ils	International Sign
ilu	Iliuun
ilv	Ilue
ima	Mala Malasar
imi	Anamgura
imn	Imonda
imo	Imbongu
imr	Imroing
inb	Inga
ind	Indonesian
ing	Degexitan
inh	Ingush
inj	Jungle Inga
inl	Indonesian Sign Language
inn	Isinai
ino	Inoke-Yate
inp	Iñapari
ins	Indian Sign Language
int	Intha
ior	Inor
iou	Tuma-Irumu
ipi	Ipili
ipo	Ipiko
iqu	Iquito
iqw	Ikwo
ire	Iresim
irh	Irarutu
iri	Rigwe
irk	Iraqw
irn	Irántxe
irr	Ir
iru	Irula
irx	Kamberau
iry	Iraya
isa	Isabi
isc	Isconahua
isd	Isnag
ise	Italian Sign Language
isg	Irish Sign Language
ish	Esan
isi	Nkem-Nkum
isk	Ishkashimi
isl	Icelandic
ism	Masimasi
isn	Isanzu
iso	Isoko
isr	Israeli Sign Language
ist	Istriot
isu	Isu (Menchum Division)
ita	Italian
itb	Binongan Itneg
itd	Southern Tidung
iti	Inlaod Itneg
itk	Judeo-Italian
itl	Itelmen
itm	Itu Mbon Uzo
ito	Itonama
itr	Iteri
its	Isekiri
itt	Maeng Itneg
itv	Itawit
itw	Ito
itx	Itik
ity	Moyadan Itneg
itz	Itzá
ium	Iu Mien
ivb	Ibatan
ivv	Ivatan
iwk	I-Wak
iwm	Iwam
iwo	Iwur
iws	Sepik Iwam
ixc	Ixcatec
ixl	Ixil
iya	Iyayu
iyo	Mesaka
iyx	Yaka (Congo)
izh	Ingrian
izr	Izere
izz	Izii
jaa	Jamamadí
jab	Hyam
jac	Popti
jad	Jahanka
jae	Yabem
jaf	Jara
jah	Jah Hut
jaj	Zazao
jak	Jakun
jal	Yalahatan
jam	Jamaican Creole English
jao	Yanyuwa
jaq	Yaqay
jas	New Caledonian Javanese
jat	Jakati
jau	Yaur
jav	Javanese
jax	Jambi Malay
jay	Yan-nhangu
jaz	Jawe
jbe	Judeo-Berber
jbj	Arandai
jbk	Barikewa
jbn	Nafusi
jbr	Jofotek-Bromnya
jbt	Jabutí
jbu	Jukun Takum
jcs	Jamaican Country Sign Language
jct	Krymchak
jda	Jad
jdg	Jadgali
jdt	Judeo-Tat
jeb	Jebero
jee	Jerung
jeh	Jeh
jei	Yei
jek	Jeri Kuo
jel	Yelmek
jen	Dza
jer	Jere
jet	Manem
jeu	Jonkor Bourmataguil
jge	Judeo-Georgian
jgk	Gwak
jgo	Ngomba
jhi	Jehai
jhs	Jhankot Sign Language
jia	Jina
jib	Jibu
jic	Tol
jid	Bu
jie	Jilbe
jig	Jingulu
jih	sTodsde
jii	Jiiddu
jil	Jilim
jim	Jimi (Cameroon)
jio	Jiamao
jiq	Guanyinqiao
jit	Jita
jiu	Youle Jinuo
jiv	Shuar
jiy	Buyuan Jinuo
jje	Jejueo
jjr	Bankal
jka	Kaera
jkm	Mobwa Karen
jko	Kubo
jkp	Paku Karen
jkr	Koro (India)
jku	Labir
jle	Ngile
jls	Jamaican Sign Language
jma	Dima
jmb	Zumbun
jmc	Machame
jmd	Yamdena
jmi	Jimi (Nigeria)
jml	Jumli
jmn	Makuri Naga
jmr	Kamara
jms	Mashi (Nigeria)
jmw	Mouwase
jmx	Western Juxtlahuaca Mixtec
jna	Jangshung
jnd	Jandavra
jni	Janji
jnj	Yemsa
jnl	Rawat
jns	Jaunsari
job	Joba
jod	Wojenaka
jog	Jogi
jos	Jordanian Sign Language
jow	Jowulu
jpn	Japanese
jpr	Judeo-Persian
jqr	Jaqaru
jra	Jarai
jrr	Jiru
jrt	Jorto
jru	Japrería
jsl	Japanese Sign Language
jua	Júma
jub	Wannu
jud	Worodougou
juh	Hõne
juk	Wapan
jul	Jirel
jum	Jumjum
jun	Juang
juo	Jiba
jup	Hupdë
jur	Jurúna
jus	Jumla Sign Language
juu	Ju
juw	Wãpha
juy	Juray
jvd	Javindo
jvn	Caribbean Javanese
jwi	Jwira-Pepesa
jya	Jiarong
jye	Judeo-Yemeni Arabic
jyy	Jaya
kaa	Kara-Kalpak
kab	Kabyle
kac	Kachin
kad	Adara
kaf	Katso
kag	Kajaman
kah	Kara (Central African Republic)
kai	Karekare
kaj	Jju
kak	Kalanguya
kal	Kalaallisut
kam	Kamba (Kenya)
kan	Kannada
kao	Xaasongaxango
kap	Bezhta
kaq	Capanahua
kas	Kashmiri
kat	Georgian
kav	Katukína
kax	Kao
kay	Kamayurá
kaz	Kazakh
kbc	Kadiwéu
kbd	Kabardian
kbe	Kanju
kbg	Khamba
kbh	Camsá
kbi	Kaptiau
kbj	Kari
kbk	Grass Koiari
kbl	Kanembu
kbm	Iwal
kbn	Kare (Central African Republic)
kbo	Keliko
kbp	Kabiyè
kbq	Kamano
kbr	Kafa
kbs	Kande
kbt	Abadi
kbu	Kabutra
kbv	Dera (Indonesia)
kbw	Kaiep
kbx	Ap Ma
kby	Manga Kanuri
kbz	Duhwa
kca	Khanty
kcb	Kawacha
kcc	Lubila
kcd	Ngkâlmpw Kanum
kce	Kaivi
kcf	Ukaan
kcg	Tyap
kch	Vono
kci	Kamantan
kcj	Kobiana
kck	Kalanga
kcl	Kela (Papua New Guinea)
kcm	Gula (Central African Republic)
kcn	Nubi
kco	Kinalakna
kcp	Kanga
kcq	Kamo
kcr	Katla
kcs	Koenoem
kct	Kaian
kcu	Kami (Tanzania)
kcv	Kete
kcw	Kabwari
kcx	Kachama-Ganjule
kcy	Korandje
kcz	Konongo
kdc	Kutu
kdd	Yankunytjatjara
kde	Makonde
kdf	Mamusi
kdg	Seba
kdh	Tem
kdi	Kumam
kdj	Karamojong
kdk	Numèè
kdl	Tsikimba
kdm	Kagoma
kdn	Kunda
kdp	Kaningdon-Nindem
kdq	Koch
kdr	Karaim
kdt	Kuy
kdu	Kadaru
kdw	Koneraw
kdx	Kam
kdy	Keder
kdz	Kwaja
kea	Kabuverdianu
keb	Kélé
kec	Keiga
ked	Kerewe
kee	Eastern Keres
kef	Kpessi
keg	Tese
keh	Keak
kei	Kei
kej	Kadar
kek	Kekchí
kel	Kela (Democratic Republic of Congo)
kem	Kemak
ken	Kenyang
keo	Kakwa
kep	Kaikadi
keq	Kamar
ker	Kera
kes	Kugbo
ket	Ket
keu	Akebu
kev	Kanikkaran
kew	West Kewa
kex	Kukna
key	Kupia
kez	Kukele
kfa	Kodava
kfb	Northwestern Kolami
kfc	Konda-Dora
kfd	Korra Koraga
kfe	Kota (India)
kff	Koya
kfg	Kudiya
kfh	Kurichiya
kfi	Kannada Kurumba
kfj	Kemiehua
kfk	Kinnauri
kfl	Kung
kfm	Khunsari
kfn	Kuk
kfo	Koro (Côte dIvoire)
kfp	Korwa
kfq	Korku
kfr	Kachhi
kfs	Bilaspuri
kft	Kanjari
kfu	Katkari
kfv	Kurmukar
kfw	Kharam Naga
kfx	Kullu Pahari
kfy	Kumaoni
kfz	Koromfé
kga	Koyaga
kgb	Kawe
kge	Komering
kgf	Kube
kgg	Kusunda
kgi	Selangor Sign Language
kgj	Gamale Kham
kgk	Kaiwá
kgn	Karingani
kgo	Krongo
kgp	Kaingang
kgq	Kamoro
kgr	Abun
kgs	Kumbainggar
kgt	Somyev
kgu	Kobol
kgv	Karas
kgw	Karon Dori
kgx	Kamaru
kgy	Kyerung
kha	Khasi
khb	Lü
khc	Tukang Besi North
khd	Bädi Kanum
khe	Korowai
khf	Khuen
khg	Khams Tibetan
khh	Kehu
khj	Kuturmi
khk	Halh Mongolian
khl	Lusi
khm	Khmer
khn	Khandesi
khp	Kapori
khq	Koyra Chiini Songhay
khr	Kharia
khs	Kasua
kht	Khamti
khu	Nkhumbi
khv	Khvarshi
khw	Khowar
khx	Kanu
khy	Kele (Democratic Republic of Congo)
khz	Keapara
kia	Kim
kib	Koalib
kic	Kickapoo
kid	Koshin
kie	Kibet
kif	Eastern Parbate Kham
kig	Kimaama
kih	Kilmeri
kij	Kilivila
kik	Kikuyu
kil	Kariya
kim	Karagas
kin	Kinyarwanda
kio	Kiowa
kip	Sheshi Kham
kiq	Kosadle
kir	Kirghiz
kis	Kis
kit	Agob
kiu	Kirmanjki (individual language)
kiv	Kimbu
kiw	Northeast Kiwai
kix	Khiamniungan Naga
kiy	Kirikiri
kiz	Kisi
kja	Mlap
kjb	Qanjobal
kjc	Coastal Konjo
kjd	Southern Kiwai
kje	Kisar
kjg	Khmu
kjh	Khakas
kji	Zabana
kjj	Khinalugh
kjk	Highland Konjo
kjl	Western Parbate Kham
kjm	Kháng
kjn	Kunjen
kjo	Harijan Kinnauri
kjp	Pwo Eastern Karen
kjq	Western Keres
kjr	Kurudu
kjs	East Kewa
kjt	Phrae Pwo Karen
kju	Kashaya
kjx	Ramopa
kjy	Erave
kjz	Bumthangkha
kka	Kakanda
kkb	Kwerisa
kkc	Odoodee
kkd	Kinuku
kke	Kakabe
kkf	Kalaktang Monpa
kkg	Mabaka Valley Kalinga
kkh	Khün
kki	Kagulu
kkj	Kako
kkk	Kokota
kkl	Kosarek Yale
kkm	Kiong
kkn	Kon Keu
kko	Karko
kkp	Gugubera
kkq	Kaeku
kkr	Kir-Balar
kks	Giiwo
kkt	Koi
kku	Tumi
kkv	Kangean
kkw	Teke-Kukuya
kkx	Kohin
kky	Guugu Yimidhirr
kkz	Kaska
klb	Kiliwa
klc	Kolbila
kld	Gamilaraay
kle	Kulung (Nepal)
klf	Kendeje
klg	Tagakaulo
klh	Weliki
kli	Kalumpang
klj	Khalaj
klk	Kono (Nigeria)
kll	Kagan Kalagan
klm	Migum
klo	Kapya
klp	Kamasa
klq	Rumu
klr	Khaling
kls	Kalasha
klt	Nukna
klu	Klao
klv	Maskelynes
klw	Tado
klx	Koluwawa
kly	Kalao
klz	Kabola
kma	Konni
kmb	Kimbundu
kmc	Southern Dong
kmd	Majukayang Kalinga
kme	Bakole
kmf	Kare (Papua New Guinea)
kmg	Kâte
kmh	Kalam
kmi	Kami (Nigeria)
kmj	Kumarbhag Paharia
kmk	Limos Kalinga
kml	Tanudan Kalinga
kmm	Kom (India)
kmn	Awtuw
kmo	Kwoma
kmp	Gimme
kmq	Kwama
kmr	Northern Kurdish
kms	Kamasau
kmt	Kemtuik
kmu	Kanite
kmv	Karipúna Creole French
kmw	Komo (Democratic Republic of Congo)
kmx	Waboda
kmy	Koma
kmz	Khorasani Turkish
kna	Dera (Nigeria)
knb	Lubuagan Kalinga
knc	Central Kanuri
knd	Konda
kne	Kankanaey
knf	Mankanya
kng	Koongo
kni	Kanufi
knj	Western Kanjobal
knk	Kuranko
knl	Keninjal
knm	Kanamarí
knn	Konkani (individual language)
kno	Kono (Sierra Leone)
knp	Kwanja
knq	Kintaq
knr	Kaningra
kns	Kensiu
knt	Panoan Katukína
knu	Kono (Guinea)
knv	Tabo
knw	Kung-Ekoka
knx	Kendayan
kny	Kanyok
knz	Kalamsé
koa	Konomala
kod	Kodi
koe	Kacipo-Balesi
kog	Cogui
koh	Koyo
koi	Komi-Permyak
kol	Kol (Papua New Guinea)
koo	Konzo
kop	Waube
koq	Kota (Gabon)
kor	Korean
kos	Kosraean
kot	Lagwan
kou	Koke
kov	Kudu-Camo
kow	Kugama
koy	Koyukon
koz	Korak
kpa	Kutto
kpb	Mullu Kurumba
kpc	Curripaco
kpd	Koba
kpf	Komba
kpg	Kapingamarangi
kph	Kplang
kpi	Kofei
kpj	Karajá
kpk	Kpan
kpl	Kpala
kpm	Koho
kpo	Ikposo
kpq	Korupun-Sela
kpr	Korafe-Yegha
kps	Tehit
kpt	Karata
kpu	Kafoa
kpv	Komi-Zyrian
kpw	Kobon
kpx	Mountain Koiali
kpy	Koryak
kpz	Kupsabiny
kqa	Mum
kqb	Kovai
kqc	Doromu-Koki
kqd	Koy Sanjaq Surat
kqe	Kalagan
kqf	Kakabai
kqg	Khe
kqh	Kisankasa
kqi	Koitabu
kqj	Koromira
kqk	Kotafon Gbe
kql	Kyenele
kqm	Khisa
kqn	Kaonde
kqo	Eastern Krahn
kqp	Kimré
kqq	Krenak
kqr	Kimaragang
kqs	Northern Kissi
kqt	Klias River Kadazan
kqv	Okolod
kqw	Kandas
kqx	Mser
kqy	Koorete
kra	Kumhali
krc	Karachay-Balkar
krd	Kairui-Midiki
kre	Panará
krf	Koro (Vanuatu)
krh	Kurama
kri	Krio
krj	Kinaray-A
krl	Karelian
krn	Sapo
krp	Korop
krr	Krung
krs	Gbaya (Sudan)
krt	Tumari Kanuri
kru	Kurukh
krv	Kavet
krw	Western Krahn
krx	Karon
kry	Kryts
krz	Sota Kanum
ksa	Shuwa-Zamani
ksb	Shambala
ksc	Southern Kalinga
ksd	Kuanua
kse	Kuni
ksf	Bafia
ksg	Kusaghe
ksh	Kölsch
ksi	Krisa
ksj	Uare
ksk	Kansa
ksl	Kumalu
ksm	Kumba
ksn	Kasiguranin
kso	Kofa
ksp	Kaba
ksq	Kwaami
ksr	Borong
kss	Southern Kisi
kst	Winyé
ksu	Khamyang
ksv	Kusu
ksw	Sgaw Karen
ksx	Kedang
ksy	Kharia Thar
ksz	Kodaku
kta	Katua
ktb	Kambaata
ktc	Kholok
ktd	Kokata
kte	Nubri
ktf	Kwami
kth	Karanga
kti	North Muyu
ktj	Plapo Krumen
ktl	Koroshi
ktm	Kurti
ktn	Karitiâna
kto	Kuot
ktp	Kaduo
kts	South Muyu
ktt	Ketum
ktu	Kituba (Democratic Republic of Congo)
ktv	Eastern Katu
ktx	Kaxararí
kty	Kango (Bas-Uélé District)
ktz	Juǀʼhoan
kua	Kuanyama
kub	Kutep
kuc	Kwinsu
kud	Auhelawa
kue	Kuman (Papua New Guinea)
kuf	Western Katu
kug	Kupa
kuh	Kushi
kui	Kuikúro-Kalapálo
kuj	Kuria
kuk	Kepo
kul	Kulere
kum	Kumyk
kun	Kunama
kuo	Kumukio
kup	Kunimaipa
kuq	Karipuna
kus	Kusaal
kut	Kutenai
kuu	Upper Kuskokwim
kuv	Kur
kuw	Kpagua
kux	Kukatja
kuy	Kuuku-Yau
kva	Bagvalal
kvb	Kubu
kvc	Kove
kvd	Kui (Indonesia)
kve	Kalabakan
kvf	Kabalai
kvg	Kuni-Boazi
kvh	Komodo
kvi	Kwang
kvj	Psikye
kvk	Korean Sign Language
kvl	Kayaw
kvm	Kendem
kvn	Border Kuna
kvo	Dobel
kvp	Kompane
kvq	Geba Karen
kvr	Kerinci
kvt	Lahta Karen
kvu	Yinbaw Karen
kvv	Kola
kvw	Wersing
kvx	Parkari Koli
kvy	Yintale Karen
kvz	Tsakwambo
kwa	Dâw
kwb	Kwa
kwc	Likwala
kwd	Kwaio
kwe	Kwerba
kwf	Kwaraae
kwg	Sara Kaba Deme
kwh	Kowiai
kwi	Awa-Cuaiquer
kwj	Kwanga
kwk	Kwakiutl
kwl	Kofyar
kwm	Kwambi
kwn	Kwangali
kwo	Kwomtari
kwp	Kodia
kwr	Kwer
kws	Kwese
kwt	Kwesten
kwu	Kwakum
kwv	Sara Kaba Náà
kww	Kwinti
kwx	Khirwar
kwy	San Salvador Kongo
kxa	Kairiru
kxb	Krobu
kxc	Konso
kxd	Brunei
kxf	Manumanaw Karen
kxh	Karo (Ethiopia)
kxi	Keningau Murut
kxj	Kulfa
kxk	Zayein Karen
kxm	Northern Khmer
kxn	Kanowit-Tanjong Melanau
kxp	Wadiyara Koli
kxq	Smärky Kanum
kxr	Koro (Papua New Guinea)
kxs	Kangjia
kxt	Koiwat
kxv	Kuvi
kxw	Konai
kxx	Likuba
kxy	Kayong
kxz	Kerewo
kya	Kwaya
kyb	Butbut Kalinga
kyc	Kyaka
kyd	Karey
kye	Krache
kyf	Kouya
kyg	Keyagana
kyh	Karok
kyi	Kiput
kyj	Karao
kyk	Kamayo
kyl	Kalapuya
kym	Kpatili
kyn	Northern Binukidnon
kyo	Kelon
kyp	Kang
kyq	Kenga
kyr	Kuruáya
kys	Baram Kayan
kyt	Kayagar
kyu	Western Kayah
kyv	Kayort
kyw	Kudmali
kyx	Rapoisi
kyy	Kambaira
kyz	Kayabí
kza	Western Karaboro
kzb	Kaibobo
kzc	Bondoukou Kulango
kzd	Kadai
kze	Kosena
kzf	Daa Kaili
kzg	Kikai
kzi	Kelabit
kzl	Kayeli
kzm	Kais
kzn	Kokola
kzo	Kaningi
kzp	Kaidipang
kzq	Kaike
kzr	Karang
kzs	Sugut Dusun
kzu	Kayupulau
kzv	Komyandaret
kzy	Kango (Tshopo District)
kzz	Kalabra
laa	Southern Subanen
lac	Lacandon
lad	Ladino
lae	Pattani
laf	Lafofa
lag	Langi
lai	Lambya
laj	Lango (Uganda)
lak	Laka (Nigeria)
lal	Lalia
lam	Lamba
lan	Laru
lao	Lao
lap	Laka (Chad)
laq	Qabiao
lar	Larteh
las	Lama (Togo)
lau	Laba
law	Lauje
lax	Tiwa
lay	Lama Bai
lbb	Label
lbc	Lakkia
lbe	Lak
lbf	Tinani
lbg	Laopang
lbi	Labi
lbj	Ladakhi
lbk	Central Bontok
lbl	Libon Bikol
lbm	Lodhi
lbn	Rmeet
lbo	Laven
lbq	Wampar
lbr	Lohorung
lbs	Libyan Sign Language
lbt	Lachi
lbu	Labu
lbv	Lavatbura-Lamusong
lbw	Tolaki
lbx	Lawangan
lbz	Lardil
lcc	Legenyem
lcd	Lola
lce	Loncong
lcf	Lubu
lch	Luchazi
lcl	Lisela
lcm	Tungag
lcp	Western Lawa
lcq	Luhu
lcs	Lisabata-Nuniali
lda	Kla-Dan
ldb	Dũya
ldd	Luri
ldg	Lenyima
ldh	Lamja-Dengsa-Tola
ldi	Laari
ldj	Lemoro
ldk	Leelau
ldl	Kaan
ldm	Landoma
ldo	Loo
ldp	Tso
ldq	Lufu
lea	Lega-Shabunda
leb	Lala-Bisa
lec	Leco
led	Lendu
lee	Lyélé
lef	Lelemi
leh	Lenje
lei	Lemio
lej	Lengola
lek	Leipon
lel	Lele (Democratic Republic of Congo)
lem	Nomaande
leo	Leti (Cameroon)
lep	Lepcha
leq	Lembena
ler	Lenkau
les	Lese
let	Lesing-Gelimi
leu	Kara (Papua New Guinea)
lev	Lamma
lew	Ledo Kaili
lex	Luang
ley	Lemolang
lez	Lezghian
lfa	Lefa
lga	Lungga
lgb	Laghu
lgg	Lugbara
lgh	Laghuu
lgi	Lengilu
lgk	Lingarak
lgl	Wala
lgm	Lega-Mwenga
lgn	Tapo
lgq	Logba
lgr	Lengo
lgt	Pahi
lgu	Longgu
lgz	Ligenza
lha	Laha (Viet Nam)
lhh	Laha (Indonesia)
lhi	Lahu Shi
lhl	Lahul Lohar
lhm	Lhomi
lhn	Lahanan
lhp	Lhokpu
lht	Lo-Toga
lhu	Lahu
lia	West-Central Limba
lib	Likum
lic	Hlai
lid	Nyindrou
lie	Likila
lif	Limbu
lig	Ligbi
lih	Lihir
lij	Ligurian
lik	Lika
lil	Lillooet
lim	Limburgan
lin	Lingala
lio	Liki
lip	Sekpele
liq	Libido
lir	Liberian English
lis	Lisu
lit	Lithuanian
liu	Logorik
liv	Liv
liw	Col
lix	Liabuku
liy	Banda-Bambari
liz	Libinza
lje	Rampi
lji	Laiyolo
ljl	Lio
ljp	Lampung Api
ljw	Yirandali
lka	Lakalei
lkb	Kabras
lkc	Kucong
lkd	Lakondê
lke	Kenyi
lkh	Lakha
lki	Laki
lkj	Remun
lkl	Laeko-Libuat
lkn	Lakon
lko	Khayo
lkr	Päri
lks	Kisa
lkt	Lakota
lky	Lokoya
lla	Lala-Roba
llb	Lolo
llc	Lele (Guinea)
lld	Ladin
lle	Lele (Papua New Guinea)
llg	Lole
llh	Lamu
lli	Teke-Laali
lll	Lilau
llm	Lasalimu
lln	Lele (Chad)
llp	North Efate
llq	Lolak
lls	Lithuanian Sign Language
llu	Lau
llx	Lauan
lma	East Limba
lmb	Merei
lmd	Lumun
lme	Pévé
lmf	South Lembata
lmg	Lamogai
lmh	Lambichhong
lmi	Lombi
lmj	West Lembata
lmk	Lamkang
lml	Hano
lmn	Lambadi
lmo	Lombard
lmp	Limbum
lmq	Lamatuka
lmr	Lamalera
lmu	Lamenu
lmv	Lomaiviti
lmw	Lake Miwok
lmx	Laimbue
lmy	Lamboya
lna	Langbashe
lnb	Mbalanhu
lnd	Lundayeh
lnh	Lanoh
lni	Daantanai
lnl	South Central Banda
lnm	Langam
lnn	Lorediakarkar
lno	Lango (South Sudan)
lns	Lamnso
lnu	Longuda
lnz	Lonzo
loa	Loloda
lob	Lobi
loc	Inonhan
loe	Saluan
lof	Logol
log	Logo
loh	Narim
loi	Loma (Côte dIvoire)
loj	Lou
lok	Loko
lol	Mongo
lom	Loma (Liberia)
lon	Malawi Lomwe
loo	Lombo
lop	Lopa
loq	Lobala
lor	Téén
los	Loniu
lot	Otuho
lou	Louisiana Creole
lov	Lopi
low	Tampias Lobu
lox	Loun
loy	Loke
loz	Lozi
lpa	Lelepa
lpe	Lepki
lpn	Long Phuri Naga
lpo	Lipo
lpx	Lopit
lra	Rara Bakati
lrc	Northern Luri
lri	Marachi
lrk	Loarki
lrl	Lari
lrm	Marama
lrn	Lorang
lro	Laro
lrr	Southern Yamphu
lrt	Larantuka Malay
lrv	Larevat
lrz	Lemerig
lsa	Lasgerdi
lsd	Lishana Deni
lse	Lusengo
lsh	Lish
lsi	Lashi
lsl	Latvian Sign Language
lsm	Saamia
lsn	Tibetan Sign Language
lso	Laos Sign Language
lsp	Panamanian Sign Language
lsr	Aruop
lss	Lasi
lst	Trinidad and Tobago Sign Language
lsv	Sivia Sign Language
lsy	Mauritian Sign Language
ltg	Latgalian
lth	Thur
lti	Leti (Indonesia)
ltn	Latundê
lto	Tsotso
lts	Tachoni
ltu	Latu
ltz	Luxembourgish
lua	Luba-Lulua
lub	Luba-Katanga
luc	Aringa
lud	Ludian
lue	Luvale
luf	Laua
lug	Ganda
luj	Luna
luk	Lunanakha
lul	Olubo
lum	Luimbi
lun	Lunda
luo	Luo (Kenya and Tanzania)
lup	Lumbu
luq	Lucumi
lur	Laura
lus	Lushai
lut	Lushootseed
luu	Lumba-Yakkha
luv	Luwati
luw	Luo (Cameroon)
luz	Southern Luri
lva	Makua
lvi	Lavi
lvk	Lavukaleve
lvs	Standard Latvian
lvu	Levuka
lwa	Lwalu
lwe	Lewo Eleng
lwg	Wanga
lwh	White Lachi
lwl	Eastern Lawa
lwm	Laomian
lwo	Luwo
lws	Malawian Sign Language
lwt	Lewotobi
lwu	Lawu
lww	Lewo
lya	Layakha
lyg	Lyngngam
lyn	Luyana
lzl	Litzlitz
lzn	Leinong Naga
lzz	Laz
maa	San Jerónimo Tecóatl Mazatec
mab	Yutanduchi Mixtec
mad	Madurese
mae	Bo-Rukul
maf	Mafa
mag	Magahi
mah	Marshallese
mai	Maithili
maj	Jalapa De Díaz Mazatec
mak	Makasar
mal	Malayalam
mam	Mam
maq	Chiquihuitlán Mazatec
mar	Marathi
mas	Masai
mat	San Francisco Matlatzinca
mau	Huautla Mazatec
mav	Sateré-Mawé
maw	Mampruli
max	North Moluccan Malay
maz	Central Mazahua
mba	Higaonon
mbb	Western Bukidnon Manobo
mbc	Macushi
mbd	Dibabawon Manobo
mbf	Baba Malay
mbh	Mangseng
mbi	Ilianen Manobo
mbj	Nadëb
mbk	Malol
mbl	Maxakalí
mbm	Ombamba
mbn	Macaguán
mbo	Mbo (Cameroon)
mbp	Malayo
mbq	Maisin
mbr	Nukak Makú
mbs	Sarangani Manobo
mbt	Matigsalug Manobo
mbu	Mbula-Bwazza
mbv	Mbulungish
mbw	Maring
mbx	Mari (East Sepik Province)
mby	Memoni
mbz	Amoltepec Mixtec
mca	Maca
mcb	Machiguenga
mcc	Bitur
mcd	Sharanahua
mce	Itundujia Mixtec
mcf	Matsés
mcg	Mapoyo
mch	Maquiritari
mci	Mese
mcj	Mvanip
mck	Mbunda
mcm	Malaccan Creole Portuguese
mcn	Masana
mco	Coatlán Mixe
mcp	Makaa
mcq	Ese
mcr	Menya
mcs	Mambai
mct	Mengisa
mcu	Cameroon Mambila
mcv	Minanibai
mcw	Mawa (Chad)
mcx	Mpiemo
mcy	South Watut
mcz	Mawan
mda	Mada (Nigeria)
mdb	Morigi
mdc	Male (Papua New Guinea)
mdd	Mbum
mde	Maba (Chad)
mdf	Moksha
mdg	Massalat
mdh	Maguindanaon
mdi	Mamvu
mdj	Mangbetu
mdk	Mangbutu
mdl	Maltese Sign Language
mdm	Mayogo
mdn	Mbati
mdp	Mbala
mdq	Mbole
mdr	Mandar
mds	Maria (Papua New Guinea)
mdt	Mbere
mdu	Mboko
mdv	Santa Lucía Monteverde Mixtec
mdw	Mbosi
mdx	Dizin
mdy	Male (Ethiopia)
mdz	Suruí Do Pará
mea	Menka
meb	Ikobi
mec	Marra
med	Melpa
mee	Mengen
mef	Megam
meh	Southwestern Tlaxiaco Mixtec
mei	Midob
mej	Meyah
mek	Mekeo
mel	Central Melanau
men	Mende (Sierra Leone)
meo	Kedah Malay
mep	Miriwoong
meq	Merey
mer	Meru
mes	Masmaje
met	Mato
meu	Motu
mev	Mano
mew	Maaka
mey	Hassaniyya
mez	Menominee
mfa	Pattani Malay
mfb	Bangka
mfc	Mba
mfd	Mendankwe-Nkwen
mfe	Morisyen
mff	Naki
mfg	Mogofin
mfh	Matal
mfi	Wandala
mfj	Mefele
mfk	North Mofu
mfl	Putai
mfm	Marghi South
mfn	Cross River Mbembe
mfo	Mbe
mfp	Makassar Malay
mfq	Moba
mfr	Marrithiyel
mfs	Mexican Sign Language
mft	Mokerang
mfu	Mbwela
mfv	Mandjak
mfx	Melo
mfy	Mayo
mfz	Mabaan
mgb	Mararit
mgc	Morokodo
mgd	Moru
mge	Mango
mgf	Maklew
mgg	Mpumpong
mgh	Makhuwa-Meetto
mgi	Lijili
mgj	Abureni
mgk	Mawes
mgl	Maleu-Kilenge
mgm	Mambae
mgn	Mbangi
mgo	Meta
mgp	Eastern Magar
mgq	Malila
mgr	Mambwe-Lungu
mgs	Manda (Tanzania)
mgt	Mongol
mgu	Mailu
mgv	Matengo
mgw	Matumbi
mgy	Mbunga
mgz	Mbugwe
mha	Manda (India)
mhb	Mahongwe
mhc	Mocho
mhd	Mbugu
mhe	Besisi
mhf	Mamaa
mhg	Margu
mhi	Madi
mhj	Mogholi
mhk	Mungaka
mhl	Mauwake
mhm	Makhuwa-Moniga
mhn	Mócheno
mho	Mashi (Zambia)
mhp	Balinese Malay
mhq	Mandan
mhr	Eastern Mari
mhs	Buru (Indonesia)
mht	Mandahuaca
mhu	Digaro-Mishmi
mhw	Mbukushu
mhx	Maru
mhy	Maanyan
mhz	Mor (Mor Islands)
mia	Miami
mib	Atatláhuca Mixtec
mic	Mikmaq
mid	Mandaic
mie	Ocotepec Mixtec
mif	Mofu-Gudur
mig	San Miguel El Grande Mixtec
mih	Chayuco Mixtec
mii	Chigmecatitlán Mixtec
mij	Abar
mik	Mikasuki
mil	Peñoles Mixtec
mim	Alacatlatzala Mixtec
min	Minangkabau
mio	Pinotepa Nacional Mixtec
mip	Apasco-Apoala Mixtec
miq	Mískito
mir	Isthmus Mixe
mit	Southern Puebla Mixtec
miu	Cacaloxtepec Mixtec
miw	Akoye
mix	Mixtepec Mixtec
miy	Ayutla Mixtec
miz	Coatzospan Mixtec
mjb	Makalero
mjc	San Juan Colorado Mixtec
mjd	Northwest Maidu
mjg	Tu
mjh	Mwera (Nyasa)
mji	Kim Mun
mjj	Mawak
mjk	Matukar
mjl	Mandeali
mjm	Medebur
mjn	Ma (Papua New Guinea)
mjo	Malankuravan
mjp	Malapandaram
mjr	Malavedan
mjs	Miship
mjt	Sauria Paharia
mju	Manna-Dora
mjv	Mannan
mjw	Karbi
mjx	Mahali
mjz	Majhi
mka	Mbre
mkb	Mal Paharia
mkc	Siliput
mkd	Macedonian
mke	Mawchi
mkf	Miya
mkg	Mak (China)
mki	Dhatki
mkj	Mokilese
mkk	Byep
mkl	Mokole
mkm	Moklen
mkn	Kupang Malay
mko	Mingang Doso
mkp	Moikodi
mkr	Malas
mks	Silacayoapan Mixtec
mkt	Vamale
mku	Konyanka Maninka
mkv	Mafea
mkw	Kituba (Congo)
mkx	Kinamiging Manobo
mky	East Makian
mkz	Makasae
mla	Malo
mlb	Mbule
mlc	Cao Lan
mle	Manambu
mlf	Mal
mlh	Mape
mli	Malimpung
mlj	Miltu
mlk	Ilwana
mll	Malua Bay
mlm	Mulam
mln	Malango
mlo	Mlomp
mlp	Bargam
mlq	Western Maninkakan
mlr	Vame
mls	Masalit
mlt	Maltese
mlu	Toabaita
mlv	Motlav
mlw	Moloko
mlx	Malfaxal
mlz	Malaynon
mma	Mama
mmb	Momina
mmc	Michoacán Mazahua
mmd	Maonan
mme	Mae
mmf	Mundat
mmg	North Ambrym
mmh	Mehináku
mmi	Musar
mmj	Majhwar
mmk	Mukha-Dora
mml	Man Met
mmm	Maii
mmn	Mamanwa
mmo	Mangga Buang
mmp	Siawi
mmq	Musak
mmr	Western Xiangxi Miao
mmt	Malalamai
mmu	Mmaala
mmw	Emae
mmx	Madak
mmy	Migaama
mmz	Mabaale
mna	Mbula
mnb	Muna
mnc	Manchu
mnd	Mondé
mne	Naba
mnf	Mundani
mng	Eastern Mnong
mnh	Mono (Democratic Republic of Congo)
mni	Manipuri
mnj	Munji
mnk	Mandinka
mnl	Tiale
mnm	Mapena
mnn	Southern Mnong
mnp	Min Bei Chinese
mnq	Minriq
mnr	Mono (USA)
mns	Mansi
mnu	Mer
mnv	Rennell-Bellona
mnw	Mon
mnx	Manikion
mny	Manyawa
mnz	Moni
moa	Mwan
moc	Mocoví
moe	Innu
mog	Mongondow
moh	Mohawk
moi	Mboi
moj	Monzombo
mok	Morori
moo	Monom
mop	Mopán Maya
moq	Mor (Bomberai Peninsula)
mor	Moro
mos	Mossi
mot	Barí
mou	Mogum
mov	Mohave
mow	Moi (Congo)
mox	Molima
moy	Shekkacho
moz	Mukulu
mpa	Mpoto
mpb	Malak Malak
mpc	Mangarrayi
mpd	Machinere
mpe	Majang
mpg	Marba
mph	Maung
mpi	Mpade
mpj	Martu Wangka
mpk	Mbara (Chad)
mpl	Middle Watut
mpm	Yosondúa Mixtec
mpn	Mindiri
mpo	Miu
mpp	Migabac
mpq	Matís
mpr	Vangunu
mps	Dadibi
mpt	Mian
mpu	Makuráp
mpv	Mungkip
mpw	Mapidian
mpx	Misima-Panaeati
mpy	Mapia
mpz	Mpi
mqa	Maba (Indonesia)
mqb	Mbuko
mqc	Mangole
mqe	Matepi
mqf	Momuna
mqg	Kota Bangun Kutai Malay
mqh	Tlazoyaltepec Mixtec
mqi	Mariri
mqj	Mamasa
mqk	Rajah Kabunsuwan Manobo
mql	Mbelime
mqm	South Marquesan
mqn	Moronene
mqo	Modole
mqp	Manipa
mqq	Minokok
mqr	Mander
mqs	West Makian
mqt	Mok
mqu	Mandari
mqv	Mosimo
mqw	Murupi
mqx	Mamuju
mqy	Manggarai
mqz	Pano
mra	Mlabri
mrb	Marino
mrc	Maricopa
mrd	Western Magar
mrf	Elseng
mrg	Mising
mrh	Mara Chin
mri	Maori
mrj	Western Mari
mrk	Hmwaveke
mrl	Mortlockese
mrm	Merlav
mrn	Cheke Holo
mro	Mru
mrp	Morouas
mrq	North Marquesan
mrr	Maria (India)
mrs	Maragus
mrt	Marghi Central
mru	Mono (Cameroon)
mrv	Mangareva
mrw	Maranao
mrx	Maremgi
mry	Mandaya
mrz	Marind
msb	Masbatenyo
msc	Sankaran Maninka
msd	Yucatec Maya Sign Language
mse	Musey
msf	Mekwei
msg	Moraid
msh	Masikoro Malagasy
msi	Sabah Malay
msj	Ma (Democratic Republic of Congo)
msk	Mansaka
msl	Molof
msm	Agusan Manobo
msn	Vurës
mso	Mombum
msq	Caac
msr	Mongolian Sign Language
mss	West Masela
msu	Musom
msv	Maslam
msw	Mansoanka
msx	Moresada
msy	Aruamu
msz	Momare
mta	Cotabato Manobo
mtb	Anyin Morofo
mtc	Munit
mtd	Mualang
mte	Mono (Solomon Islands)
mtf	Murik (Papua New Guinea)
mtg	Una
mth	Munggui
mti	Maiwa (Papua New Guinea)
mtj	Moskona
mtk	Mbe
mtl	Montol
mto	Totontepec Mixe
mtp	Wichí Lhamtés Nocten
mtq	Muong
mtr	Mewari
mts	Yora
mtt	Mota
mtu	Tututepec Mixtec
mtv	Asaroo
mtw	Southern Binukidnon
mtx	Tidaá Mixtec
mty	Nabi
mua	Mundang
mub	Mubi
muc	Ajumbu
mud	Mednyj Aleut
mue	Media Lengua
mug	Musgu
muh	Mündü
mui	Musi
muj	Mabire
muk	Mugom
mum	Maiwala
muo	Nyong
mup	Malvi
muq	Eastern Xiangxi Miao
mur	Murle
mus	Creek
mut	Western Muria
muu	Yaaku
muv	Muthuvan
mux	Bo-Ung
muy	Muyang
muz	Mursi
mva	Manam
mvd	Mamboru
mve	Marwari (Pakistan)
mvf	Peripheral Mongolian
mvg	Yucuañe Mixtec
mvh	Mulgi
mvi	Miyako
mvk	Mekmek
mvm	Muya
mvn	Minaveha
mvo	Marovo
mvp	Duri
mvq	Moere
mvr	Marau
mvs	Massep
mvt	Mpotovoro
mvu	Marfa
mvv	Tagal Murut
mvw	Machinga
mvx	Meoswar
mvy	Indus Kohistani
mvz	Mesqan
mwa	Mwatebu
mwb	Juwal
mwc	Are
mwe	Mwera (Chimwera)
mwf	Murrinh-Patha
mwg	Aiklep
mwh	Mouk-Aria
mwi	Labo
mwk	Kita Maninkakan
mwl	Mirandese
mwm	Sar
mwn	Nyamwanga
mwo	Central Maewo
mwp	Kala Lagaw Ya
mwq	Mün Chin
mws	Mwimbi-Muthambi
mwt	Moken
mwv	Mentawai
mww	Hmong Daw
mwz	Moingi
mxa	Northwest Oaxaca Mixtec
mxb	Tezoatlán Mixtec
mxc	Manyika
mxd	Modang
mxe	Mele-Fila
mxf	Malgbe
mxg	Mbangala
mxh	Mvuba
mxj	Miju-Mishmi
mxk	Monumbo
mxl	Maxi Gbe
mxm	Meramera
mxn	Moi (Indonesia)
mxo	Mbowe
mxp	Tlahuitoltepec Mixe
mxq	Juquila Mixe
mxr	Murik (Malaysia)
mxs	Huitepec Mixtec
mxt	Jamiltepec Mixtec
mxu	Mada (Cameroon)
mxv	Metlatónoc Mixtec
mxw	Namo
mxx	Mahou
mxy	Southeastern Nochixtlán Mixtec
mxz	Central Masela
mya	Burmese
myb	Mbay
myc	Mayeka
mye	Myene
myf	Bambassi
myg	Manta
myh	Makah
myj	Mangayat
myk	Mamara Senoufo
myl	Moma
mym	Meen
myo	Anfillo
myp	Pirahã
myr	Muniche
myu	Mundurukú
myv	Erzya
myw	Muyuw
myx	Masaaba
myy	Macuna
mza	Santa María Zacatepec Mixtec
mzb	Tumzabt
mzc	Madagascar Sign Language
mzd	Malimba
mze	Morawa
mzg	Monastic Sign Language
mzh	Wichí Lhamtés Güisnay
mzi	Ixcatlán Mazatec
mzj	Manya
mzk	Nigeria Mambila
mzl	Mazatlán Mixe
mzm	Mumuye
mzn	Mazanderani
mzp	Movima
mzq	Mori Atas
mzr	Marúbo
mzs	Macanese
mzt	Mintil
mzu	Inapang
mzv	Manza
mzw	Deg
mzx	Mawayana
mzy	Mozambican Sign Language
mzz	Maiadomu
naa	Namla
nab	Southern Nambikuára
nac	Narak
naf	Nabak
nag	Naga Pidgin
naj	Nalu
nak	Nakanai
nal	Nalik
nam	Ngangityemerri
nan	Min Nan Chinese
nao	Naaba
nap	Neapolitan
naq	Khoekhoe
nar	Iguta
nas	Naasioi
nat	Ca̱hungwa̱rya̱
nau	Nauru
nav	Navajo
naw	Nawuri
nax	Nakwi
naz	Coatepec Nahuatl
nba	Nyemba
nbb	Ndoe
nbc	Chang Naga
nbd	Ngbinda
nbe	Konyak Naga
nbg	Nagarchal
nbh	Ngamo
nbi	Mao Naga
nbj	Ngarinyman
nbk	Nake
nbl	South Ndebele
nbm	Ngbaka Mabo
nbn	Kuri
nbo	Nkukoli
nbp	Nnam
nbq	Nggem
nbr	Numana
nbs	Namibian Sign Language
nbt	Na
nbu	Rongmei Naga
nbv	Ngamambo
nbw	Southern Ngbandi
nby	Ningera
nca	Iyo
ncb	Central Nicobarese
ncc	Ponam
ncd	Nachering
nce	Yale
ncf	Notsi
ncg	Nisgaa
nch	Central Huasteca Nahuatl
ncj	Northern Puebla Nahuatl
nck	Na-kara
ncl	Michoacán Nahuatl
ncm	Nambo
ncn	Nauna
nco	Sibe
ncq	Northern Katang
ncr	Ncane
ncs	Nicaraguan Sign Language
nct	Chothe Naga
ncu	Chumburung
ncx	Central Puebla Nahuatl
nda	Ndasa
ndb	Kenswei Nsei
ndc	Ndau
ndd	Nde-Nsele-Nta
nde	North Ndebele
ndg	Ndengereko
ndh	Ndali
ndi	Samba Leko
ndj	Ndamba
ndk	Ndaka
ndl	Ndolo
ndm	Ndam
ndn	Ngundi
ndo	Ndonga
ndp	Ndo
ndq	Ndombe
ndr	Ndoola
nds	Low German
ndt	Ndunga
ndu	Dugun
ndv	Ndut
ndw	Ndobo
ndx	Nduga
ndy	Lutos
ndz	Ndogo
nea	Eastern Ngada
neb	Toura (Côte dIvoire)
nec	Nedebang
ned	Nde-Gbite
nee	Nêlêmwa-Nixumwak
nef	Nefamese
neg	Negidal
neh	Nyenkha
nej	Neko
nek	Neku
nem	Nemi
nen	Nengone
neo	Ná-Meo
neq	North Central Mixe
ner	Yahadian
nes	Bhoti Kinnauri
net	Nete
nev	Nyaheun
new	Newari
nex	Neme
ney	Neyo
nez	Nez Perce
nfa	Dhao
nfd	Ahwai
nfl	Ayiwo
nfr	Nafaanra
nfu	Mfumte
nga	Ngbaka
ngb	Northern Ngbandi
ngc	Ngombe (Democratic Republic of Congo)
ngd	Ngando (Central African Republic)
nge	Ngemba
ngg	Ngbaka Manza
ngh	Nǁng
ngi	Ngizim
ngj	Ngie
ngk	Dalabon
ngl	Lomwe
ngm	Ngatik Mens Creole
ngn	Ngwo
ngo	Ngoni
ngp	Ngulu
ngq	Ngurimi
ngr	Engdewu
ngs	Gvoko
ngt	Kriang
ngu	Guerrero Nahuatl
ngw	Ngwaba
ngx	Nggwahyi
ngy	Tibea
ngz	Ngungwel
nha	Nhanda
nhb	Beng
nhd	Chiripá
nhe	Eastern Huasteca Nahuatl
nhf	Nhuwala
nhg	Tetelcingo Nahuatl
nhh	Nahari
nhi	Zacatlán-Ahuacatlán-Tepetzintla Nahuatl
nhk	Isthmus-Cosoleacaque Nahuatl
nhm	Morelos Nahuatl
nhn	Central Nahuatl
nho	Takuu
nhp	Isthmus-Pajapan Nahuatl
nhq	Huaxcaleca Nahuatl
nhr	Naro
nht	Ometepec Nahuatl
nhu	Noone
nhv	Temascaltepec Nahuatl
nhw	Western Huasteca Nahuatl
nhx	Isthmus-Mecayapan Nahuatl
nhy	Northern Oaxaca Nahuatl
nhz	Santa María La Alta Nahuatl
nia	Nias
nib	Nakame
nie	Niellim
nif	Nek
nih	Nyiha (Tanzania)
nii	Nii
nij	Ngaju
nik	Southern Nicobarese
nil	Nila
nim	Nilamba
nin	Ninzo
nio	Nganasan
niq	Nandi
nir	Nimboran
nis	Nimi
nit	Southeastern Kolami
niu	Niuean
niv	Gilyak
niw	Nimo
nix	Hema
niy	Ngiti
niz	Ningil
nja	Nzanyi
njb	Nocte Naga
njd	Ndonde Hamba
njh	Lotha Naga
nji	Gudanji
njj	Njen
njl	Njalgulgule
njm	Angami Naga
njn	Liangmai Naga
njo	Ao Naga
njr	Njerep
njs	Nisa
njt	Ndyuka-Trio Pidgin
nju	Ngadjunmaya
njx	Kunyi
njy	Njyem
njz	Nyishi
nka	Nkoya
nkb	Khoibu Naga
nkc	Nkongho
nkd	Koireng
nke	Duke
nkf	Inpui Naga
nkg	Nekgini
nkh	Khezha Naga
nki	Thangal Naga
nkj	Nakai
nkk	Nokuku
nkm	Namat
nkn	Nkangala
nko	Nkonya
nkq	Nkami
nkr	Nukuoro
nks	North Asmat
nkt	Nyika (Tanzania)
nku	Bouna Kulango
nkv	Nyika (Malawi and Zambia)
nkw	Nkutu
nkx	Nkoroo
nkz	Nkari
nla	Ngombale
nlc	Nalca
nld	Dutch
nle	East Nyala
nlg	Gela
nli	Grangali
nlj	Nyali
nlk	Ninia Yali
nll	Nihali
nlm	Mankiyali
nlo	Ngul
nlq	Lao Naga
nlu	Nchumbulu
nlv	Orizaba Nahuatl
nlx	Nahali
nly	Nyamal
nlz	Nalögo
nma	Maram Naga
nmb	Big Nambas
nmc	Ngam
nmd	Ndumu
nme	Mzieme Naga
nmf	Tangkhul Naga (India)
nmg	Kwasio
nmh	Monsang Naga
nmi	Nyam
nmj	Ngombe (Central African Republic)
nmk	Namakura
nml	Ndemli
nmm	Manangba
nmn	ǃXóõ
nmo	Moyon Naga
nmq	Nambya
nms	Letemboi
nmt	Namonuito
nmu	Northeast Maidu
nmw	Nimoa
nmx	Nama (Papua New Guinea)
nmy	Namuyi
nmz	Nawdm
nna	Nyangumarta
nnb	Nande
nnc	Nancere
nnd	West Ambae
nne	Ngandyera
nnf	Ngaing
nng	Maring Naga
nnh	Ngiemboon
nni	North Nuaulu
nnj	Nyangatom
nnk	Nankina
nnl	Northern Rengma Naga
nnm	Namia
nnn	Ngete
nno	Norwegian Nynorsk
nnp	Wancho Naga
nnq	Ngindo
nnu	Dwang
nnw	Southern Nuni
nnz	Ndanda
noa	Woun Meu
nob	Norwegian Bokmål
noc	Nuk
nod	Northern Thai
noe	Nimadi
nof	Nomane
nog	Nogai
noh	Nomu
noi	Noiri
noj	Nonuya
nop	Numanggang
noq	Ngongo
nos	Eastern Nisu
not	Nomatsiguenga
nou	Ewage-Notu
now	Nyambo
noy	Noy
noz	Nayi
npa	Nar Phu
npb	Nupbikha
npg	Ponyo-Gongwang Naga
nph	Phom Naga
npi	Nepali (individual language)
npl	Southeastern Puebla Nahuatl
npn	Mondropolon
npo	Pochuri Naga
nps	Nipsan
npu	Puimei Naga
npx	Noipx
npy	Napu
nqg	Southern Nago
nqk	Kura Ede Nago
nql	Ngendelengo
nqm	Ndom
nqn	Nen
nqo	NKo
nqq	Kyan-Karyaw Naga
nqy	Akyaung Ari Naga
nra	Ngom
nrb	Nara
nre	Southern Rengma Naga
nrf	Jèrriais
nrg	Narango
nri	Chokri Naga
nrk	Ngarla
nrl	Ngarluma
nrm	Narom
nru	Narua
nrz	Lala
nsa	Sangtam Naga
nsc	Nshi
nsd	Southern Nisu
nse	Nsenga
nsf	Northwestern Nisu
nsg	Ngasa
nsh	Ngoshie
nsi	Nigerian Sign Language
nsk	Naskapi
nsl	Norwegian Sign Language
nsm	Sumi Naga
nsn	Nehan
nso	Pedi
nsp	Nepalese Sign Language
nsq	Northern Sierra Miwok
nsr	Maritime Sign Language
nss	Nali
nst	Tase Naga
nsu	Sierra Negra Nahuatl
nsv	Southwestern Nisu
nsw	Navut
nsx	Nsongo
nsy	Nasal
nsz	Nisenan
ntd	Northern Tidung
nte	Nathembo
nti	Natioro
ntj	Ngaanyatjarra
ntk	Ikoma-Nata-Isenye
ntm	Nateni
nto	Ntomba
ntp	Northern Tepehuan
ntr	Delo
ntu	Natügu
ntx	Tangkhul Naga (Myanmar)
nty	Mantsi
ntz	Natanzi
nua	Yuanga
nud	Ngala
nue	Ngundu
nuf	Nusu
nuh	Ndunda
nui	Ngumbi
nuj	Nyole
nuk	Nuu-chah-nulth
num	Niuafoou
nun	Anong
nuo	Nguôn
nup	Nupe-Nupe-Tako
nuq	Nukumanu
nur	Nukuria
nus	Nuer
nut	Nung (Viet Nam)
nuu	Ngbundu
nuv	Northern Nuni
nuw	Nguluwan
nux	Mehek
nuy	Nunggubuyu
nuz	Tlamacazapa Nahuatl
nvh	Nasarian
nvm	Namiae
nvo	Nyokon
nwb	Nyabwa
nwe	Ngwe
nwi	Southwest Tanna
nwm	Nyamusa-Molo
nwr	Nawaru
nxa	Nauete
nxd	Ngando (Democratic Republic of Congo)
nxe	Nage
nxg	Ngada
nxi	Nindi
nxk	Koki Naga
nxl	South Nuaulu
nxo	Ndambomo
nxq	Naxi
nxr	Ninggerum
nxx	Nafri
nya	Nyanja
nyb	Nyangbo
nyc	Nyanga-li
nyd	Nyore
nye	Nyengo
nyf	Giryama
nyg	Nyindu
nyh	Nyikina
nyi	Ama (Sudan)
nyj	Nyanga
nyk	Nyaneka
nyl	Nyeu
nym	Nyamwezi
nyn	Nyankole
nyo	Nyoro
nyq	Nayini
nyr	Nyiha (Malawi)
nys	Nyungar
nyu	Nyungwe
nyw	Nyaw
nyy	Nyakyusa-Ngonde
nza	Tigon Mbembe
nzb	Njebi
nzd	Nzadi
nzi	Nzima
nzk	Nzakara
nzm	Zeme Naga
nzs	New Zealand Sign Language
nzu	Teke-Nzikou
nzy	Nzakambay
nzz	Nanga Dama Dogon
oaa	Orok
oac	Oroch
obk	Southern Bontok
obl	Oblo
obo	Obo Manobo
obu	Obulom
oca	Ocaina
oci	Occitan (post 1500)
ocu	Atzingo Matlatzinca
oda	Odut
odk	Od
odu	Odual
ofu	Efutop
ogb	Ogbia
ogc	Ogbah
ogg	Ogbogolo
ogo	Khana
ogu	Ogbronuagum
oia	Oirata
oin	Inebu One
ojb	Northwestern Ojibwa
ojc	Central Ojibwa
ojg	Eastern Ojibwa
ojs	Severn Ojibwa
ojv	Ontong Java
ojw	Western Ojibwa
oka	Okanagan
okb	Okobo
okd	Okodia
oke	Okpe (Southwestern Edo)
okh	Koresh-e Rostam
oki	Okiek
okk	Kwamtim One
okn	Oki-No-Erabu
okr	Kirike
oks	Oko-Eni-Osayen
oku	Oku
okv	Orokaiva
okx	Okpe (Northwestern Edo)
ola	Walungge
old	Mochi
ole	Olekha
olm	Oloma
olo	Livvi
olr	Olrat
olu	Kuvale
oma	Omaha-Ponca
omb	East Ambae
omg	Omagua
oml	Ombo
omo	Utarmbung
omt	Omotik
omw	South Tairora
ona	Ona
onb	Lingao
one	Oneida
ong	Olo
oni	Onin
onj	Onjob
onk	Kabore One
onn	Onobasulu
ono	Onondaga
onp	Sartang
onr	Northern One
ons	Ono
ont	Ontenu
onu	Unua
onx	Onin Based Pidgin
ood	Tohono Oodham
oog	Ong
oon	Önge
oor	Oorlams
opa	Okpamheri
opk	Kopkaka
opm	Oksapmin
opo	Opao
opy	Ofayé
ora	Oroha
orc	Orma
ore	Orejón
org	Oring
orh	Oroqen
orn	Orang Kanaq
oro	Orokolo
orr	Oruma
ors	Orang Seletar
ort	Adivasi Oriya
oru	Ormuri
orw	Oro Win
orx	Oro
ory	Odia
orz	Ormu
osa	Osage
osi	Osing
oso	Ososo
oss	Ossetian
ost	Osatu
osu	Southern One
otd	Ot Danum
ote	Mezquital Otomi
otl	Tilapa Otomi
otm	Eastern Highland Otomi
otn	Tenango Otomi
otq	Querétaro Otomi
otr	Otoro
ots	Estado de México Otomi
ott	Temoaya Otomi
otw	Ottawa
otx	Texcatepec Otomi
otz	Ixtenco Otomi
oua	Tagargrent
oub	Glio-Oubi
oue	Oune
ovd	Elfdalian
owi	Owiniga
oyb	Oy
oyd	Oyda
oym	Wayampi
oyy	Oyaoya
ozm	Koonzime
pab	Parecís
pac	Pacoh
pad	Paumarí
pae	Pagibete
pag	Pangasinan
pah	Tenharim
pai	Pe
pak	Parakanã
pam	Pampanga
pan	Panjabi
pao	Northern Paiute
pap	Papiamento
paq	Parya
par	Panamint
pas	Papasena
pat	Papitalai
pau	Palauan
pav	Pakaásnovos
paw	Pawnee
pay	Pech
pbb	Páez
pbc	Patamona
pbe	Mezontla Popoloca
pbf	Coyotepec Popoloca
pbh	Eñapa Woromaipu
pbi	Parkwa
pbl	Mak (Nigeria)
pbm	Puebla Mazatec
pbn	Kpasam
pbo	Papel
pbp	Badyara
pbr	Pangwa
pbs	Central Pame
pbt	Southern Pashto
pbu	Northern Pashto
pbv	Pnar
pby	Pyu (Papua New Guinea)
pca	Santa Inés Ahuatempan Popoloca
pcb	Pear
pcc	Bouyei
pcd	Picard
pce	Ruching Palaung
pcf	Paliyan
pcg	Paniya
pch	Pardhan
pci	Duruwa
pcj	Parenga
pck	Paite Chin
pcl	Pardhi
pcm	Nigerian Pidgin
pcn	Piti
pcp	Pacahuara
pcw	Pyapun
pda	Anam
pdc	Pennsylvania German
pdi	Pa Di
pdn	Podena
pdo	Padoe
pdt	Plautdietsch
pdu	Kayan
pea	Peranakan Indonesian
ped	Mala (Papua New Guinea)
pee	Taje
peg	Pengo
peh	Bonan
pei	Chichimeca-Jonaz
pek	Penchal
pel	Pekal
pem	Phende
pep	Kunja
peq	Southern Pomo
pes	Iranian Persian
pev	Pémono
pex	Petats
pey	Petjo
pez	Eastern Penan
pfa	Pááfang
pfe	Pere
pfl	Pfaelzisch
pga	Sudanese Creole Arabic
pgg	Pangwali
pgi	Pagi
pgk	Rerep
pgs	Pangseng
pgu	Pagu
pgz	Papua New Guinean Sign Language
pha	Pa-Hng
phd	Phudagi
phg	Phuong
phh	Phukha
phk	Phake
phl	Phalura
phm	Phimbi
pho	Phunoi
phq	Phana
phr	Pahari-Potwari
pht	Phu Thai
phu	Phuan
phv	Pahlavani
phw	Phangduwali
pia	Pima Bajo
pib	Yine
pic	Pinji
pid	Piaroa
pif	Pingelapese
pig	Pisabo
pih	Pitcairn-Norfolk
pii	Pini
pil	Yom
pin	Piame
pio	Piapoco
pip	Pero
pir	Piratapuyo
pis	Pijin
piu	Pintupi-Luritja
piv	Pileni
piw	Pimbwe
pix	Piu
piy	Piya-Kwonci
piz	Pije
pjt	Pitjantjatjara
pkb	Pokomo
pkg	Pak-Tong
pkh	Pankhu
pkn	Pakanha
pko	Pökoot
pkp	Pukapuka
pkr	Attapady Kurumba
pks	Pakistan Sign Language
pkt	Maleng
pku	Paku
pla	Miani
plb	Polonombauk
plc	Central Palawano
pld	Polari
ple	Palue
plg	Pilagá
plh	Paulohi
plj	Polci
plk	Kohistani Shina
pll	Shwe Palaung
pln	Palenquero
plo	Oluta Popoluca
plr	Palaka Senoufo
pls	San Marcos Tlacoyalco Popoloca
plt	Plateau Malagasy
plu	Palikúr
plv	Southwest Palawano
plw	Brookes Point Palawano
ply	Bolyu
plz	Paluan
pma	Paama
pmb	Pambia
pme	Pwaamei
pmf	Pamona
pmi	Northern Pumi
pmj	Southern Pumi
pmm	Pomo
pmn	Pam
pmo	Pom
pmq	Northern Pame
pmr	Paynamar
pms	Piemontese
pmt	Tuamotuan
pmw	Plains Miwok
pmx	Poumei Naga
pmy	Papuan Malay
pna	Punan Bah-Biau
pnb	Western Panjabi
pnc	Pannei
pnd	Mpinda
pne	Western Penan
png	Pongu
pnh	Penrhyn
pni	Aoheng
pnk	Paunaka
pnl	Paleni
pnm	Punan Batu 1
pnn	Pinai-Hagahai
pnp	Pancana
pnq	Pana (Burkina Faso)
pnr	Panim
pns	Ponosakan
pnt	Pontic
pnu	Jiongnai Bunu
pnv	Pinigura
pnw	Banyjima
pnx	Phong-Kniang
pny	Pinyin
pnz	Pana (Central African Republic)
poc	Poqomam
poe	San Juan Atzingo Popoloca
pof	Poke
poh	Poqomchi
poi	Highland Popoluca
pok	Pokangá
pol	Polish
pom	Southeastern Pomo
pon	Pohnpeian
pop	Pwapwâ
poq	Texistepec Popoluca
por	Portuguese
pos	Sayula Popoluca
pot	Potawatomi
pov	Upper Guinea Crioulo
pow	San Felipe Otlaltepec Popoloca
poy	Pogolo
ppe	Papi
ppi	Paipai
ppk	Uma
ppl	Pipil
ppm	Papuma
ppn	Papapana
ppo	Folopa
ppp	Pelende
ppq	Pei
pps	San Luís Temalacayuca Popoloca
ppt	Pare
pqa	Paa
pqm	Malecite-Passamaquoddy
prc	Parachi
prd	Parsi-Dari
pre	Principense
prf	Paranan
prg	Prussian
prh	Porohanon
pri	Paicî
prk	Parauk
prl	Peruvian Sign Language
prm	Kibiri
prn	Prasuni
prp	Parsi
prq	Ashéninka Perené
prs	Dari
prt	Phai
pru	Puragi
prw	Parawen
prx	Purik
prz	Providencia Sign Language
psa	Asue Awyu
psc	Persian Sign Language
psd	Plains Indian Sign Language
pse	Central Malay
psg	Penang Sign Language
psh	Southwest Pashai
psi	Southeast Pashai
psl	Puerto Rican Sign Language
psn	Panasuan
pso	Polish Sign Language
psp	Philippine Sign Language
psq	Pasi
psr	Portuguese Sign Language
pss	Kaulong
pst	Central Pashto
psw	Port Sandwich
pta	Pai Tavytera
pti	Pindiini
ptn	Patani
pto	Zoé
ptp	Patep
ptq	Pattapu
ptr	Piamatsina
ptt	Enrekang
ptu	Bambam
ptv	Port Vato
pty	Pathiya
pua	Western Highland Purepecha
pub	Purum
puc	Punan Merap
pud	Punan Aput
puf	Punan Merah
pug	Phuie
pui	Puinave
puj	Punan Tubu
pum	Puma
puo	Puoc
pup	Pulabu
pur	Puruborá
put	Putoh
puu	Punu
puw	Puluwatese
pux	Puare
pwa	Pawaia
pwb	Panawa
pwg	Gapapaiwa
pwm	Molbog
pwn	Paiwan
pwo	Pwo Western Karen
pwr	Powari
pww	Pwo Northern Karen
pxm	Quetzaltepec Mixe
pye	Pye Krumen
pym	Fyam
pyn	Poyanáwa
pys	Paraguayan Sign Language
pyu	Puyuma
pyy	Pyen
pzn	Para Naga
qua	Quapaw
qub	Huallaga Huánuco Quechua
quc	Kiche
qud	Calderón Highland Quichua
quf	Lambayeque Quechua
qug	Chimborazo Highland Quichua
quh	South Bolivian Quechua
qui	Quileute
quk	Chachapoyas Quechua
qul	North Bolivian Quechua
qum	Sipacapense
qup	Southern Pastaza Quechua
quq	Quinqui
qur	Yanahuanca Pasco Quechua
qus	Santiago del Estero Quichua
quv	Sacapulteco
quw	Tena Lowland Quichua
qux	Yauyos Quechua
quy	Ayacucho Quechua
quz	Cusco Quechua
qva	Ambo-Pasco Quechua
qvc	Cajamarca Quechua
qve	Eastern Apurímac Quechua
qvh	Huamalíes-Dos de Mayo Huánuco Quechua
qvi	Imbabura Highland Quichua
qvj	Loja Highland Quichua
qvl	Cajatambo North Lima Quechua
qvm	Margos-Yarowilca-Lauricocha Quechua
qvn	North Junín Quechua
qvo	Napo Lowland Quechua
qvp	Pacaraos Quechua
qvs	San Martín Quechua
qvw	Huaylla Wanca Quechua
qvy	Queyu
qvz	Northern Pastaza Quichua
qwa	Corongo Ancash Quechua
qwh	Huaylas Ancash Quechua
qws	Sihuas Ancash Quechua
qxa	Chiquián Ancash Quechua
qxc	Chincha Quechua
qxh	Panao Huánuco Quechua
qxl	Salasaca Highland Quichua
qxn	Northern Conchucos Ancash Quechua
qxo	Southern Conchucos Ancash Quechua
qxp	Puno Quechua
qxq	Qashqai
qxr	Cañar Highland Quichua
qxs	Southern Qiang
qxt	Santa Ana de Tusi Pasco Quechua
qxu	Arequipa-La Unión Quechua
qxw	Jauja Wanca Quechua
raa	Dungmali
rab	Camling
rac	Rasawa
rad	Rade
raf	Western Meohang
rag	Logooli
rah	Rabha
rai	Ramoaaina
rak	Tulu-Bohuai
ral	Ralte
ram	Canela
ran	Riantana
rao	Rao
rap	Rapanui
raq	Saam
rar	Rarotongan
ras	Tegali
rat	Razajerdi
rau	Raute
rav	Sampang
raw	Rawang
rax	Rang
ray	Rapa
raz	Rahambuu
rbb	Rumai Palaung
rbk	Northern Bontok
rbl	Miraya Bikol
rcf	Réunion Creole French
rdb	Rudbari
rea	Rerau
reb	Rembong
ree	Rejang Kayan
reg	Kara (Tanzania)
rei	Reli
rej	Rejang
rel	Rendille
ren	Rengao
res	Reshe
ret	Retta
rey	Reyesano
rga	Roria
rge	Romano-Greek
rgn	Romagnol
rgr	Resígaro
rgs	Southern Roglai
rgu	Ringgou
rhg	Rohingya
rhp	Yahang
ria	Riang (India)
rif	Tarifit
ril	Riang Lang
rim	Nyaturu
rin	Nungu
rir	Ribun
rit	Ritharrngu
riu	Riung
rjg	Rajong
rji	Raji
rjs	Rajbanshi
rka	Kraol
rkb	Rikbaktsa
rkh	Rakahanga-Manihiki
rki	Rakhine
rkm	Marka
rkt	Rangpuri
rma	Rama
rmb	Rembarrnga
rmc	Carpathian Romani
rme	Angloromani
rmf	Kalo Finnish Romani
rmg	Traveller Norwegian
rmh	Murkim
rmi	Lomavren
rmk	Romkun
rml	Baltic Romani
rmm	Roma
rmn	Balkan Romani
rmo	Sinte Romani
rmp	Rempi
rmq	Caló
rms	Romanian Sign Language
rmt	Domari
rmu	Tavringer Romani
rmw	Welsh Romani
rmx	Romam
rmy	Vlax Romani
rmz	Marma
rnd	Ruund
rng	Ronga
rnl	Ranglong
rnn	Roon
rnp	Rongpo
rnw	Rungwa
rob	Tae
roc	Cacgia Roglai
rod	Rogo
roe	Ronji
rof	Rombo
rog	Northern Roglai
roh	Romansh
rol	Romblomanon
ron	Romanian
roo	Rotokas
rop	Kriol
ror	Rongga
rou	Runga
row	Dela-Oenale
rpn	Repanbitip
rpt	Rapting
rri	Ririo
rro	Waima
rsb	Romano-Serbian
rsl	Russian Sign Language
rsm	Miriwoong Sign Language
rtc	Rungtu Chin
rth	Ratahan
rtm	Rotuman
rtw	Rathawi
rub	Gungu
ruc	Ruuli
rue	Rusyn
ruf	Luguru
rug	Roviana
ruh	Ruga
rui	Rufiji
ruk	Che
run	Rundi
ruo	Istro Romanian
rup	Macedo-Romanian
ruq	Megleno Romanian
rus	Russian
rut	Rutul
ruu	Lanas Lobu
ruy	Mala (Nigeria)
ruz	Ruma
rwa	Rawo
rwk	Rwa
rwm	Amba (Uganda)
rwo	Rawa
rwr	Marwari (India)
rxd	Ngardi
ryn	Northern Amami-Oshima
rys	Yaeyama
ryu	Central Okinawan
rzh	Rāziḥī
saa	Saba
sab	Buglere
sac	Meskwaki
sad	Sandawe
sae	Sabanê
saf	Safaliba
sag	Sango
sah	Yakut
saj	Sahu
sak	Sake
sao	Sause
saq	Samburu
sas	Sasak
sat	Santali
sau	Saleman
sav	Saafi-Saafi
saw	Sawi
sax	Sa
say	Saya
saz	Saurashtra
sba	Ngambay
sbb	Simbo
sbc	Kele (Papua New Guinea)
sbd	Southern Samo
sbe	Saliba
sbf	Chabu
sbg	Seget
sbh	Sori-Harengan
sbi	Seti
sbj	Surbakhal
sbk	Safwa
sbl	Botolan Sambal
sbm	Sagala
sbn	Sindhi Bhil
sbo	Sabüm
sbp	Sangu (Tanzania)
sbq	Sileibi
sbr	Sembakung Murut
sbs	Subiya
sbt	Kimki
sbu	Stod Bhoti
sbw	Simba
sbx	Seberuang
sby	Soli
sbz	Sara Kaba
scb	Chut
sce	Dongxiang
scf	San Miguel Creole French
scg	Sanggau
sch	Sakachep
sci	Sri Lankan Creole Malay
sck	Sadri
scl	Shina
scn	Sicilian
sco	Scots
scp	Hyolmo
scq	Saoch
scs	North Slavey
sct	Southern Katang
scu	Shumcho
scv	Sheni
scw	Sha
sda	Toraja-Sadan
sdb	Shabak
sdc	Sassarese Sardinian
sde	Surubu
sdf	Sarli
sdg	Savi
sdh	Southern Kurdish
sdj	Suundi
sdk	Sos Kundi
sdl	Saudi Arabian Sign Language
sdn	Gallurese Sardinian
sdo	Bukar-Sadung Bidayuh
sdp	Sherdukpen
sdq	Semandang
sdr	Oraon Sadri
sdu	Sarudu
sdx	Sibu Melanau
sdz	Sallands
sea	Semai
seb	Shempire Senoufo
sec	Sechelt
sed	Sedang
see	Seneca
sef	Cebaara Senoufo
seg	Segeju
seh	Sena
sei	Seri
sej	Sene
sek	Sekani
sel	Selkup
sen	Nanerigé Sénoufo
seo	Suarmin
sep	Sìcìté Sénoufo
seq	Senara Sénoufo
ser	Serrano
ses	Koyraboro Senni Songhai
set	Sentani
seu	Serui-Laut
sev	Nyarafolo Senoufo
sew	Sewa Bay
sey	Secoya
sez	Senthang Chin
sfb	Langue des signes de Belgique Francophone
sfe	Eastern Subanen
sfm	Small Flowery Miao
sfs	South African Sign Language
sfw	Sehwi
sgb	Mag-antsi Ayta
sgc	Kipsigis
sgd	Surigaonon
sge	Segai
sgg	Swiss-German Sign Language
sgh	Shughni
sgi	Suga
sgj	Surgujia
sgk	Sangkong
sgp	Singpho
sgr	Sangisari
sgs	Samogitian
sgt	Brokpake
sgu	Salas
sgw	Sebat Bet Gurage
sgx	Sierra Leone Sign Language
sgy	Sanglechi
sgz	Sursurunga
sha	Shall-Zwall
shb	Ninam
shc	Sonde
shd	Kundal Shahi
she	Sheko
shg	Shua
shh	Shoshoni
shi	Tachelhit
shj	Shatt
shk	Shilluk
shl	Shendu
shm	Shahrudi
shn	Shan
sho	Shanga
shp	Shipibo-Conibo
shq	Sala
shr	Shi
shs	Shuswap
shu	Chadian Arabic
shv	Shehri
shw	Shwai
shx	She
shy	Tachawit
shz	Syenara Senoufo
sib	Sebop
sid	Sidamo
sie	Simaa
sif	Siamou
sig	Paasaal
sih	Zire
sii	Shom Peng
sij	Numbami
sik	Sikiana
sil	Tumulung Sisaala
sim	Mende (Papua New Guinea)
sin	Sinhala
sip	Sikkimese
siq	Sonia
sir	Siri
siu	Sinagen
siv	Sumariup
siw	Siwai
six	Sumau
siy	Sivandi
siz	Siwi
sja	Epena
sjb	Sajau Basap
sjd	Kildin Sami
sje	Pite Sami
sjg	Assangori
sjl	Sajalong
sjm	Mapun
sjo	Xibe
sjp	Surjapuri
sjr	Siar-Lak
sjt	Ter Sami
sju	Ume Sami
sjw	Shawnee
ska	Skagit
skb	Saek
skc	Ma Manda
skd	Southern Sierra Miwok
ske	Seke (Vanuatu)
skf	Sakirabiá
skg	Sakalava Malagasy
skh	Sikule
ski	Sika
skj	Seke (Nepal)
skm	Kutong
skn	Kolibugan Subanon
sko	Seko Tengah
skp	Sekapan
skq	Sininkere
skr	Saraiki
sks	Maia
skt	Sakata
sku	Sakao
skv	Skou
skx	Seko Padang
sky	Sikaiana
skz	Sekar
slc	Sáliba
sld	Sissala
sle	Sholaga
slf	Swiss-Italian Sign Language
slg	Selungai Murut
slh	Southern Puget Sound Salish
sli	Lower Silesian
slj	Salumá
slk	Slovak
sll	Salt-Yui
slm	Pangutaran Sama
slp	Lamaholot
slr	Salar
sls	Singapore Sign Language
slt	Sila
slu	Selaru
slv	Slovenian
slw	Sialum
slx	Salampasu
sly	Selayar
slz	Maya
sma	Southern Sami
smb	Simbari
smd	Sama
sme	Northern Sami
smf	Auwe
smg	Simbali
smh	Samei
smj	Lule Sami
smk	Bolinao
sml	Central Sama
smm	Musasa
smn	Inari Sami
smo	Samoan
smq	Samo
smr	Simeulue
sms	Skolt Sami
smt	Simte
smv	Samvedi
smw	Sumbawa
smx	Samba
smy	Semnani
smz	Simeku
sna	Shona
snb	Sebuyau
snc	Sinaugoro
snd	Sindhi
sne	Bau Bidayuh
snf	Noon
sng	Sanga (Democratic Republic of Congo)
snj	Riverain Sango
snk	Soninke
snl	Sangil
snm	Southern Madi
snn	Siona
sno	Snohomish
snp	Siane
snq	Sangu (Gabon)
snr	Sihan
sns	South West Bay
snu	Senggi
snv	Saban
snw	Selee
snx	Sam
sny	Saniyo-Hiyewe
snz	Kou
soa	Thai Song
sob	Sobei
soc	So (Democratic Republic of Congo)
sod	Songoora
soe	Songomeno
soh	Aka
soi	Sonha
soj	Soi
sok	Sokoro
sol	Solos
som	Somali
soo	Songo
sop	Songe
soq	Kanasi
sor	Somrai
sos	Seeku
sot	Southern Sotho
sou	Southern Thai
sov	Sonsorol
sow	Sowanda
sox	Swo
soy	Miyobe
soz	Temi
spa	Spanish
spb	Sepa (Indonesia)
spc	Sapé
spd	Saep
spe	Sepa (Papua New Guinea)
spg	Sian
spi	Saponi
spk	Sengo
spl	Selepet
spm	Akukem
spn	Sanapaná
spo	Spokane
spp	Supyire Senoufo
spq	Loreto-Ucayali Spanish
spr	Saparua
sps	Saposa
spt	Spiti Bhoti
spu	Sapuan
spv	Sambalpuri
spy	Sabaot
sqa	Shama-Sambuga
sqh	Shau
sqk	Albanian Sign Language
sqm	Suma
sqo	Sorkhei
sqq	Sou
sqs	Sri Lankan Sign Language
sqt	Soqotri
squ	Squamish
sra	Saruga
srb	Sora
src	Logudorese Sardinian
sre	Sara
srf	Nafi
srg	Sulod
srh	Sarikoli
sri	Siriano
srk	Serudung Murut
srl	Isirawa
srm	Saramaccan
srn	Sranan Tongo
sro	Campidanese Sardinian
srp	Serbian
srq	Sirionó
srr	Serer
srs	Sarsi
srt	Sauri
sru	Suruí
srv	Southern Sorsoganon
srw	Serua
srx	Sirmauri
sry	Sera
srz	Shahmirzadi
ssb	Southern Sama
ssc	Suba-Simbiti
ssd	Siroi
sse	Balangingi
ssg	Seimat
ssh	Shihhi Arabic
ssi	Sansi
ssj	Sausi
ssk	Sunam
ssl	Western Sisaala
ssm	Semnam
ssn	Waata
sso	Sissano
ssp	Spanish Sign Language
ssq	Soa
ssr	Swiss-French Sign Language
sss	Sô
sst	Sinasina
ssu	Susuami
ssv	Shark Bay
ssw	Swati
ssx	Samberigi
ssy	Saho
ssz	Sengseng
sta	Settla
stb	Northern Subanen
std	Sentinel
ste	Liana-Seti
stf	Seta
stg	Trieng
sth	Shelta
sti	Bulo Stieng
stj	Matya Samo
stk	Arammba
stl	Stellingwerfs
stm	Setaman
stn	Owa
sto	Stoney
stp	Southeastern Tepehuan
stq	Saterfriesisch
str	Straits Salish
sts	Shumashti
stt	Budeh Stieng
stu	Samtao
stv	Silte
stw	Satawalese
sty	Siberian Tatar
sua	Sulka
sub	Suku
suc	Western Subanon
sue	Suena
sug	Suganga
sui	Suki
suj	Shubi
suk	Sukuma
sun	Sundanese
suq	Suri
sur	Mwaghavul
sus	Susu
suv	Puroik
suw	Sumbwa
suy	Suyá
suz	Sunwar
sva	Svan
svb	Ulau-Suain
svc	Vincentian Creole English
sve	Serili
svk	Slovakian Sign Language
svm	Slavomolisano
svs	Savosavo
swb	Maore Comorian
swc	Congo Swahili
swe	Swedish
swf	Sere
swg	Swabian
swh	Swahili (individual language)
swi	Sui
swj	Sira
swk	Malawi Sena
swl	Swedish Sign Language
swm	Samosa
swn	Sawknah
swo	Shanenawa
swp	Suau
swq	Sharwa
swr	Saweru
sws	Seluwasan
swt	Sawila
swu	Suwawa
swv	Shekhawati
swx	Suruahá
swy	Sarua
sxb	Suba
sxe	Sighu
sxg	Shuhi
sxm	Samre
sxn	Sangir
sxr	Saaroa
sxs	Sasaru
sxu	Upper Saxon
sxw	Saxwe Gbe
sya	Siang
syb	Central Subanen
syi	Seki
syk	Sukur
syl	Sylheti
sym	Maya Samo
syn	Senaya
syo	Suoy
sys	Sinyar
syw	Kagate
syx	Samay
syy	Al-Sayyid Bedouin Sign Language
sza	Semelai
szb	Ngalum
szc	Semaq Beri
sze	Seze
szg	Sengele
szl	Silesian
szn	Sula
szp	Suabo
szs	Solomon Islands Sign Language
szv	Isu (Fako Division)
szw	Sawai
szy	Sakizaya
taa	Lower Tanana
tab	Tabassaran
tac	Lowland Tarahumara
tad	Tause
tae	Tariana
taf	Tapirapé
tag	Tagoi
tah	Tahitian
taj	Eastern Tamang
tak	Tala
tal	Tal
tam	Tamil
tan	Tangale
tao	Yami
tap	Taabwa
taq	Tamasheq
tar	Central Tarahumara
tat	Tatar
tau	Upper Tanana
tav	Tatuyo
taw	Tai
tax	Tamki
tay	Atayal
taz	Tocho
tba	Aikanã
tbc	Takia
tbd	Kaki Ae
tbe	Tanimbili
tbf	Mandara
tbg	North Tairora
tbi	Gaam
tbj	Tiang
tbk	Calamian Tagbanwa
tbl	Tboli
tbm	Tagbu
tbn	Barro Negro Tunebo
tbo	Tawala
tbp	Taworta
tbr	Tumtum
tbs	Tanguat
tbt	Tembo (Kitembo)
tbv	Tobo
tbw	Tagbanwa
tbx	Kapin
tby	Tabaru
tbz	Ditammari
tca	Ticuna
tcb	Tanacross
tcc	Datooga
tcd	Tafi
tce	Southern Tutchone
tcf	Malinaltepec Mephaa
tcg	Tamagario
tch	Turks And Caicos Creole English
tci	Wára
tck	Tchitchege
tcm	Tanahmerah
tcn	Tichurong
tco	Taungyo
tcp	Tawr Chin
tcq	Kaiy
tcs	Torres Strait Creole
tct	Ten
tcu	Southeastern Tarahumara
tcw	Tecpatlán Totonac
tcx	Toda
tcy	Tulu
tcz	Thado Chin
tda	Tagdal
tdb	Panchpargania
tdc	Emberá-Tadó
tdd	Tai Nüa
tde	Tiranige Diga Dogon
tdf	Talieng
tdg	Western Tamang
tdh	Thulung
tdi	Tomadino
tdj	Tajio
tdk	Tambas
tdl	Sur
tdm	Taruma
tdn	Tondano
tdo	Teme
tdq	Tita
tdr	Todrah
tds	Doutai
tdt	Tetun Dili
tdv	Toro
tdx	Tandroy-Mahafaly Malagasy
tdy	Tadyawan
tea	Temiar
tec	Terik
ted	Tepo Krumen
tee	Huehuetla Tepehua
tef	Teressa
teg	Teke-Tege
teh	Tehuelche
tei	Torricelli
tek	Ibali Teke
tel	Telugu
tem	Timne
teo	Teso
teq	Temein
ter	Tereno
tes	Tengger
tet	Tetum
teu	Soo
tev	Teor
tew	Tewa (USA)
tex	Tennet
tey	Tulishi
tez	Tetserret
tfi	Tofin Gbe
tfn	Tanaina
tfo	Tefaro
tfr	Teribe
tft	Ternate
tga	Sagalla
tgb	Tobilung
tgc	Tigak
tgd	Ciwogai
tge	Eastern Gorkha Tamang
tgf	Chalikha
tgh	Tobagonian Creole English
tgi	Lawunuia
tgj	Tagin
tgk	Tajik
tgl	Tagalog
tgn	Tandaganon
tgo	Sudest
tgp	Tangoa
tgq	Tring
tgr	Tareng
tgs	Nume
tgt	Central Tagbanwa
tgu	Tanggu
tgw	Tagwana Senoufo
tgx	Tagish
tha	Thai
thd	Kuuk Thaayorre
the	Chitwania Tharu
thf	Thangmi
thh	Northern Tarahumara
thi	Tai Long
thk	Tharaka
thl	Dangaura Tharu
thm	Aheu
thn	Thachanadan
thp	Thompson
thq	Kochila Tharu
thr	Rana Tharu
ths	Thakali
tht	Tahltan
thu	Thuri
thv	Tahaggart Tamahaq
thy	Tha
thz	Tayart Tamajeq
tia	Tidikelt Tamazight
tic	Tira
tif	Tifal
tig	Tigre
tih	Timugon Murut
tii	Tiene
tij	Tilung
tik	Tikar
tim	Timbe
tin	Tindi
tio	Teop
tip	Trimuris
tiq	Tiéfo
tir	Tigrinya
tis	Masadiit Itneg
tit	Tinigua
tiu	Adasen
tiv	Tiv
tiw	Tiwi
tix	Southern Tiwa
tiy	Tiruray
tiz	Tai Hongjin
tja	Tajuasohn
tjg	Tunjung
tji	Northern Tujia
tjj	Tjungundji
tjl	Tai Laing
tjo	Temacine Tamazight
tjp	Tjupany
tjs	Southern Tujia
tjw	Djabwurrung
tkb	Buksa
tkd	Tukudede
tke	Takwane
tkg	Tesaka Malagasy
tkl	Tokelau
tkn	Toku-No-Shima
tkp	Tikopia
tkq	Tee
tkr	Tsakhur
tks	Takestani
tkt	Kathoriya Tharu
tku	Upper Necaxa Totonac
tkv	Mur Pano
tkw	Teanu
tkx	Tangko
tkz	Takua
tla	Southwestern Tepehuan
tlb	Tobelo
tlc	Yecuatla Totonac
tld	Talaud
tlf	Telefol
tlg	Tofanma
tli	Tlingit
tlj	Talinga-Bwisi
tlk	Taloki
tll	Tetela
tlm	Tolomako
tln	Talondo
tlo	Talodi
tlp	Filomena Mata-Coahuitlán Totonac
tlq	Tai Loi
tlr	Talise
tls	Tambotalo
tlt	Sou Nama
tlu	Tulehu
tlv	Taliabu
tlx	Khehek
tly	Talysh
tma	Tama (Chad)
tmb	Katbol
tmc	Tumak
tmd	Haruai
tmf	Toba-Maskoy
tmi	Tutuba
tmj	Samarokena
tmk	Northwestern Tamang
tml	Tamnim Citak
tmm	Tai Thanh
tmn	Taman (Indonesia)
tmo	Temoq
tmq	Tumleo
tms	Tima
tmt	Tasmate
tmu	Iau
tmv	Tembo (Motembo)
tmw	Temuan
tmy	Tami
tna	Tacana
tnb	Western Tunebo
tnc	Tanimuca-Retuarã
tnd	Angosturas Tunebo
tng	Tobanga
tnh	Maiani
tni	Tandia
tnk	Kwamera
tnl	Lenakel
tnm	Tabla
tnn	North Tanna
tno	Toromono
tnp	Whitesands
tnr	Ménik
tns	Tenis
tnt	Tontemboan
tnu	Tay Khang
tnv	Tangchangya
tnw	Tonsawang
tnx	Tanema
tny	Tongwe
tnz	Tenedn
tob	Toba
toc	Coyutla Totonac
tod	Toma
tof	Gizrra
tog	Tonga (Nyasa)
toh	Gitonga
toi	Tonga (Zambia)
toj	Tojolabal
tom	Tombulu
ton	Tonga (Tonga Islands)
too	Xicotepec De Juárez Totonac
top	Papantla Totonac
toq	Toposa
tor	Togbo-Vara Banda
tos	Highland Totonac
tou	Tho
tov	Upper Taromi
tow	Jemez
tox	Tobian
toy	Topoiyo
toz	To
tpa	Taupota
tpc	Azoyú Mephaa
tpe	Tippera
tpf	Tarpia
tpg	Kula
tpi	Tok Pisin
tpj	Tapieté
tpl	Tlacoapa Mephaa
tpm	Tampulma
tpo	Tai Pao
tpp	Pisaflores Tepehua
tpq	Tukpa
tpr	Tuparí
tpt	Tlachichilco Tepehua
tpu	Tampuan
tpv	Tanapag
tpx	Acatepec Mephaa
tpy	Trumai
tpz	Tinputz
tqb	Tembé
tql	Lehali
tqm	Turumsa
tqn	Tenino
tqo	Toaripi
tqp	Tomoip
tqq	Tunni
tqt	Western Totonac
tqu	Touo
tra	Tirahi
trb	Terebu
trc	Copala Triqui
trd	Turi
tre	East Tarangan
trf	Trinidadian Creole English
trg	Lishán Didán
trh	Turaka
tri	Trió
trj	Toram
trl	Traveller Scottish
trm	Tregami
trn	Trinitario
tro	Tarao Naga
trp	Kok Borok
trq	San Martín Itunyoso Triqui
trr	Taushiro
trs	Chicahuaxtla Triqui
trt	Tunggare
tru	Turoyo
trv	Taroko
trw	Torwali
trx	Tringgus-Sembaan Bidayuh
tsa	Tsaangi
tsb	Tsamai
tsc	Tswa
tsd	Tsakonian
tse	Tunisian Sign Language
tsg	Tausug
tsh	Tsuvan
tsi	Tsimshian
tsj	Tshangla
tsk	Tseku
tsl	Tsün-Lao
tsm	Turkish Sign Language
tsn	Tswana
tso	Tsonga
tsp	Northern Toussian
tsq	Thai Sign Language
tsr	Akei
tss	Taiwan Sign Language
tst	Tondi Songway Kiini
tsu	Tsou
tsv	Tsogo
tsw	Tsishingini
tsx	Mubami
tsy	Tebul Sign Language
tsz	Purepecha
ttb	Gaa
ttc	Tektiteko
ttd	Tauade
tte	Bwanabwana
ttf	Tuotomb
ttg	Tutong
tth	Upper Taoih
tti	Tobati
ttj	Tooro
ttk	Totoro
ttl	Totela
ttm	Northern Tutchone
ttn	Towei
tto	Lower Taoih
ttp	Tombelala
ttq	Tawallammat Tamajaq
ttr	Tera
tts	Northeastern Thai
ttt	Muslim Tat
ttu	Torau
ttv	Titan
ttw	Long Wat
tty	Sikaritai
ttz	Tsum
tua	Wiarumus
tuc	Mutu
tue	Tuyuca
tuf	Central Tunebo
tug	Tunia
tuh	Taulil
tui	Tupuri
tuj	Tugutil
tuk	Turkmen
tul	Tula
tum	Tumbuka
tun	Tunica
tuo	Tucano
tuq	Tedaga
tur	Turkish
tus	Tuscarora
tuu	Tututni
tuv	Turkana
tuy	Tugen
tuz	Turka
tva	Vaghua
tvd	Tsuvadi
tve	Teun
tvk	Southeast Ambrym
tvl	Tuvalu
tvm	Tela-Masbuar
tvn	Tavoyan
tvo	Tidore
tvs	Taveta
tvt	Tutsa Naga
tvu	Tunen
tvw	Sedoa
twb	Western Tawbuid
twd	Twents
twe	Tewa (Indonesia)
twf	Northern Tiwa
twg	Tereweng
twh	Tai Dón
twi	Twi
twl	Tawara
twm	Tawang Monpa
twn	Twendi
two	Tswapong
twp	Ere
twq	Tasawaq
twr	Southwestern Tarahumara
twu	Termanu
tww	Tuwari
twx	Tewe
twy	Tawoyan
txa	Tombonuo
txe	Totoli
txi	Ikpeng
txj	Tarjumo
txm	Tomini
txn	West Tarangan
txo	Toto
txq	Tii
txs	Tonsea
txt	Citak
txu	Kayapó
txx	Tatana
txy	Tanosy Malagasy
tya	Tauya
tye	Kyanga
tyh	Odu
tyi	Teke-Tsaayi
tyj	Tai Do
tyl	Thu Lao
tyn	Kombai
tyr	Tai Daeng
tys	Tày Sa Pa
tyt	Tày Tac
tyu	Kua
tyv	Tuvinian
tyx	Teke-Tyee
tyz	Tày
tza	Tanzanian Sign Language
tzh	Tzeltal
tzj	Tzutujil
tzm	Central Atlas Tamazight
tzn	Tugun
tzo	Tzotzil
tzx	Tabriak
uan	Kuan
uar	Tairuma
uba	Ubang
ubi	Ubi
ubl	Buhinon Bikol
ubr	Ubir
ubu	Umbu-Ungu
uda	Uda
ude	Udihe
udg	Muduga
udi	Udi
udj	Ujir
udl	Wuzlam
udm	Udmurt
udu	Uduk
ues	Kioko
ufi	Ufim
uge	Ughele
ugn	Ugandan Sign Language
ugo	Ugong
ugy	Uruguayan Sign Language
uha	Uhami
uhn	Damal
uig	Uighur
uis	Uisai
uiv	Iyive
uji	Tanjijili
uka	Kaburi
ukg	Ukuriguma
ukh	Ukhwejo
uki	Kui (India)
ukk	Muak Sa-aak
ukl	Ukrainian Sign Language
ukp	Ukpe-Bayobiri
ukq	Ukwa
ukr	Ukrainian
uks	Urubú-Kaapor Sign Language
uku	Ukue
ukv	Kuku
ukw	Ukwuani-Aboh-Ndoni
ula	Fungwa
ulb	Ulukwumi
ulc	Ulch
ulf	Usku
uli	Ulithian
ulk	Meriam Mir
ull	Ullatan
ulm	Ulumanda
uln	Unserdeutsch
ulu	Uma Lung
ulw	Ulwa
uma	Umatilla
umb	Umbundu
umi	Ukit
umm	Umon
umn	Makyan Naga
ump	Umpila
ums	Pendau
umu	Munsee
una	North Watut
une	Uneme
ung	Ngarinyin
unk	Enawené-Nawé
unn	Kurnai
unr	Mundari
unu	Unubahe
unx	Munda
unz	Unde Kaili
upi	Umeda
upv	Uripiv-Wala-Rano-Atchin
ura	Urarina
urb	Urubú-Kaapor
urd	Urdu
ure	Uru
urg	Urigina
urh	Urhobo
uri	Urim
urk	Urak Lawoi
url	Urali
urm	Urapmin
urn	Uruangnirin
uro	Ura (Papua New Guinea)
urp	Uru-Pa-In
urr	Lehalurup
urt	Urat
urw	Sop
urx	Urimo
ury	Orya
urz	Uru-Eu-Wau-Wau
usa	Usarufa
ush	Ushojo
usi	Usui
usk	Usaghade
usp	Uspanteco
uss	us-Saare
usu	Uya
uta	Otank
ute	Ute-Southern Paiute
uth	ut-Hun
utp	Amba (Solomon Islands)
utr	Etulo
utu	Utu
uum	Urum
uun	Kulon-Pazeh
uur	Ura (Vanuatu)
uuu	U
uve	West Uvean
uvh	Uri
uvl	Lote
uwa	Kuku-Uwanh
uya	Doko-Uyanga
uzn	Northern Uzbek
uzs	Southern Uzbek
vaa	Vaagri Booli
vae	Vale
vaf	Vafsi
vag	Vagla
vah	Varhadi-Nagpuri
vai	Vai
vaj	Sekele
val	Vehes
vam	Vanimo
van	Valman
vao	Vao
vap	Vaiphei
var	Huarijio
vas	Vasavi
vau	Vanuma
vav	Varli
vay	Wayu
vbb	Southeast Babar
vbk	Southwestern Bontok
vec	Venetian
ved	Veddah
vel	Veluws
vem	Vemgo-Mabas
ven	Venda
vep	Veps
ver	Mom Jango
vgr	Vaghri
vgt	Vlaamse Gebarentaal
vic	Virgin Islands Creole English
vid	Vidunda
vie	Vietnamese
vif	Vili
vig	Viemo
vil	Vilela
vin	Vinza
vis	Vishavan
vit	Viti
viv	Iduna
vki	Ija-Zuba
vkj	Kujarge
vkk	Kaur
vkl	Kulisusu
vko	Kodeoha
vkp	Korlai Creole Portuguese
vkt	Tenggarong Kutai Malay
vku	Kurrama
vlp	Valpei
vls	Vlaams
vma	Martuyhunira
vmc	Juxtlahuaca Mixtec
vmd	Mudu Koraga
vme	East Masela
vmf	Mainfränkisch
vmg	Lungalunga
vmh	Maraghei
vmj	Ixtayutla Mixtec
vmk	Makhuwa-Shirima
vmm	Mitlatongo Mixtec
vmp	Soyaltepec Mazatec
vmq	Soyaltepec Mixtec
vmr	Marenje
vmw	Makhuwa
vmx	Tamazola Mixtec
vmy	Ayautla Mazatec
vmz	Mazatlán Mazatec
vnk	Vano
vnm	Vinmavis
vnp	Vunapu
vor	Voro
vot	Votic
vra	Veraa
vro	Võro
vrs	Varisi
vrt	Burmbar
vsi	Moldova Sign Language
vsl	Venezuelan Sign Language
vsv	Valencian Sign Language
vto	Vitou
vum	Vumbu
vun	Vunjo
vut	Vute
vwa	Awa (China)
waa	Walla Walla
wab	Wab
wad	Wandamen
wae	Walser
wag	Waema
wah	Watubela
wai	Wares
waj	Waffa
wal	Wolaytta
wan	Wan
wap	Wapishana
waq	Wagiman
war	Waray (Philippines)
was	Washo
wat	Kaninuwa
wau	Waurá
wav	Waka
waw	Waiwai
wax	Watam
way	Wayana
waz	Wampur
wba	Warao
wbb	Wabo
wbe	Waritai
wbf	Wara
wbh	Wanda
wbi	Vwanji
wbj	Alagwa
wbk	Waigali
wbl	Wakhi
wbm	Wa
wbp	Warlpiri
wbq	Waddar
wbr	Wagdi
wbs	West Bengal Sign Language
wbt	Warnman
wbv	Wajarri
wbw	Woi
wca	Yanomámi
wci	Waci Gbe
wdd	Wandji
wdg	Wadaginam
wdj	Wadjiginy
wec	Wè Western
wed	Wedau
weg	Wergaia
weh	Weh
wei	Kiunum
wem	Weme Gbe
weo	Wemale
wep	Westphalien
wer	Weri
wes	Cameroon Pidgin
wet	Perai
weu	Rawngtu Chin
wew	Wejewa
wfg	Yafi
wgb	Wagawaga
wgi	Wahgi
wgo	Waigeo
wgy	Warrgamay
wha	Sou Upaa
whg	North Wahgi
whk	Wahau Kenyah
whu	Wahau Kayan
wib	Southern Toussian
wig	Wik Ngathan
wih	Wik-Meanha
wii	Minidien
wij	Wik-Iiyanh
wik	Wikalkan
wim	Wik-Mungkan
win	Ho-Chunk
wiu	Wiru
wiv	Vitu
wja	Waja
wji	Warji
wkb	Kumbaran
wkd	Wakde
wkl	Kalanadi
wkr	Keerray-Woorroong
wku	Kunduvadi
wla	Walio
wlc	Mwali Comorian
wle	Wolane
wlg	Kunbarlang
wlh	Welaun
wli	Waioli
wll	Wali (Sudan)
wln	Walloon
wlo	Wolio
wlr	Wailapa
wls	Wallisian
wlv	Wichí Lhamtés Vejoz
wlw	Walak
wlx	Wali (Ghana)
wmb	Wambaya
wmc	Wamas
wmd	Mamaindé
wme	Wambule
wmh	Waimaa
wmm	Maiwa (Indonesia)
wmo	Wom (Papua New Guinea)
wms	Wambon
wmt	Walmajarri
wmw	Mwani
wmx	Womo
wnb	Wanambre
wnc	Wantoat
wne	Waneci
wng	Wanggom
wni	Ndzwani Comorian
wnk	Wanukaka
wno	Wano
wnp	Wanap
wnu	Usan
wnw	Wintu
wny	Wanyi
woa	Kuwema
wob	Wè Northern
woc	Wogeo
wod	Wolani
woe	Woleaian
wof	Gambian Wolof
wog	Wogamusin
woi	Kamang
wok	Longto
wol	Wolof
wom	Wom (Nigeria)
won	Wongo
woo	Manombai
wor	Woria
wos	Hanga Hundi
wow	Wawonii
wpc	Maco
wra	Warapu
wrd	Warduji
wrk	Garrwa
wrl	Warlmanpa
wrm	Warumungu
wrn	Warnang
wrp	Waropen
wrr	Wardaman
wrs	Waris
wru	Waru
wrv	Waruna
wrx	Wae Rana
wry	Merwari
wsa	Warembori
wsg	Adilabad Gondi
wsi	Wusi
wsk	Waskia
wsr	Owenia
wss	Wasa
wtf	Watiwa
wti	Berta
wtk	Watakataui
wtm	Mewati
wtw	Wotu
wua	Wikngenchera
wub	Wunambal
wud	Wudu
wuh	Wutunhua
wul	Silimo
wum	Wumbvu
wun	Bungu
wut	Wutung
wuu	Wu Chinese
wuv	Wuvulu-Aua
wux	Wulna
wuy	Wauyai
wwa	Waama
wwo	Wetamut
www	Wawa
wxa	Waxianghua
wya	Wyandot
wyb	Wangaaybuwan-Ngiyambaa
wym	Wymysorys
wyr	Wayoró
wyy	Western Fijian
xab	Sambe
xac	Kachari
xal	Kalmyk
xan	Xamtanga
xao	Khao
xat	Katawixi
xau	Kauwera
xav	Xavánte
xaw	Kawaiisu
xay	Kayan Mahakam
xbi	Kombio
xbr	Kambera
xby	Batjala
xda	Darkinyung
xdo	Kwandu
xdy	Malayic Dayak
xed	Hdi
xel	Kelo
xem	Kembayan
xer	Xerénte
xes	Kesawai
xet	Xetá
xeu	Keoru-Ahia
xgu	Unggumi
xhe	Khetrani
xho	Xhosa
xhv	Khua
xii	Xiri
xis	Kisan
xiy	Xipaya
xka	Kalkoti
xkb	Northern Nago
xkc	Khoini
xkd	Mendalam Kayan
xke	Kereho
xkf	Khengkha
xkg	Kagoro
xki	Kenyan Sign Language
xkj	Kajali
xkk	Kaco
xkl	Mainstream Kenyah
xkn	Kayan River Kayan
xko	Kiorr
xkp	Kabatei
xkq	Koroni
xks	Kumbewaha
xkt	Kantosi
xku	Kaamba
xkv	Kgalagadi
xkw	Kembra
xkx	Karore
xky	Uma Lasan
xkz	Kurtokha
xla	Kamula
xma	Mushungulu
xmb	Mbonga
xmc	Makhuwa-Marrevone
xmd	Mbudum
xmf	Mingrelian
xmg	Mengaka
xmh	Kugu-Muminh
xmj	Majera
xml	Malaysian Sign Language
xmm	Manado Malay
xmo	Morerebi
xms	Moroccan Sign Language
xmt	Matbat
xmv	Antankarana Malagasy
xmw	Tsimihety Malagasy
xmx	Maden
xmy	Mayaguduna
xmz	Mori Bawah
xnb	Kanakanabu
xnh	Kuanhua
xnn	Northern Kankanay
xnr	Kangri
xns	Kanashi
xny	Nyiyaparli
xnz	Kenzi
xod	Kokoda
xog	Soga
xoi	Kominimung
xok	Xokleng
xom	Komo (Sudan)
xon	Konkomba
xop	Kopar
xor	Korubo
xow	Kowaki
xpe	Liberia Kpelle
xpk	Kulina Pano
xra	Krahô
xrb	Eastern Karaboro
xre	Kreye
xri	Krikati-Timbira
xru	Marriammu
xrw	Karawa
xsb	Sambal
xse	Sempan
xsh	Shamang
xsi	Sio
xsj	Subi
xsl	South Slavey
xsm	Kasem
xsn	Sanga (Nigeria)
xsp	Silopi
xsq	Makhuwa-Saka
xsr	Sherpa
xsu	Sanumá
xsy	Saisiyat
xta	Alcozauca Mixtec
xtb	Chazumba Mixtec
xtc	Katcha-Kadugli-Miri
xtd	Diuxi-Tilantongo Mixtec
xte	Ketengban
xti	Sinicahua Mixtec
xtj	San Juan Teita Mixtec
xtl	Tijaltepec Mixtec
xtm	Magdalena Peñasco Mixtec
xtn	Northern Tlaxiaco Mixtec
xtp	San Miguel Piedras Mixtec
xts	Sindihui Mixtec
xtt	Tacahua Mixtec
xtu	Cuyamecalco Mixtec
xtw	Tawandê
xty	Yoloxochitl Mixtec
xua	Alu Kurumba
xub	Betta Kurumba
xug	Kunigami
xuj	Jennu Kurumba
xuo	Kuo
xuu	Kxoe
xvi	Kamviri
xwa	Kwaza
xwe	Xwela Gbe
xwg	Kwegu
xwl	Western Xwla Gbe
xwr	Kwerba Mamberamo
xxk	Keo
xyy	Yorta Yorta
yaa	Yaminahua
yab	Yuhup
yac	Pass Valley Yali
yad	Yagua
yae	Pumé
yaf	Yaka (Democratic Republic of Congo)
yag	Yámana
yah	Yazgulyam
yai	Yagnobi
yaj	Banda-Yangere
yak	Yakama
yal	Yalunka
yam	Yamba
yan	Mayangna
yao	Yao
yap	Yapese
yaq	Yaqui
yar	Yabarana
yas	Nugunu (Cameroon)
yat	Yambeta
yau	Yuwana
yav	Yangben
yaw	Yawalapití
yax	Yauma
yay	Agwagwune
yaz	Lokaa
yba	Yala
ybb	Yemba
ybe	West Yugur
ybh	Yakha
ybi	Yamphu
ybj	Hasha
ybk	Bokha
ybl	Yukuben
ybm	Yaben
ybo	Yabong
ybx	Yawiyo
yby	Yaweyuha
ych	Chesu
ycl	Lolopo
ycn	Yucuna
ycp	Chepya
ydd	Eastern Yiddish
yde	Yangum Dey
ydg	Yidgha
ydk	Yoidik
yea	Ravula
yec	Yeniche
yee	Yimas
yej	Yevanic
yel	Yela
yer	Tarok
yes	Nyankpa
yet	Yetfa
yeu	Yerukula
yev	Yapunda
yey	Yeyi
ygl	Yangum Gel
ygm	Yagomi
ygp	Gepo
ygr	Yagaria
ygs	Yolŋu Sign Language
ygu	Yugul
ygw	Yagwoia
yha	Baha Buyang
yhd	Judeo-Iraqi Arabic
yhl	Hlepho Phowa
yhs	Yan-nhaŋu Sign Language
yia	Yinggarda
yif	Ache
yig	Wusa Nasu
yih	Western Yiddish
yii	Yidiny
yij	Yindjibarndi
yik	Dongshanba Lalo
yim	Yimchungru Naga
yin	Riang Lai
yip	Pholo
yiq	Miqie
yir	North Awyu
yis	Yis
yit	Eastern Lalu
yiu	Awu
yiv	Northern Nisu
yix	Axi Yi
yiz	Azhe
yka	Yakan
ykg	Northern Yukaghir
yki	Yoke
ykk	Yakaikeke
ykl	Khlula
ykm	Kap
ykn	Kua-nsi
yko	Yasa
ykr	Yekora
ykt	Kathu
yku	Kuamasi
yky	Yakoma
yla	Yaul
ylb	Yaleba
yle	Yele
ylg	Yelogu
yli	Angguruk Yali
yll	Yil
ylm	Limi
yln	Langnian Buyang
ylo	Naluo Yi
ylu	Aribwaung
yly	Nyâlayu
ymb	Yambes
ymc	Southern Muji
ymd	Muda
ymg	Yamongeri
ymh	Mili
ymi	Moji
ymk	Makwe
yml	Iamalele
ymm	Maay
ymn	Yamna
ymo	Yangum Mon
ymp	Yamap
ymq	Qila Muji
ymr	Malasar
ymx	Northern Muji
ymz	Muzi
yna	Aluo
yne	Lange
yng	Yango
ynk	Naukan Yupik
ynl	Yangulam
yno	Yong
ynq	Yendang
yns	Yansi
yog	Yogad
yoi	Yonaguni
yok	Yokuts
yom	Yombe
yon	Yongkom
yor	Yoruba
yot	Yotti
yox	Yoron
yoy	Yoy
ypa	Phala
ypb	Labo Phowa
ypg	Phola
yph	Phupha
ypm	Phuma
ypn	Ani Phowa
ypo	Alo Phola
ypp	Phupa
ypz	Phuza
yra	Yerakai
yrb	Yareba
yre	Yaouré
yrk	Nenets
yrl	Nhengatu
yrm	Yirrk-Mel
yrn	Yerong
yro	Yaroamë
yrs	Yarsun
yrw	Yarawata
yry	Yarluyandi
ysd	Samatao
ysg	Sonaga
ysl	Yugoslavian Sign Language
ysn	Sani
yso	Nisi (China)
ysp	Southern Lolopo
yss	Yessan-Mayo
ysy	Sanie
yta	Talu
ytl	Tanglang
ytp	Thopho
ytw	Yout Wam
yua	Yucateco
yuc	Yuchi
yud	Judeo-Tripolitanian Arabic
yue	Yue Chinese
yuf	Havasupai-Walapai-Yavapai
yui	Yurutí
yuj	Karkar-Yuri
yul	Yulu
yum	Quechan
yun	Bena (Nigeria)
yup	Yukpa
yuq	Yuqui
yut	Yopno
yuw	Yau (Morobe Province)
yux	Southern Yukaghir
yuy	East Yugur
yuz	Yuracare
yva	Yawa
ywa	Kalou
ywg	Yinhawangka
ywl	Western Lalu
ywn	Yawanawa
ywq	Wuding-Luquan Yi
ywr	Yawuru
ywt	Xishanba Lalo
ywu	Wumeng Nasu
yyu	Yau (Sandaun Province)
yyz	Ayizi
yzg	Ema Buyang
yzk	Zokhuo
zaa	Sierra de Juárez Zapotec
zab	Western Tlacolula Valley Zapotec
zac	Ocotlán Zapotec
zad	Cajonos Zapotec
zae	Yareni Zapotec
zaf	Ayoquesco Zapotec
zag	Zaghawa
zah	Zangwal
zai	Isthmus Zapotec
zaj	Zaramo
zak	Zanaki
zal	Zauzou
zam	Miahuatlán Zapotec
zao	Ozolotepec Zapotec
zaq	Aloápam Zapotec
zar	Rincón Zapotec
zas	Santo Domingo Albarradas Zapotec
zat	Tabaa Zapotec
zau	Zangskari
zav	Yatzachi Zapotec
zaw	Mitla Zapotec
zax	Xadani Zapotec
zay	Zayse-Zergulla
zaz	Zari
zbc	Central Berawan
zbe	East Berawan
zbt	Batui
zbw	West Berawan
zca	Coatecas Altas Zapotec
zch	Central Hongshuihe Zhuang
zdj	Ngazidja Comorian
zea	Zeeuws
zeg	Zenag
zeh	Eastern Hongshuihe Zhuang
zen	Zenaga
zga	Kinga
zgb	Guibei Zhuang
zgh	Standard Moroccan Tamazight
zgm	Minz Zhuang
zgn	Guibian Zhuang
zgr	Magori
zhb	Zhaba
zhd	Dai Zhuang
zhi	Zhire
zhn	Nong Zhuang
zhw	Zhoa
zia	Zia
zib	Zimbabwe Sign Language
zik	Zimakani
zil	Zialo
zim	Mesme
zin	Zinza
ziw	Zigula
ziz	Zizilivakan
zka	Kaimbulawa
zkd	Kadu
zkn	Kanan
zkr	Zakhring
zku	Kaurna
zlj	Liujiang Zhuang
zlm	Malay (individual language)
zln	Lianshan Zhuang
zlq	Liuqian Zhuang
zma	Manda (Australia)
zmb	Zimba
zmd	Maridan
zmf	Mfinu
zmg	Marti Ke
zmi	Negeri Sembilan Malay
zmj	Maridjabin
zmm	Marimanindji
zmn	Mbangwe
zmo	Molo
zmp	Mpuono
zmq	Mituku
zmr	Maranunggu
zms	Mbesa
zmt	Maringarr
zmw	Mbo (Democratic Republic of Congo)
zmx	Bomitaba
zmy	Mariyedi
zmz	Mbandja
zna	Zan Gula
zne	Zande (individual language)
zng	Mang
zns	Mangas
zoc	Copainalá Zoque
zoh	Chimalapa Zoque
zom	Zou
zoo	Asunción Mixtepec Zapotec
zoq	Tabasco Zoque
zor	Rayón Zoque
zos	Francisco León Zoque
zpa	Lachiguiri Zapotec
zpb	Yautepec Zapotec
zpc	Choapan Zapotec
zpd	Southeastern Ixtlán Zapotec
zpe	Petapa Zapotec
zpf	San Pedro Quiatoni Zapotec
zpg	Guevea De Humboldt Zapotec
zph	Totomachapan Zapotec
zpi	Santa María Quiegolani Zapotec
zpj	Quiavicuzas Zapotec
zpk	Tlacolulita Zapotec
zpl	Lachixío Zapotec
zpm	Mixtepec Zapotec
zpn	Santa Inés Yatzechi Zapotec
zpo	Amatlán Zapotec
zpp	El Alto Zapotec
zpq	Zoogocho Zapotec
zpr	Santiago Xanica Zapotec
zps	Coatlán Zapotec
zpt	San Vicente Coatlán Zapotec
zpu	Yalálag Zapotec
zpv	Chichicapan Zapotec
zpw	Zaniza Zapotec
zpx	San Baltazar Loxicha Zapotec
zpy	Mazaltepec Zapotec
zpz	Texmelucan Zapotec
zqe	Qiubei Zhuang
zrg	Mirgan
zrn	Zerenkel
zro	Záparo
zrs	Mairasi
zsa	Sarasira
zsl	Zambian Sign Language
zsm	Standard Malay
zsr	Southern Rincon Zapotec
zsu	Sukurum
zte	Elotepec Zapotec
ztg	Xanaguía Zapotec
ztl	Lapaguía-Guivini Zapotec
ztm	San Agustín Mixtepec Zapotec
ztn	Santa Catarina Albarradas Zapotec
ztp	Loxicha Zapotec
ztq	Quioquitani-Quierí Zapotec
zts	Tilquiapan Zapotec
ztt	Tejalapan Zapotec
ztu	Güilá Zapotec
ztx	Zaachila Zapotec
zty	Yatee Zapotec
zua	Zeem
zuh	Tokano
zul	Zulu
zum	Kumzari
zun	Zuni
zuy	Zumaya
zwa	Zay
zyb	Yongbei Zhuang
zyg	Yang Zhuang
zyj	Youjiang Zhuang
zyn	Yongnan Zhuang
zyp	Zyphe Chin
zzj	Zuojiang Zhuang
\.


--
-- TOC entry 5173 (class 0 OID 21856)
-- Dependencies: 317
-- Data for Name: lifts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lifts (id, capacity, persons_per_chair) FROM stdin;
\.


--
-- TOC entry 5147 (class 0 OID 21495)
-- Dependencies: 291
-- Data for Name: media_objects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.media_objects (id, license_holder_id, author, content_type, duration, height, license, width) FROM stdin;
\.


--
-- TOC entry 5174 (class 0 OID 21866)
-- Dependencies: 318
-- Data for Name: mountain_areas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mountain_areas (id, area_owner_id, area, total_park_area, total_slope_length) FROM stdin;
\.


--
-- TOC entry 5191 (class 0 OID 22101)
-- Dependencies: 335
-- Data for Name: multimedia_descriptions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.multimedia_descriptions (resource_id, media_object_id) FROM stdin;
\.


--
-- TOC entry 5141 (class 0 OID 19651)
-- Dependencies: 285
-- Data for Name: mytable; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mytable (pk, name, size, geom) FROM stdin;
1	Peter	1	010100000000000000000000400000000000004140
2	Paul	2	010100000000000000000014400000000000C05040
\.


--
-- TOC entry 5155 (class 0 OID 21608)
-- Dependencies: 299
-- Data for Name: names; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.names (lang, resource_id, content) FROM stdin;
eng	schema:BusinessEvent	Business Event
eng	schema:ChildrensEvent	Childrens Event
eng	schema:ComedyEvent	Comedy Event
eng	schema:CourseInstance	Course Instance
eng	schema:DanceEvent	Dance Event
eng	schema:DeliveryEvent	Delivery Event
eng	schema:EducationEvent	Education Event
eng	schema:EventSeries	Event Series
eng	schema:ExhibitionEvent	Exhibition Event
eng	schema:Festival	Festival
eng	schema:FoodEvent	Food Event
eng	schema:Hackathon	Hackathon
eng	schema:LiteraryEvent	Literary Event
eng	schema:MusicEvent	Music Event
eng	schema:PublicationEvent	Publication Event
eng	schema:SaleEvent	Sale Event
eng	schema:ScreeningEvent	Screening Event
eng	schema:SocialEvent	Social Event
eng	schema:SportsEvent	Sports Event
eng	schema:TheaterEvent	Theater Event
eng	schema:VisualArtsEvent	Visual Arts Event
eng	alpinebits:inPersonEvent	In-Person Event
eng	alpinebits:virtualEvent	Virtual Event
eng	alpinebits:hybridEvent	Hybrid Event
eng	alpinebits:person	Person
ita	alpinebits:person	Persona
deu	alpinebits:person	Person
deu	alpinebits:organization	Organisation
ita	alpinebits:organization	Organizzazione
eng	alpinebits:organization	Organization
eng	alpinebits:standard-ski-slope	Standard Ski-Slope
eng	alpinebits:sledge-slope	Sledge-Slope
eng	alpinebits:cross-country	Cross-Country
eng	alpinebits:chairlift	Cablecar
eng	alpinebits:gondola	Chairlift
eng	alpinebits:skilift	Funicular
eng	alpinebits:cablecar	Gondola
eng	alpinebits:funicular	Magic-Carpet
eng	alpinebits:magic-carpet	Skibus
eng	alpinebits:skibus	Skilift
eng	alpinebits:train	Train
\.


--
-- TOC entry 5189 (class 0 OID 22071)
-- Dependencies: 333
-- Data for Name: organizers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.organizers (organizer_id, event_id) FROM stdin;
\.


--
-- TOC entry 5158 (class 0 OID 21659)
-- Dependencies: 302
-- Data for Name: participation_urls; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.participation_urls (lang, event_id, content) FROM stdin;
\.


--
-- TOC entry 5168 (class 0 OID 21787)
-- Dependencies: 312
-- Data for Name: places; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.places (id, address_id, geometries, length, max_altitude, min_altitude, opening_hours) FROM stdin;
\.


--
-- TOC entry 5164 (class 0 OID 21734)
-- Dependencies: 308
-- Data for Name: regions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regions (lang, address_id, content) FROM stdin;
\.


--
-- TOC entry 5159 (class 0 OID 21676)
-- Dependencies: 303
-- Data for Name: registration_urls; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registration_urls (lang, event_id, content) FROM stdin;
\.


--
-- TOC entry 5182 (class 0 OID 21976)
-- Dependencies: 326
-- Data for Name: resource_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_categories (category_id, resource_id) FROM stdin;
\.


--
-- TOC entry 5183 (class 0 OID 21991)
-- Dependencies: 327
-- Data for Name: resource_features; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_features (feature_id, resource_id) FROM stdin;
\.


--
-- TOC entry 5142 (class 0 OID 21445)
-- Dependencies: 286
-- Data for Name: resource_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resource_types (type, title) FROM stdin;
agents	Agents
categories	Categories
events	Events
eventSeries	Event Series
features	Features
lifts	Lifts
mediaObjects	Media Objects
mountainAreas	Mountain Areas
skiSlopes	Ski Slopes
snowparks	Snowparks
venues	Venues
\.


--
-- TOC entry 5143 (class 0 OID 21450)
-- Dependencies: 287
-- Data for Name: resources; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.resources (id, type, odh_id, data_provider, created_at, last_update, simple_url) FROM stdin;
schema:BusinessEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:ChildrensEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:ComedyEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:CourseInstance	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:DanceEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:DeliveryEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:EducationEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:EventSeries	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:ExhibitionEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:Festival	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:FoodEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:Hackathon	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:LiteraryEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:MusicEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:PublicationEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:SaleEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:ScreeningEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:SocialEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:SportsEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:TheaterEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
schema:VisualArtsEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:inPersonEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:virtualEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:hybridEvent	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:person	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:organization	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:standard-ski-slope	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:sledge-slope	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:cross-country	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:chairlift	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:gondola	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:skilift	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:cablecar	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:funicular	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:magic-carpet	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:skibus	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
alpinebits:train	categories	\N	http://tourism.opendatahub.bz.it/	2022-12-22 21:46:28.528881+00	2022-12-22 21:46:28.528881+00	\N
\.


--
-- TOC entry 5148 (class 0 OID 21512)
-- Dependencies: 292
-- Data for Name: series_frequencies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.series_frequencies (frequency, title) FROM stdin;
daily	Daily
weekly	Weekly
monthly	Monthly
bimonthly	Bimonthly
quarterly	Quarterly
annual	Annual
biennial	Biennial
triennial	Triennial
\.


--
-- TOC entry 5156 (class 0 OID 21625)
-- Dependencies: 300
-- Data for Name: short_names; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.short_names (lang, resource_id, content) FROM stdin;
\.


--
-- TOC entry 5175 (class 0 OID 21881)
-- Dependencies: 319
-- Data for Name: ski_slopes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ski_slopes (id, difficulty_eu, difficulty_us) FROM stdin;
\.


--
-- TOC entry 5170 (class 0 OID 21821)
-- Dependencies: 314
-- Data for Name: snow_conditions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.snow_conditions (id, base_snow, base_snow_range_lower, base_snow_range_upper, groomed, latest_storm, obtained_in, primary_surface, secondary_surface, snow_making, snow_over_night) FROM stdin;
\.


--
-- TOC entry 5176 (class 0 OID 21891)
-- Dependencies: 320
-- Data for Name: snowparks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.snowparks (id, difficulty) FROM stdin;
\.


--
-- TOC entry 4745 (class 0 OID 18318)
-- Dependencies: 223
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- TOC entry 5190 (class 0 OID 22086)
-- Dependencies: 334
-- Data for Name: sponsors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sponsors (sponsor_id, event_id) FROM stdin;
\.


--
-- TOC entry 5165 (class 0 OID 21751)
-- Dependencies: 309
-- Data for Name: streets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.streets (lang, address_id, content) FROM stdin;
\.


--
-- TOC entry 5180 (class 0 OID 21946)
-- Dependencies: 324
-- Data for Name: sub_areas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sub_areas (parent_id, child_id) FROM stdin;
\.


--
-- TOC entry 5157 (class 0 OID 21642)
-- Dependencies: 301
-- Data for Name: urls; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.urls (lang, resource_id, content) FROM stdin;
eng	schema:BusinessEvent	https://schema.org/BusinessEvent
eng	schema:ChildrensEvent	https://schema.org/ChildrensEvent
eng	schema:ComedyEvent	https://schema.org/ComedyEvent
eng	schema:CourseInstance	https://schema.org/CourseInstance
eng	schema:DanceEvent	https://schema.org/DanceEvent
eng	schema:DeliveryEvent	https://schema.org/DeliveryEvent
eng	schema:EducationEvent	https://schema.org/EducationEvent
eng	schema:EventSeries	https://schema.org/EventSeries
eng	schema:ExhibitionEvent	https://schema.org/ExhibitionEvent
eng	schema:Festival	https://schema.org/Festival
eng	schema:FoodEvent	https://schema.org/FoodEvent
eng	schema:Hackathon	https://schema.org/Hackathon
eng	schema:LiteraryEvent	https://schema.org/LiteraryEvent
eng	schema:MusicEvent	https://schema.org/MusicEvent
eng	schema:PublicationEvent	https://schema.org/PublicationEvent
eng	schema:SaleEvent	https://schema.org/SaleEvent
eng	schema:ScreeningEvent	https://schema.org/ScreeningEvent
eng	schema:SocialEvent	https://schema.org/SocialEvent
eng	schema:SportsEvent	https://schema.org/SportsEvent
eng	schema:TheaterEvent	https://schema.org/TheaterEvent
eng	schema:VisualArtsEvent	https://schema.org/VisualArtsEvent
\.


--
-- TOC entry 5171 (class 0 OID 21831)
-- Dependencies: 315
-- Data for Name: venues; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.venues (id) FROM stdin;
\.


--
-- TOC entry 4748 (class 0 OID 19232)
-- Dependencies: 234
-- Data for Name: geocode_settings; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY tiger.geocode_settings (name, setting, unit, category, short_desc) FROM stdin;
\.


--
-- TOC entry 4749 (class 0 OID 19564)
-- Dependencies: 279
-- Data for Name: pagc_gaz; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY tiger.pagc_gaz (id, seq, word, stdword, token, is_custom) FROM stdin;
\.


--
-- TOC entry 4750 (class 0 OID 19574)
-- Dependencies: 281
-- Data for Name: pagc_lex; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY tiger.pagc_lex (id, seq, word, stdword, token, is_custom) FROM stdin;
\.


--
-- TOC entry 4751 (class 0 OID 19584)
-- Dependencies: 283
-- Data for Name: pagc_rules; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY tiger.pagc_rules (id, rule, is_custom) FROM stdin;
\.


--
-- TOC entry 4746 (class 0 OID 19054)
-- Dependencies: 228
-- Data for Name: topology; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY topology.topology (id, name, srid, "precision", hasz) FROM stdin;
\.


--
-- TOC entry 4747 (class 0 OID 19066)
-- Dependencies: 229
-- Data for Name: layer; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY topology.layer (topology_id, layer_id, schema_name, table_name, feature_column, feature_type, level, child_id) FROM stdin;
\.


--
-- TOC entry 5206 (class 0 OID 0)
-- Dependencies: 304
-- Name: addresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.addresses_id_seq', 1, false);


--
-- TOC entry 5207 (class 0 OID 0)
-- Dependencies: 310
-- Name: contact_points_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.contact_points_id_seq', 1, false);


--
-- TOC entry 5208 (class 0 OID 0)
-- Dependencies: 284
-- Name: mytable_pk_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.mytable_pk_seq', 2, true);


--
-- TOC entry 4811 (class 2606 OID 21590)
-- Name: abstracts abstracts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.abstracts
    ADD CONSTRAINT abstracts_pkey PRIMARY KEY (lang, resource_id);


--
-- TOC entry 4825 (class 2606 OID 21699)
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- TOC entry 4793 (class 2606 OID 21469)
-- Name: agents agents_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agents
    ADD CONSTRAINT agents_pkey PRIMARY KEY (id);


--
-- TOC entry 4855 (class 2606 OID 21915)
-- Name: area_lifts area_lifts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_lifts
    ADD CONSTRAINT area_lifts_pkey PRIMARY KEY (area_id, lift_id);


--
-- TOC entry 4857 (class 2606 OID 21930)
-- Name: area_ski_slopes area_ski_slopes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_ski_slopes
    ADD CONSTRAINT area_ski_slopes_pkey PRIMARY KEY (area_id, ski_slope_id);


--
-- TOC entry 4859 (class 2606 OID 21945)
-- Name: area_snowparks area_snowparks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_snowparks
    ADD CONSTRAINT area_snowparks_pkey PRIMARY KEY (area_id, snowpark_id);


--
-- TOC entry 4795 (class 2606 OID 21479)
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- TOC entry 4869 (class 2606 OID 22020)
-- Name: category_covered_types category_covered_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_covered_types
    ADD CONSTRAINT category_covered_types_pkey PRIMARY KEY (category_id, type);


--
-- TOC entry 4871 (class 2606 OID 22035)
-- Name: category_specializations category_specializations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_specializations
    ADD CONSTRAINT category_specializations_pkey PRIMARY KEY (parent_id, child_id);


--
-- TOC entry 4827 (class 2606 OID 21716)
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (lang, address_id);


--
-- TOC entry 4829 (class 2606 OID 21733)
-- Name: complements complements_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.complements
    ADD CONSTRAINT complements_pkey PRIMARY KEY (lang, address_id);


--
-- TOC entry 4863 (class 2606 OID 21975)
-- Name: connections connections_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connections
    ADD CONSTRAINT connections_pkey PRIMARY KEY (a_id, b_id);


--
-- TOC entry 4835 (class 2606 OID 21776)
-- Name: contact_points contact_points_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_points
    ADD CONSTRAINT contact_points_pkey PRIMARY KEY (id);


--
-- TOC entry 4877 (class 2606 OID 22070)
-- Name: contributors contributors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contributors
    ADD CONSTRAINT contributors_pkey PRIMARY KEY (contributor_id, event_id);


--
-- TOC entry 4813 (class 2606 OID 21607)
-- Name: descriptions descriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.descriptions
    ADD CONSTRAINT descriptions_pkey PRIMARY KEY (lang, resource_id);


--
-- TOC entry 4803 (class 2606 OID 21521)
-- Name: event_series event_series_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_series
    ADD CONSTRAINT event_series_pkey PRIMARY KEY (id);


--
-- TOC entry 4805 (class 2606 OID 21536)
-- Name: event_status event_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_status
    ADD CONSTRAINT event_status_pkey PRIMARY KEY (status);


--
-- TOC entry 4845 (class 2606 OID 21855)
-- Name: event_venues event_venues_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_venues
    ADD CONSTRAINT event_venues_pkey PRIMARY KEY (venue_id, event_id);


--
-- TOC entry 4807 (class 2606 OID 21543)
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- TOC entry 4873 (class 2606 OID 22050)
-- Name: feature_covered_types feature_covered_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feature_covered_types
    ADD CONSTRAINT feature_covered_types_pkey PRIMARY KEY (feature_id, type);


--
-- TOC entry 4875 (class 2606 OID 22055)
-- Name: feature_specializations feature_specializations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feature_specializations
    ADD CONSTRAINT feature_specializations_pkey PRIMARY KEY (parent_id, child_id);


--
-- TOC entry 4797 (class 2606 OID 21489)
-- Name: features features_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.features
    ADD CONSTRAINT features_pkey PRIMARY KEY (id);


--
-- TOC entry 4839 (class 2606 OID 21820)
-- Name: how_to_arrive how_to_arrive_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.how_to_arrive
    ADD CONSTRAINT how_to_arrive_pkey PRIMARY KEY (lang, place_id);


--
-- TOC entry 4809 (class 2606 OID 21573)
-- Name: language_codes language_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.language_codes
    ADD CONSTRAINT language_codes_pkey PRIMARY KEY (lang);


--
-- TOC entry 4847 (class 2606 OID 21860)
-- Name: lifts lifts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lifts
    ADD CONSTRAINT lifts_pkey PRIMARY KEY (id);


--
-- TOC entry 4799 (class 2606 OID 21501)
-- Name: media_objects media_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.media_objects
    ADD CONSTRAINT media_objects_pkey PRIMARY KEY (id);


--
-- TOC entry 4849 (class 2606 OID 21870)
-- Name: mountain_areas mountain_areas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mountain_areas
    ADD CONSTRAINT mountain_areas_pkey PRIMARY KEY (id);


--
-- TOC entry 4883 (class 2606 OID 22115)
-- Name: multimedia_descriptions multimedia_descriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.multimedia_descriptions
    ADD CONSTRAINT multimedia_descriptions_pkey PRIMARY KEY (resource_id, media_object_id);


--
-- TOC entry 4787 (class 2606 OID 19658)
-- Name: mytable mytable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mytable
    ADD CONSTRAINT mytable_pkey PRIMARY KEY (pk);


--
-- TOC entry 4815 (class 2606 OID 21624)
-- Name: names names_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.names
    ADD CONSTRAINT names_pkey PRIMARY KEY (lang, resource_id);


--
-- TOC entry 4879 (class 2606 OID 22085)
-- Name: organizers organizers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizers
    ADD CONSTRAINT organizers_pkey PRIMARY KEY (organizer_id, event_id);


--
-- TOC entry 4821 (class 2606 OID 21675)
-- Name: participation_urls participation_urls_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participation_urls
    ADD CONSTRAINT participation_urls_pkey PRIMARY KEY (lang, event_id);


--
-- TOC entry 4837 (class 2606 OID 21793)
-- Name: places places_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.places
    ADD CONSTRAINT places_pkey PRIMARY KEY (id);


--
-- TOC entry 4831 (class 2606 OID 21750)
-- Name: regions regions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (lang, address_id);


--
-- TOC entry 4823 (class 2606 OID 21692)
-- Name: registration_urls registration_urls_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registration_urls
    ADD CONSTRAINT registration_urls_pkey PRIMARY KEY (lang, event_id);


--
-- TOC entry 4865 (class 2606 OID 21990)
-- Name: resource_categories resource_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_categories
    ADD CONSTRAINT resource_categories_pkey PRIMARY KEY (resource_id, category_id);


--
-- TOC entry 4867 (class 2606 OID 22005)
-- Name: resource_features resource_features_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_features
    ADD CONSTRAINT resource_features_pkey PRIMARY KEY (resource_id, feature_id);


--
-- TOC entry 4789 (class 2606 OID 21449)
-- Name: resource_types resource_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_types
    ADD CONSTRAINT resource_types_pkey PRIMARY KEY (type);


--
-- TOC entry 4791 (class 2606 OID 21459)
-- Name: resources resources_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resources
    ADD CONSTRAINT resources_pkey PRIMARY KEY (id);


--
-- TOC entry 4801 (class 2606 OID 21516)
-- Name: series_frequencies series_frequencies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.series_frequencies
    ADD CONSTRAINT series_frequencies_pkey PRIMARY KEY (frequency);


--
-- TOC entry 4817 (class 2606 OID 21641)
-- Name: short_names short_names_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.short_names
    ADD CONSTRAINT short_names_pkey PRIMARY KEY (lang, resource_id);


--
-- TOC entry 4851 (class 2606 OID 21885)
-- Name: ski_slopes ski_slopes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ski_slopes
    ADD CONSTRAINT ski_slopes_pkey PRIMARY KEY (id);


--
-- TOC entry 4841 (class 2606 OID 21825)
-- Name: snow_conditions snow_conditions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.snow_conditions
    ADD CONSTRAINT snow_conditions_pkey PRIMARY KEY (id);


--
-- TOC entry 4853 (class 2606 OID 21895)
-- Name: snowparks snowparks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.snowparks
    ADD CONSTRAINT snowparks_pkey PRIMARY KEY (id);


--
-- TOC entry 4881 (class 2606 OID 22100)
-- Name: sponsors sponsors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsors
    ADD CONSTRAINT sponsors_pkey PRIMARY KEY (sponsor_id, event_id);


--
-- TOC entry 4833 (class 2606 OID 21767)
-- Name: streets streets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.streets
    ADD CONSTRAINT streets_pkey PRIMARY KEY (lang, address_id);


--
-- TOC entry 4861 (class 2606 OID 21960)
-- Name: sub_areas sub_areas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_areas
    ADD CONSTRAINT sub_areas_pkey PRIMARY KEY (parent_id, child_id);


--
-- TOC entry 4819 (class 2606 OID 21658)
-- Name: urls urls_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.urls
    ADD CONSTRAINT urls_pkey PRIMARY KEY (lang, resource_id);


--
-- TOC entry 4843 (class 2606 OID 21835)
-- Name: venues venues_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.venues
    ADD CONSTRAINT venues_pkey PRIMARY KEY (id);


--
-- TOC entry 4963 (class 2620 OID 22231)
-- Name: contact_points contact_point_address_deletion; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER contact_point_address_deletion AFTER DELETE ON public.contact_points FOR EACH ROW EXECUTE FUNCTION public.delete_contact_points_address();


--
-- TOC entry 4964 (class 2620 OID 22232)
-- Name: places place_address_deletion; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER place_address_deletion AFTER DELETE ON public.places FOR EACH ROW EXECUTE FUNCTION public.delete_place_address();


--
-- TOC entry 4962 (class 2620 OID 22233)
-- Name: resources sync_last_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER sync_last_update BEFORE UPDATE ON public.resources FOR EACH ROW EXECUTE FUNCTION public.sync_last_update();


--
-- TOC entry 4897 (class 2606 OID 21579)
-- Name: abstracts abstracts_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.abstracts
    ADD CONSTRAINT abstracts_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4898 (class 2606 OID 21584)
-- Name: abstracts abstracts_resource_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.abstracts
    ADD CONSTRAINT abstracts_resource_id_foreign FOREIGN KEY (resource_id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4885 (class 2606 OID 21470)
-- Name: agents agents_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agents
    ADD CONSTRAINT agents_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4934 (class 2606 OID 21904)
-- Name: area_lifts area_lifts_area_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_lifts
    ADD CONSTRAINT area_lifts_area_id_foreign FOREIGN KEY (area_id) REFERENCES public.mountain_areas(id) ON DELETE CASCADE;


--
-- TOC entry 4935 (class 2606 OID 21909)
-- Name: area_lifts area_lifts_lift_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_lifts
    ADD CONSTRAINT area_lifts_lift_id_foreign FOREIGN KEY (lift_id) REFERENCES public.lifts(id) ON DELETE CASCADE;


--
-- TOC entry 4936 (class 2606 OID 21919)
-- Name: area_ski_slopes area_ski_slopes_area_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_ski_slopes
    ADD CONSTRAINT area_ski_slopes_area_id_foreign FOREIGN KEY (area_id) REFERENCES public.mountain_areas(id) ON DELETE CASCADE;


--
-- TOC entry 4937 (class 2606 OID 21924)
-- Name: area_ski_slopes area_ski_slopes_ski_slope_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_ski_slopes
    ADD CONSTRAINT area_ski_slopes_ski_slope_id_foreign FOREIGN KEY (ski_slope_id) REFERENCES public.ski_slopes(id) ON DELETE CASCADE;


--
-- TOC entry 4938 (class 2606 OID 21934)
-- Name: area_snowparks area_snowparks_area_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_snowparks
    ADD CONSTRAINT area_snowparks_area_id_foreign FOREIGN KEY (area_id) REFERENCES public.mountain_areas(id) ON DELETE CASCADE;


--
-- TOC entry 4939 (class 2606 OID 21939)
-- Name: area_snowparks area_snowparks_snowpark_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_snowparks
    ADD CONSTRAINT area_snowparks_snowpark_id_foreign FOREIGN KEY (snowpark_id) REFERENCES public.snowparks(id) ON DELETE CASCADE;


--
-- TOC entry 4886 (class 2606 OID 21480)
-- Name: categories categories_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4948 (class 2606 OID 22009)
-- Name: category_covered_types category_covered_types_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_covered_types
    ADD CONSTRAINT category_covered_types_category_id_foreign FOREIGN KEY (category_id) REFERENCES public.categories(id) ON DELETE CASCADE;


--
-- TOC entry 4949 (class 2606 OID 22014)
-- Name: category_covered_types category_covered_types_type_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_covered_types
    ADD CONSTRAINT category_covered_types_type_foreign FOREIGN KEY (type) REFERENCES public.resource_types(type) ON DELETE CASCADE;


--
-- TOC entry 4950 (class 2606 OID 22029)
-- Name: category_specializations category_specializations_child_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_specializations
    ADD CONSTRAINT category_specializations_child_id_foreign FOREIGN KEY (child_id) REFERENCES public.categories(id) ON DELETE CASCADE;


--
-- TOC entry 4951 (class 2606 OID 22024)
-- Name: category_specializations category_specializations_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_specializations
    ADD CONSTRAINT category_specializations_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public.categories(id) ON DELETE CASCADE;


--
-- TOC entry 4911 (class 2606 OID 21710)
-- Name: cities cities_address_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_address_id_foreign FOREIGN KEY (address_id) REFERENCES public.addresses(id) ON DELETE CASCADE;


--
-- TOC entry 4912 (class 2606 OID 21705)
-- Name: cities cities_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4913 (class 2606 OID 21727)
-- Name: complements complements_address_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.complements
    ADD CONSTRAINT complements_address_id_foreign FOREIGN KEY (address_id) REFERENCES public.addresses(id) ON DELETE CASCADE;


--
-- TOC entry 4914 (class 2606 OID 21722)
-- Name: complements complements_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.complements
    ADD CONSTRAINT complements_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4942 (class 2606 OID 21964)
-- Name: connections connections_a_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connections
    ADD CONSTRAINT connections_a_id_foreign FOREIGN KEY (a_id) REFERENCES public.places(id) ON DELETE CASCADE;


--
-- TOC entry 4943 (class 2606 OID 21969)
-- Name: connections connections_b_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connections
    ADD CONSTRAINT connections_b_id_foreign FOREIGN KEY (b_id) REFERENCES public.places(id) ON DELETE CASCADE;


--
-- TOC entry 4919 (class 2606 OID 21782)
-- Name: contact_points contact_points_address_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_points
    ADD CONSTRAINT contact_points_address_id_foreign FOREIGN KEY (address_id) REFERENCES public.addresses(id) ON DELETE SET NULL;


--
-- TOC entry 4920 (class 2606 OID 21777)
-- Name: contact_points contact_points_agent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_points
    ADD CONSTRAINT contact_points_agent_id_foreign FOREIGN KEY (agent_id) REFERENCES public.agents(id) ON DELETE CASCADE;


--
-- TOC entry 4954 (class 2606 OID 22059)
-- Name: contributors contributors_contributor_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contributors
    ADD CONSTRAINT contributors_contributor_id_foreign FOREIGN KEY (contributor_id) REFERENCES public.agents(id) ON DELETE CASCADE;


--
-- TOC entry 4955 (class 2606 OID 22064)
-- Name: contributors contributors_event_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contributors
    ADD CONSTRAINT contributors_event_id_foreign FOREIGN KEY (event_id) REFERENCES public.events(id) ON DELETE CASCADE;


--
-- TOC entry 4899 (class 2606 OID 21596)
-- Name: descriptions descriptions_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.descriptions
    ADD CONSTRAINT descriptions_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4900 (class 2606 OID 21601)
-- Name: descriptions descriptions_resource_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.descriptions
    ADD CONSTRAINT descriptions_resource_id_foreign FOREIGN KEY (resource_id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4890 (class 2606 OID 21527)
-- Name: event_series event_series_frequency_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_series
    ADD CONSTRAINT event_series_frequency_foreign FOREIGN KEY (frequency) REFERENCES public.series_frequencies(frequency) ON DELETE SET NULL;


--
-- TOC entry 4891 (class 2606 OID 21522)
-- Name: event_series event_series_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_series
    ADD CONSTRAINT event_series_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4927 (class 2606 OID 21849)
-- Name: event_venues event_venues_event_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_venues
    ADD CONSTRAINT event_venues_event_id_foreign FOREIGN KEY (event_id) REFERENCES public.events(id) ON DELETE CASCADE;


--
-- TOC entry 4928 (class 2606 OID 21844)
-- Name: event_venues event_venues_venue_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_venues
    ADD CONSTRAINT event_venues_venue_id_foreign FOREIGN KEY (venue_id) REFERENCES public.venues(id) ON DELETE CASCADE;


--
-- TOC entry 4892 (class 2606 OID 21544)
-- Name: events events_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4893 (class 2606 OID 21549)
-- Name: events events_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public.events(id) ON DELETE SET NULL;


--
-- TOC entry 4894 (class 2606 OID 21554)
-- Name: events events_publisher_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_publisher_id_foreign FOREIGN KEY (publisher_id) REFERENCES public.agents(id);


--
-- TOC entry 4895 (class 2606 OID 21559)
-- Name: events events_series_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_series_id_foreign FOREIGN KEY (series_id) REFERENCES public.event_series(id) ON DELETE SET NULL;


--
-- TOC entry 4896 (class 2606 OID 21564)
-- Name: events events_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_status_foreign FOREIGN KEY (status) REFERENCES public.event_status(status) ON DELETE SET NULL;


--
-- TOC entry 4952 (class 2606 OID 22039)
-- Name: feature_covered_types feature_covered_types_feature_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feature_covered_types
    ADD CONSTRAINT feature_covered_types_feature_id_foreign FOREIGN KEY (feature_id) REFERENCES public.features(id) ON DELETE CASCADE;


--
-- TOC entry 4953 (class 2606 OID 22044)
-- Name: feature_covered_types feature_covered_types_type_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feature_covered_types
    ADD CONSTRAINT feature_covered_types_type_foreign FOREIGN KEY (type) REFERENCES public.resource_types(type) ON DELETE CASCADE;


--
-- TOC entry 4887 (class 2606 OID 21490)
-- Name: features features_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.features
    ADD CONSTRAINT features_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4923 (class 2606 OID 21809)
-- Name: how_to_arrive how_to_arrive_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.how_to_arrive
    ADD CONSTRAINT how_to_arrive_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4924 (class 2606 OID 21814)
-- Name: how_to_arrive how_to_arrive_place_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.how_to_arrive
    ADD CONSTRAINT how_to_arrive_place_id_foreign FOREIGN KEY (place_id) REFERENCES public.places(id) ON DELETE CASCADE;


--
-- TOC entry 4929 (class 2606 OID 21861)
-- Name: lifts lifts_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lifts
    ADD CONSTRAINT lifts_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4888 (class 2606 OID 21502)
-- Name: media_objects media_objects_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.media_objects
    ADD CONSTRAINT media_objects_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4889 (class 2606 OID 21507)
-- Name: media_objects media_objects_license_holder_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.media_objects
    ADD CONSTRAINT media_objects_license_holder_id_foreign FOREIGN KEY (license_holder_id) REFERENCES public.agents(id) ON DELETE SET NULL;


--
-- TOC entry 4930 (class 2606 OID 21876)
-- Name: mountain_areas mountain_areas_area_owner_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mountain_areas
    ADD CONSTRAINT mountain_areas_area_owner_id_foreign FOREIGN KEY (area_owner_id) REFERENCES public.agents(id);


--
-- TOC entry 4931 (class 2606 OID 21871)
-- Name: mountain_areas mountain_areas_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mountain_areas
    ADD CONSTRAINT mountain_areas_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4960 (class 2606 OID 22109)
-- Name: multimedia_descriptions multimedia_descriptions_media_object_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.multimedia_descriptions
    ADD CONSTRAINT multimedia_descriptions_media_object_id_foreign FOREIGN KEY (media_object_id) REFERENCES public.media_objects(id) ON DELETE CASCADE;


--
-- TOC entry 4961 (class 2606 OID 22104)
-- Name: multimedia_descriptions multimedia_descriptions_resource_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.multimedia_descriptions
    ADD CONSTRAINT multimedia_descriptions_resource_id_foreign FOREIGN KEY (resource_id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4901 (class 2606 OID 21613)
-- Name: names names_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.names
    ADD CONSTRAINT names_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4902 (class 2606 OID 21618)
-- Name: names names_resource_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.names
    ADD CONSTRAINT names_resource_id_foreign FOREIGN KEY (resource_id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4956 (class 2606 OID 22079)
-- Name: organizers organizers_event_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizers
    ADD CONSTRAINT organizers_event_id_foreign FOREIGN KEY (event_id) REFERENCES public.events(id) ON DELETE CASCADE;


--
-- TOC entry 4957 (class 2606 OID 22074)
-- Name: organizers organizers_organizer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organizers
    ADD CONSTRAINT organizers_organizer_id_foreign FOREIGN KEY (organizer_id) REFERENCES public.agents(id) ON DELETE CASCADE;


--
-- TOC entry 4907 (class 2606 OID 21669)
-- Name: participation_urls participation_urls_event_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participation_urls
    ADD CONSTRAINT participation_urls_event_id_foreign FOREIGN KEY (event_id) REFERENCES public.events(id) ON DELETE CASCADE;


--
-- TOC entry 4908 (class 2606 OID 21664)
-- Name: participation_urls participation_urls_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participation_urls
    ADD CONSTRAINT participation_urls_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4921 (class 2606 OID 21799)
-- Name: places places_address_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.places
    ADD CONSTRAINT places_address_id_foreign FOREIGN KEY (address_id) REFERENCES public.addresses(id) ON DELETE SET NULL;


--
-- TOC entry 4922 (class 2606 OID 21794)
-- Name: places places_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.places
    ADD CONSTRAINT places_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4915 (class 2606 OID 21744)
-- Name: regions regions_address_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regions
    ADD CONSTRAINT regions_address_id_foreign FOREIGN KEY (address_id) REFERENCES public.addresses(id) ON DELETE CASCADE;


--
-- TOC entry 4916 (class 2606 OID 21739)
-- Name: regions regions_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regions
    ADD CONSTRAINT regions_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4909 (class 2606 OID 21686)
-- Name: registration_urls registration_urls_event_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registration_urls
    ADD CONSTRAINT registration_urls_event_id_foreign FOREIGN KEY (event_id) REFERENCES public.events(id) ON DELETE CASCADE;


--
-- TOC entry 4910 (class 2606 OID 21681)
-- Name: registration_urls registration_urls_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registration_urls
    ADD CONSTRAINT registration_urls_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4944 (class 2606 OID 21979)
-- Name: resource_categories resource_categories_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_categories
    ADD CONSTRAINT resource_categories_category_id_foreign FOREIGN KEY (category_id) REFERENCES public.categories(id) ON DELETE CASCADE;


--
-- TOC entry 4945 (class 2606 OID 21984)
-- Name: resource_categories resource_categories_resource_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_categories
    ADD CONSTRAINT resource_categories_resource_id_foreign FOREIGN KEY (resource_id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4946 (class 2606 OID 21994)
-- Name: resource_features resource_features_feature_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_features
    ADD CONSTRAINT resource_features_feature_id_foreign FOREIGN KEY (feature_id) REFERENCES public.features(id) ON DELETE CASCADE;


--
-- TOC entry 4947 (class 2606 OID 21999)
-- Name: resource_features resource_features_resource_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resource_features
    ADD CONSTRAINT resource_features_resource_id_foreign FOREIGN KEY (resource_id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4884 (class 2606 OID 21460)
-- Name: resources resources_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.resources
    ADD CONSTRAINT resources_type_fkey FOREIGN KEY (type) REFERENCES public.resource_types(type) ON DELETE CASCADE;


--
-- TOC entry 4903 (class 2606 OID 21630)
-- Name: short_names short_names_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.short_names
    ADD CONSTRAINT short_names_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4904 (class 2606 OID 21635)
-- Name: short_names short_names_resource_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.short_names
    ADD CONSTRAINT short_names_resource_id_foreign FOREIGN KEY (resource_id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4932 (class 2606 OID 21886)
-- Name: ski_slopes ski_slopes_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ski_slopes
    ADD CONSTRAINT ski_slopes_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4925 (class 2606 OID 21826)
-- Name: snow_conditions snow_conditions_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.snow_conditions
    ADD CONSTRAINT snow_conditions_id_foreign FOREIGN KEY (id) REFERENCES public.places(id) ON DELETE CASCADE;


--
-- TOC entry 4933 (class 2606 OID 21896)
-- Name: snowparks snowparks_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.snowparks
    ADD CONSTRAINT snowparks_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4958 (class 2606 OID 22094)
-- Name: sponsors sponsors_event_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsors
    ADD CONSTRAINT sponsors_event_id_foreign FOREIGN KEY (event_id) REFERENCES public.events(id) ON DELETE CASCADE;


--
-- TOC entry 4959 (class 2606 OID 22089)
-- Name: sponsors sponsors_sponsor_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sponsors
    ADD CONSTRAINT sponsors_sponsor_id_foreign FOREIGN KEY (sponsor_id) REFERENCES public.agents(id) ON DELETE CASCADE;


--
-- TOC entry 4917 (class 2606 OID 21761)
-- Name: streets streets_address_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.streets
    ADD CONSTRAINT streets_address_id_foreign FOREIGN KEY (address_id) REFERENCES public.addresses(id) ON DELETE CASCADE;


--
-- TOC entry 4918 (class 2606 OID 21756)
-- Name: streets streets_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.streets
    ADD CONSTRAINT streets_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4940 (class 2606 OID 21954)
-- Name: sub_areas sub_areas_child_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_areas
    ADD CONSTRAINT sub_areas_child_id_foreign FOREIGN KEY (child_id) REFERENCES public.mountain_areas(id) ON DELETE CASCADE;


--
-- TOC entry 4941 (class 2606 OID 21949)
-- Name: sub_areas sub_areas_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_areas
    ADD CONSTRAINT sub_areas_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public.mountain_areas(id) ON DELETE CASCADE;


--
-- TOC entry 4905 (class 2606 OID 21647)
-- Name: urls urls_lang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.urls
    ADD CONSTRAINT urls_lang_foreign FOREIGN KEY (lang) REFERENCES public.language_codes(lang) ON DELETE CASCADE;


--
-- TOC entry 4906 (class 2606 OID 21652)
-- Name: urls urls_resource_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.urls
    ADD CONSTRAINT urls_resource_id_foreign FOREIGN KEY (resource_id) REFERENCES public.resources(id) ON DELETE CASCADE;


--
-- TOC entry 4926 (class 2606 OID 21836)
-- Name: venues venues_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.venues
    ADD CONSTRAINT venues_id_foreign FOREIGN KEY (id) REFERENCES public.resources(id) ON DELETE CASCADE;


-- Completed on 2022-12-22 22:47:16 CET

--
-- PostgreSQL database dump complete
--

