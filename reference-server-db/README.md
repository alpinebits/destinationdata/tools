# AlpineBits DestinationData Database

This project contains a dump of the PostgreSQL database used in the Reference Server. This database contains all schemas and default data that are comprised in the AlpineBits DestinationData 2022-04 standard.

This database was developed using:

- PostgreSQL 15
- PostGIS 3.1 (required for geo-location data and queries)