# Create - Demonstrations and Answers

On the `/agents` endpoint:

- Create a new agent for the AlpineBits Alliance and post it on the server.

```http
POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "alpinebits",
    "type": "agents",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "contactPoints": [
        {
          "email": "info@alpinebits.org",
          "telephone": "+390000000000",
          "address": {
            "street": {
              "deu": "Bozner Straße Nr. 63/A"
            },
            "city": {
              "deu": "Frangart"
            },
            "region": {
              "deu": "Frangart"
            },
            "country": "IT",
            "zipCode": "39057"
          },
          "availableHours": null
        }
      ],
      "description": {
        "eng": "The \"AlpineBits Alliance\" is a group of SME operating in the touristic sector working together to innovate and open the data exchange in the alpine tourism, and therefore to optimize the online presence, sales and marketing efforts of the hotels and other accommodations in the alpine territory and also worldwide."
      },
      "name": {
        "eng": "AlpineBits Alliance"
      },
      "url": "https://people.utwente.nl/c.moraisfonseca"
    },
    "relationships" : {
      "categories": {
        "data": [
          {
            "id": "alpinebits:organization"
          }
        ]
      },
      "multimediaDescriptions": null
    }
  }
}
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits
```

On the `/mediaObjects` endpoint:

- Create a new media object for the AlpineBits Alliance logo and post it on the server.

```http
POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "aa-logo-1",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "contentType": "image/png",
      "height": 120,
      "width": 300,
      "license": "AlpineBits Example License v0.1",
      "name": {
        "eng": "AlpineBits Alliance new logo"
      },
      "url": "https://www.alpinebits.org/wp-content/uploads/2015/10/alpine_bits_rgb.png"
    }
  }
}
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/aa-logo-1
```

- Create a new media object for the old AlpineBits Alliance logo and post it on the server.

```http
POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "aa-logo-2",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "contentType": "image/png",
      "height": 121,
      "width": 284,
      "license": "AlpineBits Example License v0.1",
      "name": {
        "eng": "AlpineBits Alliance traditional logo"
      },
      "url": "https://development.alpinebits.org/alpine_bits_rgb.png"
    }
  }
}
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/aa-logo-2
```
