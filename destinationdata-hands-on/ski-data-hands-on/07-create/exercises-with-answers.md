# Create - Exercises and Answers

On the `/agents` endpoint:

- Create an agent object for your company/organization and post it on the server.

```http
POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "claudenir",
    "type": "agents",
    "meta": {
      "dataProvider": "https://people.utwente.nl/c.moraisfonseca"
    },
    "attributes": {
      "abstract": {
        "eng": "Claudenir M. Fonseca is a researcher affiliated to the University of Twente, located in Enschede, The Netherlands."
      },
      "contactPoints": [
        {
          "email": "contact@example.com",
          "telephone": "+31000000000",
          "address": {
            "street": {
              "eng": "street"
            },
            "city": {
              "eng": "Enschede"
            },
            "region": {
              "eng": "Twente"
            },
            "complement": {
              "eng": "complement"
            },
            "country": "NL",
            "zipcode": "7070XX",
            "type": "office"
          },
          "availableHours": {
            "dailySchedules": {
              "2022-12-25": null,
              "2022-12-24": [
                {
                  "opens": "08:30:00",
                  "closes": "12:30:00"
                }
              ]
            },
            "weeklySchedules": [
              {
                "validFrom": "2022-08-01",
                "validTo": "2023-07-31",
                "monday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "tuesday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "wednesday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "thursday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "friday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "saturday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "sunday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ]
              }
            ]
          }
        }
      ],
      "description": {
        "eng": "Claudenir M. Fonseca is a researcher affiliated to the University of Twente, located in Enschede, The Netherlands. Claudenir holds a Ph.D. in Computer Science by the Free University of Bozen-Bolzano."
      },
      "name": {
        "por": "Claudenir Morais Fonseca"
      },
      "shortName": {
        "por": "Claudenir M. Fonseca"
      },
      "url": "https://people.utwente.nl/c.moraisfonseca"
    },
    "relationships" : {
      "categories": {
        "data": [
          {
            "id": "alpinebits:person"
          }
        ]
      },
      "multimediaDescriptions": null
    }
  }
}
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/claudenir
```

On the `/mediaObjects` endpoint:

- Pick your favorite ski area in Alto Adige and create the following media objects in the server. Fill-in all attributes of these media objects.

  - A picture with the prettiest view of this area. 
  - A ski map of this area.
  - A YouTube video of this area.

<!-- Dolomiti Superski (8260DC5B815D40B98A1B53E84EC2B419) -->

```http
POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "area-photo",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "author": "Dolomiti Superski",
      "contentType": "image/jpg",
      "height": 1136,
      "width": 1136,
      "license": null,
      "name": {
        "eng": "Dolomiti Superski - Photo"
      },
      "url": "https://www.dolomitisuperski.com/.imaging/default/dam/Comprensori-Sciistici/Lista-comprensori/Winter/Orizzontale/12_Civetta.jpg/jcr:content.jpg%20width="
    }
  }
}
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/area-photo
```

```http
POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "area-map",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "author": "Dolomiti Superski",
      "contentType": "image/jpg",
      "height": 1552,
      "width": 3494,
      "license": null,
      "name": {
        "eng": "Dolomiti Superski - Map"
      },
      "url": "https://www.valgardenaskimap.com/img/big-map/dolomiti-superski-map.jpg"
    }
  }
}
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/area-map
```

```http
POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "area-video",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "author": "Dolomiti Superski",
      "contentType": "video/vnd.youtube.yt",
      "duration": 63,
      "height": 1080,
      "width": 1920,
      "license": "YouTube License",
      "name": {
        "eng": "We Care About The Dolomites - EN 60 sec"
      },
      "url": "https://www.youtube.com/watch?v=CXkDoGeyf-I"
    }
  }
}
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/area-video
```

<!-- 
mountain area SKI04A713E6479A4BE4BD158019A6DA546CDSS_182959 tre cime
ski slope DSS_182959  M.Elmo - Stiergarten
https://youtu.be/LMq-OI7Oxf8

```http
POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "stiergarten-yt-1",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "author": "CLAUDIO LIPPI",
      "contentType": "video/vnd.youtube.yt",
      "duration": 355,
      "height": 696,
      "width": 1237,
      "license": "YouTube License",
      "name": {
        "eng": "Day at M.Elmo - Stiergarten"
      },
      "url": "https://www.alpinebits.org/wp-content/uploads/2015/10/alpine_bits_rgb.png"
    }
  }
}
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/stiergarten-yt-1
``` -->
