# Delete - Demonstrations and Answers

Demonstration

- Delete the old logo of the AlpineBits Alliance.

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/aa-logo-2
```

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits/multimediaDescriptions
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/aa-logo-2
```

- Verify the endpoint where the media object was previously available and the multimedia descriptions of the AlpineBits Alliance agent.

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/aa-logo-2
```

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits/multimediaDescriptions
```
