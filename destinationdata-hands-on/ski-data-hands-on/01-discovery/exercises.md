# Discovery - Exercises

- What are the types of resources related to ski data available on the server?
- How many ski slopes are available on the server?
- When was the data about slope `DSS_215996` last updated?
- What is the name of the ski slope `DSS_215996`?
- What is the difficulty rating of slope `DSS_215996`?
- What is the Italian name of the categories applied to slope `DSS_215996`?