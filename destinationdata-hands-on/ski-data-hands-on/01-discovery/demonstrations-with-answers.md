# Discovery - Demonstrations and Answers

- Retrieve all resource types available in the root endpoint of the server ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04)).

```http
GET /2022-04
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- What is the structure of the response we receive?
- Retrieve a collection of lifts ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/lifts)). 

```http
GET /2022-04/lifts
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- What is new in the structure of the response we receive compared to the previous request?
- How many lifts are available on the server?

As of 27 October 2022, 896 lifts.

- Retrieve lift `DSS_601` ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/lifts/DSS_601)).

```http
GET /2022-04/lifts/DSS_601
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- What is the shape of a lift resource?
- When was the data about lift `DSS_601` last updated?

As of 27 October 2022, the last update to this resource occurred on `"2022-10-14T08:05:12.397Z"`.

- What is the highest altitude of lift `DSS_601`?

2108 meters above sea level.

- How is lift `DSS_601` classified in the server?

There are three categories classifying the lift `DSS_601`: `"odh:aufstiegsanlagen"`, `"odh:skilift"`, and `"odh:weitere-aufstiegsanlagen"`.

- Retrieve the categories of the first lift of the mountain area `"SKI6C7D174390D44D0ABC1C9105F8C37C5E"` ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAReas/SKI6C7D174390D44D0ABC1C9105F8C37C5E/lifts)).

```http
GET /2022-04/mountainAReas/SKI6C7D174390D44D0ABC1C9105F8C37C5E/lifts
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
