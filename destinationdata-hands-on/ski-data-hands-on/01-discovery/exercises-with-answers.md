# Discovery - Exercises and Answers

- What are the types of resources related to ski data available on the server ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04))?

```http
GET /2022-04
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- How many ski slopes are available on the server ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes))?

```http
GET /2022-04/skiSlopes
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

As of 27 October 2022, 2074 ski slopes.

- When was the data about slope `DSS_215996` last updated ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_215996))?

```http
GET /2022-04/skiSlopes/DSS_215996
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

As of 27 October 2022, the last update to this resource occurred on `"2022-10-14T08:07:27.071Z"`.

- What is the name of the ski slope `DSS_215996`?

The name of this ski slope in German, English, and Italian is `"Aloch"`.

- What is the difficulty rating of slope `DSS_215996`?

In the European classification system, this is ski slope is rated as `"expert"`. There is no information available about North American difficulty rating of this ski slope.

- What is the Italian name of the categories applied to slope `DSS_215996` ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_215996/categories))?

```http
GET /2022-04/skiSlopes/DSS_215996/categories
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

These are the Italian names of the three categories that classify `DSS_215996`:

- `"Piste"`
- `"sci alpino"`
- `"Altre piste"`