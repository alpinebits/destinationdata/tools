# Field Selection - Demonstrations and Answers

On the `/snowparks` endpoint:

- Retrieve only the name and the categories of the snowpark `47CC4349C27C3CFBAAE1DB601C2C53BA_REDUCED` ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/snowparks/47CC4349C27C3CFBAAE1DB601C2C53BA_REDUCED?fields[snowparks]=name)).

```http
GET /2022-04/snowparks/47CC4349C27C3CFBAAE1DB601C2C53BA_REDUCED
  ?fields[snowparks]=name
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve all snowparks selecting only their names and check the languages in which they are usually available ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/snowparks?fields[snowparks]=name&page[size]=19)).

```http
GET /2022-04/snowparks
  ?fields[snowparks]=name
  &page[size]=19
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
