# Field Selection - Exercises

On the `/skiSlopes` endpoint:

- Retrieve a collection of ski slopes with only their names, minimum altitude, and maximum altitude.
- Retrieve a collection of ski slopes along with their categories, selecting only the names and categories of the ski slopes and the names of the categories.
- Repeat the last request limiting the response to 50 slopes.
- Repeat the last request selecting only the names of the ski slopes. Notice what happens with the included categories.
