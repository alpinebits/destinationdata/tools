# Field Selection - Exercises and Answers

On the `/skiSlopes` endpoint:

- Retrieve a collection of ski slopes with only their names, minimum altitude, and maximum altitude ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?fields[skiSlopes]=name,minAltitude,maxAltitude)).

```http
GET /2022-04/skiSlopes
  ?fields[skiSlopes]=name,minAltitude,maxAltitude
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of ski slopes along with their categories, selecting only the names and categories of the ski slopes and the names of the categories ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?include=categories&fields[skiSlopes]=name,categories&fields[categories]=name)).

```http
GET /2022-04/skiSlopes
  ?include=categories
  &fields[skiSlopes]=name,categories
  &fields[categories]=name
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Repeat the last request limiting the response to 50 slopes ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?include=categories&fields[skiSlopes]=name,categories&fields[categories]=name&page[size]=50)).

```http
GET /2022-04/skiSlopes
  ?include=categories
  &fields[skiSlopes]=name,categories
  &fields[categories]=name
  &page[size]=50
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Repeat the last request selecting only the names of the ski slopes. Notice what happens with the included categories.
([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?include=categories&fields[skiSlopes]=name&fields[categories]=name&page[size]=50)).

```http
GET /2022-04/skiSlopes
  ?include=categories
  &fields[skiSlopes]=name
  &fields[categories]=name
  &page[size]=50
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```