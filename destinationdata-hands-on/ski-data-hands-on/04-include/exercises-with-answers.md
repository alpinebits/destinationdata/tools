# Include - Exercises and Answers

On the `/mountainAreas` endpoint:

- Retrieve the mountain area `SKI04A713E6479A4BE4BD158019A6DA546C` along with its area owner and its ski lopes ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/SKI04A713E6479A4BE4BD158019A6DA546C?include=areaOwner,skiSlopes)).

```http
GET /2022-04/mountainAreas/SKI04A713E6479A4BE4BD158019A6DA546C?include=areaOwner,skiSlopes
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve mountain areas along with their area owners, lifts, ski slopes, and snowparks ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas?include=areaOwner,lifts,skiSlopes,snowparks)).

```http
GET /2022-04/mountainAreas?include=areaOwner,lifts,skiSlopes,snowparks
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Repeat the previous request but limit it to 5 mountain areas ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas?include=areaOwner,lifts,skiSlopes,snowparks&page[size]=5)).

```http
GET /2022-04/mountainAreas?include=areaOwner,lifts,skiSlopes,snowparks&page[size]=5
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Repeat the previous request but sort them by last update ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas?include=areaOwner,lifts,skiSlopes,snowparks&page[size]=5&sort=lastUpdate)).

```http
GET /2022-04/mountainAreas?include=areaOwner,lifts,skiSlopes,snowparks&page[size]=5&sort=lastUpdate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
