# Include - Demonstrations and Answers

On the `/snowparks` endpoint:

- Retrieve the snowpark `47CC4349C27C3CFBAAE1DB601C2C53BA_REDUCED` along with its categories ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/snowparks/47CC4349C27C3CFBAAE1DB601C2C53BA_REDUCED?include=categories)).

```http
GET /2022-04/snowparks/47CC4349C27C3CFBAAE1DB601C2C53BA_REDUCED?include=categories
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve 50 snowparks along with their categories ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/snowparks?include=categories&page[size]=50)).

```http
GET /2022-04/snowparks?include=categories&page[size]=50
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
