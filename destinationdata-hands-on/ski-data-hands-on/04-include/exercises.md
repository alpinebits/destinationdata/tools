# Include - Exercises

On the `/mountainAreas` endpoint:

- Retrieve the mountain area `SKI04A713E6479A4BE4BD158019A6DA546C` along with its area owner and its ski lopes.
- Retrieve mountain areas along with their area owners, lifts, ski slopes, and snowparks.
- Repeat the previous request but limit it to 5 mountain areas.
- Repeat the previous request but sort them by last update.
