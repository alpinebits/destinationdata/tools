# Sorting - Exercises and Answers

On the `/skiSlopes` endpoint:

- Retrieve a collection of ski slopes sorted by min altitude (lowest first) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?sort=minAltitude)).

```http
GET /2022-04/skiSlopes?sort=minAltitude
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of ski slopes sorted by max altitude (highest first) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?sort=-maxAltitude)).

```http
GET /2022-04/skiSlopes?sort=-maxAltitude
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of ski slopes sorted by when they were last updated ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?sort=lastUpdate)).

```http
GET /2022-04/skiSlopes?sort=lastUpdate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of ski slopes sorted by max altitude and by when they were last updated ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?sort=maxAltitude,lastUpdate)).

```http
GET /2022-04/skiSlopes?sort=maxAltitude,lastUpdate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of 30 ski slopes sorted by max altitude ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?sort=maxAltitude)).

```http
GET /2022-04/skiSlopes?sort=maxAltitude&page[size]=30
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
