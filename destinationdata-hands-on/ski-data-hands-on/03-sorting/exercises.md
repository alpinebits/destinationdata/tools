# Sorting - Exercises

On the `/skiSlopes` endpoint:

- Retrieve a collection of ski slopes sorted by min altitude (lowest first)
- Retrieve a collection of ski slopes sorted by max altitude (highest first)
- Retrieve a collection of ski slopes sorted by when they were last updated.
- Retrieve a collection of ski slopes sorted by max altitude and by when they were last updated.
- Retrieve a collection of 30 ski slopes sorted by max altitude.
