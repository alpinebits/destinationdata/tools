# Sorting - Demonstrations and Answers

On the `/lifts` endpoint:

- Retrieve a collection of lifts sorted by length (smallest first) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/lifts?sort=length)).

```http
GET /2022-04/lifts?sort=length
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of lifts sorted by when they were last updated (oldest first) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/lifts?sort=lastUpdate)).

```http
GET /2022-04/lifts?sort=lastUpdate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of 80 lifts sorted by length ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/lifts?page[size]=80&sort=length)).

```http
GET /2022-04/lifts?page[size]=80&sort=length
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of lifts sorted by length and by when they were last updated (tip: check last page) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/lifts?sort=length,lastUpdate)).

```http
GET /2022-04/lifts?sort=length,lastUpdate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
