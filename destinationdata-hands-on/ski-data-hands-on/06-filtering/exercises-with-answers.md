# Filtering - Exercises and Answers

On the `/skiSlopes` endpoint:

- Retrieve only the ski slopes updated after 01/10/2022 ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?filter[lastUpdate][gt]=2022-10-01)).

```http
GET /2022-04/skiSlopes
  ?filter[lastUpdate][gt]=2022-10-01
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve only the ski slopes with a defined difficulty in the European classification ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?filter[difficulty][exists]=true)).

```http
GET /2022-04/skiSlopes
  ?filter[difficulty.eu][exists]=true
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve only the ski slopes with a max altitude higher than 1000 meters above the sea level ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?filter[maxAltitude][gt]=1000)).

```http
GET /2022-04/skiSlopes
  ?filter[maxAltitude][gt]=1000
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of slopes ordered by their last update and having some difficulty rating in the European classification ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?filter[difficulty.eu][exists]=true&sort=lastUpdate)).

```http
GET /2022-04/skiSlopes
  ?filter[difficulty.eu][exists]=true
  &sort=lastUpdate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Repeat the last request retrieving only 5 slopes ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?filter[difficulty.eu][exists]=true&sort=lastUpdate&page[size]=5)).

```http
GET /2022-04/skiSlopes
  ?filter[difficulty.eu][exists]=true
  &sort=lastUpdate
  &page[size]=5
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Repeat the last request selecting only the names and difficulties of the slopes ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?filter[difficulty.eu][exists]=true&sort=lastUpdate&page[size]=5&fields[skiSlopes]=difficulty,name)).

```http
GET /2022-04/skiSlopes
  ?filter[difficulty.eu][exists]=true
  &sort=lastUpdate
  &page[size]=5
  &fields[skiSlopes]=difficulty,name
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
