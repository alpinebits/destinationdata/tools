# Filtering - Demonstrations and Answers

On the `/mountainAreas` endpoint:

- Retrieve only the mountain areas updated after 19/08/2022 ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas?filter[lastUpdate][gt]=2022-08-19)).

```http
GET /2022-04/mountainAreas
  ?filter[lastUpdate][gt]=2022-08-19
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve the mountain areas with a max altitude higher than 2500 meters above sea level. What happens to mountain areas that lack a max altitude ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas?filter[maxAltitude][gt]=2500))?

```http
GET /2022-04/mountainAreas
  ?filter[maxAltitude][gt]=2500
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve the mountain areas which have a max altitude ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas?filter[maxAltitude][exists]=true)).

```http
GET /2022-04/mountainAreas
  ?filter[maxAltitude][exists]=true
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve the mountain areas which are lacking a max altitude.
([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas?filter[maxAltitude][exists]=false)).

```http
GET /2022-04/mountainAreas
  ?filter[maxAltitude][exists]=false
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
