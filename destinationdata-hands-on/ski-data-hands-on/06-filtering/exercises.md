# Field Selection - Exercises

On the `/skiSlopes` endpoint:

- Retrieve only the ski slopes updated after 01/10/2022.
- Retrieve only the ski slopes with a max altitude higher than 1000 meters above the sea level.
- Retrieve a collection of slopes ordered by their last update and having some difficulty rating.
- Repeat the last request retrieving only 5 slopes.
- Repeat the last request selecting only the names and difficulties of the slopes.

