# Pagination - Exercises

On the `/skiSlopes` endpoint:

- Retrieve a collection of ski slopes and then visit the second page without manually writing another request (tip: look for URLs within the JSON you got in the first request).
- Discover the default page size of the `/skiSlopes` endpoint and retrieve its third page.
- Retrieve 50 ski slopes in a single request.
- Retrieve the 3rd page of ski slopes using the default page size.
- Retrieve the fourth page of ski slopes using a page size of 5.
- Navigate to the categories of ski slope `DSS_215996` and retrieve the second page using a page size of 1.