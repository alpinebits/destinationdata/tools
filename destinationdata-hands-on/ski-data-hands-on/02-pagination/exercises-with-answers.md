# Pagination - Exercises and Answers

On the `/skiSlopes` endpoint:

- Retrieve a collection of ski slopes and then visit the second page without manually writing another request (tip: look for URLs within the JSON you got in the first request) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes)).

```http
GET /2022-04/skiSlopes
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

Inside the `"links"` field, use the link labeled `"next"`.

- Discover the default page size of the `/skiSlopes` endpoint and retrieve its third page ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes)).

```http
GET /2022-04/skiSlopes
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

Inside the `"meta"` field, the fields `"count"` and `"pages"`. By dividing the number of ski slopes by the number of pages (rounding the division up), you can learn the default page size `10`.

- Retrieve 50 ski slopes in a single request ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?page[size]=50)).

```http
GET /2022-04/skiSlopes?page[size]=50
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve the 3rd page of ski slopes using the default page size ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?page[number]=3)).

```http
GET /2022-04/skiSlopes?page[number]=3
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve the fourth page of ski slopes using a page size of 5 ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?page[number]=4&page[size]=5)).

```http
GET /2022-04/skiSlopes
  ?page[number]=4
  &page[size]=5
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Navigate to the categories of ski slope `DSS_215996` and retrieve the second page using a page size of 1 ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_215996/categories?page[number]=2&page[size]=1)).

```http
GET /2022-04/skiSlopes/DSS_215996/categories
  ?page[number]=2
  &page[size]=1
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
