# Resources

## Christmas Video

- **Content-Type: application/vnd.api+json**

- **Data Provider: https://www.youtube.com/**

- Id: crema-natale-sulle-dolomiti-youtube

- URL: https://youtu.be/8M1tNe-TW_k

- Height: 1080

- Width: 1920

- Duration: 31

- MIME Type: video/vnd.youtube.yt

- Description:

  - eng: Christmas in the Dolomites
  - ita: Natale nelle Dolomiti
  - deu: Weihnachten in den Dolomiten

- Author: Andrea Crema

- License: UMG (em nome de Verve Reissues); ASCAP, Abramus Digital, UMPI, LatinAutor, CMRRA, ARESA, Concord Music Publishing, LatinAutorPerf, Hexacorp (music publishing), LatinAutor - UMPG, MINT_BMG e 18 associações de direitos musicais


## Christmas Image

- **Content-Type: application/vnd.api+json**

- **Data Provider: https://www.pexels.com/**

- URL: https://images.pexels.com/photos/263875/pexels-photo-263875.jpeg

- Height: 5184

- Width: 3456

- MIME Type: image/jpeg

- Description: Photo by Brigitte Tohm from Pexels: https://www.pexels.com/photo/snowman-figurine-on-table-263875/

- Author: Brigitte Tohm

- License: Pexels license https://www.pexels.com/license/