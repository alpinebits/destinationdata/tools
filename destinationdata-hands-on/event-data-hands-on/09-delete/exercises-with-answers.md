# Delete - Exercises and Answers

 - Repeat the demonstration with one of the media objects that you have created earlier.

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/area-video
```

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/8260DC5B815D40B98A1B53E84EC2B419/multimediaDescriptions
```

```http
DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/area-video
```

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/area-video
```

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/8260DC5B815D40B98A1B53E84EC2B419/multimediaDescriptions
```