# EventData Workshop
## Demonstrations and Exercises

This repository contains a list of demonstrations and exercises designed to teach developers how to interact with a server implementing the [AlpineBits DestinationData standard version 2022-04](https://www.alpinebits.org/wp-content/uploads/2022/05/AlpineBits-DestinationData-2022-04.pdf).

The exercises are built-up incrementally, so the later exercises may combine features from earlier ones.

In order to follow the demonstrations and exercises in this repository, please observe the following requirements and support resources.

## What do you need to know?

- HTTP
- JSON

## Tool Requirements

- Any software that can make HTTP requests, such as:
  - [Postman](https://learning.postman.com/docs/getting-started/introduction/).
  - [REST Client for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=humao.rest-client).
  - [cURL](https://curl.se/)
- For retrieval requests only, a web browser can be used (e.g. [Firefox](https://www.mozilla.org/en-US/firefox/new/), and [Chrome](https://www.google.com/chrome/)), but they are not recommended.

## Available Resources

- Swagger documentation
  - [AlpineBits DestinationData Reference Server in Swagger](https://swagger.opendatahub.bz.it/?url=https://destinationdata.alpinebits.opendatahub.testingmachine.eu/specification.json)
  - [AlpineBits DestinationData in Swagger](https://alpinebits.gitlab.io/destinationdata/tools)
- [Gitlab `/tools` repository](https://gitlab.com/alpinebits/destinationdata/tools)
- [AlpineBits DestinationData Reference Server](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/)
- [AlpineBits DestinationData Standard Version 2022-04](https://www.alpinebits.org/wp-content/uploads/2022/05/AlpineBits-DestinationData-2022-04.pdf)

## Learning outcomes

By the end of this workshop, we expect you to be able to:

- Understand the data schemas and the way to interact with a server using the AlpineBits DestinationData standard.
- Retrieve and navigate through SkiData resource resources using DestinationData
- Create, update, and delete SkiData using the DestinationData API
