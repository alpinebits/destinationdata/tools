# Filtering - Exercises and Answers

On the `/events` endpoint:

- Retrieve a collection of events starting on "2022-12-01" or after ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?filter[startDate][gte]=2022-12-01&sort=startDate&fields[events]=name,startDate)).

**Hint: use sorting by start date (oldest first, ascending order) to better visualize the results.**

```http
GET /2022-04/events
  ?filter[startDate][gte]=2022-12-01
  &sort=startDate
  &fields[events]=name,startDate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
<!-- Host:localhost:8080 -->

- Retrieve a collection of events starting on "2022-12-01" or after and ending before "2023-01-01" ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?filter[startDate][gte]=2022-12-01&filter[endDate][lt]=2023-01-01&sort=startDate,endDate&fields[events]=name,startDate,endDate)).

```http
GET /2022-04/events
  ?filter[startDate][gte]=2022-12-01
  &filter[endDate][lt]=2023-01-01
  &sort=startDate,endDate
  &fields[events]=name,startDate,endDate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of events that either the category "Exhibitions/art" (id `"odh:C72CE969B98947FABC99CBC7B033F28E"`) or "Guided tours" (id `"odh:B5467FEFE5C74FA5AD32B83793A76165"`) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?filter[categories][any]=odh:C72CE969B98947FABC99CBC7B033F28E,odh:B5467FEFE5C74FA5AD32B83793A76165&fields[events]=name,categories,startDate,endDate&fields[categories]=name&include=categories)).

```http
GET /2022-04/events
  ?filter[categories][any]=odh:C72CE969B98947FABC99CBC7B033F28E,odh:B5467FEFE5C74FA5AD32B83793A76165
  &fields[events]=name,categories,startDate,endDate
  &fields[categories]=name
  &include=categories
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
<!-- Host:localhost:8080 -->

- Retrieve a collection of events occurring inside a radius of 10km from Piazza Walther (longitude `11.354638`, latitude `46.498411`) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?filter[venues][near]=11.354638,46.498411,10000&include=venues&fields[events]=name&fields[venues]=geometries)).

```http
GET /2022-04/events
  ?filter[venues][near]=11.354638,46.498411,10000
  &include=venues
  &fields[events]=name
  &fields[venues]=geometries
Host:localhost:8080
```
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu

On the `"\categories"` endpoint:

- Retrieve categories that contain the `"music"` substring in either their names or descriptions ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/categories?search=music&fields[categories]=description,name)).

```http
GET /2022-04/categories
  ?search=music
  &fields[categories]=description,name
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
<!-- Host:localhost:8080 -->

```http
GET /2022-04/events
  ?filter[venues][within]={"coordinates":[[[11.31,46.55],[11.31,46.47],[11.42,46.47],[11.42,46.55],[11.31,46.55]]],"type":"Polygon"}
  &include=venues
  &fields[events]=name
  &fields[venues]=geometries
Host:localhost:8080
```