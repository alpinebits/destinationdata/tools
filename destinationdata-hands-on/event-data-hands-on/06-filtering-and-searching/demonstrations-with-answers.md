# Filtering - Demonstrations and Answers

On the `/events` endpoint:

- Retrieve only the events that have some description information ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?filter[description][exists]=true&fields[events]=description,name)).

```http
GET /2022-04/events
  ?filter[description][exists]=true
  &fields[events]=description,name
Host:localhost:8080
```
<!-- Host:destinationdata.alpinebits.opendatahub.testingmachine.eu -->

- Retrieve only events that have not been canceled ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?filter[status][neq]=canceled&fields[events]=name,status)).

```http
GET /2022-04/events
  ?filter[status][neq]=canceled
  &fields[events]=name,status
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
<!-- Host:localhost:8080 -->

- Retrieve only the events updated after 19/08/2022 ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?filter[lastUpdate][gt]=2022-08-19&fields[events]=name)).

```http
GET /2022-04/events
  ?filter[lastUpdate][gt]=2022-08-19
  &fields[events]=name
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve events that contain the `"bozen"` substring in either their names or descriptions ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?filter[description][exists]=true&fields[events]=description,name)).

```http
GET /2022-04/categories
  ?search=music
  &fields[categories]=description,name
Host:localhost:8080
```
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu


```http
GET /2022-04/events
  ?filter[endDate][gte]=2022-01-01
  &filter[endDate][lt]=2023-01-01
  &fields[events]=name,categories
  &page[size]=500
  &include=categories
  &fields[categories]=name
Host:localhost:8080
```