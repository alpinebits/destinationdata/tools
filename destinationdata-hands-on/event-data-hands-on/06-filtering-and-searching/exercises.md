# Field Selection - Exercises

On the `/events` endpoint:

- Retrieve a collection of events starting on "2022-12-01" or after.

**Hint: use sorting by start date (oldest first, ascending order) to better visualize the results.**

- Retrieve a collection of events starting on "2022-12-01" or after and ending before "2023-01-01".
- Retrieve a collection of events that either the category "Exhibitions/art" (id `"odh:C72CE969B98947FABC99CBC7B033F28E"`) or "Guided tours" (id `"odh:B5467FEFE5C74FA5AD32B83793A76165"`).
- Retrieve a collection of events occurring inside a radius of 10km from Piazza Walther (longitude `11.354638`, latitude `46.498411`).
- Retrieve categories that contain the `"music"` substring in either their names or descriptions.