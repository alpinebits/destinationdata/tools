# Discovery - Demonstrations and Answers

- Retrieve all resource types available in the root endpoint of the server ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04)).

```http
GET /2022-04
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- What are the types of resources related to event data available on the server?

The data available in the server related to events includes `agents`, `categories`, `events`, `eventSeries`, `mediaObjects`, and `venues`.

- What is the structure of the response we receive?
- Retrieve a collection of events ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events)). 

```http
GET /2022-04/events
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- What is new in the structure of the response we receive compared to the previous request?
- How many events are available on the server?

As of 06 December 2022, 5619 events.

- Retrieve event `20062022-0900-TREVILAB-LAB1-IT-MOSTRA-2000-2406202` ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events/20062022-0900-TREVILAB-LAB1-IT-MOSTRA-2000-2406202)).

```http
GET /2022-04/events/20062022-0900-TREVILAB-LAB1-IT-MOSTRA-2000-2406202
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- What is the shape of a event resource?
- What is the name of the event in English?

The event is called `"Exhibition Venezia...A modo mio"`.

- When was the data about "Exhibition Venezia" last updated?

As of 06 December 2022, the last update to this resource occurred on `"2022-09-19T15:00:10.782Z"`.

- When is "Exhibition Venezia" scheduled to start?

The event is planned to start on `"2022-06-20T09:00:00.000Z"`.

- Is "Exhibition Venezia" classified as an in-person, virtual, or hybrid event?

It is classified as an in-person event (`"alpinebits:inPersonEvent"`).

- What is the name of the organizer of "Exhibition Venezia" ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events/20062022-0900-TREVILAB-LAB1-IT-MOSTRA-2000-2406202/organizers)).

```http
GET /2022-04/events/20062022-0900-TREVILAB-LAB1-IT-MOSTRA-2000-2406202/organizers
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

`"TreviLab"` is the name of the event's organizer.
