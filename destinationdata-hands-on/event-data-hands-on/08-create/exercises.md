# Create - Exercises

On the `/agents` endpoint:

- Create an agent object for your company/organization and post it on the server.

On the `/mediaObjects` endpoint:

- Pick your favorite ski area in Alto Adige and create the following media objects in the server. Fill-in all attributes of these media objects.
  - A picture with the prettiest view of this area. 
  - A ski map of this area.
  - A YouTube video of this area.
