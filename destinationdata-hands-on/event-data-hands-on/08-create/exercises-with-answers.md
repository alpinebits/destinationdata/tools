# Create - Exercises and Answers

On the `/agents` endpoint:

- Create an agent resource for your company/organization and post it on the server.

<!-- POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents -->
      "dataProvider": "https://people.utwente.nl/c.moraisfonseca"
```http
POST http://localhost:8080/2022-04/agents
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "claudenir",
    "type": "agents",
    "meta": {
    },
    "attributes": {
      "abstract": {
        "eng": "Claudenir M. Fonseca is a researcher affiliated to the University of Twente, located in Enschede, The Netherlands."
      },
      "contactPoints": [
        {
          "email": "contact@example.com",
          "telephone": "+31000000000",
          "address": {
            "street": {
              "eng": "street"
            },
            "city": {
              "eng": "Enschede"
            },
            "region": {
              "eng": "Twente"
            },
            "complement": {
              "eng": "complement"
            },
            "country": "NL",
            "zipcode": "7070XX",
            "type": "office"
          },
          "availableHours": {
            "dailySchedules": {
              "2022-12-25": null,
              "2022-12-24": [
                {
                  "opens": "08:30:00",
                  "closes": "12:30:00"
                }
              ]
            },
            "weeklySchedules": [
              {
                "validFrom": "2022-08-01",
                "validTo": "2023-07-31",
                "monday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "tuesday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "wednesday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "thursday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "friday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "saturday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ],
                "sunday": [
                  {
                    "opens": "08:30:00",
                    "closes": "17:00:00"
                  }
                ]
              }
            ]
          }
        }
      ],
      "description": {
        "eng": "Claudenir M. Fonseca is a researcher affiliated to the University of Twente, located in Enschede, The Netherlands. Claudenir holds a Ph.D. in Computer Science by the Free University of Bozen-Bolzano."
      },
      "name": {
        "por": "Claudenir Morais Fonseca"
      },
      "shortName": {
        "por": "Claudenir M. Fonseca"
      },
      "url": "https://people.utwente.nl/c.moraisfonseca"
    },
    "relationships" : {
      "categories": {
        "data": [
          {
            "id": "alpinebits:person"
          }
        ]
      },
      "multimediaDescriptions": null
    }
  }
}
```

<!-- DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/claudenir -->
```http
DELETE http://localhost:8080/2022-04/agents/claudenir
```

On the `/mediaObjects` endpoint:

- Create a media object resource that serves as a multimedia description of your company/organization, for example:

  - The logo of your company.
  - A photo of the team.
  - An institutional video on YouTube.

  In case you prefer not to use your company's media, feel free to use online stock images and videos from the web:

  - Stock office video
    - Url: https://youtu.be/zwUsFN__jtE
    - Duration: 785 seconds
    - Height: 1080 pixels
    - Width:  1920 pixels
    - MIME Type: video/vnd.youtube.yt
  - Stock office photo
    - Url: https://www.pexels.com/photo/people-in-couch-1024248/
    - Height: 3361 pixels
    - Width:  5041 pixels
    - MIME Type: image/jpeg


<!-- Dolomiti Superski (8260DC5B815D40B98A1B53E84EC2B419) -->

<!-- POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects -->
```http
POST http://localhost:8080/2022-04/mediaObjects
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "area-photo",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "author": "Dolomiti Superski",
      "contentType": "image/jpg",
      "height": 1136,
      "width": 1136,
      "license": null,
      "name": {
        "eng": "Dolomiti Superski - Photo"
      },
      "url": "https://www.dolomitisuperski.com/.imaging/default/dam/Comprensori-Sciistici/Lista-comprensori/Winter/Orizzontale/12_Civetta.jpg/jcr:content.jpg%20width="
    }
  }
}
```

<!-- DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/area-photo -->
```http
DELETE http://localhost:8080/2022-04/mediaObjects/area-photo
```

<!-- POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects -->
```http
POST http://localhost:8080/2022-04/mediaObjects
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "area-map",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "author": "Dolomiti Superski",
      "contentType": "image/jpg",
      "height": 1552,
      "width": 3494,
      "license": null,
      "name": {
        "eng": "Dolomiti Superski - Map"
      },
      "url": "https://www.valgardenaskimap.com/img/big-map/dolomiti-superski-map.jpg"
    }
  }
}
```

<!-- DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/area-map -->
```http
DELETE http://localhost:8080/2022-04/mediaObjects/area-map
```

<!-- POST https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects -->
```http
POST http://localhost:8080/2022-04/mediaObjects
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "area-video",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "author": "Dolomiti Superski",
      "contentType": "video/vnd.youtube.yt",
      "duration": 63,
      "height": 1080,
      "width": 1920,
      "license": "YouTube License",
      "name": {
        "eng": "We Care About The Dolomites - EN 60 sec"
      },
      "url": "https://www.youtube.com/watch?v=CXkDoGeyf-I"
    }
  }
}
```

<!-- DELETE https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects/area-video -->
```http
DELETE http://localhost:8080/2022-04/mediaObjects/area-video
```
