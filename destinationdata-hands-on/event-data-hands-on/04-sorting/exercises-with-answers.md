# Sorting - Exercises and Answers

- Using the sorting feature, fill in the missing digits in the phrase below.

  `The most exclusive event in the server allows for [ ] in-person participants.`

  To find out this information, we can retrieve all events sorting them by `inPersonCapactity`. 

  On the `/events` endpoint, make the following request:

  ```http
  GET /2022-04/events?sort=inPersonCapactity
  ```

  You should see that the most exclusive events allows for **10 in-person participants**.


- Retrieve a collection of categories sorted by last update (oldest first) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/categories?sort=lastUpdate&fields[categories]=name)).

  ```http
  GET /2022-04/categories
    ?sort=lastUpdate
    &fields[events]=name
  Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
  ```

On the `/events` endpoint:

- Retrieve the fourth page of events sorted by start date (newest first) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?sort=-startDate&page[number]=2&fields[events]=name,startDate,endDate)).

```http
GET /2022-04/events
  ?sort=-startDate
  &page[number]=2
  &fields[events]=name,startDate,endDate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- In the previous request, the events "3 Zinnen Schlagermove Party at Grand Hotel" (id `"2806C02B6F964BFBAAEB9B7CDB3F9EA7_REDUCED"`) and "Rock the Dolomites 2023" (id `"9F95DB02F91341E196236FD3EB283CB9_REDUCED"`) start at the same day. Repeat the request using now end date as the second sorting criteria (newest first) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?sort=-startDate,-endDate&page[number]=2&fields[events]=name,startDate)).

```http
GET /2022-04/events
  ?sort=-startDate,-endDate
  &page[number]=2
  &fields[events]=name,startDate,endDate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of events randomly sorted ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?random=11&fields[events]=name,startDate,endDate)).

```http
GET /2022-04/events
  ?random=11
  &fields[events]=name,startDate,endDate
Host:localhost:8080
```
<!-- Host:destinationdata.alpinebits.opendatahub.testingmachine.eu -->

- Repeat the previous request retrieving the last page of events ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?random=11&page[number]=562&fields[events]=name,startDate,endDate)).

```http
GET /2022-04/events
  ?random=11
  &page[number]=562
  &fields[events]=name,startDate,endDate
Host:localhost:8080
```
<!-- Host:destinationdata.alpinebits.opendatahub.testingmachine.eu -->
