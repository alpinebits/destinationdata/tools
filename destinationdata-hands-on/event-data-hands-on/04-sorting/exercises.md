# Sorting - Exercises

On the `/categories` endpoint:

- Retrieve a collection of categories sorted by last update (oldest first).

On the `/events` endpoint:

- Retrieve the fourth page of events sorted by start date (newest first).
- In the previous request, the events "3 Zinnen Schlagermove Party at Grand Hotel" (id `"2806C02B6F964BFBAAEB9B7CDB3F9EA7_REDUCED"`) and "Rock the Dolomites 2023" (id `"9F95DB02F91341E196236FD3EB283CB9_REDUCED"`) start at the same day. Repeat the request using now end date as the second sorting criteria (newest first).
- Retrieve a collection of events randomly sorted.
- Repeat the previous request retrieving the last page of events.