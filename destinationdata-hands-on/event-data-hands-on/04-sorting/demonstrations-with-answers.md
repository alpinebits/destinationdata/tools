# Sorting - Demonstrations and Answers

On the `/events` endpoint:

- Retrieve a collection of events sorted by in-person capacity (smallest first) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?sort=inPersonCapacity&fields[events]=inPersonCapacity,name)).

**Hint: Simplify the response by selecting only the desired fields.**

```http
GET /2022-04/events
  ?sort=inPersonCapacity
  &fields[events]=inPersonCapacity,name
Host:localhost:8080
```
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu

- Retrieve a collection of events sorted by when they were last updated (newest first) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?sort=-lastUpdate&fields[events]=name)).

```http
GET /2022-04/events
  ?sort=-lastUpdate
  &fields[events]=name
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

On the `/categories` endpoint:

- Retrieve a collection of categories randomly sorted ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/categories?random=1&fields[categories]=name)).

```http
GET /2022-04/categories
  ?random=1
  &fields[categories]=name
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
<!-- Host:localhost:8080 -->

- Retrieve a collection of categories randomly sorted ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/categories?random=1&sort=lastUpdate)).

```http
GET /2022-04/events
  ?random=1
  &sort=lastUpdate
Host:localhost:8080
```
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
