# Include - Demonstrations and Answers

On the `/events` endpoint:

- Retrieve the event `19062022-2000-TREVILAB-LAB1-IT-CONCERTO-2115-19062` along with its categories ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events/19062022-2000-TREVILAB-LAB1-IT-CONCERTO-2115-19062?include=categories)).

```http
GET /2022-04/events/19062022-2000-TREVILAB-LAB1-IT-CONCERTO-2115-19062
  ?include=categories
  &fields[events]=name,categories
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve 50 events along with their categories ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?include=categories&page[size]=50&fields[events]=name,categories)).

```http
GET /2022-04/events
  ?include=categories
  &page[size]=50
  &fields[events]=name,categories
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
