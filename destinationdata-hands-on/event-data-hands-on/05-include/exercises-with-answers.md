# Include - Exercises and Answers

On the `/events` endpoint:

- Retrieve the event `19062022-2000-TREVILAB-LAB1-IT-CONCERTO-2115-19062` along with its publisher ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events/19062022-2000-TREVILAB-LAB1-IT-CONCERTO-2115-19062?include=publisher&fields[events]=name,publisher)).

```http
GET /2022-04/events/19062022-2000-TREVILAB-LAB1-IT-CONCERTO-2115-19062
  ?include=publisher
  &fields[events]=name,publisher
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Repeat the last request also including the event's organizers, and venues ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events/19062022-2000-TREVILAB-LAB1-IT-CONCERTO-2115-19062?include=publisher,organizers,venues&fields[events]=name,publisher,organizers,venues)).

```http
GET /2022-04/events/19062022-2000-TREVILAB-LAB1-IT-CONCERTO-2115-19062
  ?include=publisher,organizers,venues
  &fields[events]=name,publisher,organizers,venues
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve a collection of events along with their categories, and venues ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?include=publisher,organizers,venues&fields[events]=name,categories,venues)).

```http
GET /2022-04/events
  ?include=categories,venues
  &fields[events]=name,categories,venues
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Repeat the previous request selecting only the following fields of each resource type ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?include=categories,venues&fields[events]=name&fields[categories]=name&fields[venues]=geometries)):
  - `events`: names
  - `categories`: names
  - `venues`: geometries

```http
GET /2022-04/events
  ?include=categories,venues
  &fields[events]=name
  &fields[categories]=name
  &fields[venues]=geometries
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Repeat the previous request selecting only the following fields of each resource type ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?include=categories,venues&fields[events]=name&fields[categories]=name&fields[venues]=geometries)):
  - `events`: names
  - `categories`: names
  - `venues`: geometries

```http
GET /2022-04/events
  ?include=categories
  &page[size]=10
  &fields[events]=name
  &fields[categories]=name
  &fields[agents]=name
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

```http
GET /2022-04/categories
  ?fields[categories]=name
  &search=business
Host:localhost:8080
```