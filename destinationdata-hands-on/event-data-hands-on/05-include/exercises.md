# Include - Exercises

On the `/events` endpoint:

- Retrieve the event `19062022-2000-TREVILAB-LAB1-IT-CONCERTO-2115-19062` along with its publisher.
- Repeat the last request also including the event's organizers, and venues.
- Retrieve a collection of events along with their categories, and venues.
- Repeat the previous request selecting only the following fields of each resource type:
  - `events`: names
  - `categories`: names
  - `venues`: geometries
