# Update - Exercises

On the `/skiSlopes` endpoint:

- Add a description to a ski slope without one in English, Italian, and German.
- Add a translation of the name and description of a ski slope in a new language.
- Remove the description of a ski slope.
- Some ski slopes in the server only have a difficulty rating according to the European classification system. Find such a slope and complement its data with the rating in the American system. The European ratings are "novice", "beginner", "intermediate", and "expert". The American ratings are "beginner", "beginner-intermediate", "intermediate", "intermediate-advanced", and "expert". 
- Remove the relationship between a ski slope and a category.


On the `/mountainAreas` endpoint:

- Edit the object identifying your favorite mountain area so it is related to the 3 media objects you created in the previous exercise.