# Update - Exercises and Answers

On the `/skiSlopes` endpoint:

- Add a description to a ski slope without one in English, Italian, and German.

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes?fields[skiSlopes]=description&page[size]=100
```

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_182960
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_182960
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "skiSlopes",
    "id": "DSS_182960",
    "attributes": {
      "description": {
        "ita": "La Trainingspiste Hahnspiel è una pista da sci nell'area delle 3 Cime con una lunghezza di 618 metri e un grado di difficoltà \"intermedio\".",
        "deu": "Die Trainingspiste Hahnspiel ist eine Skipiste im 3-Zinnen-Gebiet mit einer Länge von 618 Metern und einer Schwierigkeitseinstufung von \"intermediate\".",
        "eng": "The Trainingspiste Hahnspiel is a ski slope in the 3 Cime area with 618 meters in length and a diffilcuty rating of \"intermediate\"."
      }
    }
  }
}
```

- Add a translation of the name and description of a ski slope in a new language.

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_182960
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "skiSlopes",
    "id": "DSS_182960",
    "attributes": {
      "name": {
        "deu": "Trainingspiste Hahnspiel",
        "eng": "Trainingspiste Hahnspiel",
        "ita": "Trainingspiste Hahnspiel",
        "nld": "Trainingspiste Hahnspiel"
      },
      "description": {
        "ita": "La Trainingspiste Hahnspiel è una pista da sci nell'area delle 3 Cime con una lunghezza di 618 metri e un grado di difficoltà \"intermedio\".",
        "deu": "Die Trainingspiste Hahnspiel ist eine Skipiste im 3-Zinnen-Gebiet mit einer Länge von 618 Metern und einer Schwierigkeitseinstufung von \"intermediate\".",
        "nld": "De Trainingspiste Hahnspiel is een skipiste in het 3 Cime gebied met een lengte van 618 meter en een moeilijkheidsgraad van gemiddeld.",
        "eng": "The Trainingspiste Hahnspiel is a ski slope in the 3 Cime area with 618 meters in length and a diffilcuty rating of \"intermediate\"."
      }
    }
  }
}
```

- Remove the description of a ski slope.

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_182960
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "skiSlopes",
    "id": "DSS_182960",
    "attributes": {
      "name": {
        "deu": "Trainingspiste Hahnspiel",
        "eng": "Trainingspiste Hahnspiel",
        "ita": "Trainingspiste Hahnspiel"
      },
      "description": null
    }
  }
}
```

- Some ski slopes in the server only have a difficulty rating according to the European classification system. Find such a slope and complement its data with the rating in the American system. The European ratings are "novice", "beginner", "intermediate", and "expert". The American ratings are "beginner", "beginner-intermediate", "intermediate", "intermediate-advanced", and "expert". 

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_182960?fields[skiSlopes]=difficulty
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_182960
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "skiSlopes",
    "id": "DSS_182960",
    "attributes": {
      "difficulty": {
        "eu": "intermediate",
        "us": "intermediate"
      }
    }
  }
}
```

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_1191383?fields[skiSlopes]=difficulty
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_1191383
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "skiSlopes",
    "id": "DSS_1191383",
    "attributes": {
      "difficulty": {
        "eu": "expert",
        "us": "expert"
      }
    }
  }
}
```

- Remove the relationship between a ski slope and a category.

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_1191383?fields[skiSlopes]=categories
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_1191383
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "skiSlopes",
    "id": "DSS_1191383",
    "relationships": {
      "categories": {
        "data": [
          {
            "id": "odh:piste",
            "type": "categories"
          },
          {
            "id": "odh:ski-alpin",
            "type": "categories"
          }
        ]
      }
    }
  }
}
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/skiSlopes/DSS_1191383
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "skiSlopes",
    "id": "DSS_1191383",
    "relationships": {
      "categories": {
        "data": [
          {
            "id": "odh:piste",
            "type": "categories"
          },
          {
            "id": "odh:ski-alpin",
            "type": "categories"
          },
          {
            "id": "odh:weitere-pisten",
            "type": "categories"
          }
        ]
      }
    }
  }
}
```

On the `/mountainAreas` endpoint:

- Edit the object identifying your favorite mountain area so it is related to the 3 media objects you created in the previous exercise.
  

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/8260DC5B815D40B98A1B53E84EC2B419?fields[mountainAreas]=multimediaDescriptions
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/8260DC5B815D40B98A1B53E84EC2B419
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "mountainAreas",
    "id": "8260DC5B815D40B98A1B53E84EC2B419",
    "relationships": {
      "multimediaDescriptions": {
        "data": [
          {
            "id": "area-photo",
            "type": "mediaObjects"
          },
          {
            "id": "area-map",
            "type": "mediaObjects"
          },
          {
            "id": "area-video",
            "type": "mediaObjects"
          }
        ]
      }
    }
  }
}
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/8260DC5B815D40B98A1B53E84EC2B419
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "mountainAreas",
    "id": "8260DC5B815D40B98A1B53E84EC2B419",
    "relationships": {
      "multimediaDescriptions": {
        "data": [
          {
            "id": "area-photo",
            "type": "mediaObjects"
          },
          {
            "id": "area-map",
            "type": "mediaObjects"
          },
          {
            "id": "area-video",
            "type": "mediaObjects"
          }
        ]
      }
    }
  }
}
```

<!-- ```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "stiergarten-yt-1",
    "type": "mediaObjects",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "author": "CLAUDIO LIPPI",
      "contentType": "video/vnd.youtube.yt",
      "duration": 355,
      "height": 696,
      "width": 1237,
      "license": "YouTube License",
      "name": {
        "eng": "Day at M.Elmo - Stiergarten"
      },
      "url": "https://www.alpinebits.org/wp-content/uploads/2015/10/alpine_bits_rgb.png"
    }
  }
}
``` -->