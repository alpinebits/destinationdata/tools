# Update - Demonstrations and Answers

On the `/agents` endpoint:

- Add an abstract for the AlpineBits Alliance object in two languages.

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "alpinebits",
    "type": "agents",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "abstract": {
        "eng": "The \"AlpineBits Alliance\" is a group of SME operating in the touristic sector working together to innovate and open the data exchange in the alpine tourism.",
        "ita": "\"AlpineBits Alliance\" è un gruppo di PMI operanti nel settore turistico che lavorano insieme per innovare e aprire lo scambio di dati nel turismo alpino."
      },
      "description": {
        "eng": "The \"AlpineBits Alliance\" is a group of SME operating in the touristic sector working together to innovate and open the data exchange in the alpine tourism, and therefore to optimize the online presence, sales and marketing efforts of the hotels and other accommodations in the alpine territory and also worldwide.",
        "ita": "\"AlpineBits Alliance\" è un gruppo di PMI operanti nel settore turistico che lavorano insieme per innovare e aprire lo scambio di dati nel turismo alpino, e quindi per ottimizzare la presenza online, le vendite e gli sforzi di marketing degli hotel e delle altre strutture ricettive nel territorio alpino e nel mondo."
      }
    }
  }
}
```

- Edit this abstract to add a third language. 

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "alpinebits",
    "type": "agents",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "abstract": {
        "eng": "The \"AlpineBits Alliance\" is a group of SME operating in the touristic sector working together to innovate and open the data exchange in the alpine tourism.",
        "ita": "\"AlpineBits Alliance\" è un gruppo di PMI operanti nel settore turistico che lavorano insieme per innovare e aprire lo scambio di dati nel turismo alpino.",
        "deu": "Die \"AlpineBits Alliance\" ist eine Gruppe von KMU, die im Tourismussektor tätig sind und gemeinsam an der Innovation und Öffnung des Datenaustauschs im alpinen Tourismus arbeiten."
      },
      "description": {
        "eng": "The \"AlpineBits Alliance\" is a group of SME operating in the touristic sector working together to innovate and open the data exchange in the alpine tourism, and therefore to optimize the online presence, sales and marketing efforts of the hotels and other accommodations in the alpine territory and also worldwide.",
        "ita": "\"AlpineBits Alliance\" è un gruppo di PMI operanti nel settore turistico che lavorano insieme per innovare e aprire lo scambio di dati nel turismo alpino, e quindi per ottimizzare la presenza online, le vendite e gli sforzi di marketing degli hotel e delle altre strutture ricettive nel territorio alpino e nel mondo.",
        "deu": "Die \"AlpineBits Alliance\" ist eine Gruppe von KMU, die im Tourismussektor tätig sind und zusammenarbeiten, um den Datenaustausch im alpinen Tourismus zu innovieren und zu öffnen und somit die Online-Präsenz, den Verkauf und die Marketingbemühungen von Hotels und anderen Unterkünften im Alpenraum und auch weltweit zu optimieren."
      }
    }
  }
}
```

- Remove all values for the abstract field in the AlpineBits Alliance object.

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "alpinebits",
    "type": "agents",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "attributes": {
      "abstract": null,
      "description": {
        "eng": "The \"AlpineBits Alliance\" is a group of SME operating in the touristic sector working together to innovate and open the data exchange in the alpine tourism, and therefore to optimize the online presence, sales and marketing efforts of the hotels and other accommodations in the alpine territory and also worldwide."
      }
    }
  }
}
```

- Associate the AlpineBits Alliance object with its old and new logos via the multimedia description relationship.

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits
Content-Type:application/vnd.api+json

{
  "data": {
    "id": "alpinebits",
    "type": "agents",
    "meta": {
      "dataProvider": "https://example.com"
    },
    "relationships": {
      "multimediaDescriptions": {
        "data": [
          {
            "id": "aa-logo-1",
            "type": "mediaObjects"
          },
          {
            "id": "aa-logo-2",
            "type": "mediaObjects"
          }
        ]
      }
    }
  }
}
```

- Remove the old logo from the multimedia descriptions of the AlpineBits Alliance object.


```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mediaObjects?fields[mediaObjects]=name
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/agents/alpinebits
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "agents",
    "id": "alpinebits",
    "meta": {
      "dataProvider": "https://example.com",
      "lastUpdate": "2022-10-26T04:00:01.061Z"
    },
    "links": {
      "self": "http://localhost:8080/2022-04/agents/alpinebits"
    },
    "attributes": {
      "abstract": null,
      "description": {
        "eng": "The \"AlpineBits Alliance\" is a group of SME operating in the touristic sector working together to innovate and open the data exchange in the alpine tourism, and therefore to optimize the online presence, sales and marketing efforts of the hotels and other accommodations in the alpine territory and also worldwide."
      },
      "name": {
        "eng": "AlpineBits Alliance"
      },
      "shortName": null,
      "url": "https://people.utwente.nl/c.moraisfonseca",
      "contactPoints": [
        {
          "availableHours": null,
          "email": "info@alpinebits.org",
          "telephone": "+390000000000",
          "address": {
            "country": "IT",
            "zipcode": null,
            "type": null,
            "city": {
              "deu": "Frangart"
            },
            "complement": null,
            "region": {
              "deu": "Frangart"
            },
            "street": {
              "deu": "Bozner Straße Nr. 63/A"
            }
          }
        }
      ]
    },
    "relationships": {
      "categories": {
        "data": [
          {
            "id": "alpinebits:organization",
            "type": "categories"
          }
        ],
        "link": "http://localhost:8080/2022-04/agents/alpinebits/categories"
      },
      "multimediaDescriptions": {
        "data": [
          {
            "id": "aa-logo-1",
            "type": "mediaObjects"
          }
        ]
      }
    }
  }
}
```

On the `/mountainAreas` endpoint:

- We found two mountain areas with the same name in Italian. Let's fix it? Their IDs are `SKIFFC3B47C3CEA4426AE850E333EFE79CE` and `SKI2C99CEA3DF2F458FB75E45CA39222E9D`.

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/SKIFFC3B47C3CEA4426AE850E333EFE79CE?fields[mountainAreas]=name,geometries
```

```http
GET https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/SKI2C99CEA3DF2F458FB75E45CA39222E9D?fields[mountainAreas]=name,geometries
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/SKIFFC3B47C3CEA4426AE850E333EFE79CE
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "mountainAreas",
    "id": "SKIFFC3B47C3CEA4426AE850E333EFE79CE",
    "attributes": {
      "name": {
        "ces": "Lyžařská oblast Seiser Alm - Val Gardena (East)",
        "deu": "Skigebiet Seiser Alm - Gröden (East)",
        "eng": "Alpe di Siusi - Val Gardena ski area (East)",
        "fra": "Station de ski Seiser Alm - Val Gardena (East)",
        "ita": "Località sciistica Alpe di Siusi - Val Gardena (East)",
        "nld": "Skigebied Alpe di Siusi - Val Gardena (East)",
        "pol": "Ośrodek narciarski Seiser Alm - Val Gardena (East)",
        "rus": "Горнолыжный курорт Зайзер Альм — Валь-Гардена (East)"
      }
    }
  }
}
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/SKI2C99CEA3DF2F458FB75E45CA39222E9D
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "mountainAreas",
    "id": "SKI2C99CEA3DF2F458FB75E45CA39222E9D",
    "attributes": {
      "name": {
        "ces": "Lyžařská oblast Val Gardena - Seiser Alm (West)",
        "deu": "Skigebiet Gröden - Seiser Alm (West)",
        "eng": "Val Gardena - Alpe di Siusi ski area - East (West)",
        "fra": "Station de ski Val Gardena - Seiser Alm (West)",
        "ita": "Località sciistica Alpe di Siusi - Val Gardena (West)",
        "nld": "Skigebied Val Gardena - Alpe di Siusi (West)",
        "pol": "Ośrodek narciarski Val Gardena - Seiser Alm (West)",
        "rus": "Горнолыжный курорт Валь-Гардена — Зайзер Альм (West)"
      }
    }
  }
}
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/SKIFFC3B47C3CEA4426AE850E333EFE79CE
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "mountainAreas",
    "id": "SKIFFC3B47C3CEA4426AE850E333EFE79CE",
    "attributes": {
      "name": {
        "ces": "Lyžařská oblast Seiser Alm - Val Gardena" ,
        "deu": "Skigebiet Seiser Alm - Gröden" ,
        "eng": "Alpe di Siusi - Val Gardena ski area" ,
        "fra": "Station de ski Seiser Alm - Val Gardena" ,
        "ita": "Località sciistica Alpe di Siusi - Val Gardena" ,
        "nld": "Skigebied Alpe di Siusi - Val Gardena" ,
        "pol": "Ośrodek narciarski Seiser Alm - Val Gardena" ,
        "rus": "Горнолыжный курорт Зайзер Альм — Валь-Гардена"
      }
    }
  }
}
```

```http
PATCH https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/mountainAreas/SKI2C99CEA3DF2F458FB75E45CA39222E9D
Content-Type:application/vnd.api+json

{
  "data": {
    "type": "mountainAreas",
    "id": "SKI2C99CEA3DF2F458FB75E45CA39222E9D",
    "attributes": {
      "name": {
        "ces": "Lyžařská oblast Val Gardena - Seiser Alm",
        "deu": "Skigebiet Gröden - Seiser Alm",
        "eng": "Val Gardena - Alpe di Siusi ski area - East",
        "fra": "Station de ski Val Gardena - Seiser Alm",
        "ita": "Località sciistica Alpe di Siusi - Val Gardena",
        "nld": "Skigebied Val Gardena - Alpe di Siusi",
        "pol": "Ośrodek narciarski Val Gardena - Seiser Alm",
        "rus": "Горнолыжный курорт Валь-Гардена — Зайзер Альм"
      }
    }
  }
}
```