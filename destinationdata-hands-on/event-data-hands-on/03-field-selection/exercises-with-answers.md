# Field Selection - Exercises and Answers

On the `/events` endpoint:

- Retrieve a collection of events with only their names, start date, and end date ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?fields[events]=name,startDate,endDate)).

```http
GET /2022-04/events
  ?fields[events]=name,startDate,endDate
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve 50 events selecting their names and their categories ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?fields[events]=name,categories&page[size]=50)).

```http
GET /2022-04/events
  ?fields[events]=name,categories
  &page[size]=50
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
