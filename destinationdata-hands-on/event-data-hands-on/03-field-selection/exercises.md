# Field Selection - Exercises

On the `/events` endpoint:

- Retrieve a collection of events with only their names, start date, and end date.
- Repeat the last request changing the page size to 50 events.
- Repeat the last request selecting only the names of the events. Notice what happens with the included categories.

