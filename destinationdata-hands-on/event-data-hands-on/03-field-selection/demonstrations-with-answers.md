# Field Selection - Demonstrations and Answers

On the `/events` endpoint:

- Retrieve only the name and the categories of the event `05092022-1800-TREVILAB-LAB1-IT-CONFERENZA-2000-050` ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events/05092022-1800-TREVILAB-LAB1-IT-CONFERENZA-2000-050?fields[events]=name,categories)).

```http
GET /2022-04/events/05092022-1800-TREVILAB-LAB1-IT-CONFERENZA-2000-050
  ?fields[events]=name,categories
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve several events selecting only their names and check the languages in which they are usually available ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?fields[events]=name&page[size]=50)).

```http
GET /2022-04/events
  ?fields[events]=name
  &page[size]=50
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
