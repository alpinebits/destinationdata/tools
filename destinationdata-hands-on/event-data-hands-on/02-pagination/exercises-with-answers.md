# Pagination - Exercises and Answers

On the `/events` endpoint:

- Retrieve a collection of events and then visit the second page without manually writing another request (tip: look for URLs within the JSON you got in the first request) ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?page[number]=2)).

```http
GET /2022-04/events?page[number]=2
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

Inside the `"links"` field, use the link labeled `"next"`.

- Retrieve 50 events in a single request ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?page[size]=50)).

```http
GET /2022-04/events?page[size]=49
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve the fourth page of events using a page size of 5 ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?page[number]=4&page[size]=5)).

```http
GET /2022-04/events
  ?page[number]=4
  &page[size]=5
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Navigate to the categories of event `13042022-0900-TREVILAB-LAB3-IT-EVENTO-1800-1304202` and retrieve the second page using a page size of 1 ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events/13042022-0900-TREVILAB-LAB3-IT-EVENTO-1800-1304202/categories?page[number]=2&page[size]=1)).

```http
GET /2022-04/events/13042022-0900-TREVILAB-LAB3-IT-EVENTO-1800-1304202/categories
  ?page[number]=2
  &page[size]=1
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
