# Pagination - Exercises

On the `/events` endpoint:

- Retrieve a collection of events and then visit the second page without manually writing another request (tip: look for URLs within the JSON you got in the first request).
- Retrieve 50 events in a single request.
- Retrieve the 3rd page of events.
- Retrieve the fourth page of events using a page size of 5.
- Navigate to the categories of events `13042022-0900-TREVILAB-LAB3-IT-EVENTO-1800-1304202` and retrieve the second page using a page size of 1.