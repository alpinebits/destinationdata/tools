# Pagination - Demonstrations and Answers

On the `/events` endpoint:

- Retrieve a collection of 100 events in a single request ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?page[size]=100)).

```http
GET /2022-04/events?page[size]=100
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve the second page of events ([link]((https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?page[number]=2))).

```http
GET /2022-04/events?page[number]=2
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```

- Retrieve the fourth page of events using a page size of 5 ([link](https://destinationdata.alpinebits.opendatahub.testingmachine.eu/2022-04/events?page[number]=4&page[size]=5)).

```http
GET /2022-04/events?page[number]=4&page[size]=5
Host:destinationdata.alpinebits.opendatahub.testingmachine.eu
```
